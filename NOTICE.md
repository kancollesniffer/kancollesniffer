# [KancolleSniffer](https://kancollesniffer.bitbucket.io/)

Copyright (C) 2013-2021 Kazuhiro Fujieda <fujieda@users.osdn.me><br>
Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");<br>
you may not use this file except in compliance with the License.<br>
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software<br>
distributed under the License is distributed on an "AS IS" BASIS,<br>
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>
See the License for the specific language governing permissions and<br>
limitations under the License.

## 音声ファイル

[Open JTalk](http://open-jtalk.sp.nitech.ac.jp/index.php)で生成したものを、[CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)に基づき利用しています。

The Nitech Japanese Speech Database "NIT ATR503 M001"<br>
Copyright (c) 2003-2012 Nagoya Insitute of Technology
