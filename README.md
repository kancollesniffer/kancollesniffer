KancolleSniffer は、艦これのサーバーとブラウザの間の通信をキャプチャして、小さなウィンドウにさまざまな情報を表示するツールです。詳細は[ウェブサイト](https://kancollesniffer.bitbucket.io/)を参照してください。

KancolleSniffer は Windows 10/11 で動作します。

このソフトウェアは Apache License Version 2.0 に基づいてライセンスされます。
