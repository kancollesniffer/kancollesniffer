// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class LoSTest : ApiStart2
    {
        [TestMethod]
        public void アメリカ艦にSGレーダー初期型()
        {
            var ship = NewShip(440);
            ship.LoS = 71 + 8 + 4 + 8 + 4;
            ship.SetItems(SGレーダー初期型(), SGレーダー初期型());
            PAssert.That(() => ship.RawLoS == 79, "SGレーダー初期型の索敵ボーナスは無視されるバグは解消した");
        }

        [TestMethod]
        public void アメリカ艦にSKレーダー()
        {
            var ship = NewShip(440);
            ship.LoS = 71 + 10 + 1 + 10 + 1;
            ship.SetItems(SKレーダー(), SKレーダー());
            PAssert.That(() => ship.RawLoS == 73, "SGレーダー初期型以外の索敵ボーナスは加算される");
        }

        [TestMethod]
        public void 丹陽雪風改二にSGレーダー初期型()
        {
            var ship = NewShip(656);
            ship.LoS = 48 + 8 + 3 + 8 + 0;
            ship.SetItems(SGレーダー初期型(), SGレーダー初期型());
            PAssert.That(() => ship.RawLoS == 51, "1個目のSGレーダー初期型の索敵ボーナスだけ加算される");
        }

        /// <summary>
        /// マップ索敵の判定式(33)を正しく計算する
        /// </summary>
        [TestMethod]
        public void LineOfSight()
        {
            var yuudachi  = NewShip(144).SetItems([_12_7cm連装砲(10), _12_7cm連装砲(10), _22号対水上電探改四(6)], Damecon());
            yuudachi.LoS  = 65 + yuudachi.Items.Sum(item => item.Spec.LoS);
            var myoukou   = NewShip(319).SetItems(_20_3cm3号連装砲(6), _20_3cm3号連装砲(6), 探照灯(), 九八式水上偵察機夜偵(4));
            myoukou.LoS   = 75 + myoukou.Items.Sum(item => item.Spec.LoS);
            var ooyodo    = NewShip(321).SetItems(_20_3cm3号連装砲(6), _20_3cm3号連装砲(6), 紫雲(6), FuMO25レーダー());
            ooyodo.LoS   = 106 + ooyodo.Items.Sum(item => item.Spec.LoS);
            var ayanami   = NewShip(195).SetItems(_12_7cm連装砲(10), _12_7cm連装砲(10), _22号対水上電探改四(6));
            ayanami.LoS   = 65 + ayanami.Items.Sum(item => item.Spec.LoS);
            var yukikaze  = NewShip(228).SetItems(_12_7cm連装砲B型改二(4), _12_7cm連装砲B型改二(4), _33号対水上電探());
            yukikaze.LoS  = 53 + yukikaze.Items.Sum(item => item.Spec.LoS);
            var shimakaze = NewShip(229).SetItems(_12_7cm連装砲B型改二(4), _12_7cm連装砲B型改二(4), _33号対水上電探());
            shimakaze.LoS = 52 + shimakaze.Items.Sum(item => item.Spec.LoS);
            var fleet = NewFleet().SetShips(yuudachi.Id, myoukou.Id, ooyodo.Id, ayanami.Id, yukikaze.Id, shimakaze.Id);

            PAssert.That(() => Math.Round(fleet.GetLineOfSights(1), 2) == 39.45);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(2), 2) == 77.32);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(3), 2) == 115.19);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(4), 2) == 153.07);

            fleet.SetShips(yuudachi.Id);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(1), 2) == -25.10, "艦隊に空きがある");

            var mutsuki = NewShip(1);
            mutsuki.LoS = 17 + mutsuki.Items.Sum(item => item.Spec.LoS);
            fleet.SetShips(yuudachi.Id, myoukou.Id, ooyodo.Id, ayanami.Id, yukikaze.Id, shimakaze.Id, mutsuki.Id);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(1), 2) == 41.57, "7隻編成では隻数補正が負数になる");
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(2), 2) == 79.44);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(3), 2) == 117.32);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(4), 2) == 155.19);
        }

        /// <summary>
        /// 補強増設スロットに見張り員を装備した場合の判定式(33)
        /// </summary>
        [TestMethod]
        public void LineOfSightWithExSlot()
        {
            var gotland = NewShip(579).SetItems([], 熟練見張員());
            gotland.LoS = 64 + gotland.Items.Sum(item => item.Spec.LoS);
            var fleet = NewFleet().SetShips(gotland.Id);
            PAssert.That(() => Math.Round(fleet.GetLineOfSights(1), 2) == -28.8);
        }

        [TestMethod]
        public void ShowSpeed()
        {
            var ship1 = NewShip(360);
            ship1.Speed = 20;
            var ship2 = NewShip(545);
            ship2.Speed = 15;
            var fleet1 = NewFleet().SetShips(ship1.Id, ship2.Id);
            PAssert.That(() => ShipStatus.SpeedName(fleet1.Speed) == "高速+");

            var ship3 = NewShip(387);
            ship3.Speed = 15;
            var ship4 = NewShip(370);
            ship4.Speed = 10;
            var fleet2 = NewFleet().SetShips(ship3.Id, ship4.Id);
            PAssert.That(() => ShipStatus.SpeedName(fleet2.Speed) == "高速");

            var ship5 = NewShip(80);
            ship5.Speed = 10;
            var ship6 = NewShip(81);
            ship6.Speed = 5;
            var fleet3 = NewFleet().SetShips(ship5.Id, ship6.Id);
            PAssert.That(() => ShipStatus.SpeedName(fleet3.Speed) == "低速");

            var fleet0 = NewFleet();
            PAssert.That(() => ShipStatus.SpeedName(fleet0.Speed) == "未定");
        }

        /// <summary>
        /// 連合艦隊補正の載った火力と電探搭載艦を計算する
        /// </summary>
        [TestMethod]
        public void CombinedFleetFirepower()
        {
            var haguro   = NewShip(194).SetItems(_20_3cm3号連装砲(), _20_3cm3号連装砲(), 紫雲(4), 艦隊司令部施設());
            var zuikaku  = NewShip(467).SetItems([彗星一二型甲(34), 流星改(24), 烈風六〇一空(12), 紫電改四(6)], A25mm三連装機銃集中配備());
            var shoukaku = NewShip(461).SetItems([彗星一二型甲(27), 流星改(27), 零式艦戦53型岩本隊(27), 零式艦戦52型熟練(12)], A25mm三連装機銃集中配備());
            var tone     = NewShip(188).SetItems([_20_3cm3号連装砲(), _20_3cm3号連装砲(), 二式水戦改熟練(9), 零式水上偵察機11型乙熟練(5)], A25mm三連装機銃集中配備());
            var chiyoda  = NewShip(297).SetItems([烈風一一型(24), Re2005改(16), 零式艦戦52型熟練(11), 試製烈風後期型(8)], A12cm30連装噴進砲改二());
            var chitose  = NewShip(296).SetItems([烈風一一型(24), 烈風一一型(16), 烈風六〇一空(11), 彩雲東カロリン空(8)], A12cm30連装噴進砲改二());
            var fleet1 = NewFleet(0, CombinedType.Carrier).SetShips(haguro.Id, zuikaku.Id, shoukaku.Id, tone.Id, chiyoda.Id, chitose.Id);

            var abukuma  = NewShip(200).SetItems([試製61cm六連装酸素魚雷(), 試製61cm六連装酸素魚雷(), 甲標的甲型()], Bofors40mm四連装機関砲());
            var akizuki  = NewShip(330).SetItems([_10cm連装高角砲高射装置(), _10cm連装高角砲高射装置(), _13号対空電探改()], QF2ポンド8連装ポンポン砲());
            var maya     = NewShip(428).SetItems([_20_3cm3号連装砲(), _90mm単装高角砲(), 九八式水上偵察機夜偵(3), FuMO25レーダー()], Bofors40mm四連装機関砲());
            var yukikaze = NewShip(228).SetItems([試製61cm六連装酸素魚雷(), _61cm五連装酸素魚雷(), _61cm五連装酸素魚雷()], Bofors40mm四連装機関砲());
            var kitakami = NewShip(119).SetItems([_10cm連装高角砲高射装置(), _10cm連装高角砲高射装置(), 甲標的甲型()], QF2ポンド8連装ポンポン砲());
            var zuihou   = NewShip(560).SetItems(TBM3D(18), F6F5N(15), F6F3N(15), 夜間作戦航空要員());
            var fleet2 = NewFleet(1, CombinedType.Carrier).SetShips(abukuma.Id, akizuki.Id, maya.Id, yukikaze.Id, kitakami.Id, zuihou.Id);

            PAssert.That(() => fleet1.RadarShips == 0);
            PAssert.That(() => fleet2.RadarShips == 2);
            PAssert.That(() => fleet1.SurfaceRadarShips == 0);
            PAssert.That(() => fleet2.SurfaceRadarShips == 1);
        }

        /// <summary>
        /// 航空偵察スコアを計算する
        /// </summary>
        [TestMethod]
        public void AirReconScore()
        {
            var akitsushima = NewShip(450).SetItems(二式大艇(1, alv: 2));
            var cteste = NewShip(372).SetItems(紫雲(12), Megami());
            var fleet = NewFleet().SetShips(akitsushima.Id, cteste.Id);
            PAssert.That(() => Math.Round(fleet.AirReconScore, 2) == 26.89);
        }
    }
}
