// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static KancolleSniffer.Model.ShipTypeCode;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class EffectiveAswTest : ApiStart2
    {
        [TestMethod]
        public void EffectiveAswToString()
        {
            var asw = new EffectiveAsw { AswPower = 0, Enabled = true, Opening = true, Unresolved = true };
            PAssert.That(() => asw.ToString() == "");

            asw = new() { AswPower = 23.45, Enabled = true, Opening = false, Unresolved = false };
            PAssert.That(() => asw.ToString() == "潜23.4");

            asw = new() { AswPower = 23.45, Enabled = true, Opening = false, Unresolved = true };
            PAssert.That(() => asw.ToString() == "潜?23.4");

            asw = new() { AswPower = 23.45, Enabled = true, Opening = true, Unresolved = false };
            PAssert.That(() => asw.ToString() == "潜23.4*");

            asw = new() { AswPower = 23.45, Enabled = false, Opening = true, Unresolved = false };
            PAssert.That(() => asw.ToString() == "潜23.4~");

            asw = new() { AswPower = 23.45, Enabled = true, Opening = true, Unresolved = true };
            PAssert.That(() => asw.ToString() == "潜?23.4*");

            asw = new() { AswPower = 23.45, Enabled = false, Opening = true, Unresolved = true };
            PAssert.That(() => asw.ToString() == "潜?23.4~");
        }

        /// 先制対潜判定

        /// <summary>
        /// 通常の先制対潜
        /// </summary>
        [TestMethod]
        public void CheckStandardCase()
        {
            var ship = NewShip(51);
            ship.Level = 99;
            ship.MaxAsw = 2;
            ship.SetItems(三式水中探信儀());

            ship.ShownAsw = 99;
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening, "対潜不足");
            ship.ShownAsw = 100;
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);
            ship.SetItems();
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening, "ソナー未搭載");
        }

        /// <summary>
        /// 海防艦の先制対潜
        /// </summary>
        [TestMethod]
        public void CheckCoastGuard()
        {
            var ship = NewShip(517);
            ship.SetItems(九五式爆雷());
            ship.ShownAsw = 74;
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening, "対潜不足");

            ship.ShownAsw = 75;
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(SGレーダー初期型());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening, "装備対潜不足");

            ship.SetItems(SGレーダー初期型(), SGレーダー初期型());
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);
        }

        /// <summary>
        /// 無条件で先制対潜が可能
        /// </summary>
        [DataTestMethod]
        [DataRow(141, "五十鈴改二")]
        [DataRow(478, "龍田改二")]
        [DataRow(394, "Jervis改")]
        [DataRow(893, "Janus改")]
        [DataRow(906, "Javelin改")]
        [DataRow(681, "Samuel B.Roberts改")]
        [DataRow(562, "Johnston")]
        [DataRow(689, "Johnston改")]
        [DataRow(596, "Fletcher")]
        [DataRow(692, "Fletcher改")]
        [DataRow(624, "夕張改二丁")]
        [DataRow(628, "Fletcher改 Mod.2")]
        [DataRow(629, "Fletcher Mk.II")]
        [DataRow(726, "Heywood L.E.改")]
        public void CheckNonConditional(int id, string name)
        {
            var ship = NewShip(id);
            PAssert.That(() => ship.Spec.Name == name);
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);
        }

        /// <summary>
        /// 未改は無条件先制対潜が不可
        /// </summary>
        [DataTestMethod]
        [DataRow(519, "Jervis")]
        [DataRow(520, "Janus")]
        [DataRow(901, "Javelin")]
        [DataRow(561, "Samuel B.Roberts")]
        [DataRow(941, "Heywood L.E.")]
        public void CheckFalseNonConditional(int id, string name)
        {
            var ship = NewShip(id);
            PAssert.That(() => ship.Spec.Name == name);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);
        }

        /// <summary>
        /// 大鷹改・改二、雲鷹改・改二、神鷹改・改二、加賀改二護
        /// </summary>
        [DataTestMethod]
        [DataRow(380, "大鷹改")]
        [DataRow(529, "大鷹改二")]
        [DataRow(382, "雲鷹改")]
        [DataRow(889, "雲鷹改二")]
        [DataRow(381, "神鷹改")]
        [DataRow(536, "神鷹改二")]
        [DataRow(646, "加賀改二護")]
        public void CheckAntiSubmarineAircraftCarrier(int id, string name)
        {
            var ship = NewShip(id);
            PAssert.That(() => ship.Spec.Name == name);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(流星改(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(流星改(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(九九式艦爆(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(九九式艦爆(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(彗星一二型三一号光電管爆弾搭載機());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);
        }

        /// <summary>
        /// 春日丸、大鷹、八幡丸、雲鷹、神鷹
        /// </summary>
        [DataTestMethod]
        [DataRow(521, "春日丸")]
        [DataRow(526, "大鷹")]
        [DataRow(522, "八幡丸")]
        [DataRow(884, "雲鷹")]
        [DataRow(534, "神鷹")]
        public void CheckFalseAntiSubmarineAircraftCarrier(int id, string name)
        {
            var ship = NewShip(id);
            PAssert.That(() => ship.Spec.Name == name);

            ship.SetItems(流星改(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(九九式艦爆(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);
        }

        [DataTestMethod]
        public void CheckLightAircraftCarrierLevel50()
        {
            var ship = NewShip(89);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 49;
            ship.SetItems(水中聴音機零式(), 九七式艦攻九三一空(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 50;
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 九七式艦攻九三一空(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 九七式艦攻九三一空(0), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 九七式艦攻九三一空(0), TBF(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), TBF(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 三式指揮連絡機対潜(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 三式指揮連絡機対潜(0), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 三式指揮連絡機対潜(0), TBF(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 三式指揮連絡機対潜(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), カ号観測機(0), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), カ号観測機(0), TBF(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);
        }

        [DataTestMethod]
        public void CheckLightAircraftCarrierLevel65()
        {
            var ship = NewShip(89);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 64;
            ship.SetItems(九七式艦攻九三一空(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 65;
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(九七式艦攻九三一空(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(九七式艦攻九三一空(0), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(九七式艦攻九三一空(0), TBF(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(TBF(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(0), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(0), TBF(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(0), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(0), TBF(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式指揮連絡機対潜(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);
        }

        [DataTestMethod]
        public void CheckLightAircraftCarrierLevel100()
        {
            var ship = NewShip(89);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 100;
            ship.SetItems(水中聴音機零式(), カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 流星改(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 流星改(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 九九式艦爆(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 九九式艦爆(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 99;
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 100;
            ship.SetItems(水中聴音機零式(), Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(水中聴音機零式(), 彗星一二型三一号光電管爆弾搭載機());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);
        }

        [DataTestMethod]
        public void CheckAtacktypeLightAircraftCarrier()
        {
            var ship = NewShip(508); // 鈴谷航改二
            ship.SetItems(S51J改(1), S51J改(1), S51J改(1), 水中聴音機零式());
            ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => ship.ShownAsw == 50);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship = NewShip(509); // 熊野航改二
            ship.SetItems(S51J改(1), S51J改(1), S51J改(1), 水中聴音機零式());
            ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => ship.ShownAsw == 50);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);
        }

        [TestMethod]
        public void 揚陸艦()
        {
            var ship = NewShip(626);
            ship.ShownAsw = 48;
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), カ号観測機(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), 瑞雲(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 100;

            ship.SetItems(カ号観測機(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(瑞雲(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), 瑞雲(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), 瑞雲(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);
        }

        [TestMethod]
        public void 日向改二()
        {
            var ship = NewShip(554);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(1), カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(カ号観測機(1), カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(S51J(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(S51J(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(S51J改(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Opening);

            ship.ShownAsw = 100;
            ship.SetItems(三式水中探信儀(), 九九式艦爆(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), 瑞雲(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), カ号観測機(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);

            ship.SetItems(三式水中探信儀(), 三式指揮連絡機対潜(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Opening);
        }

        /// 対潜可否

        [TestMethod]
        public void 軽空母()
        {
            var ship = NewShip(560);
            ship.Level = 99;
            ship.MaxAsw = 47;
            ship.ShownAsw = 47 + 11;
            ship.SetItems(水中聴音機零式());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "艦載機なし");

            ship.ShownAsw = 47 + 18;
            ship.SetItems(九七式艦攻九三一空(0), 水中聴音機零式());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "艦載機0機");

            ship.SetItems(九七式艦攻九三一空(1), 水中聴音機零式());
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "艦載機あり");
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 48.71, "艦載機あり");
        }

        [TestMethod]
        public void 水上機母艦()
        {
            var ship = NewShip(102);
            ship.ShownAsw = 10;
            ship.SetItems(三式水中探信儀());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "艦載機なし");

            ship.ShownAsw = 19;
            ship.SetItems(三式水中探信儀(), カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "艦載機0機");

            ship.SetItems(三式水中探信儀(), カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "艦載機あり");
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 36.5);
        }

        [TestMethod]
        public void 速吸改()
        {
            var ship = NewShip(352);
            ship.Level = 99;
            ship.MaxAsw = 36;
            ship.ShownAsw = 36;
            ship.SetItems(Re2001G改());
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "Re2001G改のみで対潜不可");

            ship.SetItems(Re2001G改(), 流星改(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "Re2001G改 + 流星改0機");

            ship.SetItems(Re2001G改(), 流星改(3));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "Re2001G改 + 流星改");

            ship.SetItems(Re2001G改(), 瑞雲(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "Re2001G改 + 瑞雲0機");

            ship.SetItems(Re2001G改(), 瑞雲(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "Re2001G改 + 瑞雲");

            ship.SetItems(Re2001G改(), カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "Re2001G改 + カ号観測機0機");

            ship.SetItems(Re2001G改(), カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "Re2001G改 + カ号観測機");

            ship.SetItems();
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "無装備");

            ship.SetItems(流星改(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "流星改0機");

            ship.SetItems(流星改(6));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "流星改");

            ship.SetItems(瑞雲(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "瑞雲0機");

            ship.SetItems(瑞雲(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "瑞雲");

            ship.SetItems(カ号観測機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "カ号観測機0機");

            ship.SetItems(カ号観測機(1));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "カ号観測機");

            ship.SetItems(流星改(0), 瑞雲(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "流星改0機＋瑞雲");

            ship.SetItems(流星改(0), カ号観測機(1));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "流星改0機＋カ号観測機");
        }

        [TestMethod]
        public void 山汐丸()
        {
            var ship = NewShip(900);
            ship.Level = 78;
            ship.MaxAsw = 63;
            ship.ShownAsw = 54 + 7 + 1; // 装備ボーナス +1

            ship.SetItems(三式指揮連絡機対潜(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "三式指揮連絡機対潜0機");

            ship.SetItems(三式指揮連絡機対潜(8));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "三式指揮連絡機対潜");
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 34.7);

            ship.SetItems(三式爆雷投射機());
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "三式爆雷投射機");
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 39.7);
        }

        [TestMethod]
        public void 山汐丸改()
        {
            var ship = NewShip(717);
            ship.Level = 78;
            ship.MaxAsw = 82;
            ship.ShownAsw = 71 + 8;

            ship.SetItems(零式艦戦64型複座KMX搭載機(0));
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled, "零式艦戦64型複座KMX搭載機0機");

            ship.SetItems(零式艦戦64型複座KMX搭載機(8));
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "零式艦戦64型複座KMX搭載機");
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 36.85);

            ship.SetItems(三式爆雷投射機());
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled, "三式爆雷投射機");
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 41.85);
        }

        [TestMethod]
        public void 第百一号輸送艦()
        {
            var ship = NewShip(945);
            ship.Level = 1;
            ship.MaxAsw = 0;
            PAssert.That(() => !ship.UnresolvedAsw);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled);

            ship.SetItems(三式爆雷投射機());
            ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => !ship.UnresolvedAsw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => EffectiveAsw.Calc(ship).AswPower == 25);

            ship.SetItems(九五式爆雷());
            ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => !ship.UnresolvedAsw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => EffectiveAsw.Calc(ship).AswPower == 19);

            ship.SetItems(二式12cm迫撃砲改());
            ship.ShownAsw = ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => !ship.UnresolvedAsw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => EffectiveAsw.Calc(ship).AswPower == 17.5);
        }

        [TestMethod]
        public void 扶桑改二山城改二()
        {
            var ship = NewShip(411);
            ship.Level = 99;
            ship.MaxAsw = 28;

            ship.SetItems(瑞雲(0));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled);

            ship.SetItems(瑞雲(1));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 24.58);

            ship.SetItems(カ号観測機(0));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => !EffectiveAsw.Calc(ship).Enabled);

            ship.SetItems(カ号観測機(1));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 32.08);

            ship.SetItems(三式爆雷投射機());
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 35.58);

            ship.SetItems(九五式爆雷());
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 29.58);

            ship.SetItems(二式12cm迫撃砲改());
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 28.08);

            ship.SetItems(二式12cm迫撃砲改(), 瑞雲(0));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 29.08);

            ship.SetItems(二式12cm迫撃砲改(), 瑞雲(1));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 29.08);

            ship.SetItems(二式12cm迫撃砲改(), カ号観測機(0));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 36.58);

            ship.SetItems(二式12cm迫撃砲改(), カ号観測機(1));
            ship.ShownAsw = ship.MaxAsw + ship.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 36.58);


            var ship2 = NewShip(412);
            ship2.Level = 99;
            ship2.MaxAsw = 29;

            ship2.SetItems(二式12cm迫撃砲改());
            ship2.ShownAsw = ship2.MaxAsw + ship2.Items.Sum(item => item.Spec.Asw);
            PAssert.That(() => EffectiveAsw.Calc(ship2).Enabled);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship2).AswPower, 2) == 28.27);
        }

        // その他

        [TestMethod]
        public void Log_antisubmarine_001()
        {
            var ship = NewShip(386);
            ship.Level = 74;
            ship.MaxAsw = 81;
            ship.ShownAsw = 92;
            ship.SetItems(NewItem(260), NewItem(44), NewItem(227));
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 92.16);

            var ship2 = NewShip(678);
            ship2.Level = 74;
            ship2.MaxAsw = 83;
            ship2.ShownAsw = 101;
            ship2.SetItems(NewItem(47), NewItem(288), NewItem(226));
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship2).AswPower, 2) == 105.61);

            var ship3 = NewShip(679);
            ship3.Level = 74;
            ship3.MaxAsw = 85;
            ship3.ShownAsw = 88;
            ship3.SetItems(NewItem(45), NewItem(227));
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship3).AswPower, 2) == 57.85);

            var ship4 = NewShip(385);
            ship4.Level = 73;
            ship4.MaxAsw = 79;
            ship4.ShownAsw = 83;
            ship4.SetItems(NewItem(149), NewItem(226));
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship4).AswPower, 2) == 61.38);
        }

        [TestMethod]
        public void 対潜装備一つ()
        {
            var ship = NewShip(195);
            ship.Level = 99;
            ship.MaxAsw = 63;
            ship.ShownAsw = 63 + 10;
            ship.SetItems(三式水中探信儀());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 43.87);

            ship.ShownAsw = 63 + 8;
            ship.SetItems(三式爆雷投射機());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 40.87);

            ship.ShownAsw = 63 + 4;
            ship.SetItems(九五式爆雷());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 34.87);
        }

        [TestMethod]
        public void 爆雷投射機と爆雷()
        {
            var ship = NewShip(195);
            ship.Level = 99;
            ship.MaxAsw = 63;
            ship.ShownAsw = 63 + 12;
            ship.SetItems(三式爆雷投射機(), 九五式爆雷());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 51.56);
        }

        [TestMethod]
        public void ソナーとそれ以外()
        {
            var ship = NewShip(195);
            ship.Level = 99;
            ship.MaxAsw = 63;
            ship.ShownAsw = 63 + 18;
            ship.SetItems(三式水中探信儀(), 三式爆雷投射機());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 64.26);

            ship.ShownAsw = 63 + 14;
            ship.SetItems(三式水中探信儀(), 九五式爆雷());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 57.36);

            ship.ShownAsw = 63 + 13;
            ship.SetItems(三式水中探信儀(), 二式12cm迫撃砲改());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 55.63);
        }

        [TestMethod]
        public void 三種コンビネーション()
        {
            var ship = NewShip(195);
            ship.Level = 99;
            ship.MaxAsw = 63;
            ship.ShownAsw = 63 + 22;
            ship.SetItems(三式水中探信儀(), 三式爆雷投射機(), 九五式爆雷());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 88.94);

            ship.ShownAsw = 63 + 17;
            ship.SetItems(三式水中探信儀(), 二式12cm迫撃砲改(), 九五式爆雷());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 62.53, "三種コンビネーションにならない");

            ship.ShownAsw = 63 + 23;
            ship.SetItems(水中聴音機零式(), 三式爆雷投射機(), 九五式爆雷());
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 80.17, "大型ソナーは三種倍率が落ちる");
        }

        [TestMethod]
        public void Lvから素対潜を計算()
        {
            var ship = NewShip(714);
            ship.Level = 37;
            ship.MaxAsw = 81;
            PAssert.That(() => !ship.UnresolvedAsw);
            PAssert.That(() => ship.RawAsw == 54);

            ship.Level = 41;
            PAssert.That(() => ship.RawAsw == 55);
            ship.Level++;
            PAssert.That(() => ship.RawAsw == 56);
        }

        [TestMethod]
        public void 装備ボーナス()
        {
            var ship = NewShip(662);
            ship.Level = 99;
            ship.MaxAsw = 84;
            ship.ShownAsw = 84 + 7 + 3;
            ship.SetItems(零式水上偵察機11型乙(1));
            PAssert.That(() => ship.RawAsw == 84);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 35.83);
        }

        [TestMethod]
        public void 対潜改修()
        {
            var ship = NewShip(555);
            ship.Level = 99;
            ship.MaxAsw = 0;
            ship.ShownAsw = 10;
            ship.SetItems(SwordfishMkIII熟練(21));
            PAssert.That(() => ship.RawAsw == 0);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 23.0);

            ship.ImprovedAsw = 9;
            ship.ShownAsw += ship.ImprovedAsw;
            PAssert.That(() => ship.RawAsw == 9);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 29.0);
        }

        [TestMethod]
        public void 対潜最小値不明な新艦()
        {
            var ship = new ShipStatus
            {
                Level = 84,
                MaxAsw = 86,
                Spec = new ShipSpec
                {
                    Id = 58800,
                    ShipType = DD,
                    Name = "山風改二",
                },
                ShownAsw = 77 + 10 + 3,
            };
            ship.Spec.GetMinAsw = () => AdditionalData.MinAsw(ship.Spec.Id);
            ship.GetItem = itemId => ItemInventory[itemId];
            ship.SetItems(三式水中探信儀());
            PAssert.That(() => ship.UnresolvedAsw);
            PAssert.That(() => ship.RawAsw == 80);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 45.89);

            ship.Spec.Id = 588;
            PAssert.That(() => !ship.UnresolvedAsw);
            PAssert.That(() => ship.RawAsw == 77);
            PAssert.That(() => Math.Round(EffectiveAsw.Calc(ship).AswPower, 2) == 50.05);
        }

        [TestMethod]
        public void 対潜支援()
        {
            var ship = NewShip(717);

            ship.SetItems(カ号観測機(8), 三式指揮連絡機対潜(8), 一式戦隼II型改20戦隊(8));
            PAssert.That(() => Math.Round(ship.Items[0].CalcAswSupportPower()[0], 2) == 35.7);
            PAssert.That(() => Math.Round(ship.Items[1].CalcAswSupportPower()[0], 2) == 29.4);
            PAssert.That(() => Math.Round(ship.Items[2].CalcAswSupportPower()[0], 2) == 29.4);

            ship.SetItems(カ号観測機(8, level: 10), 三式指揮連絡機対潜(8, level: 10), 一式戦隼II型改20戦隊(8, level: 10));
            PAssert.That(() => Math.Round(ship.Items[0].CalcAswSupportPower()[0], 2) == 35.7, "改修値は影響しない");
            PAssert.That(() => Math.Round(ship.Items[1].CalcAswSupportPower()[0], 2) == 29.4, "改修値は影響しない");
            PAssert.That(() => Math.Round(ship.Items[2].CalcAswSupportPower()[0], 2) == 29.4, "改修値は影響しない");
        }
    }
}
