// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class TransportPointTest : ApiStart2
    {
        // 遠征大発ボーナス

        [TestMethod]
        public void DaihatsuBonusKinuKai2()
        {
            var ship1 = NewShip(487); // 鬼怒改二
            ship1.SetItems(Daihatsu(), Daihatsu());
            var ship2 = NewShip(348); // 瑞穂改
            ship2.SetItems(Tokudaihatsu(), Tokudaihatsu(), Tokudaihatsu());
            var fleet = NewFleet();
            fleet.SetShips(ship1.Id, ship2.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 25.2);
        }

        [TestMethod]
        public void DaihatsuBonusMax()
        {
            var ship1 = NewShip(586); // 日進甲
            ship1.SetItems(DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax());
            var ship2 = NewShip(372); // Commandant Teste改
            ship2.SetItems(DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax());
            var fleet = NewFleet();
            fleet.SetShips(ship1.Id, ship2.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 28);
        }

        [TestMethod]
        public void DaihatsuBonusOverMax()
        {
            var ship1 = NewShip(487); // 鬼怒改二
            ship1.SetItems(DaihatsuMax(), TokudaihatsuMax());
            var ship2 = NewShip(586); // 日進甲
            ship2.SetItems(DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax());
            var ship3 = NewShip(372); // Commandant Teste改
            ship3.SetItems(DaihatsuMax(), DaihatsuMax(), TokudaihatsuMax(), TokudaihatsuMax());
            var fleet = NewFleet();
            fleet.SetShips(ship1.Id, ship2.Id, ship3.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 28);
        }

        [TestMethod]
        public void Daihatsu89Type2TankBonus()
        {
            var ship = NewShip(348); // 瑞穂改
            ship.SetItems(Daihatsu89Max(), Type2TankMax(), NorthAfrica());
            var fleet = NewFleet();
            fleet.SetShips(ship.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 5.3);
        }

        [TestMethod]
        public void OtherDaihatsuBonus1()
        {
            var ship = NewShip(348); // 瑞穂改
            ship.SetItems(TokudaihatsuMax(), NorthAfrica());
            var fleet = NewFleet();
            fleet.SetShips(ship.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 9.3);
        }

        [TestMethod]
        public void OtherDaihatsuBonus2()
        {
            var ship = NewShip(348); // 瑞穂改
            ship.SetItems(TokudaihatsuMax(), Daihatu11th(), M4A1DD());
            var fleet = NewFleet();
            fleet.SetShips(ship.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 7.5);
        }

        [TestMethod]
        public void OtherDaihatsuBonus3()
        {
            var ship = NewShip(348); // 瑞穂改
            ship.SetItems(TokudaihatsuMax(), ArmoredBoatMax(), ArmedDaihatsu());
            var fleet = NewFleet();
            fleet.SetShips(ship.Id);
            PAssert.That(() => fleet.DaihatsuBonus == 12.6);
        }

        [TestMethod]
        public void TransportPoint1()
        {
            var shipInfo = NewShipInfo();
            var ship = NewShip(348); // 瑞穂改
            ship.SetItems(Daihatsu(), Daihatsu89Max(), Type2TankMax());
            shipInfo.Fleets[0].SetShips(ship.Id);
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 27);
        }

        // 輸送ゲージTPボーナス

        [TestMethod]
        public void TransportPoint2()
        {
            var shipInfo = NewShipInfo();
            var ship = NewShip(487);
            ship.SetItems(Tokudaihatsu(), CombatRation());
            shipInfo.Fleets[0].SetShips(ship.Id);
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 19, "鬼怒改二+特大発+おにぎり");
        }

        [TestMethod]
        public void TransportPoint3()
        {
            var shipInfo = NewShipInfo();
            var ship = NewShip(147);
            ship.SetItems(Daihatu11th());
            shipInfo.Fleets[0].SetShips(ship.Id);
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 13, "駆逐艦+士魂部隊");
        }

        [TestMethod]
        public void TransportPoint4()
        {
            var shipInfo = NewShipInfo();
            var ship = NewShip(352);
            shipInfo.Fleets[0].SetShips(ship.Id);
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 15, "補給艦");
        }

        [TestMethod]
        public void CombinedKinuKai2()
        {
            var shipInfo = NewShipInfo();
            var mizuho   = NewShip(348);
            var kinu1    = NewShip(487);
            var kinu2    = NewShip(487);
            var hibiki   = NewShip(147);
            var hayasui  = NewShip(352);
            var houshou  = NewShip(899);

            shipInfo.Fleets[0].SetShips(mizuho.Id, kinu1.Id);
            shipInfo.Fleets[1].SetShips(kinu2.Id, hibiki.Id);
            shipInfo.Fleets[2].SetShips(hayasui.Id);
            shipInfo.Fleets[3].SetShips(houshou.Id);
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 19, "通常艦隊1");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 15, "通常艦隊2");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "通常艦隊3");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            shipInfo.Fleets[0].CombinedType = shipInfo.Fleets[1].CombinedType = CombinedType.Transport;
            shipInfo.Fleets[0].State = shipInfo.Fleets[1].State = FleetState.Sortie;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 26, "連合での鬼怒改二ボーナスは1隻分のみ");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "通常艦隊3");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            kinu1.NowHp = 1;
            kinu2.NowHp = 2;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 24, "片方の鬼怒改二が大破してもボーナスは有効と仮定");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "通常艦隊3");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            kinu1.NowHp = 2;
            kinu2.NowHp = 1;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 24, "片方の鬼怒改二が大破してもボーナスは有効と仮定");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "通常艦隊3");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            kinu1.NowHp = 1;
            kinu2.NowHp = 1;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 14, "両方が大破でボーナスは無効");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "通常艦隊3");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            shipInfo.Fleets[0].State = shipInfo.Fleets[1].State = FleetState.Port;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 26, "母港では大破を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "通常艦隊3");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            shipInfo.Fleets[2].State = FleetState.Sortie;
            hayasui.Escaped = true;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 26, "母港では大破を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 0, "退避艦は除外");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            hayasui.NowHp = 0;
            hayasui.Escaped = false;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 26, "母港では大破を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 0, "撃沈艦は除外");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "通常艦隊4");

            shipInfo.Fleets[2].State = FleetState.Port;
            houshou.SetItems(Daihatsu());
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 26, "母港では大破を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "母港では撃沈を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 8, "輸送装備は艦種問わず有効");

            shipInfo.Fleets[3].State = FleetState.Sortie;
            houshou.NowHp = 1;
            PAssert.That(() => shipInfo.CalcTransportPoint(0) == 26, "母港では大破を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(1) == 0, "連合第二のTPは第一に合算");
            PAssert.That(() => shipInfo.CalcTransportPoint(2) == 15, "母港では撃沈を無視する");
            PAssert.That(() => shipInfo.CalcTransportPoint(3) == 0, "大破艦の輸送装備は無効");
        }
    }
}
