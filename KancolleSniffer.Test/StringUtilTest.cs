// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Text;
using System.Text.RegularExpressions;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class StringUtilTest
    {
        [TestMethod]
        public void TestCat()
        {
            PAssert.That(() => Cat("a", " ", "b") == "a b");
            PAssert.That(() => Cat("a", "\n", "b") == "a\nb");

            PAssert.That(() => Cat("a", "", "b") == "ab");
            PAssert.That(() => Cat("a", null, "b") == "ab");

            PAssert.That(() => Cat("a", " ", "") == "a");
            PAssert.That(() => Cat("a", " ", null) == "a");
            PAssert.That(() => Cat("a", "\n", "") == "a");
            PAssert.That(() => Cat("a", "\n", null) == "a");

            PAssert.That(() => Cat("", " ", "b") == "b");
            PAssert.That(() => Cat(null, " ", "b") == "b");
            PAssert.That(() => Cat("", "\n", "b") == "b");
            PAssert.That(() => Cat(null, "\n", "b") == "b");

            PAssert.That(() => Cat("", " ", "") == "");
            PAssert.That(() => Cat(null, " ", null) == "");
            PAssert.That(() => Cat(null, "\n", null) == "");
        }

        [TestMethod]
        public void TestStringBuilderCat()
        {
            PAssert.That(() => new StringBuilder("a").Cat(" ", "b").ToString() == "a b");
            PAssert.That(() => new StringBuilder("a").Cat("\n", "b").ToString() == "a\nb");

            PAssert.That(() => new StringBuilder("a").Cat("", "b").ToString() == "ab");
            PAssert.That(() => new StringBuilder("a").Cat(null, "b").ToString() == "ab");

            PAssert.That(() => new StringBuilder("a").Cat(" ", "").ToString() == "a");
            PAssert.That(() => new StringBuilder("a").Cat(" ", null).ToString() == "a");
            PAssert.That(() => new StringBuilder("a").Cat("\n", "").ToString() == "a");
            PAssert.That(() => new StringBuilder("a").Cat("\n", null).ToString() == "a");

            PAssert.That(() => new StringBuilder("").Cat(" ", "b").ToString() == "b");
            PAssert.That(() => new StringBuilder(null).Cat(" ", "b").ToString() == "b");
            PAssert.That(() => new StringBuilder("").Cat("\n", "b").ToString() == "b");
            PAssert.That(() => new StringBuilder(null).Cat("\n", "b").ToString() == "b");

            PAssert.That(() => new StringBuilder("").Cat(" ", "").ToString() == "");
            PAssert.That(() => new StringBuilder(null).Cat(" ", null).ToString() == "");
            PAssert.That(() => new StringBuilder(null).Cat("\n", null).ToString() == "");
        }

        [TestMethod]
        public void TestToS()
        {
            PAssert.That(() => ToS(0.95, 1) == "0.9");
            PAssert.That(() => ToS(0.94, 1) == "0.9");
            PAssert.That(() => ToS(0.05, 1) == "0.0");
            PAssert.That(() => ToS(0.04, 1) == "0.0");

            PAssert.That(() => ToS(-0.95, 1) == "-1.0");
            PAssert.That(() => ToS(-0.94, 1) == "-1.0");
            PAssert.That(() => ToS(-0.05, 1) == "-0.1");
            PAssert.That(() => ToS(-0.04, 1) == "-0.1");

            PAssert.That(() => ToS(0.123, 2) == "0.12");
            PAssert.That(() => ToS(0.1234, 3) == "0.123");
        }

        private double DivN(int n) => 1 / n;

        [TestMethod]
        public void TestGenerateErrorDetails()
        {
            PAssert.That(() => GenerateErrorDetails("テスト", new Exception()) ==
                "テスト\r\n" +
                "\r\n" +
                "System.Exception: 種類 'System.Exception' の例外がスローされました。");

            try
            {
                DivN(0);
            }
            catch (Exception e)
            {
                e.Data["aa"] = "bb";
                e.Data["cc"] = "dd";
                PAssert.That(() => new Regex("場所 [A-Za-z]:.+?KancolleSniffer.Test").Replace(
                    GenerateErrorDetails("追加データ付き\r\n改行アリ", e), "場所 \\KancolleSniffer.Test") ==
                    "追加データ付き\r\n" +
                    "改行アリ\r\n" +
                    "\r\n" +
                    "aa\t : bb\r\n" +
                    "cc\t : dd\r\n" +
                    "\r\n" +
                    "System.DivideByZeroException: 0 で除算しようとしました。\r\n" +
                    "   場所 KancolleSniffer.Test.StringUtilTest.DivN(Int32 n) 場所 \\KancolleSniffer.Test\\StringUtilTest.cs:行 92\r\n" +
                    "   場所 KancolleSniffer.Test.StringUtilTest.TestGenerateErrorDetails() 場所 \\KancolleSniffer.Test\\StringUtilTest.cs:行 104");
            }
        }
    }
}
