// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ItemApiTest
    {
        /// <summary>
        /// 装備交換のAPIの仕様変更に対応する
        /// </summary>
        [TestMethod]
        public void SlotExchangeVersion2()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("slot_exchange_002");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Slot.ParamsEqual(157798, 59001, 157804, -1, -1));
        }

        /// <summary>
        /// 入渠中の艦から装備を奪っても入渠中を維持する
        /// </summary>
        [TestMethod]
        public void SlotDepriveNDockShip()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("slot_deprive_ndock-101");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 15306).InNDock);
            sniffer.SniffLogFile("slot_deprive_ndock-102");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 15306).InNDock);
        }

        /// <summary>
        /// 装備改修画面で入渠中の艦から改修装備を奪っても入渠中を維持する
        /// </summary>
        [TestMethod]
        public void RemodelSlotlistSlotset()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodel_slotlist_slotset-101");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 56).InNDock);
            sniffer.SniffLogFile("remodel_slotlist_slotset-102");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 56).InNDock);
        }

        /// <summary>
        /// 改修による資材の減少をすぐに反映する
        /// </summary>
        [TestMethod]
        public void ConsumptionByRemodelSlot()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodel_slot_001");
            PAssert.That(() => sniffer.Material.Current.ParamsEqual(25292, 25570, 25244, 41113, 1405, 1525, 2137, 8));
        }

        /// <summary>
        /// 改修更新による特殊資材の減少をすぐに反映する
        /// </summary>
        [TestMethod]
        public void ConsumptionSpecialMaterialByRemodelSlot()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodel_slot_101");
            PAssert.That(() => sniffer.Material.Current.ParamsEqual(344646, 339665, 349398, 350000, 2831, 2696, 2979, 91));
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x60");
            var upgradeitem = sniffer.SlotItems.First(item => item.Id == 170899);
            PAssert.That(() => upgradeitem.Spec.Name == "16inch Mk.I連装砲");
            PAssert.That(() => upgradeitem.Level == 10);

            sniffer.SniffLogFile("remodel_slot_102");
            PAssert.That(() => sniffer.Material.Current.ParamsEqual(344616, 339315, 348918, 350000, 2831, 2696, 2955, 83));
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x59");
            upgradeitem = sniffer.SlotItems.First(item => item.Id == 170899);
            PAssert.That(() => upgradeitem.Spec.Name == "16inch Mk.V連装砲");
            PAssert.That(() => upgradeitem.Level == 0);
        }

        /// <summary>
        /// 改修更新の確認画面まで行ってキャンセルし、特殊資材を消費しない別の改修を実行して、特殊資材を消費してないのを確認する
        /// </summary>
        [TestMethod]
        public void CancelRemodelSlotWithSpecialMaterial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodel_slot_201");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(23).ToString() == "熟練搭乗員 x29");
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型航空兵装資材 x52");
            PAssert.That(() => sniffer.RemodelSlotReqUseitem.Count == 0);

            sniffer.SniffLogFile("remodel_slot_202");
            PAssert.That(() => sniffer.RemodelSlotReqUseitem.Count == 2);
            PAssert.That(() => sniffer.RemodelSlotReqUseitem[77] == 1);
            PAssert.That(() => sniffer.RemodelSlotReqUseitem[70] == 2);

            sniffer.SniffLogFile("remodel_slot_203");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(23).ToString() == "熟練搭乗員 x29");
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型航空兵装資材 x52");
            PAssert.That(() => sniffer.RemodelSlotReqUseitem.Count == 0);
        }

        /// <summary>
        /// 装備の数を正しく数える
        /// </summary>
        [TestMethod]
        public void CountItem()
        {
            var createItems = new TestingSniffer();
            createItems.SniffLogFile("createitem_001");
            PAssert.That(() => createItems.ItemCounter.Now == 854);
            var createShips = new TestingSniffer();
            createShips.SniffLogFile("createship_001");
            PAssert.That(() => createShips.ItemCounter.Now == 858);
            var multiItems = new TestingSniffer();
            multiItems.SniffLogFile("createitem_002");
            PAssert.That(() => multiItems.ItemCounter.Now == 1250);
        }

        /// <summary>
        /// 装備数の超過を警告する
        /// </summary>
        [TestMethod]
        public void WarnItemCount()
        {
            static void func(int i) { }
            var sniffer1 = new TestingSniffer();
            sniffer1.ItemCounter.Margin = 51; // 応急修理要員、戦闘糧食、洋上補給は所持数から引かれるようになった
            sniffer1.SniffLogFile("item_count_001");
            func(sniffer1.ItemCounter.Now); // Nowを読まないとAlarmが立たない
            PAssert.That(() => sniffer1.ItemCounter.Alarm, "出撃から母港に戻ったとき");
            var sniffer2 = new TestingSniffer();
            sniffer2.ItemCounter.Margin = 51; // 応急修理要員、戦闘糧食、洋上補給は所持数から引かれるようになった
            sniffer2.SniffLogFile("item_count_002");
            func(sniffer2.ItemCounter.Now);
            PAssert.That(() => sniffer2.ItemCounter.Alarm, "ログインしたとき");
        }

        /// <summary>
        /// 警告値ギリギリでドロップして帰投しても不要な装備数の超過を警告しない
        /// </summary>
        [TestMethod]
        public void WarnItemCountMarginBorder()
        {
            var sniffer = new TestingSniffer();
            sniffer.ItemCounter.Margin = 16;
            sniffer.AdditionalData.RecordNumEquips(61, "青葉", 2);

            // ログイン
            sniffer.SniffLogFile("itemwarning_margin16_101");
            PAssert.That(() => sniffer.ShipCounter.Now == 547);
            PAssert.That(() => sniffer.ItemCounter.Now == 2348);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);

            // 出撃して青葉ドロップ後、資源マスで出撃終了
            sniffer.SniffLogFile("itemwarning_margin16_102");
            PAssert.That(() => sniffer.ShipCounter.Now == 548);
            PAssert.That(() => sniffer.ItemCounter.Now == 2350);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);

            // 帰投時のapi_portで装備数超過の警告が出てはならない
            sniffer.SniffLogFile("itemwarning_margin16_103");
            PAssert.That(() => sniffer.ShipCounter.Now == 548);
            PAssert.That(() => sniffer.ItemCounter.Now == 2350);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);

            // api_port後の各apiでも同様
            sniffer.SniffLogFile("itemwarning_margin16_104");
            PAssert.That(() => sniffer.ShipCounter.Now == 548);
            PAssert.That(() => sniffer.ItemCounter.Now == 2350);
            PAssert.That(() => !sniffer.ItemCounter.TooMany);
        }

        /// <summary>
        /// 装備の所持者を設定する
        /// </summary>
        [TestMethod]
        public void SetItemHolder()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("itemholder_001");
            var itemIds = new HashSet<int> {75298, 76572, 82725, 90213, 5910};
            var items = sniffer.ItemList.Where(status => itemIds.Contains(status.Id));
            PAssert.That(() => items.All(x => x.Holder.Id == 861));
        }

        /// <summary>
        /// 新規のドロップ艦の初期装備数を登録する
        /// </summary>
        [TestMethod]
        public void RecordNumEquipsOfNewDropShip()
        {
            var sniffer = new TestingSniffer();
            PAssert.That(() => sniffer.AdditionalData.NumEquips(565) == -1);
            PAssert.That(() => sniffer.ShipList.FirstOrDefault(s => s.Spec.Id == 565) == null);
            sniffer.SniffLogFile("dropship_001");
            PAssert.That(() => sniffer.AdditionalData.NumEquips(565) == 2);
            PAssert.That(() => sniffer.ShipList.First(s => s.Spec.Id == 565).Spec.NumEquips == 2);
        }

        /// <summary>
        /// 既知のドロップ艦とその装備をカウントする
        /// </summary>
        [TestMethod]
        public void CountDropShip()
        {
            var sniffer = new TestingSniffer();
            sniffer.AdditionalData.RecordNumEquips(11, "", 1);
            sniffer.SniffLogFile("dropship_002");
            PAssert.That(() => sniffer.ShipCounter.Now == 250);
            PAssert.That(() => sniffer.ItemCounter.Now == 1111);
        }

        /// <summary>
        /// ドロップ艦の搭載数が0にならない
        /// </summary>
        [TestMethod]
        public void DropShipOnslot()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("dropship_onslot_001");
            var ship = sniffer.ShipList.First(s => s.Id == 143298);
            PAssert.That(() => ship.Items[1].OnSlot == 2);
            PAssert.That(() => ship.Items[1].MaxEq == 2);
        }
    }
}
