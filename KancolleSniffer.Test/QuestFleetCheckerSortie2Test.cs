// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestFleetCheckerSortie2Test : QuestCounterTester
    {
        [TestInitialize]
        public void Initialize()
        {
            battleInfo.InjectDeck(1);
        }

        /// <summary>
        /// 拡張「六水戦」、最前線へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_903()
        {
            var count = InjectQuest(903);
            var yuubari1  = NewShip(293);
            var yuubari2  = NewShip(622);
            var yuubari2t = NewShip(623);
            var yuubari2d = NewShip(624);
            var yura1     = NewShip(220);
            var yura2     = NewShip(488);
            var mutsuki   = NewShip(1);
            var kisaragi  = NewShip(2);
            var yayoi     = NewShip(164);
            var uzuki     = NewShip(165);
            var kikuzuki  = NewShip(30);
            var mochizuki = NewShip(31);
            var ayanami   = NewShip(13);

            battleInfo.InjectFleet(yuubari2, mutsuki, ayanami);
            InjectMapNext(51, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "六水戦駆逐が1隻");

            battleInfo.InjectFleet(yuubari2, mutsuki, kisaragi);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "5-1");

            InjectMapNext(54, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "ボス以外はカウントしない");

            battleInfo.InjectFleet(yuubari1, mutsuki, kisaragi);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "旗艦が夕張改");

            battleInfo.InjectFleet(mutsuki, kisaragi, yuubari2);
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "旗艦が夕張改二ではない");

            battleInfo.InjectFleet(yuubari2t, yayoi, uzuki);
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "5-4");

            battleInfo.InjectFleet(yuubari2d, kikuzuki, mochizuki);
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "6-4");

            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "6-5");

            battleInfo.InjectFleet(yuubari2, yura1);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "由良改");

            battleInfo.InjectFleet(yuubari2, yura2);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 2), "由良改二");
        }

        /// <summary>
        /// 904: 精鋭「十九駆」、躍り出る！
        /// </summary>
        [TestMethod]
        public void BattleResult_904()
        {
            var count = InjectQuest(904);
            var ayanami2   = NewShip(195);
            var shikinami  = NewShip(208);
            var shikinami2 = NewShip(627);

            battleInfo.InjectFleet(ayanami2, shikinami);
            InjectMapNext(25, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "敷波はカウントしない");

            battleInfo.InjectFleet(ayanami2, shikinami2);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "2-5");

            InjectMapNext(34, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(34, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "3-4");

            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "4-5");

            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "5-3");
        }

        /// <summary>
        /// 905: 「海防艦」、海を護る！
        /// </summary>
        [TestMethod]
        public void BattleResult_905()
        {
            var count = InjectQuest(905);
            var escort    = NewShip(517);
            var destroyer = NewShip(1);

            battleInfo.InjectFleet(escort, escort, escort, destroyer, destroyer, destroyer);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "6隻はカウントしない");

            battleInfo.InjectFleet(escort, escort, escort, destroyer, destroyer);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "B勝利はカウントしない");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "1-1");

            InjectMapNext(12, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0, 0), "1-2");

            battleInfo.InjectFleet(destroyer, escort, escort, destroyer, destroyer);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0, 0), "海防艦2隻はカウントしない");

            battleInfo.InjectFleet(escort, escort, escort, destroyer, destroyer);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0), "1-3");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 0), "1-5");

            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "1-6");
        }

        /// <summary>
        /// 912: 工作艦「明石」護衛任務
        /// </summary>
        [TestMethod]
        public void BattleResult_912()
        {
            var count = InjectQuest(912);
            var akashi    = NewShip(182);
            var escort    = NewShip(517);
            var destroyer = NewShip(1);

            battleInfo.InjectFleet(akashi, destroyer, destroyer, escort);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "駆逐艦2隻はカウントしない");

            battleInfo.InjectFleet(akashi, destroyer, destroyer, destroyer);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "B勝利はカウントしない");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "1-3");

            InjectMapNext(21, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "ボス以外はカウントしない");

            battleInfo.InjectFleet(destroyer, akashi, destroyer, destroyer);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "旗艦明石以外はカウントしない");

            battleInfo.InjectFleet(akashi, destroyer, destroyer, destroyer);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0, 0), "2-1");

            InjectMapNext(22, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0), "2-2");

            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 0), "2-3");

            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "1-6");
        }

        /// <summary>
        /// 912: 重巡戦隊、西へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_914()
        {
            var count = InjectQuest(914);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            battleInfo.InjectFleet(heavycruiser, heavycruiser, heavycruiser, escort);
            InjectMapNext(41, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "駆逐艦なしはカウントしない");

            battleInfo.InjectFleet(heavycruiser, heavycruiser, heavycruiser, destroyer);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "B勝利はカウントしない");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "4-1");

            InjectMapNext(42, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "ボス以外はカウントしない");

            battleInfo.InjectFleet(aviationcruiser, heavycruiser, heavycruiser, destroyer);
            InjectMapNext(42, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "重巡2隻はカウントしない");

            battleInfo.InjectFleet(heavycruiser, heavycruiser, heavycruiser, destroyer);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "4-2");

            InjectMapNext(43, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "4-3");

            InjectMapNext(44, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "4-3");
        }

        /// <summary>
        /// 928: 歴戦「第十方面艦隊」、全力出撃！
        /// </summary>
        [TestMethod]
        public void BattleResult_928()
        {
            var count = InjectQuest(928);
            var haguro2  = NewShip(194);
            var nachi    = NewShip(63);
            var myoukou  = NewShip(62);
            var takao    = NewShip(66);
            var kamikaze = NewShip(471);
            var escort   = NewShip(517);

            battleInfo.InjectFleet(haguro2, nachi, escort, escort, escort, escort);
            InjectMapNext(73, 5, 18);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0));

            battleInfo.InjectFleet(haguro2, myoukou, escort, escort, escort, escort);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0));
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0));

            battleInfo.InjectFleet(myoukou, takao, escort, escort, escort, escort);
            InjectMapNext(72, 5, 15);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0));

            battleInfo.InjectFleet(kamikaze, takao, escort, escort, escort, escort);
            InjectMapNext(42, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));
        }

        /// <summary>
        /// 944: 鎮守府近海海域の哨戒を実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_944()
        {
            var count = InjectQuest(944);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            battleInfo.InjectFleet(aviationcruiser, escort, escort, escort);
            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦航巡はカウントしない");

            battleInfo.InjectFleet(escort, escort, escort, escort);
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦海防はカウントしない");

            battleInfo.InjectFleet(destroyer, heavycruiser, escort, escort);
            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "随伴重巡&駆逐海防不足はカウントしない");

            battleInfo.InjectFleet(destroyer, destroyer, escort, escort);
            InjectMapNext(12, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(13, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "1-2");

            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0), "1-3");

            battleInfo.InjectFleet(heavycruiser, destroyer, escort, escort);
            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "1-4");
        }

        /// <summary>
        /// 945: 南西方面の兵站航路の安全を図れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_945()
        {
            var count = InjectQuest(945);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);

            battleInfo.InjectFleet(torpedocruiser, escort, escort, escort);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦雷巡はカウントしない");

            battleInfo.InjectFleet(escort, escort, escort, escort);
            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦海防はカウントしない");

            battleInfo.InjectFleet(destroyer, lightcruiser, escort, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "随伴軽巡&駆逐海防不足はカウントしない");

            battleInfo.InjectFleet(destroyer, destroyer, escort, escort);
            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(21, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "1-5");

            battleInfo.InjectFleet(lightcruiser, destroyer, escort, escort);
            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 1), "1-6");

            battleInfo.InjectFleet(trainingcruiser, destroyer, escort, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "2-1");
        }

        /// <summary>
        /// 946: 空母機動部隊、出撃！敵艦隊を迎撃せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_946()
        {
            var count = InjectQuest(946);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);
            var armoredcarrier  = NewShip(153);

            battleInfo.InjectFleet(heavycruiser, lightcarrier, aviationcruiser);
            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦重巡はカウントしない");

            battleInfo.InjectFleet(aviationcruiser, lightcarrier, aviationcruiser);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦航巡はカウントしない");

            battleInfo.InjectFleet(lightcarrier, lightcarrier, aviationcruiser);
            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "重巡航巡不足はカウントしない");

            battleInfo.InjectFleet(lightcarrier, heavycruiser, aviationcruiser);
            InjectMapNext(22, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "A勝はカウントしない");

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "2-2");

            battleInfo.InjectFleet(aircraftcarrier, heavycruiser, aviationcruiser);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0), "2-3");

            battleInfo.InjectFleet(armoredcarrier, heavycruiser, aviationcruiser);
            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "2-4");
        }

        /// <summary>
        /// 947: AL作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_947()
        {
            var count = InjectQuest(947);
            var destroyer    = NewShip(1);
            var lightcarrier = NewShip(89);

            battleInfo.InjectFleet(destroyer, lightcarrier, destroyer);
            InjectMapNext(31, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "軽空不足はカウントしない");

            battleInfo.InjectFleet(destroyer, lightcarrier, lightcarrier);
            InjectMapNext(33, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(34, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝はカウントしない");

            InjectMapNext(31, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "3-1");

            InjectMapNext(33, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "3-3");

            InjectMapNext(34, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "3-4");

            InjectMapNext(35, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "3-5");
        }

        /// <summary>
        /// 948: 機動部隊決戦
        /// </summary>
        [TestMethod]
        public void BattleResult_948()
        {
            var count = InjectQuest(948);
            var destroyer       = NewShip(1);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);
            var armoredcarrier  = NewShip(153);

            battleInfo.InjectFleet(destroyer, lightcarrier);
            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "旗艦空母以外はカウントしない");

            battleInfo.InjectFleet(lightcarrier, destroyer);
            InjectMapNext(55, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            battleInfo.InjectDeck(4);
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "第一艦隊以外はカウントしない");
            battleInfo.InjectDeck(1);

            InjectMapNext(52, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "5-2 A勝はカウントしない");

            InjectMapNext(55, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "5-5 A勝はカウントしない");

            InjectMapNext(64, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "6-4 B勝はカウントしない");

            InjectMapNext(65, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "6-5 A勝はカウントしない");

            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "5-2");

            battleInfo.InjectFleet(aircraftcarrier, destroyer);
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "5-5");

            battleInfo.InjectFleet(armoredcarrier, destroyer);
            InjectMapNext(64, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "6-4 A勝");

            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "6-5");
        }

        /// <summary>
        /// 953: 【梅雨限定任務】雨の南西諸島防衛戦！
        /// </summary>
        [TestMethod]
        public void BattleResult_953()
        {
            var count = InjectQuest(953);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);
            var seaplanecarrier = NewShip(102);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);

            battleInfo.InjectFleet(lightcruiser, seaplanecarrier, destroyer, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝は不可");

            InjectMapNext(21, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "1-2は2023年まで");

            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "1-3は2023年まで");

            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0));

            battleInfo.InjectFleet(torpedocruiser, seaplanecarrier, destroyer, escort);
            InjectMapNext(22, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0));

            battleInfo.InjectFleet(trainingcruiser, seaplanecarrier, destroyer, escort);
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));

            battleInfo.InjectFleet(heavycruiser, seaplanecarrier, destroyer, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(2, 1, 1));

            battleInfo.InjectFleet(aviationcruiser, seaplanecarrier, destroyer, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1));


            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "駆逐3は2023年まで");

            battleInfo.InjectFleet(torpedocruiser, destroyer, destroyer, destroyer);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "雷巡旗艦だが水母海防が不足");

            battleInfo.InjectFleet(destroyer, lightcruiser, destroyer, destroyer);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "駆逐旗艦は不可、水母海防が不足");

            battleInfo.InjectFleet(seaplanecarrier, heavycruiser, destroyer, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "巡洋艦以外は旗艦不可");

            battleInfo.InjectFleet(lightcruiser, aviationcruiser, destroyer, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "水母が不足");

            battleInfo.InjectFleet(torpedocruiser, seaplanecarrier, escort, escort);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "海防が不足");

            battleInfo.InjectFleet(trainingcruiser, seaplanecarrier, destroyer, destroyer);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1, 1), "駆逐が不足");
        }

        /// <summary>
        /// 954: 【梅雨拡張任務】梅雨の海上護衛強化！
        /// </summary>
        [TestMethod]
        public void BattleResult_954()
        {
            var count = InjectQuest(954);
            var lightcruiser    = NewShip(51);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);

            battleInfo.InjectFleet(destroyer, escort, escort);
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝は不可");

            InjectMapNext(12, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0));

            battleInfo.InjectFleet(aviationcruiser, aviationcruiser);
            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "重巡級2は2023年まで");

            battleInfo.InjectFleet(destroyer, escort, escort);
            InjectMapNext(73, 5, 23);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "7-3-2は2023年まで");

            InjectMapNext(74, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "7-4は2023年まで");

            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0));

            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));


            battleInfo.InjectFleet(destroyer, escort);
            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "艦種不足");

            battleInfo.InjectFleet(escort, destroyer, escort);
            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "旗艦指定間違い");
        }

        /// <summary>
        /// 955: 【梅雨限定月間任務】西方海域統合作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_955()
        {
            var count = InjectQuest(955);
            var lightcruiser       = NewShip(51);
            var kuma               = NewShip(99);
            var agano              = NewShip(137);
            var ooyodo             = NewShip(183);
            var battlecruiser      = NewShip(78);
            var battleship         = NewShip(26);
            var aviationbattleship = NewShip(286);
            var lightcarrier       = NewShip(89);
            var aircraftcarrier    = NewShip(83);
            var armoredcarrier     = NewShip(153);

            battleInfo.InjectFleet(armoredcarrier, aircraftcarrier, ooyodo);
            InjectMapNext(41, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "A勝は不可");

            InjectMapNext(41, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(41, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0));

            InjectMapNext(51, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "5-1は2023年まで");

            battleInfo.InjectFleet(battleship, battlecruiser, ooyodo);
            InjectMapNext(41, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "戦艦2隻は2023年まで");

            battleInfo.InjectFleet(armoredcarrier, aircraftcarrier, agano);
            InjectMapNext(41, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "阿賀野型は2023年まで");

            battleInfo.InjectFleet(lightcarrier, aircraftcarrier, ooyodo);
            InjectMapNext(42, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0, 0));

            battleInfo.InjectFleet(aircraftcarrier, lightcarrier, kuma);
            InjectMapNext(43, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0));

            battleInfo.InjectFleet(lightcarrier, armoredcarrier, kuma);
            InjectMapNext(44, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 0));

            battleInfo.InjectFleet(ooyodo, lightcarrier, armoredcarrier);
            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1));


            battleInfo.InjectFleet(lightcarrier, kuma, ooyodo);
            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "空母が不足");

            battleInfo.InjectFleet(lightcarrier, armoredcarrier, lightcruiser);
            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "球磨型大淀型以外の軽巡は不可");
        }

        /// <summary>
        /// 973: 日英米合同水上艦隊、抜錨せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_973()
        {
            var count = InjectQuest(973);
            var perth      = NewShip(618);
            var gambierBay = NewShip(707);
            var fletcher   = NewShip(629);
            var scamp      = NewShip(715);
            var warspite   = NewShip(364);

            battleInfo.InjectFleet(perth, warspite, fletcher);
            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "米英艦不足はカウントしない");

            battleInfo.InjectFleet(gambierBay, warspite, fletcher);
            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "空母系を含む場合はカウントしない");

            battleInfo.InjectFleet(scamp, warspite, fletcher);
            InjectMapNext(31, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(31, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "B勝はカウントしない");

            InjectMapNext(72, 5, 15);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "誤出撃はカウントしない");

            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "3-1");

            InjectMapNext(33, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "3-3");

            InjectMapNext(43, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "4-3");

            InjectMapNext(73, 5, 25);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "7-3-2");
        }

        /// <summary>
        /// 975: 精鋭「第十九駆逐隊」、全力出撃！
        /// </summary>
        [TestMethod]
        public void BattleResult_975()
        {
            var count = InjectQuest(975);
            var isonami    = NewShip(206);
            var isonami2   = NewShip(666);
            var uranami    = NewShip(368);
            var uranami2   = NewShip(647);
            var ayanami    = NewShip(207);
            var ayanami2   = NewShip(195);
            var shikinami  = NewShip(208);
            var shikinami2 = NewShip(627);
            var fubuki2    = NewShip(201);

            battleInfo.InjectFleet(isonami2, uranami2, ayanami2);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "指定艦が不足");

            battleInfo.InjectFleet(isonami, uranami2, ayanami2, shikinami2);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "指定艦が改二になってない");

            battleInfo.InjectFleet(isonami2, fubuki2, ayanami2, shikinami2);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "改二の指定艦が間違ってる");

            battleInfo.InjectFleet(isonami2, uranami2, ayanami2, shikinami2);
            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝はカウントしない");

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "誤出撃はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "1-5");

            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "3-3");

            InjectMapNext(32, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "4-3");

            battleInfo.InjectFleet(fubuki2, uranami2, ayanami2, isonami2, shikinami2);
            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "7-3-2");
        }

        /// <summary>
        /// 987: 【限定週間任務】秋の南瓜祭り、おかわりっ！
        /// </summary>
        [TestMethod]
        public void BattleResult_987()
        {
            var count = InjectQuest(987);
            var jingei  = NewShip(634);
            var chougei = NewShip(635);
            var i47     = NewShip(636);
            var i201    = NewShip(881);
            var i203    = NewShip(882);

            var ukuru = NewShip(921);
            var inagi = NewShip(922);
            var no4   = NewShip(637);
            var no22  = NewShip(898);
            var no30  = NewShip(638);

            var mizuho   = NewShip(451);
            var abukuma  = NewShip(114);
            var brooklyn = NewShip(896);
            var honolulu = NewShip(598);
            var johnston = NewShip(562);
            var fletcher = NewShip(596);

            battleInfo.InjectFleet(jingei, i201, i47);
            InjectMapNext(15, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "B勝は不可");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "1-5");

            battleInfo.InjectFleet(chougei, i201, i47);
            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "長鯨は不可");

            battleInfo.InjectFleet(chougei, i203, i47);
            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "伊203は不可");


            battleInfo.InjectFleet(no4, no22, no30);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "2-1");


            battleInfo.InjectFleet(abukuma, brooklyn, johnston);
            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "3-1");

            battleInfo.InjectFleet(abukuma, honolulu, johnston);
            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "honoluluは不可");

            battleInfo.InjectFleet(abukuma, brooklyn, fletcher);
            InjectMapNext(31, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "fletcherは不可");


            battleInfo.InjectFleet(no4, no22, no30);
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "6-1");

            battleInfo.InjectFleet(jingei, no22, abukuma);
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "混在は不可");


            InjectMapNext(62, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "6-2は2023年まで");

            InjectMapNext(63, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "6-3は2023年まで");

            battleInfo.InjectFleet(abukuma, i201);
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "阿武隈 伊201は2023年まで");

            battleInfo.InjectFleet(ukuru, inagi);
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "鵜来 稲木は2023年まで");

            battleInfo.InjectFleet(mizuho, brooklyn, johnston);
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "阿武隈 Brooklyn Johnstonは2023年まで");
        }

        /// <summary>
        /// 988: 【Xmas限定任務】聖夜の哨戒線
        /// </summary>
        [TestMethod]
        public void BattleResult_988()
        {
            var count = InjectQuest(988);
            var tenryu = NewShip(51);
            var ooikai = NewShip(57);
            var katori = NewShip(154);
            var kako   = NewShip(60);

            battleInfo.InjectFleet(kako);
            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "旗艦は軽巡級");

            battleInfo.InjectFleet(tenryu);
            InjectMapNext(12, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝はカウントしない");

            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "誤出撃はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "1-2");

            battleInfo.InjectFleet(ooikai);
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "1-3");

            battleInfo.InjectFleet(katori);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "1-5");

            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "2-1");
        }
    }
}
