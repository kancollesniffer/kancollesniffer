// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static KancolleSniffer.Util.EnumerableUtil;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestCountTest
    {
        [TestMethod]
        public void AdjustCount()
        {
            var count = new QuestCount { Spec = new QuestSpec {Max = 7}, Now = 3 };
            count.AdjustCount(0);
            PAssert.That(() => count.Now == 3);
            count.AdjustCount(50);
            PAssert.That(() => count.Now == 4);
            count.AdjustCount(80);
            PAssert.That(() => count.Now == 6);
            count.AdjustCount(100);
            PAssert.That(() => count.Now == 7);
            count.Now = 14;
            count.AdjustCount(100);
            PAssert.That(() => count.Now == 14);
            count.AdjustCount(80);
            PAssert.That(() => count.Now == 6);
            count.AdjustCount(50);
            PAssert.That(() => count.Now == 5);
            count.AdjustCount(0);
            PAssert.That(() => count.Now == 3);
        }

        [TestMethod]
        public void AdjustCountWithShift()
        {
            var count = new QuestCount { Spec = new QuestSpec {Max = 7, Shift = 1}, Now = 3 };
            count.AdjustCount(0);
            PAssert.That(() => count.Now == 2);
            count.AdjustCount(50);
            PAssert.That(() => count.Now == 3);
            count.AdjustCount(80);
            PAssert.That(() => count.Now == 6);
            count.AdjustCount(100);
            PAssert.That(() => count.Now == 7);
            count.Now = 14;
            count.AdjustCount(100);
            PAssert.That(() => count.Now == 14);
            count.AdjustCount(80);
            PAssert.That(() => count.Now == 6);
            count.AdjustCount(50);
            PAssert.That(() => count.Now == 5);
            count.AdjustCount(0);
            PAssert.That(() => count.Now == 2);
        }

        [TestMethod]
        public void AdjustCountMax3WithShift2()
        {
            var count = new QuestCount { Spec = new QuestSpec {Max = 3, Shift = 2}, Now = 0 };
            count.AdjustCount(0);
            PAssert.That(() => count.Now == 0);
            count.AdjustCount(50);
            PAssert.That(() => count.Now == 1);
            count.AdjustCount(80);
            PAssert.That(() => count.Now == 2);
            count.AdjustCount(100);
            PAssert.That(() => count.Now == 3);
            count.Now = 4;
            count.AdjustCount(100);
            PAssert.That(() => count.Now == 4);
            count.AdjustCount(80);
            PAssert.That(() => count.Now == 2);
            count.AdjustCount(50);
            PAssert.That(() => count.Now == 1);
            count.AdjustCount(0);
            PAssert.That(() => count.Now == 0);
        }

        [TestMethod]
        public void AdjustCount80Percent()
        {
            var count = new QuestCount { Spec = new QuestSpec() };
            for (var shift = 0; shift <= 1; shift++)
            {
                for (var max = 2; max <= 6; max++)
                {
                    count.Spec.Max = max;
                    count.Spec.Shift = shift;
                    count.Now = 1;
                    count.AdjustCount(80);
                    PAssert.That(() => count.Now == count.Spec.Max - 1);
                }
            }
        }

        [TestMethod]
        public void AdjustCountNowArray()
        {
            var count = new QuestCount
            {
                Spec = new QuestSpec { MaxArray = [36, 6, 24, 12] },
                NowArray = [1, 2, 3, 4]
            };
            count.AdjustCount(50);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 2, 3, 4));
            count.AdjustCount(100);
            PAssert.That(() => count.NowArray.ParamsEqual(36, 6, 24, 12));
            count.NowArray = [38, 12, 19, 12];
            count.AdjustCount(100);
            PAssert.That(() => count.NowArray.ParamsEqual(38, 12, 24, 12));
        }

        /// <summary>
        /// カウンターを文字列表記にする
        /// </summary>
        [TestMethod]
        public void ToStringTest()
        {
            var status = new Status
            {
                QuestCountList = [
                    new QuestCount { Id = 211, Now = 2 },
                    new QuestCount { Id = 214, NowArray = [20, 7, 10, 8] },
                    new QuestCount { Id = 854, NowArray = [2, 1, 1, 1] },
                    new QuestCount { Id = 426, NowArray = [1, 1, 1, 1] },
                    new QuestCount { Id = 428, NowArray = [1, 1, 1] },
                    new QuestCount { Id = 873, NowArray = [1, 1, 1] },
                    new QuestCount { Id = 888, NowArray = [1, 1, 1] },
                    new QuestCount { Id = 688, NowArray = [2, 1, 2, 1] },
                    new QuestCount { Id = 893, NowArray = [1, 1, 1, 1] },
                    new QuestCount { Id = 894, NowArray = [1, 1, 1, 1, 1] },
                    new QuestCount { Id = 280, NowArray = [1, 1, 1, 1] },
                    new QuestCount { Id = 872, NowArray = [1, 1, 1, 1] },
                    new QuestCount { Id = 284, NowArray = [1, 1, 1, 1] },
                    new QuestCount { Id = 226, Now = 2 },
                    new QuestCount { Id = 434, NowArray = [1, 0, 1, 0, 1] },
                    new QuestCount { Id = 436, NowArray = [1, 0, 1, 1, 1] },
                    new QuestCount { Id = 437, NowArray = [1, 0, 1, 1] },
                    new QuestCount { Id = 438, NowArray = [1, 0, 1, 1] },
                    new QuestCount { Id = 439, NowArray = [1, 0, 1, 1] },
                    new QuestCount { Id = 440, NowArray = [1, 0, 1, 1, 1] },
                    new QuestCount { Id = 680, NowArray = [0, 1] },
                    new QuestCount { Id = 686, NowArray = [1, 2] }
                ]
            };
            var countList = new QuestCountList();
            countList.SetNames(JsonArray(
                new { api_id = 1, api_name = "練習航海" },
                new { api_id = 2, api_name = "長距離練習航海" },
                new { api_id = 3, api_name = "警備任務" },
                new { api_id = 4, api_name = "対潜警戒任務" },
                new { api_id = 5, api_name = "海上護衛任務" },
                new { api_id = 9, api_name = "タンカー護衛任務" },
                new { api_id = 10, api_name = "強行偵察任務" },
                new { api_id = 11, api_name = "ボーキサイト輸送任務" },
                new { api_id = 40, api_name = "水上機前線輸送" },
                new { api_id = 41, api_name = "ブルネイ泊地沖哨戒" },
                new { api_id = 46, api_name = "南西海域戦闘哨戒" },
                new { api_id = 100, api_name = "兵站強化任務" },
                new { api_id = 101, api_name = "海峡警備行動" },
                new { api_id = 102, api_name = "長時間対潜警戒" },
                new { api_id = 104, api_name = "小笠原沖哨戒線" },
                new { api_id = 105, api_name = "小笠原沖戦闘哨戒" },
                new { api_id = 110, api_name = "南西方面航空偵察作戦" },
                new { api_id = 114, api_name = "南西諸島捜索撃滅戦" },
                new { api_id = 142, api_name = "強行鼠輸送作戦" }
            ), JsonArray(
                new { api_id = 6, api_name = "艦上戦闘機" },
                new { api_id = 7, api_name = "艦上爆撃機" },
                new { api_id = 8, api_name = "艦上攻撃機" },
                new { api_id = 10, api_name = "水上偵察機" },
                new { api_id = 12, api_name = "小型電探" },
                new { api_id = 13, api_name = "大型電探" },
                new { api_id = 21, api_name = "対空機銃" }
            ), JsonArray(
                new { api_id = 3, api_name = "10cm連装高角砲" },
                new { api_id = 121, api_name = "94式高射装置" }
            ));
            new QuestInfo(countList).LoadState(status);

            PAssert.That(() => status.QuestCountList[0].ToString() == "2/3");
            var q214 = status.QuestCountList.First(q => q.Id == 214);
            PAssert.That(() => q214.ToString() == "20/36 7/6 10/24 8/12");
            PAssert.That(() => q214.ToToolTip() == "出撃20 S勝利7 ボス到達10 ボス勝利8");
            var z = status.QuestCountList[2];
            PAssert.That(() => z.ToString() == "2\u200a1\u200a1\u200a1");
            PAssert.That(() => z.ToToolTip() == "2-4: 2\n6-1: 1\n6-3: 1\n6-4: 1");
            z.NowArray = [0, 0, 0, 0];
            PAssert.That(() => z.ToToolTip() == "2-4: 0\n6-1: 0\n6-3: 0\n6-4: 0");
            var q426 = status.QuestCountList[3];
            PAssert.That(() => q426.ToString() == "1\u200a1\u200a1\u200a1");
            PAssert.That(() => q426.ToToolTip() == "警備任務: 1\n対潜警戒任務: 1\n海上護衛任務: 1\n強行偵察任務: 1");
            var q428 = status.QuestCountList[4];
            PAssert.That(() => q428.ToToolTip() == "対潜警戒任務: 1\n海峡警備行動: 1\n長時間対潜警戒: 1");
            q428.NowArray = [0, 1, 0];
            PAssert.That(() => q428.ToToolTip() == "対潜警戒任務: 0\n海峡警備行動: 1\n長時間対潜警戒: 0");
            var q873 = status.QuestCountList[5];
            PAssert.That(() => q873.ToString() == "1\u200a1\u200a1");
            PAssert.That(() => q873.ToToolTip() == "3-1: 1\n3-2: 1\n3-3: 1");
            var q888 = status.QuestCountList[6];
            PAssert.That(() => q888.ToString() == "1\u200a1\u200a1");
            PAssert.That(() => q888.ToToolTip() == "5-1: 1\n5-3: 1\n5-4: 1");
            var q680 = status.QuestCountList.First(q => q.Id == 680);
            PAssert.That(() => q680.ToToolTip() == "対空機銃: 0\n小型電探 大型電探: 1");
            var q686 = status.QuestCountList.First(q => q.Id == 686);
            PAssert.That(() => q686.ToToolTip() == "10cm連装高角砲: 1\n94式高射装置: 2");
            var q688 = status.QuestCountList.First(q => q.Id == 688);
            PAssert.That(() => q688.ToToolTip() == "艦上戦闘機: 2\n艦上爆撃機: 1\n艦上攻撃機: 2\n水上偵察機: 1");
            var q893 = status.QuestCountList[8];
            PAssert.That(() => q893.ToToolTip() == "1-5: 1\n7-1: 1\n7-2-1: 1\n7-2-2: 1");
            var q894 = status.QuestCountList[9];
            PAssert.That(() => q894.ToString() == "1\u200a1\u200a1\u200a1\u200a1");
            PAssert.That(() => q894.ToToolTip() == "1-3: 1\n1-4: 1\n2-1: 1\n2-2: 1\n2-3: 1");
            var q280 = status.QuestCountList[10];
            PAssert.That(() => q280.ToString() == "1\u200a1\u200a1\u200a1");
            PAssert.That(() => q280.ToToolTip() == "1-2: 1\n1-3: 1\n1-4: 1\n2-1: 1");
            var q872 = status.QuestCountList.First(q => q.Id == 872);
            PAssert.That(() => q872.ToString() == "1\u200a1\u200a1\u200a1");
            PAssert.That(() => q872.ToToolTip() == "7-2-2: 1\n5-5: 1\n6-2: 1\n6-5: 1");
            var q284 = status.QuestCountList.First(q => q.Id == 284);
            PAssert.That(() => q284.ToString() == "1\u200a1\u200a1\u200a1");
            PAssert.That(() => q284.ToToolTip() == "1-4: 1\n2-1: 1\n2-2: 1\n2-3: 1");
            var q226 = status.QuestCountList.First(q => q.Id == 226);
            PAssert.That(() => q226.ToString() == "2/5");
            PAssert.That(() => q226.ToToolTip() == "");
            var q434 = status.QuestCountList.First(q => q.Id == 434);
            PAssert.That(() => q434.ToString() == "1\u200a0\u200a1\u200a0\u200a1");
            PAssert.That(() => q434.ToToolTip() == "警備任務: 1\n海上護衛任務: 0\n兵站強化任務: 1\n海峡警備行動: 0\nタンカー護衛任務: 1");
            var q436 = status.QuestCountList.First(q => q.Id == 436);
            PAssert.That(() => q436.ToString() == "1\u200a0\u200a1\u200a1\u200a1");
            PAssert.That(() => q436.ToToolTip() == "練習航海: 1\n長距離練習航海: 0\n警備任務: 1\n対潜警戒任務: 1\n強行偵察任務: 1");
            var q437 = status.QuestCountList.First(q => q.Id == 437);
            PAssert.That(() => q437.ToString() == "1\u200a0\u200a1\u200a1");
            PAssert.That(() => q437.ToToolTip() == "対潜警戒任務: 1\n小笠原沖哨戒線: 0\n小笠原沖戦闘哨戒: 1\n南西方面航空偵察作戦: 1");
            var q438 = status.QuestCountList.First(q => q.Id == 438);
            PAssert.That(() => q438.ToString() == "1\u200a0\u200a1\u200a1");
            PAssert.That(() => q438.ToToolTip() == "兵站強化任務: 1\n対潜警戒任務: 0\nタンカー護衛任務: 1\n南西諸島捜索撃滅戦: 1");
            var q439 = status.QuestCountList.First(q => q.Id == 439);
            PAssert.That(() => q439.ToString() == "1\u200a0\u200a1\u200a1");
            PAssert.That(() => q439.ToToolTip() == "海上護衛任務: 1\n兵站強化任務: 0\nボーキサイト輸送任務: 1\n南西方面航空偵察作戦: 1");
            var q440 = status.QuestCountList.First(q => q.Id == 440);
            PAssert.That(() => q440.ToString() == "1\u200a0\u200a1\u200a1\u200a1");
            PAssert.That(() => q440.ToToolTip() == "ブルネイ泊地沖哨戒: 1\n海上護衛任務: 0\n水上機前線輸送: 1\n強行鼠輸送作戦: 1\n南西海域戦闘哨戒: 1");
        }
    }
}
