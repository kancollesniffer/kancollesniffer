// Copyright (C) 2019 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using DynaJson;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static KancolleSniffer.Util.EnumerableUtil;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestCounterTest : QuestCounterTester
    {
        private static readonly TestingSniffer sniffer = new(apiStart2: true);
        protected override TestingSniffer GetSniffer() => sniffer;

        private static TSource[] TypeArray<TSource>(params TSource[] source) => source;

        private static readonly dynamic clearResult0 = new JsonObject(new { api_clear_result = 0 });
        private static readonly dynamic clearResult1 = new JsonObject(new { api_clear_result = 1 });
        private static readonly dynamic clearResult2 = new JsonObject(new { api_clear_result = 2 });

        private static readonly dynamic powerupFlag0 = new JsonObject(new { api_powerup_flag = 0 });
        private static readonly dynamic powerupFlag1 = new JsonObject(new { api_powerup_flag = 1 });

        /// <summary>
        /// 402: 「遠征」を3回成功させよう！
        /// 403: 「遠征」を10回成功させよう！
        /// 404: 大規模遠征作戦、発令！
        /// 410: 南方への輸送作戦を成功させよ！
        /// 411: 南方への鼠輸送を継続実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_402_403_404_410_411()
        {
            InjectQuestList(402, 403, 404, 410, 411);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 6) },
                new { api_id = 3, api_mission = JsonArray(2, 37) },
                new { api_id = 4, api_mission = JsonArray(2, 2) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult2);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult0);
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(
                new { Id = 402, Now = 2 }, new { Id = 403, Now = 2 }, new { Id = 404, Now = 2 },
                new { Id = 410, Now = 1 }, new { Id = 411, Now = 1 }
            ));
        }

        /// <summary>
        /// 426: 海上通商航路の警戒を厳とせよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_426()
        {
            var count = InjectQuest(426);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 3) },
                new { api_id = 3, api_mission = JsonArray(2, 4) },
                new { api_id = 4, api_mission = JsonArray(2, 5) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));
            questCounter.InspectDeck(JsonArray(new { api_id = 2, api_mission = JsonArray(2, 10) }));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 428: 近海に侵入する敵潜を制圧せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_428()
        {
            var count = InjectQuest(428);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 4) },
                new { api_id = 3, api_mission = JsonArray(2, 101) },
                new { api_id = 4, api_mission = JsonArray(2, 102) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));
        }

        /// <summary>
        /// 434: 特設護衛船団司令部、活動開始！
        /// </summary>
        [TestMethod]
        public void MissionResult_434()
        {
            var count = InjectQuest(434);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 3) },
                new { api_id = 3, api_mission = JsonArray(2, 5) },
                new { api_id = 4, api_mission = JsonArray(2, 100) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0));

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 101) },
                new { api_id = 3, api_mission = JsonArray(2, 9) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1));
        }

        /// <summary>
        /// 436: 練習航海及び警備任務を実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_436()
        {
            var count = InjectQuest(436);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 1) },
                new { api_id = 3, api_mission = JsonArray(2, 2) },
                new { api_id = 4, api_mission = JsonArray(2, 3) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0));

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 4) },
                new { api_id = 3, api_mission = JsonArray(2, 10) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1));
        }

        /// <summary>
        /// 437: 小笠原沖哨戒線の強化を実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_437()
        {
            var count = InjectQuest(437);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 4) },
                new { api_id = 3, api_mission = JsonArray(2, 104) },
                new { api_id = 4, api_mission = JsonArray(2, 105) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            questCounter.InspectDeck(JsonArray(new { api_id = 2, api_mission = JsonArray(2, 110) }));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 438: 南西諸島方面の海上護衛を強化せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_438()
        {
            var count = InjectQuest(438);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 4) },
                new { api_id = 3, api_mission = JsonArray(2, 100) },
                new { api_id = 4, api_mission = JsonArray(2, 9) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            questCounter.InspectDeck(JsonArray(new { api_id = 2, api_mission = JsonArray(2, 114) }));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 439: 兵站強化遠征任務【基本作戦】
        /// </summary>
        [TestMethod]
        public void MissionResult_439()
        {
            var count = InjectQuest(439);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 5) },
                new { api_id = 3, api_mission = JsonArray(2, 100) },
                new { api_id = 4, api_mission = JsonArray(2, 11) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            questCounter.InspectDeck(JsonArray(new { api_id = 2, api_mission = JsonArray(2, 110) }));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 440: 兵站強化遠征任務【拡張作戦】
        /// </summary>
        [TestMethod]
        public void MissionResult_440()
        {
            var count = InjectQuest(440);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 41) },
                new { api_id = 3, api_mission = JsonArray(2, 5) },
                new { api_id = 4, api_mission = JsonArray(2, 40) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0));

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 142) },
                new { api_id = 3, api_mission = JsonArray(2, 46) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1));
        }

        /// <summary>
        /// 442: 西方連絡作戦準備を実施せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_442()
        {
            var count = InjectQuest(442);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 131) },
                new { api_id = 3, api_mission = JsonArray(2, 29) },
                new { api_id = 4, api_mission = JsonArray(2, 30) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            questCounter.InspectDeck(JsonArray(new { api_id = 2, api_mission = JsonArray(2, 133) }));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 444: 新兵装開発資材輸送を船団護衛せよ！
        /// </summary>
        [TestMethod]
        public void MissionResult_444()
        {
            var count = InjectQuest(444);

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 5) },
                new { api_id = 3, api_mission = JsonArray(2, 12) },
                new { api_id = 4, api_mission = JsonArray(2, 9) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0));

            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 110) },
                new { api_id = 3, api_mission = JsonArray(2, 11) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1));
        }

        /// <summary>
        /// 449: 【艦隊11周年記念任務】資源輸出
        /// </summary>
        [TestMethod]
        public void MissionResult_449()
        {
            var count = InjectQuest(449);

            // 海上護衛任務 タンカー護衛任務 ボーキサイト輸送任務
            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 5) },
                new { api_id = 3, api_mission = JsonArray(2, 9) },
                new { api_id = 4, api_mission = JsonArray(2, 11) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));

            // 北方航路海上護衛 資源輸送任務 ボーキサイト船団護衛は間違い
            questCounter.InspectDeck(JsonArray(
                new { api_id = 2, api_mission = JsonArray(2, 24) },
                new { api_id = 3, api_mission = JsonArray(2, 12) },
                new { api_id = 4, api_mission = JsonArray(2, 45) }
            ));
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=2", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=3", clearResult1);
            questCounter.InspectMissionResult("api%5Fdeck%5Fid=4", clearResult1);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));
        }

        /// <summary>
        /// 503: 艦隊大整備！
        /// 504: 艦隊酒保祭り！
        /// </summary>
        [TestMethod]
        public void PowerUp_503_504()
        {
            InjectQuestList(503, 504);

            questCounter.CountNyukyo();
            questCounter.CountCharge();
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(new { Id = 503, Now = 1 }, new { Id = 504, Now = 1 }));
        }

        /// <summary>
        /// 605: 新装備「開発」指令
        /// 606: 新造艦「建造」指令
        /// 607: 装備「開発」集中強化！
        /// 608: 艦娘「建造」艦隊強化！
        /// 609: 軍縮条約対応！
        /// 619: 装備の改修強化
        /// </summary>
        [TestMethod]
        public void Kousyou()
        {
            InjectQuestList(605, 606, 607, 608, 609, 619);

            questCounter.InspectCreateItem("api_verno=1&api_item1=10&api_item2=10&api_item3=30&api_item4=10&api_multiple_flag=0");
            questCounter.InspectCreateItem("api_verno=1&api_item1=10&api_item2=10&api_item3=30&api_item4=10&api_multiple_flag=1");
            questCounter.CountCreateShip();
            questCounter.InspectDestroyShip("api%5Fship%5Fid=98159%2C98166%2C98168&api%5Fverno=1");
            questCounter.CountRemodelSlot();
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(
                new { Id = 605, Now = 4 }, new { Id = 606, Now = 1 }, new { Id = 607, Now = 4 },
                new { Id = 608, Now = 1 }, new { Id = 609, Now = 3 }, new { Id = 619, Now = 1 }
            ));
        }

        /// <summary>
        /// 613: 資源の再利用
        /// 626: 精鋭「艦戦」隊の新編成
        /// 628: 機種転換
        /// 638: 対空機銃量産
        /// 643: 主力「陸攻」の調達
        /// 645: 「洋上補給」物資の調達
        /// 653: 工廠稼働！次期作戦準備！
        /// 654: 精鋭複葉機飛行隊の編成
        /// 657: 新型兵装開発整備の強化
        /// 663: 新型艤装の継続研究
        /// 673: 装備開発力の整備
        /// 674: 工廠環境の整備
        /// 675: 運用装備の統合整備
        /// 676: 装備開発力の集中整備
        /// 677: 継戦支援能力の整備
        /// 678: 主力艦上戦闘機の更新
        /// 680: 対空兵装の整備拡充
        /// 681: 航空戦力の再編増強準備
        /// 688: 航空戦力の強化
        /// 1103: 潜水艦強化兵装の量産
        /// 1104: 潜水艦電子兵装の量産
        /// 1105: 夏の格納庫整備＆航空基地整備
        /// 1107: 【鋼材輸出】基地航空兵力を増備せよ！
        /// 1119: 【期間限定】Halloweenの南瓜、食べりゅ?
        /// 1120: 【機種整理統合】新型戦闘機の量産計画
        /// 1123: 改良三座水上偵察機の増備
        /// 1138: 【高射装置量産】94式高射装置の追加配備
        /// </summary>
        [TestMethod]
        public void DestroyItem()
        {
            InjectItems(
                ItemMaster[1],   // 12cm単装砲
                ItemMaster[37],  // 7.7mm機銃
                ItemMaster[19],  // 九六式艦戦
                ItemMaster[4],   // 14cm単装砲
                ItemMaster[11],  // 15.2cm単装砲
                ItemMaster[75],  // ドラム缶(輸送用)
                ItemMaster[7],   // 35.6cm連装砲
                ItemMaster[25],  // 零式水上偵察機
                ItemMaster[13],  // 61cm三連装魚雷
                ItemMaster[213], // 後期型艦首魚雷(6門)
                ItemMaster[20],  // 零式艦戦21型
                ItemMaster[28],  // 22号水上電探
                ItemMaster[31],  // 32号水上電探
                ItemMaster[35],  // 三式弾
                ItemMaster[23],  // 九九式艦爆
                ItemMaster[16],  // 九七式艦攻
                ItemMaster[3],   // 10cm連装高角砲
                ItemMaster[120], // 91式高射装置
                ItemMaster[121], // 94式高射装置
                ItemMaster[242], // Swordfish
                ItemMaster[249], // Fulmar
                ItemMaster[21],  // 零式艦戦52型
                ItemMaster[125], // 61cm三連装(酸素)魚雷
                ItemMaster[106], // 13号対空電探改
                ItemMaster[168], // 九六式陸攻
                ItemMaster[82]   // 九七式艦攻(九三一空)
            );
            InjectQuestList(613, 626, 628, 638, 643, 645, 655, 653, 654, 657, 663, 673, 674, 675, 676, 677, 678, 680, 681, 686, 688,
                1103, 1104, 1105, 1107, 1119, 1120, 1123, 1138);
            questCounter.InspectDestroyItem($"api%5Fslotitem%5Fids={Enumerable.Range(1, ItemInventory.Count).Join("%2C")}&api%5Fverno=1");
            var scalar = TypeArray(
                new {Id = 613, Now = 1}, new {Id = 628, Now = 1}, new {Id = 638, Now = 1}, new {Id = 643, Now = 1},
                new {Id = 645, Now = 1}, new {Id = 653, Now = 1}, new {Id = 663, Now = 1}, new {Id = 673, Now = 2},
                new {Id = 674, Now = 1}, new {Id = 1103, Now = 1}, new {Id = 1104, Now = 1}, new {Id = 1105, Now = 1},
                new {Id = 1123, Now = 1}, new {Id = 1138, Now = 1}
            );
            foreach (var e in scalar)
            {
                var c = questInfo.Quests.Find(q => q.Id == e.Id).Count;
                PAssert.That(() => c.Id == e.Id);
                PAssert.That(() => c.Now == e.Now, $"{c.Id}");
            }
            var array = TypeArray(
                new {Id = 626, NowArray = TypeArray(1, 1)},
                new {Id = 654, NowArray = TypeArray(1, 1)}, new {Id = 655, NowArray = TypeArray(2, 1, 1, 1, 3)},
                new {Id = 657, NowArray = TypeArray(2, 1, 3)},
                new {Id = 675, NowArray = TypeArray(4, 1)}, new {Id = 676, NowArray = TypeArray(1, 1, 1)},
                new {Id = 677, NowArray = TypeArray(1, 1, 3)}, new {Id = 678, NowArray = TypeArray(1, 1)},
                new {Id = 680, NowArray = TypeArray(1, 3)}, new {Id = 686, NowArray = TypeArray(1, 1)},
                new {Id = 681, NowArray = TypeArray(1, 3)},
                new {Id = 688, NowArray = TypeArray(4, 1, 3, 1)},
                new {Id = 1107, NowArray = TypeArray(4, 3)},
                new {Id = 1119, NowArray = TypeArray(2, 1, 1)}, new {Id = 1120, NowArray = TypeArray(4, 1, 3)}
            );
            foreach (var e in array)
            {
                var c = questInfo.Quests.Find(q => q.Id == e.Id).Count;
                PAssert.That(() => c.Id == e.Id);
                PAssert.That(() => c.NowArray.SequenceEqual(e.NowArray), $"{c.Id}");
            }
        }

        private void InjectItems(params ItemSpec[] specs)
        {
            ItemInventory.Clear();
            ItemInventory.Add(specs.Select((s, i) => new ItemStatus { Id = i + 1, Spec = s }));
        }

        /// <summary>
        /// 702: 艦の「近代化改修」を実施せよ！
        /// 703: 「近代化改修」を進め、戦備を整えよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_702_703()
        {
            ShipInventory.Clear();
            NewShip(1);   // 駆逐
            NewShip(51);  // 軽巡

            InjectQuestList(702, 703);
            questCounter.InspectPowerUp("api_id=2&api_id_items=1", powerupFlag0);
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(new { Id = 702, Now = 0 }, new { Id = 703, Now = 0 }));
            questCounter.InspectPowerUp("api_id=2&api_id_items=1", powerupFlag1);
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(new { Id = 702, Now = 1 }, new { Id = 703, Now = 1 }));
        }

        /// <summary>
        /// 714: 「駆逐艦」の改修工事を実施せよ！
        /// 715: 続：「駆逐艦」の改修工事を実施せよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_714_715()
        {
            ShipInventory.Clear();
            NewShip(1);   // 駆逐
            NewShip(1);
            NewShip(1);
            NewShip(1);
            NewShip(51);  // 軽巡
            NewShip(51);
            NewShip(51);
            NewShip(51);

            var q714 = InjectQuest(714);
            questCounter.InspectPowerUp("api_id=3&api_id_items=1,2", powerupFlag1);
            PAssert.That(() => q714.Now == 0);
            questCounter.InspectPowerUp("api_id=5&api_id_items=1,2,3", powerupFlag1);
            PAssert.That(() => q714.Now == 0);
            questCounter.InspectPowerUp("api_id=4&api_id_items=1,2,3", powerupFlag1);
            PAssert.That(() => q714.Now == 1);

            var q715 = InjectQuest(715);
            questCounter.InspectPowerUp("api_id=4&api_id_items=1,2,3", powerupFlag1);
            PAssert.That(() => q715.Now == 0);
            questCounter.InspectPowerUp("api_id=4&api_id_items=5,6,7", powerupFlag1);
            PAssert.That(() => q715.Now == 1);
        }

        /// <summary>
        /// 716: 「軽巡」級の改修工事を実施せよ！
        /// 717: 続：「軽巡」級の改修工事を実施せよ！
        /// </summary>
        [TestMethod]
        public void PowerUp_716_717()
        {
            ShipInventory.Clear();
            NewShip(51);  // 軽巡
            NewShip(51);  // 軽巡
            NewShip(58);  // 雷巡
            NewShip(154); // 練巡
            NewShip(59);  // 重巡
            NewShip(73);  // 航巡
            NewShip(73);  // 航巡

            var q716 = InjectQuest(716);
            questCounter.InspectPowerUp("api_id=1&api_id_items=2,3", powerupFlag1);
            PAssert.That(() => q716.Now == 0);
            questCounter.InspectPowerUp("api_id=1&api_id_items=2,3,5", powerupFlag1);
            PAssert.That(() => q716.Now == 0);
            questCounter.InspectPowerUp("api_id=5&api_id_items=2,3,4", powerupFlag1);
            PAssert.That(() => q716.Now == 0);
            questCounter.InspectPowerUp("api_id=1&api_id_items=2,3,4", powerupFlag1);
            PAssert.That(() => q716.Now == 1);
            questCounter.InspectPowerUp("api_id=3&api_id_items=1,2,4", powerupFlag1);
            PAssert.That(() => q716.Now == 2);
            questCounter.InspectPowerUp("api_id=4&api_id_items=1,2,3", powerupFlag1);
            PAssert.That(() => q716.Now == 3);

            var q717 = InjectQuest(717);
            questCounter.InspectPowerUp("api_id=1&api_id_items=3,4,5", powerupFlag1);
            PAssert.That(() => q717.Now == 0);
            questCounter.InspectPowerUp("api_id=1&api_id_items=5,6,7", powerupFlag1);
            PAssert.That(() => q717.Now == 1);
        }
    }
}
