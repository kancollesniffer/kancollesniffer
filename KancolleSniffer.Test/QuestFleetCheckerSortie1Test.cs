// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestFleetCheckerSortie1Test : QuestCounterTester
    {
        [TestInitialize]
        public void Initialize()
        {
            battleInfo.InjectDeck(1);
        }

        /// <summary>
        /// 226: 南西諸島海域の制海権を握れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_226()
        {
            var count = InjectQuest(226);

            InjectMapStart(21, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);
            InjectBattleResult("B");
            PAssert.That(() => count.Now == 2);
        }

        /// <summary>
        /// // 243: 南方海域珊瑚諸島沖の制空権を握れ！
        /// </summary>
        [TestMethod]
        public void BattleResult_243()
        {
            var count = InjectQuest(243);

            InjectMapNext(52, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(52, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);
        }

        /// <summary>
        /// 249: 「第五戦隊」出撃せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_249()
        {
            var count = InjectQuest(249);
            var myoukou2  = NewShip(319);
            var nachi2    = NewShip(192);
            var haguro2   = NewShip(194);
            var ashigara2 = NewShip(193);
            var chikuma2  = NewShip(189);
            var tone2     = NewShip(188);

            battleInfo.InjectFleet(myoukou2, nachi2, haguro2, ashigara2, chikuma2, tone2);
            InjectMapNext(25, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(25, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);

            ashigara2.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 2, "足柄改二轟沈");

            nachi2.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 2, "那智改二轟沈");
        }

        /// <summary>
        /// 257: 「水雷戦隊」南西へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_257()
        {
            var count = InjectQuest(257);
            var destroyer      = NewShip(1);
            var lightcruiser   = NewShip(51);
            var torpedocruiser = NewShip(58);

            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectMapNext(14, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);

            lightcruiser.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "軽巡轟沈");

            lightcruiser.NowHp = 1;
            battleInfo.InjectFleet(destroyer, lightcruiser, destroyer, destroyer, destroyer, destroyer);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "旗艦が駆逐");

            battleInfo.InjectFleet(lightcruiser, lightcruiser, lightcruiser, lightcruiser, destroyer, destroyer);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "軽巡が4隻");

            battleInfo.InjectFleet(lightcruiser, lightcruiser, lightcruiser, torpedocruiser, destroyer, destroyer);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "駆逐軽巡以外");

            battleInfo.InjectFleet(lightcruiser, lightcruiser, lightcruiser);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "駆逐が不在");

            // 0隻でクラッシュしない
            var checker = new QuestFleetChecker([], 0);
            checker.Check(count.Id);
        }

        /// <summary>
        /// 259: 「水上打撃部隊」南方へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_259()
        {
            var count = InjectQuest(259);
            var ooyodo1    = NewShip(321);
            var mutsu1     = NewShip(276);
            var fusou2     = NewShip(411);
            var yamashiro2 = NewShip(412);
            var ashigara2  = NewShip(193);
            var haguro2    = NewShip(194);
            var yamato1    = NewShip(136);
            var kitakami1  = NewShip(58);
            var ise2       = NewShip(553);

            battleInfo.InjectFleet(ooyodo1, mutsu1, fusou2, yamashiro2, ashigara2, haguro2);
            InjectMapNext(51, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);

            ooyodo1.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "軽巡轟沈");

            ooyodo1.NowHp = 1;
            battleInfo.InjectFleet(ooyodo1, mutsu1, fusou2, yamashiro2, yamato1, haguro2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "戦艦4隻");

            battleInfo.InjectFleet(kitakami1, mutsu1, fusou2, yamashiro2, ashigara2, haguro2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "軽巡なし");

            battleInfo.InjectFleet(ooyodo1, mutsu1, ise2, yamashiro2, ashigara2, haguro2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 2, "伊勢改二");
        }

        /// <summary>
        /// 264: 「空母機動部隊」西へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_264()
        {
            var count = InjectQuest(264);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);

            battleInfo.InjectFleet(lightcarrier, aircraftcarrier, lightcruiser, lightcruiser, destroyer, destroyer);
            InjectMapNext(42, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(42, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);

            lightcarrier.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "轟沈あり");
        }

        /// <summary>
        /// 266: 「水上反撃部隊」突入せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_266()
        {
            var count = InjectQuest(266);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);
            var heavycruiser = NewShip(59);

            battleInfo.InjectFleet(destroyer, heavycruiser, lightcruiser, destroyer, destroyer, destroyer);
            InjectMapNext(25, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(25, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);

            heavycruiser.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "轟沈あり");

            heavycruiser.NowHp = 1;
            battleInfo.InjectFleet(lightcruiser, heavycruiser, destroyer, destroyer, destroyer, destroyer);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "旗艦が軽巡");

            battleInfo.InjectFleet(destroyer, heavycruiser, lightcruiser, lightcruiser, destroyer, destroyer);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "軽巡が2隻");
        }

        /// <summary>
        /// 280: 兵站線確保！海上警備を強化実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_280()
        {
            var count = InjectQuest(280);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var battlecruiser   = NewShip(78);
            var lightcarrier    = NewShip(89);

            battleInfo.InjectFleet(lightcarrier, destroyer, escort, escort, battlecruiser, battlecruiser);
            InjectMapNext(12, 4);
            InjectBattleResult("S");
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0));

            InjectBattleResult("S");
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            InjectMapNext(14, 5);
            InjectBattleResult("S");
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));

            battleInfo.InjectFleet(lightcarrier, escort, escort, battlecruiser, battlecruiser, battlecruiser);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));

            battleInfo.InjectFleet(battlecruiser, escort, escort, escort, battlecruiser, battlecruiser);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));

            battleInfo.InjectFleet(lightcruiser, destroyer, escort, escort, battlecruiser, battlecruiser);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 2));

            battleInfo.InjectFleet(destroyer, torpedocruiser, destroyer, escort, battlecruiser, battlecruiser);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 3));

            battleInfo.InjectFleet(destroyer, destroyer, trainingcruiser, destroyer, battlecruiser, battlecruiser);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 4));
        }

        /// <summary>
        /// 284: 南西諸島方面「海上警備行動」発令！
        /// </summary>
        [TestMethod]
        public void BattleResult_284()
        {
            var count = InjectQuest(284);
            var escort        = NewShip(517);
            var destroyer     = NewShip(1);
            var battlecruiser = NewShip(78);
            var lightcarrier  = NewShip(89);

            battleInfo.InjectFleet(lightcarrier, destroyer, escort, escort, battlecruiser, battlecruiser);
            InjectMapNext(14, 4);
            InjectBattleResult("S");
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0));

            InjectBattleResult("S");
            InjectMapNext(21, 5);
            InjectBattleResult("S");
            InjectMapNext(22, 5);
            InjectBattleResult("S");
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));

            // 艦種チェックは280と共通
        }

        /// <summary>
        /// 840: 【節分任務:豆】節分作戦二〇二五
        /// </summary>
        [TestMethod]
        public void BattleResult_840()
        {
            var count = InjectQuest(840);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var lightcarrier    = NewShip(89);

            var houshou   = NewShip(89);
            var agano     = NewShip(137);
            var uranami   = NewShip(486);
            var miyuki    = NewShip(11);
            var oboro     = NewShip(93);
            var ushio     = NewShip(16);
            var kiyoshimo = NewShip(410);
            var kazagumo  = NewShip(453);
            var asashimo  = NewShip(425);
            var minegumo  = NewShip(583);
            var jingei    = NewShip(634);
            var chougei   = NewShip(635);

            var shouhou   = NewShip(74);
            var sakawa    = NewShip(140);
            var isonami   = NewShip(12);
            var fubuki    = NewShip(9);
            var akebono   = NewShip(15);
            var sazanami  = NewShip(94);
            var hayashimo = NewShip(409);
            var akishimo  = NewShip(625);
            var akigumo   = NewShip(132);
            var natsugumo = NewShip(582);
            var taigei    = NewShip(184);

            var asahi     = NewShip(953);
            var akashi    = NewShip(182);

            battleInfo.InjectFleet(lightcarrier, destroyer, escort);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "軽空旗艦は2023年");

            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, destroyer);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "軽巡旗艦は2023年");

            battleInfo.InjectFleet(trainingcruiser, escort, escort, escort);
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "錬巡旗艦は2023年");

            battleInfo.InjectFleet(torpedocruiser, escort, escort, escort);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "雷巡旗艦は2023年");

            battleInfo.InjectFleet(uranami, miyuki);
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "2-3は2022年まで");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "1-5は2023年");

            battleInfo.InjectFleet(houshou);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "指定艦が不足");

            battleInfo.InjectFleet(taigei, houshou, agano);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦間違い");

            battleInfo.InjectFleet(houshou, agano);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝は不可");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "阿賀野は2024年");

            battleInfo.InjectFleet(shouhou, sakawa);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(hayashimo, akishimo);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(hayashimo, akishimo);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(akigumo, natsugumo);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(uranami, miyuki);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "浦波深雪は2024年");

            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "2-3は2022年まで");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "1-5は2023年");

            battleInfo.InjectFleet(oboro, ushio);
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "1-4は2024年");

            battleInfo.InjectFleet(kiyoshimo, kazagumo);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "清霜風雲は2024年");

            battleInfo.InjectFleet(asashimo, minegumo);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "朝霜峯雲は2024年");


            battleInfo.InjectFleet(asahi);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "指定艦不足");

            battleInfo.InjectFleet(minegumo, houshou, asahi);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "指定艦が一番二番にいない");

            battleInfo.InjectFleet(houshou, asahi);
            InjectMapNext(11, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝は不可");

            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0));

            battleInfo.InjectFleet(akashi, asahi);
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0));

            battleInfo.InjectFleet(jingei, chougei);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));

            battleInfo.InjectFleet(akebono, sazanami);
            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(2, 1, 1));

            battleInfo.InjectFleet(oboro, ushio);
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(2, 2, 1));
        }

        /// <summary>
        /// 841: 【節分任務:鬼】南西方面節分作戦二〇二五
        /// </summary>
        [TestMethod]
        public void BattleResult_841()
        {
            var count = InjectQuest(841);
            var ranger    = NewShip(931);
            var johnston  = NewShip(562);
            var hayashimo = NewShip(409);
            var shinnyou  = NewShip(534);
            var ooyodo    = NewShip(183);
            var akashi    = NewShip(182);
            var amagiri   = NewShip(479);
            var sagiri    = NewShip(480);
            var mizuho    = NewShip(451);
            var commandantTeste = NewShip(491);

            var heavycruiser    = NewShip(59);
            var hornet   = NewShip(603);
            var fletcher = NewShip(596);
            var akishimo = NewShip(625);
            var unnyou   = NewShip(884);
            var akebono2 = NewShip(665);
            var ushio2   = NewShip(407);
            var kamoi    = NewShip(499);
            var nisshin  = NewShip(581);

            var gloire   = NewShip(965);
            var yuubari  = NewShip(115);
            var asashimo = NewShip(425);
            var kazagumo = NewShip(453);

            battleInfo.InjectFleet(heavycruiser, heavycruiser);
            InjectMapNext(74, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "重巡は2022年まで");

            battleInfo.InjectFleet(mizuho, commandantTeste);
            InjectMapNext(72, 5, 15);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "7-2-2は2023年");

            InjectBattleResult("A");
            InjectMapNext(73, 5, 18);
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "7-3-2は2023年");

            InjectMapNext(75, 5, 24);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "7-5-3は2023年");

            battleInfo.InjectFleet(ranger);
            InjectMapNext(74, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "指定艦が不足");

            battleInfo.InjectFleet(heavycruiser, ranger, johnston);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "旗艦間違い");

            battleInfo.InjectFleet(hornet, fletcher);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(akishimo, unnyou);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(akebono2, ushio2);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(kamoi, nisshin);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "艦違い");

            battleInfo.InjectFleet(ranger, johnston);
            InjectMapNext(74, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝は不可");

            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "7-4は2024年");

            battleInfo.InjectFleet(ooyodo, akashi);
            InjectMapNext(22, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "明石は2024年");

            battleInfo.InjectFleet(amagiri, sagiri);
            InjectMapNext(74, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "天霧狭霧は2024年");

            battleInfo.InjectFleet(mizuho, commandantTeste);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "瑞穂Cテストは2024年");


            battleInfo.InjectFleet(gloire);
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "指定艦不足");

            battleInfo.InjectFleet(gloire, commandantTeste, ranger);
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "指定艦が一番二番にいない");

            battleInfo.InjectFleet(gloire, ranger);
            InjectMapNext(14, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "B勝は不可");

            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0));

            battleInfo.InjectFleet(hayashimo, shinnyou);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0));

            battleInfo.InjectFleet(johnston, ooyodo);
            InjectMapNext(22, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));

            battleInfo.InjectFleet(yuubari, ooyodo);
            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(2, 1, 1));

            battleInfo.InjectFleet(asashimo, kazagumo);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(2, 2, 1));
        }

        /// <summary>
        /// 843: 【節分任務:柊】節分拡張作戦二〇二五 五穀豊穣！
        /// </summary>
        [TestMethod]
        public void BattleResult_843()
        {
            var count = InjectQuest(843);
            var naka               = NewShip(56);
            var tama               = NewShip(100);
            var agano              = NewShip(137);
            var ooyodo             = NewShip(183);
            var heavycruiser       = NewShip(59);
            var battlecruiser      = NewShip(78);
            var battleship         = NewShip(26);
            var aviationbattleship = NewShip(286);
            var lightcarrier       = NewShip(89);
            var aircraftcarrier    = NewShip(83);
            var armoredcarrier     = NewShip(153);

            var mogami2t = NewShip(506);
            var mikuma2  = NewShip(502);
            var mikuma2t = NewShip(507);
            var suzuya   = NewShip(124);
            var suzuya2k = NewShip(508);
            var kumano   = NewShip(125);
            var kumano2k = NewShip(509);

            battleInfo.InjectFleet(mogami2t, mikuma2, lightcarrier);
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "6-4は2022年まで");

            InjectMapNext(25, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "2-5は2023年");

            InjectMapNext(52, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "5-2は2022年まで");

            battleInfo.InjectFleet(lightcarrier, aircraftcarrier, naka);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "川内型は2022年まで");

            battleInfo.InjectFleet(aviationbattleship, aircraftcarrier, ooyodo);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "大淀型は2022年まで");

            InjectMapNext(45, 5);
            battleInfo.InjectFleet(lightcarrier, aircraftcarrier, agano);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "阿賀野型は2023年");

            battleInfo.InjectFleet(lightcarrier, aircraftcarrier, tama);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "球磨型は2023年");

            battleInfo.InjectFleet(battlecruiser, lightcarrier, ooyodo);
            InjectMapNext(25, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "戦艦空母は2023年まで");

            battleInfo.InjectFleet(lightcarrier, mogami2t, mikuma2);
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝不可");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "最上型は2024年");

            battleInfo.InjectFleet(lightcarrier, mogami2t, mikuma2t);
            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "水上機母艦の三隈改二特でも最上型判定、最上型は2024年");

            battleInfo.InjectFleet(suzuya, kumano2k);
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "鈴谷航改二熊野航改二は軽空指定と兼任不可、最上型は2024年");

            battleInfo.InjectFleet(lightcarrier, suzuya, kumano2k);
            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "最上型は2024年");

            battleInfo.InjectFleet(suzuya2k, kumano);
            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "鈴谷航改二熊野航改二は軽空指定と兼任不可、最上型は2024年");

            battleInfo.InjectFleet(lightcarrier, suzuya2k, kumano);
            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "最上型は2024年");

            battleInfo.InjectFleet(suzuya2k, mikuma2t, mogami2t);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "他の最上型がいれば鈴谷航改二熊野航改二でも軽空判定、最上型は2024年");

            battleInfo.InjectFleet(suzuya2k, kumano2k, mogami2t);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "他の最上型がいれば鈴谷航改二熊野航改二でも軽空判定、最上型は2024年");


            battleInfo.InjectFleet(lightcarrier, heavycruiser);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "指定艦不足");

            battleInfo.InjectFleet(lightcarrier, mogami2t, mikuma2);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "航巡は不可");

            battleInfo.InjectFleet(lightcarrier, heavycruiser, heavycruiser);
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝不可");

            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0));

            battleInfo.InjectFleet(heavycruiser, lightcarrier, heavycruiser);
            InjectMapNext(41, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0));

            battleInfo.InjectFleet(heavycruiser, heavycruiser, lightcarrier);
            InjectMapNext(51, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            InjectMapNext(75, 5, 24);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 822: 沖ノ島海域迎撃戦
        /// 854: 戦果拡張任務！「Z作戦」前段作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_822_854()
        {
            InjectQuestList(822, 854);
            var c822 = questInfo.Quests[0].Count;
            var c854 = questInfo.Quests[1].Count;

            InjectMapNext(24, 4);
            InjectBattleResult("S");

            PAssert.That(() => c854.NowArray.ParamsEqual(0, 0, 0, 0));
            PAssert.That(() => c822.Now == 0);

            InjectMapNext(24, 5);
            InjectBattleResult("A");
            InjectMapNext(61, 5);
            InjectBattleResult("A");
            InjectMapNext(63, 5);
            InjectBattleResult("A");
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => c854.NowArray.ParamsEqual(1, 1, 1, 1));
            PAssert.That(() => c822.Now == 0);

            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => c854.NowArray.ParamsEqual(2, 1, 1, 1));
            PAssert.That(() => c822.Now == 1);

            battleInfo.InjectDeck(2);
            InjectMapNext(64, 5);
            InjectBattleResult("S");
            PAssert.That(() => c854.NowArray.ParamsEqual(2, 1, 1, 1));
        }

        /// <summary>
        /// 845: 発令！「西方海域作戦」
        /// </summary>
        [TestMethod]
        public void BattleResult_845()
        {
            var count = InjectQuest(845);

            InjectMapNext(41, 4);
            InjectBattleResult("S");
            InjectMapNext(41, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0));

            InjectMapNext(41, 5);
            InjectBattleResult("S");
            InjectMapNext(42, 5);
            InjectBattleResult("S");
            InjectMapNext(43, 5);
            InjectBattleResult("S");
            InjectMapNext(44, 5);
            InjectBattleResult("S");
            InjectMapNext(45, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1));
        }

        /// <summary>
        /// 861: 強行輸送艦隊、抜錨！
        /// </summary>
        [TestMethod]
        public void MapNext_861()
        {
            var count = InjectQuest(861);
            var destroyer          = NewShip(1);
            var aviationbattleship = NewShip(286);
            var fleetoiler         = NewShip(460);

            battleInfo.InjectFleet(aviationbattleship, fleetoiler, destroyer, destroyer, destroyer, destroyer);
            InjectMapNext(16, 4);
            PAssert.That(() => count.Now == 0);

            InjectMapNext(16, 8);
            PAssert.That(() => count.Now == 1);

            fleetoiler.NowHp = 0;
            InjectMapNext(16, 8);
            PAssert.That(() => count.Now == 1, "轟沈あり");

            fleetoiler.NowHp = 1;
            battleInfo.InjectFleet(aviationbattleship, fleetoiler, aviationbattleship, destroyer, destroyer, destroyer);
            InjectMapNext(16, 8);
            PAssert.That(() => count.Now == 2, "補給・航戦が3隻");
        }

        /// <summary>
        /// 862: 前線の航空偵察を実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_862()
        {
            var count = InjectQuest(862);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var seaplanecarrier = NewShip(102);

            battleInfo.InjectFleet(destroyer, lightcruiser, lightcruiser, destroyer, destroyer, seaplanecarrier);
            InjectMapNext(63, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(63, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("A");
            PAssert.That(() => count.Now == 1);

            lightcruiser.NowHp = 0;
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 1, "轟沈あり");

            lightcruiser.NowHp = 1;
            battleInfo.InjectFleet(destroyer, lightcruiser, lightcruiser, lightcruiser, seaplanecarrier, seaplanecarrier);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 2, "軽巡3隻水母2隻");
        }

        /// <summary>
        /// 872: 戦果拡張任務！「Z作戦」後段作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_872()
        {
            var count = InjectQuest(872);

            InjectMapNext(55, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0));

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0));

            InjectMapNext(55, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 1, 0, 0));

            InjectMapNext(62, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 1, 1, 0));

            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 1, 1, 1));

            InjectMapNext(72, 5, 15);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "7-2-2");

            battleInfo.InjectDeck(3);
            InjectMapNext(65, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 873: 北方海域警備を実施せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_873()
        {
            var count = InjectQuest(873);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);

            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectMapNext(31, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0));

            InjectMapNext(31, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0));
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0));

            battleInfo.InjectFleet(destroyer, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "軽巡なし");

            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectMapNext(32, 5);
            InjectBattleResult("A");
            InjectMapNext(33, 5);
            InjectBattleResult("A");
            PAssert.That(() => questInfo.Quests[0].Count.NowArray.ParamsEqual(1, 1, 1));
        }

        /// <summary>
        /// 875: 精鋭「三一駆」、鉄底海域に突入せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_875()
        {
            var count = InjectQuest(875);
            var naganami1    = NewShip(304);
            var naganami2    = NewShip(543);
            var asashimo     = NewShip(425);
            var asashimo1    = NewShip(344);
            var asashimo2    = NewShip(578);
            var takanami     = NewShip(424);
            var takanami1    = NewShip(345);
            var takanami2    = NewShip(649);
            var okinami      = NewShip(452);
            var okinami1     = NewShip(359);
            var okinami2     = NewShip(569);

            battleInfo.InjectFleet(naganami2, asashimo1);
            InjectMapNext(54, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 0);

            InjectMapNext(54, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.Now == 0);

            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1);

            asashimo1.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "朝霜改轟沈");

            asashimo1.NowHp = 1;
            battleInfo.InjectFleet(takanami1, asashimo1);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "長波改二なし");

            battleInfo.InjectFleet(naganami1, asashimo1);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "長波改は不可");

            battleInfo.InjectFleet(naganami2, takanami);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 1, "高波未改は不可");

            battleInfo.InjectFleet(naganami2, takanami1);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 2, "高波改");

            battleInfo.InjectFleet(naganami2, takanami2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 3, "高波改二");

            battleInfo.InjectFleet(naganami2, okinami);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 3, "沖波改未改は不可");

            battleInfo.InjectFleet(naganami2, okinami1);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 4, "沖波改");

            battleInfo.InjectFleet(naganami2, okinami2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 5, "沖波改二");

            battleInfo.InjectFleet(naganami2, asashimo);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 5, "朝霜未改は不可");

            battleInfo.InjectFleet(naganami2, asashimo2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 6, "朝霜改二");

            battleInfo.InjectFleet(naganami2, asashimo2, okinami2, takanami2);
            InjectBattleResult("S");
            PAssert.That(() => count.Now == 7);
        }

        /// <summary>
        /// 888: 新編成「三川艦隊」、鉄底海峡に突入せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_888()
        {
            var count = InjectQuest(888);
            var choukai2  = NewShip(427);
            var furutaka2 = NewShip(416);
            var kako2     = NewShip(417);
            var aoba1     = NewShip(264);
            var kinugasa2 = NewShip(142);
            var tenryuu2  = NewShip(477);
            var yuubari2t = NewShip(623);
            var myoukou2  = NewShip(319);
            var ayanami2  = NewShip(195);
            var yuudachi2 = NewShip(144);

            battleInfo.InjectFleet(choukai2, aoba1, kinugasa2, kako2, yuudachi2, ayanami2);
            InjectMapNext(51, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0));

            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0));

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0));

            choukai2.NowHp = 0;
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "轟沈あり");

            battleInfo.InjectFleet(myoukou2, aoba1, kinugasa2, kako2, yuudachi2, ayanami2);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "三川艦隊3隻");

            battleInfo.InjectFleet(yuubari2t, aoba1, kinugasa2, kako2, yuudachi2, ayanami2);
            InjectMapNext(53, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0));

            battleInfo.InjectFleet(tenryuu2, furutaka2, kinugasa2, kako2, yuudachi2, ayanami2);
            InjectMapNext(54, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1));
        }

        /// <summary>
        /// 893: 泊地周辺海域の安全確保を徹底せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_893()
        {
            var count = InjectQuest(893);

            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0));

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "1-5");

            InjectMapNext(71, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "7-1");

            InjectMapNext(72, 5, 7);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "7-2-1");

            InjectMapNext(72, 5, 15);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "7-2-2");
        }

        /// <summary>
        /// 894: 空母戦力の投入による兵站線戦闘哨戒
        /// </summary>
        [TestMethod]
        public void BattleResult_894()
        {
            var count = InjectQuest(894);
            var destroyer    = NewShip(1);
            var lightcarrier = NewShip(89);

            battleInfo.InjectFleet(destroyer, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "空母なしはカウントしない");

            battleInfo.InjectFleet(lightcarrier, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "A勝利はカウントしない");

            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "1-3");

            InjectMapNext(14, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "1-4");

            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0, 0), "1-4");

            InjectMapNext(21, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0), "2-1");

            InjectMapNext(22, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 0), "2-2");

            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "2-3");
        }
    }
}
