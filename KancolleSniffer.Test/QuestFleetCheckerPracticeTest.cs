// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestFleetCheckerPracticeTest : QuestCounterTester
    {
        [TestInitialize]
        public void Initialize()
        {
            battleInfo.InjectDeck(1);
        }

        /// <summary>
        /// 318: 給糧艦「伊良湖」の支援
        /// </summary>
        [TestMethod]
        public void PracticeResult_318()
        {
            var count = InjectQuest(318);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);

            battleInfo.InjectFleet(destroyer, lightcruiser);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "軽巡1隻");

            battleInfo.InjectFleet(lightcruiser, lightcruiser);
            questCounter.StartPractice("api%5Fdeck%5Fid=2");
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "第2艦隊");

            questCounter.StartPractice("api%5Fdeck%5Fid=1"); // 第一艦隊
            InjectPracticeResult("C");
            PAssert.That(() => count.Now == 0, "敗北");

            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 1);

            count.Now = 2;
            InjectQuestList(318);
            PAssert.That(() => count.Now == 2, "進捗調節しない");
        }

        /// <summary>
        /// 329: 【節分任務:枡】節分演習！二〇二五
        /// </summary>
        [TestMethod]
        public void PracticeResult_329()
        {
            var count = InjectQuest(329);
            var submarine       = NewShip(126);
            var submarinecarier = NewShip(155);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var mogami2t        = NewShip(506);
            var mikuma2         = NewShip(502);
            var mikuma2t        = NewShip(507);
            var lightcarrier    = NewShip(89);
            var taigei          = NewShip(184);

            battleInfo.InjectFleet(mogami2t, mikuma2, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "駆逐が不足");

            battleInfo.InjectFleet(mogami2t, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "重巡級が不足");

            battleInfo.InjectFleet(torpedocruiser, lightcruiser, destroyer, destroyer, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "雷巡は対象外");

            battleInfo.InjectFleet(trainingcruiser, lightcruiser, destroyer, destroyer, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "練巡は対象外");

            battleInfo.InjectFleet(lightcruiser, lightcruiser, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "軽巡は対象外");

            battleInfo.InjectFleet(mogami2t, mikuma2t, destroyer, destroyer, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "三隈改二特は水上機母艦なので対象外");

            battleInfo.InjectFleet(mogami2t, mikuma2, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "重巡級2駆逐海防3は2024年まで");

            battleInfo.InjectFleet(mogami2t, mikuma2, escort, escort, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "重巡級2駆逐海防3は2024年まで");

            battleInfo.InjectFleet(mogami2t, mikuma2, escort, destroyer, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "重巡級2駆逐海防3は2024年まで");


            battleInfo.InjectFleet(taigei, submarine);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "潜水が不足");

            battleInfo.InjectFleet(taigei, submarinecarier);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "潜空が不足");

            battleInfo.InjectFleet(submarine, taigei, submarinecarier);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "旗艦違い");

            battleInfo.InjectFleet(lightcarrier, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "海防が不足");

            battleInfo.InjectFleet(lightcarrier, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "駆逐が不足");

            battleInfo.InjectFleet(escort, lightcarrier, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "旗艦違い");

            battleInfo.InjectFleet(taigei, submarine, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "潜水海防混在は不可");

            battleInfo.InjectFleet(lightcarrier, submarinecarier, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "潜水海防混在は不可");

            battleInfo.InjectFleet(lightcarrier, destroyer, destroyer, destroyer, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "駆逐海防混在は不可");

            battleInfo.InjectFleet(taigei, submarine, submarinecarier);
            InjectPracticeResult("C");
            PAssert.That(() => count.Now == 0, "C敗北は不可");

            battleInfo.InjectFleet(taigei, submarine, submarine);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(taigei, submarinecarier, submarinecarier);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(lightcarrier, submarine, submarinecarier);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(taigei, escort, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 4);

            battleInfo.InjectFleet(lightcarrier, escort, escort);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 5);

            battleInfo.InjectFleet(taigei, destroyer, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 6);

            battleInfo.InjectFleet(lightcarrier, destroyer, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 7);
        }

        /// <summary>
        /// 330: 空母機動部隊、演習始め！
        /// </summary>
        [TestMethod]
        public void PracticeResult_330()
        {
            var count = InjectQuest(330);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var lightcarrier    = NewShip(89);
            var aircraftcarrier = NewShip(83);
            var armoredcarrier  = NewShip(153);

            battleInfo.InjectFleet(armoredcarrier, lightcarrier, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 1, "装甲空母、軽空母");

            battleInfo.InjectFleet(aircraftcarrier, lightcarrier, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 2, "正規空母、軽空母");

            count.Now = 0;
            InjectPracticeResult("C");
            PAssert.That(() => count.Now == 0, "敗北");

            battleInfo.InjectFleet(destroyer, lightcarrier, aircraftcarrier, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "旗艦空母以外");

            battleInfo.InjectFleet(aircraftcarrier, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "空母一隻");

            battleInfo.InjectFleet(aircraftcarrier, lightcarrier, lightcruiser, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "駆逐一隻");
        }

        /// <summary>
        /// 337: 「十八駆」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_337()
        {
            var count = InjectQuest(337);
            var arare    = NewShip(48);
            var kagerou  = NewShip(17);
            var shiranui = NewShip(18);
            var kuroshio = NewShip(19);
            var suzukaze = NewShip(47);
            var kasumi   = NewShip(49);
            var kasumi2  = NewShip(464);

            battleInfo.InjectFleet(kasumi, arare, kagerou, shiranui, kuroshio);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "A");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(suzukaze, arare, kagerou, shiranui, kuroshio);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1, "霰→涼風");

            battleInfo.InjectFleet(suzukaze, arare, kagerou, shiranui, kasumi2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2, "黒潮→霞改二");
        }

        /// <summary>
        /// 339: 「十九駆」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_339()
        {
            var count = InjectQuest(339);
            var isonami   = NewShip(12);
            var uranami   = NewShip(486);
            var ayanami   = NewShip(13);
            var shikinami = NewShip(14);
            var hatsuyuki = NewShip(32);
            var miyuki    = NewShip(11);

            battleInfo.InjectFleet(isonami, uranami, ayanami, shikinami, hatsuyuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "A");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(miyuki, uranami, ayanami, shikinami, hatsuyuki);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1, "磯波→深雪");
        }

        /// <summary>
        /// 342: 小艦艇群演習強化任務
        /// </summary>
        [TestMethod]
        public void PracticeResult_342()
        {
            var count = InjectQuest(342);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var heavycruiser    = NewShip(59);
            var lightcarrier    = NewShip(89);

            battleInfo.InjectFleet(escort, escort, destroyer, heavycruiser);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0);

            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(escort, escort, lightcruiser, heavycruiser);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "駆逐海防が不足");

            battleInfo.InjectFleet(escort, escort, destroyer, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(escort, escort, destroyer, lightcruiser);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(escort, escort, destroyer, torpedocruiser);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(escort, escort, destroyer, trainingcruiser);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4);

            battleInfo.InjectFleet(escort, escort, destroyer, lightcarrier);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "軽空は艦違い");
        }

        /// <summary>
        /// 345: 演習ティータイム！
        /// </summary>
        [TestMethod]
        public void PracticeResult_345()
        {
            var count = InjectQuest(345);
            var Warspite   = NewShip(439);
            var kongou     = NewShip(78);
            var hiei       = NewShip(86);
            var Nelson     = NewShip(571);
            var Rodney     = NewShip(572);
            var ArkRoyal   = NewShip(515);
            var Victorious = NewShip(885);
            var Perth      = NewShip(613);
            var Sheffield  = NewShip(514);
            var Jervis     = NewShip(519);
            var Janus      = NewShip(520);
            var Javelin    = NewShip(901);
            var Richelieu  = NewShip(492);

            battleInfo.InjectFleet(Warspite, kongou, ArkRoyal, Perth);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "元英国艦は不可");

            battleInfo.InjectFleet(Janus, Richelieu, ArkRoyal, Jervis);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "他欧州艦は不可");

            battleInfo.InjectFleet(Janus, hiei, ArkRoyal, Jervis);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "比叡は不可");

            battleInfo.InjectFleet(Warspite, kongou, ArkRoyal, Jervis);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(Janus, kongou, ArkRoyal, Jervis);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(Janus, kongou, ArkRoyal, Nelson);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(Javelin, kongou, ArkRoyal, Nelson);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4);

            battleInfo.InjectFleet(Javelin, kongou, ArkRoyal, Rodney);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "Rodneyは未対応");

            battleInfo.InjectFleet(Javelin, kongou, Victorious, Nelson);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "Victoriousは未対応");

            battleInfo.InjectFleet(Javelin, Sheffield, ArkRoyal, Nelson);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "Sheffieldは未対応");
        }

        /// <summary>
        /// 346: 最精鋭！主力オブ主力、演習開始！
        /// </summary>
        [TestMethod]
        public void PracticeResult_346()
        {
            var count = InjectQuest(346);
            var yuugumo2  = NewShip(542);
            var makigumo2 = NewShip(563);
            var kazagumo2 = NewShip(564);
            var akigumo1  = NewShip(301);
            var akigumo2  = NewShip(648);
            var ayanami2  = NewShip(195);

            battleInfo.InjectFleet(yuugumo2, makigumo2, kazagumo2, akigumo1);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0);

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(yuugumo2, makigumo2, kazagumo2, akigumo2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(ayanami2, makigumo2, kazagumo2, akigumo2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);
        }

        /// <summary>
        /// 348: 「精鋭軽巡」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_348()
        {
            var count = InjectQuest(348);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);

            battleInfo.InjectFleet(lightcruiser, lightcruiser, trainingcruiser, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0);

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(torpedocruiser, lightcruiser, trainingcruiser, destroyer, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(trainingcruiser, lightcruiser, trainingcruiser, destroyer, escort);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(trainingcruiser, lightcruiser, trainingcruiser, destroyer, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);
        }

        /// <summary>
        /// 350: 精鋭「第七駆逐隊」演習開始！
        /// </summary>
        [TestMethod]
        public void PracticeResult_350()
        {
            var count = InjectQuest(350);
            var oboro    = NewShip(93);
            var akebono  = NewShip(15);
            var sazanami = NewShip(94);
            var ushio    = NewShip(16);
            var akatsuki = NewShip(34);
            var asashio  = NewShip(95);

            battleInfo.InjectFleet(oboro, akebono, sazanami, ushio);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(oboro, akebono, sazanami, akatsuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1, "潮を配置と一字間違いで暁");

            battleInfo.InjectFleet(oboro, akebono, sazanami, asashio);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1, "「潮」が艦名に含まれる判定のチェック");
        }

        /// <summary>
        /// 353: 「巡洋艦戦隊」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_353()
        {
            var count = InjectQuest(353);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);
            var heavycruiser    = NewShip(59);
            var aviationcruiser = NewShip(73);

            battleInfo.InjectFleet(destroyer, heavycruiser, aviationcruiser, heavycruiser, aviationcruiser, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(heavycruiser, heavycruiser, aviationcruiser, heavycruiser, aviationcruiser, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(heavycruiser, destroyer, escort, heavycruiser, aviationcruiser, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(heavycruiser, destroyer, aviationcruiser, heavycruiser, aviationcruiser, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(aviationcruiser, destroyer, aviationcruiser, heavycruiser, aviationcruiser, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(heavycruiser, destroyer, aviationcruiser, heavycruiser, aviationcruiser, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);
        }

        /// <summary>
        /// 354: 「改装特務空母」任務部隊演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_354()
        {
            var count = InjectQuest(354);
            var gambierBay  = NewShip(396);
            var gambierBay2 = NewShip(707);
            var johnston    = NewShip(562);
            var fletcher    = NewShip(596);
            var samuel      = NewShip(561);
            var jervis      = NewShip(519);

            battleInfo.InjectFleet(gambierBay, johnston, samuel);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(gambierBay2, jervis, samuel);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(gambierBay2, johnston, jervis);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(fletcher, johnston, gambierBay2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(gambierBay2, johnston, samuel);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0);

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(gambierBay2, johnston, fletcher);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(gambierBay2, samuel, fletcher);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 3);
        }

        /// <summary>
        /// 355: 精鋭「第十五駆逐隊」第一小隊演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_355()
        {
            var count = InjectQuest(355);
            var kuroshio  = NewShip(227);
            var kuroshio2 = NewShip(568);
            var oyashio   = NewShip(362);
            var oyashio2  = NewShip(670);
            var kagero2   = NewShip(566);
            var shiranui2 = NewShip(567);

            battleInfo.InjectFleet(oyashio2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(kuroshio2, oyashio, kagero2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(kuroshio, oyashio2, shiranui2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(kagero2, oyashio2, kuroshio2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(kuroshio2, shiranui2, oyashio2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0);

            battleInfo.InjectFleet(oyashio2, kuroshio2, shiranui2);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0);

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(kuroshio2, oyashio2, kagero2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);
        }

        /// <summary>
        /// 356: 精鋭「第十九駆逐隊」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_356()
        {
            var count = InjectQuest(356);
            var isonami    = NewShip(206);
            var isonami2   = NewShip(666);
            var uranami2   = NewShip(647);
            var ayanami2   = NewShip(195);
            var shikinami2 = NewShip(627);
            var fubuki2    = NewShip(201);

            battleInfo.InjectFleet(isonami2, uranami2, ayanami2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦が不足");

            battleInfo.InjectFleet(isonami, uranami2, ayanami2, shikinami2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦が改二になってない");

            battleInfo.InjectFleet(isonami2, fubuki2, ayanami2, shikinami2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "改二の指定艦が間違ってる");

            battleInfo.InjectFleet(isonami2, uranami2, ayanami2, shikinami2);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "S勝が取れなかった");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(ayanami2, isonami2, uranami2, shikinami2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2, "旗艦指定はなし");

            battleInfo.InjectFleet(fubuki2, uranami2, ayanami2, isonami2, shikinami2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 3, "旗艦指定はなし");
        }

        /// <summary>
        /// 357: 「大和型戦艦」第一戦隊演習、始め！
        /// </summary>
        [TestMethod]
        public void PracticeResult_357()
        {
            var count = InjectQuest(357);
            var yamato  = NewShip(131);
            var musashi = NewShip(143);

            var escort         = NewShip(517);
            var destroyer      = NewShip(1);
            var lightcruiser   = NewShip(51);
            var torpedocruiser = NewShip(58);

            battleInfo.InjectFleet(yamato, musashi, lightcruiser, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "駆逐が不足");

            battleInfo.InjectFleet(yamato, musashi, lightcruiser, destroyer, escort);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "海防は無関係");

            battleInfo.InjectFleet(yamato, musashi, torpedocruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "雷巡は無関係");

            battleInfo.InjectFleet(musashi, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "大和不在");

            battleInfo.InjectFleet(yamato, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "武蔵不在");

            battleInfo.InjectFleet(yamato, musashi, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "S勝が取れなかった");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(escort, yamato, musashi, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2, "旗艦指定はなし");
        }

        /// <summary>
        /// 362: 特型初代「第十一駆逐隊」演習スペシャル！
        /// </summary>
        [TestMethod]
        public void PracticeResult_362()
        {
            var count = InjectQuest(362);
            var fubuki    = NewShip(9);
            var shirayuki = NewShip(10);
            var hatsuyuki = NewShip(32);
            var miyuki    = NewShip(11);
            var isonami   = NewShip(12);

            battleInfo.InjectFleet(fubuki, shirayuki, hatsuyuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "指定艦が不足");

            battleInfo.InjectFleet(miyuki, shirayuki, hatsuyuki, fubuki);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "A勝が取れなかった");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(shirayuki, miyuki, fubuki, isonami);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1, "指定艦間違い");
        }

        /// <summary>
        /// 363: 【艦隊11周年記念任務】記念艦隊演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_363()
        {
            var count = InjectQuest(363);
            var yamato       = NewShip(131);
            var shoukaku     = NewShip(110);
            var fubuki       = NewShip(9);
            var oboro        = NewShip(93);
            var tuscaloosa   = NewShip(923);
            var houston      = NewShip(595);
            var northampton  = NewShip(655);
            var yamashiomaru = NewShip(900);
            var kumanomaru   = NewShip(943);

            var musashi      = NewShip(143);
            var zuikaku      = NewShip(111);
            var miyuki       = NewShip(11);
            var sazanami     = NewShip(94);
            var helena       = NewShip(615);
            var akitsumaru   = NewShip(161);
            var shinshuumaru = NewShip(621);

            battleInfo.InjectFleet(tuscaloosa, houston, northampton);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "A勝が取れなかった");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(yamato, shoukaku, fubuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(oboro, yamashiomaru, kumanomaru);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(helena, houston, northampton);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");

            battleInfo.InjectFleet(musashi, shoukaku, fubuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");

            battleInfo.InjectFleet(yamato, zuikaku, fubuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");

            battleInfo.InjectFleet(yamato, shoukaku, miyuki);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");

            battleInfo.InjectFleet(sazanami, yamashiomaru, kumanomaru);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");

            battleInfo.InjectFleet(oboro, akitsumaru, kumanomaru);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");

            battleInfo.InjectFleet(oboro, yamashiomaru, shinshuumaru);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3, "指定艦間違い");
        }

        /// <summary>
        /// 367: 【梅雨限定任務】海上護衛隊、雨中演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_367()
        {
            var count = InjectQuest(367);
            var lightcruiser    = NewShip(51);
            var torpedocruiser  = NewShip(58);
            var trainingcruiser = NewShip(154);
            var escort          = NewShip(517);
            var destroyer       = NewShip(1);

            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(trainingcruiser, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);


            battleInfo.InjectFleet(lightcruiser, destroyer, destroyer, destroyer, destroyer, escort);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 3, "旗艦軽巡錬巡駆逐5は2023年まで");

            battleInfo.InjectFleet(torpedocruiser, destroyer, destroyer, destroyer, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 4, "旗艦指定は2023年まで");

            battleInfo.InjectFleet(destroyer, lightcruiser, destroyer, destroyer, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 5, "旗艦指定は2023年まで");

            battleInfo.InjectFleet(destroyer, destroyer, destroyer, escort);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 5, "指定艦不足");

            battleInfo.InjectFleet(destroyer, destroyer, destroyer, escort, escort);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 6, "海防2");

            battleInfo.InjectFleet(destroyer, destroyer, destroyer, destroyer, escort);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 7, "駆逐4");
        }

        /// <summary>
        /// 368: 「十六駆」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_368()
        {
            var count = InjectQuest(368);
            var hatsukaze   = NewShip(190);
            var yukikaze    = NewShip(20);
            var amatsukaze  = NewShip(181);
            var tokitsukaze = NewShip(186);
            var shimakaze   = NewShip(50);
            var maikaze     = NewShip(122);

            battleInfo.InjectFleet(yukikaze, amatsukaze);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "A勝は不可");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(shimakaze, maikaze, hatsukaze, tokitsukaze);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(amatsukaze, tokitsukaze);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(hatsukaze, yukikaze);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 4);


            battleInfo.InjectFleet(yukikaze, shimakaze);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 4, "島風は艦違い");

            battleInfo.InjectFleet(hatsukaze, maikaze);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 4, "舞風は艦違い");
        }

        /// <summary>
        /// 370: 【Xmas限定】聖夜の海上護衛隊演習
        /// </summary>
        [TestMethod]
        public void PracticeResult_370()
        {
            var count = InjectQuest(370);
            var kashima   = NewShip(465);
            var etorofu   = NewShip(524);
            var matsuwa   = NewShip(525);
            var sado      = NewShip(531);
            var fukae     = NewShip(565);
            var ukuru     = NewShip(921);
            var kishinami = NewShip(527);
            var hayanami  = NewShip(528);
            var tanikaze  = NewShip(169);
            var kawakaze  = NewShip(459);
            var yamagumo  = NewShip(414);
            var asagumo   = NewShip(413);
            var yamashiomaru = NewShip(900);
            var no4de     = NewShip(637);
            var no22de    = NewShip(898);
            var no30de    = NewShip(638);

            var katori     = NewShip(154);
            var tsushima   = NewShip(540);
            var hirato     = NewShip(570);
            var inagi      = NewShip(922);
            var fujinami   = NewShip(485);
            var hamanami   = NewShip(484);
            var urakaze    = NewShip(168);
            var yamakaze   = NewShip(457);
            var natsugumo  = NewShip(582);
            var minegumo   = NewShip(583);
            var kumanomaru = NewShip(943);

            battleInfo.InjectFleet(kashima, etorofu, matsuwa);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "3隻は不可");

            battleInfo.InjectFleet(kashima, etorofu, matsuwa, sado);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(fukae, ukuru, kishinami, hayanami);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(tanikaze, kawakaze, yamagumo, asagumo);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(yamashiomaru, no4de, no22de, no30de);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4);


            battleInfo.InjectFleet(katori, etorofu, matsuwa, sado);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "香取は艦違い");

            battleInfo.InjectFleet(kashima, tsushima, matsuwa, sado);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "対馬は艦違い");

            battleInfo.InjectFleet(kashima, hirato, matsuwa, sado);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "平戸は艦違い");

            battleInfo.InjectFleet(fukae, inagi, kishinami, hayanami);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "稲木は艦違い");

            battleInfo.InjectFleet(fukae, ukuru, fujinami, hayanami);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "藤波は艦違い");

            battleInfo.InjectFleet(fukae, ukuru, hamanami, hayanami);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "浜波は艦違い");

            battleInfo.InjectFleet(urakaze, kawakaze, yamagumo, asagumo);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "浦風は艦違い");

            battleInfo.InjectFleet(tanikaze, yamakaze, yamagumo, asagumo);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "山風は艦違い");

            battleInfo.InjectFleet(tanikaze, kawakaze, natsugumo, asagumo);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "夏雲は艦違い");

            battleInfo.InjectFleet(tanikaze, kawakaze, minegumo, asagumo);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "峯雲は艦違い");

            battleInfo.InjectFleet(kumanomaru, no4de, no22de, no30de);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "熊野丸は艦違い");
        }

        /// <summary>
        /// 371: 春です！「春雨」、演習しますっ！
        /// </summary>
        [TestMethod]
        public void PracticeResult_371()
        {
            var count = InjectQuest(371);
            var harusame   = NewShip(405);
            var murasame   = NewShip(44);
            var yuudachi   = NewShip(45);
            var samidare   = NewShip(46);
            var shiratsuyu = NewShip(42);
            var shigure    = NewShip(43);
            var suzukaze   = NewShip(47);

            battleInfo.InjectFleet(harusame, murasame, yuudachi, samidare);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(harusame, shiratsuyu, shigure, samidare);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);


            battleInfo.InjectFleet(shiratsuyu, shigure, samidare, harusame);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2, "旗艦違い");

            battleInfo.InjectFleet(harusame, murasame, yuudachi, suzukaze);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2, "涼風は艦違い");
        }

        /// <summary>
        /// 372: 水上艦「艦隊防空演習」を実施せよ！
        /// </summary>
        [TestMethod]
        public void PracticeResult_372()
        {
            var count = InjectQuest(372);
            var akizuki    = NewShip(421);
            var hatsuzuki  = NewShip(423);
            var shimakaze  = NewShip(50);
            var amatsukaze = NewShip(181);
            var matsu      = NewShip(641);
            var ise        = NewShip(82);
            var hyuuga     = NewShip(88);
            var yamato2h   = NewShip(916);

            var shimushu = NewShip(517);
            var akagi    = NewShip(83);
            var houshou  = NewShip(89);
            var mogami   = NewShip(73);
            var nagato   = NewShip(80);

            battleInfo.InjectFleet(akizuki, shimakaze, amatsukaze, ise, hyuuga);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(hatsuzuki, shimakaze, amatsukaze, matsu, ise, hyuuga);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(akizuki, shimakaze, amatsukaze, ise, hyuuga, yamato2h);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(hatsuzuki, akizuki, shimakaze, ise, hyuuga);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4);


            battleInfo.InjectFleet(shimakaze, akizuki, amatsukaze, ise, hyuuga);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "旗艦違い");

            battleInfo.InjectFleet(akizuki, shimushu, amatsukaze, ise, hyuuga);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "駆逐が不足");

            battleInfo.InjectFleet(akizuki, shimakaze, amatsukaze, ise, akagi);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "空母は不可");

            battleInfo.InjectFleet(akizuki, shimakaze, amatsukaze, ise, houshou);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "軽空は不可");

            battleInfo.InjectFleet(akizuki, shimakaze, amatsukaze, ise, mogami);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "航巡は不可");

            battleInfo.InjectFleet(akizuki, shimakaze, amatsukaze, ise, nagato);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4, "戦艦は不可");
        }

        /// <summary>
        /// 373: 「フランス艦隊」演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_373()
        {
            var count = InjectQuest(373);
            var richelieu = NewShip(492);
            var jeanbart  = NewShip(935);
            var cteste    = NewShip(491);
            var gloire    = NewShip(965);
            var mogador   = NewShip(962);
            var javelin   = NewShip(901);

            battleInfo.InjectFleet(richelieu, jeanbart, cteste);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(cteste, gloire, mogador, javelin);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);


            battleInfo.InjectFleet(javelin, cteste, gloire, mogador);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2, "旗艦違い");

            battleInfo.InjectFleet(richelieu, jeanbart, javelin);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2, "フランス艦が不足");
        }

        /// <summary>
        /// 374: 【期間限定任務】「三十二駆」特別演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_374()
        {
            var count = InjectQuest(374);
            var tamanami  = NewShip(674);
            var suzunami  = NewShip(675);
            var fujinami  = NewShip(485);
            var hayanami  = NewShip(528);
            var hamanami  = NewShip(484);

            var makinami  = NewShip(671);
            var kishinami = NewShip(527);

            battleInfo.InjectFleet(tamanami, suzunami, fujinami);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "A勝は不可");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(fujinami, hayanami, hamanami);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);


            battleInfo.InjectFleet(hamanami, tamanami, makinami);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2, "巻波は艦違い");

            battleInfo.InjectFleet(suzunami, fujinami, kishinami);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2, "岸波は艦違い");
        }

        /// <summary>
        /// 375: 「第三戦隊」第二小隊、演習開始！
        /// </summary>
        [TestMethod]
        public void PracticeResult_375()
        {
            var count = InjectQuest(375);
            var hiei      = NewShip(86);
            var kirishima = NewShip(85);

            var escort         = NewShip(517);
            var destroyer      = NewShip(1);
            var lightcruiser   = NewShip(51);
            var torpedocruiser = NewShip(58);

            battleInfo.InjectFleet(hiei, kirishima, lightcruiser, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "駆逐が不足");

            battleInfo.InjectFleet(hiei, kirishima, lightcruiser, destroyer, escort);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "海防は無関係");

            battleInfo.InjectFleet(hiei, kirishima, torpedocruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "雷巡は無関係");

            battleInfo.InjectFleet(kirishima, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "比叡不在");

            battleInfo.InjectFleet(hiei, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "霧島不在");

            battleInfo.InjectFleet(hiei, kirishima, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "S勝が取れなかった");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(escort, hiei, kirishima, lightcruiser, destroyer, destroyer);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2, "旗艦指定はなし");
        }

        /// <summary>
        /// 377: 「第二駆逐隊(後期編成)」、練度向上！
        /// </summary>
        [TestMethod]
        public void PracticeResult_377()
        {
            var count = InjectQuest(377);
            var asashimo  = NewShip(425);
            var hayashimo = NewShip(409);
            var akishimo  = NewShip(625);
            var kiyoshimo = NewShip(410);

            battleInfo.InjectFleet(asashimo, hayashimo, kiyoshimo);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "旗艦違い");

            battleInfo.InjectFleet(hayashimo, kiyoshimo);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(akishimo, hayashimo, kiyoshimo);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "S勝が取れなかった");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(hayashimo, akishimo, asashimo);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);
        }

        /// <summary>
        /// 378: 新春限定！第六艦隊特別演習
        /// </summary>
        [TestMethod]
        public void PracticeResult_378()
        {
            var count = InjectQuest(378);
            var taigei  = NewShip(184);
            var jingei  = NewShip(634);
            var chougei = NewShip(635);
            var i400    = NewShip(493);
            var i401    = NewShip(155);
            var i201    = NewShip(881);
            var i203    = NewShip(882);
            var mizuho  = NewShip(451);

            battleInfo.InjectFleet(mizuho, taigei, i400);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "旗艦違い");

            battleInfo.InjectFleet(i401, taigei, i400);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "旗艦違い");

            battleInfo.InjectFleet(taigei, i400);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(taigei, jingei, chougei);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(i201, i203, i401);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(taigei, i400, i401);
            InjectPracticeResult("B");
            PAssert.That(() => count.Now == 0, "B勝は不可");

            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(taigei, jingei, i401);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(jingei, i201, i203);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(chougei, i401, i203);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 4);
        }

        /// <summary>
        /// 379: 【期間限定任務】「精鋭十一駆」特別演習！
        /// </summary>
        [TestMethod]
        public void PracticeResult_379()
        {
            var count = InjectQuest(379);
            var fubuki2    = NewShip(426);
            var shirayuki2 = NewShip(986);
            var miyuki2    = NewShip(959);
            var hatsuyuki1 = NewShip(203);

            var fubuki1    = NewShip(201);
            var shirayuki1 = NewShip(202);
            var miyuki1    = NewShip(204);
            var hatsuyuki  = NewShip(32);

            battleInfo.InjectFleet(fubuki1, shirayuki2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(shirayuki1, miyuki2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(miyuki1, hatsuyuki1);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(hatsuyuki, fubuki2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 0, "指定艦不足");

            battleInfo.InjectFleet(shirayuki2, fubuki2);
            InjectPracticeResult("A");
            PAssert.That(() => count.Now == 0, "A勝は不可");

            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 1);

            battleInfo.InjectFleet(hatsuyuki1, miyuki2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 2);

            battleInfo.InjectFleet(hatsuyuki1, shirayuki2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 3);

            battleInfo.InjectFleet(fubuki2, miyuki2);
            InjectPracticeResult("S");
            PAssert.That(() => count.Now == 4);
        }
    }
}
