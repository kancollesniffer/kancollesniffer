// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class MiscTextTest
    {
        /// <summary>
        /// 出撃中にアイテムを取得する
        /// </summary>
        [TestMethod]
        public void ItemGetInSortie()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.GaugeStatus[54] = new(); // dummy for null
            sniffer.SniffLogFile("itemget_001");
            PAssert.That(() => sniffer.MiscText ==
                               "[獲得アイテム]\n" +
                               "燃料: 1115\n" +
                               "弾薬: 25\n" +
                               "鋼材: 70\n" +
                               "家具箱（大）: 1\n" +
                               "給糧艦「間宮」: 1\n" +
                               "勲章: 1\n" +
                               "給糧艦「伊良湖」: 3\n" +
                               "プレゼント箱: 1\n" +
                               "補強増設: 2\n" +
                               "戦闘詳報: 1\n" +
                               "瑞雲(六三一空): 1\n" +
                               "夜間作戦航空要員: 1\n" +
                               "130mm B-13連装砲: 1\n" +
                               "潜水空母な桐箪笥: 1\n" +
                               "Gambier Bay: 1");
        }

        /// <summary>
        /// 出撃直後に資源を獲得する
        /// </summary>
        [TestMethod]
        public void ItemGetAtStart()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("itemget_002");
            PAssert.That(() => sniffer.MiscText ==
                               "[獲得アイテム]\n" +
                               "燃料: 65");
        }

        /// <summary>
        /// 航空偵察でアイテムを取得する
        /// </summary>
        [TestMethod]
        public void ItemGetInAirRecon()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airrecon_001");
            PAssert.That(() =>
                sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "失敗の場合");

            sniffer.SniffLogFile("airrecon_002");
            PAssert.That(() => sniffer.MiscText == "[獲得アイテム]\n弾薬: 150\n開発資材: 1", "成功");

            sniffer.SniffLogFile("airrecon_003");
            PAssert.That(() => sniffer.MiscText == "[獲得アイテム]\n弾薬: 150\n開発資材: 1", "途中でリロードして再出撃");
        }

        /// <summary>
        /// 秋刀魚を漁獲して帰投する
        /// </summary>
        [TestMethod]
        public void ItemGetSanma()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("sanma_2get_101");
            PAssert.That(() => sniffer.MiscText ==
                               "[獲得アイテム]\n" +
                               "秋刀魚: 2");
        }

        /// <summary>
        /// 海域突破時に選択報酬を決定する
        /// </summary>
        [TestMethod]
        public void SelectRewardOnResult()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("select_reward-on_result_101");
            PAssert.That(() => sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "戦闘終了して結果画面で報酬選択待ち");
            PAssert.That(() => sniffer.UseItems.ElementAt(0).ToString() == "燃料 x347899");

            sniffer.SniffLogFile("select_reward-on_result_102");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\n" +
                "燃料: 8000\n" +
                "勲章: 1\n" +
                "熟練搭乗員: 1\n" +
                "新型航空兵装資材: 2\n" +
                "戦闘詳報: 1\n" +
                "F4U-2 Night Corsair: 1\n" +
                "熟練甲板要員: 2\n" +
                "熟練甲板要員+航空整備員: 1",
                "報酬を選択して母港に戻る");
            PAssert.That(() => sniffer.UseItems.ElementAt(0).ToString() == "燃料 x355800", "報酬で上限超える");

            sniffer.SniffLogFile("select_reward-on_result_103");
            PAssert.That(() => sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "次の出撃で獲得アイテムが初期化されてる");
        }

        /// <summary>
        /// 海域突破時の選択報酬を決定せず、再読み込みで母港に戻る
        /// </summary>
        [TestMethod]
        public void SelectRewardOnPort()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("select_reward-on_port_101");
            PAssert.That(() => sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "戦闘終了して結果画面で報酬選択待ち");

            sniffer.SniffLogFile("select_reward-on_port_102");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\n" +
                "応急修理女神: 2\n" +
                "給糧艦「間宮」: 2\n" +
                "勲章: 1\n" +
                "給糧艦「伊良湖」: 2\n" +
                "補強増設: 1\n" +
                "Loire 130M: 1\n" +
                "熟練甲板要員: 2",
                "報酬選択せず再読み込みで母港に戻る");

            sniffer.SniffLogFile("select_reward-on_port_103");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\n" +
                "応急修理女神: 2\n" +
                "給糧艦「間宮」: 2\n" +
                "勲章: 1\n" +
                "給糧艦「伊良湖」: 2\n" +
                "補強増設: 1\n" +
                "Loire 130M: 1\n" +
                "熟練甲板要員: 2\n" +
                "明石: 1",
                "母港で報酬選択して獲得アイテム表示に追加される");

            sniffer.SniffLogFile("select_reward-on_port_104");
            PAssert.That(() => sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "次の出撃で獲得アイテムが初期化されてる");
        }

        /// <summary>
        /// 海域突破時に装備運用枠を獲得する
        /// </summary>
        [TestMethod]
        public void SelectRewardMaxSlotitem()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("select_reward-max_slotitem_101");
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\n" +
                "改修資材: 4\n" +
                "応急修理女神: 2\n" +
                "給糧艦「間宮」: 3\n" +
                "補強増設: 1\n" +
                "12cm単装高角砲+25mm機銃増備★5: 1\n" +
                "特四式内火艇★2: 1\n" +
                "特四式内火艇改★1: 1\n" +
                "装備運用枠: 5",
                "装備運用枠と増加数が獲得アイテム表示に追加される");
        }

        /// <summary>
        /// 海域突破時の選択報酬を決定せず、再読み込みで母港に戻り、装備運用枠を選択する
        /// </summary>
        [TestMethod]
        public void SelectRewardMaxSlotitemOnPort()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("select_reward_max_slotitem_on_port-101");
            PAssert.That(() => sniffer.ItemCounter.Max == 2710);
            PAssert.That(() => sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "増加前の装備枠");

            sniffer.SniffLogFile("select_reward_max_slotitem_on_port-102");
            PAssert.That(() => sniffer.ItemCounter.Max == 2710);
            PAssert.That(() => sniffer.MiscText ==
                "[演習情報]\n 演習相手を選ぶと表示します。\n" +
                "[獲得アイテム]\n 帰投したときに表示します。",
                "戦闘終了して結果画面で報酬選択待ち");

            sniffer.SniffLogFile("select_reward_max_slotitem_on_port-103");
            PAssert.That(() => sniffer.ItemCounter.Max == 2710);
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\n" +
                "給糧艦「間宮」: 3\n" +
                "勲章: 1\n" +
                "補強増設: 1\n" +
                "海外艦最新技術: 1\n" +
                "Type281 レーダー★2: 1\n" +
                "通信装置&要員: 1\n" +
                "13.8cm連装砲★2: 1",
                "報酬選択せず再読み込みで母港に戻る");

            sniffer.SniffLogFile("select_reward_max_slotitem_on_port-104");
            PAssert.That(() => sniffer.ItemCounter.Max == 2715);
            PAssert.That(() => sniffer.MiscText ==
                "[獲得アイテム]\n" +
                "給糧艦「間宮」: 3\n" +
                "勲章: 1\n" +
                "補強増設: 1\n" +
                "海外艦最新技術: 1\n" +
                "Type281 レーダー★2: 1\n" +
                "通信装置&要員: 1\n" +
                "13.8cm連装砲★2: 1\n" +
                "装備運用枠: 5",
                "母港で報酬選択して獲得アイテム表示に追加され、装備枠が増えてる");
        }

        /// <summary>
        /// 演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPoint()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice_004");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 第一艦隊\n獲得経験値 : 878\nS勝利 : 1053");
        }

        /// <summary>
        /// 錬巡朝日単艦での演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAsahiOnly()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-asahi-only_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 4-2"
                + "\n獲得経験値(水上艦) : 397\nS勝利 : 476"
                + "\n獲得経験値(潜水艦) : 860\nS勝利 : 1032");
        }

        /// <summary>
        /// 他の艦と錬巡朝日で演習の獲得経験値を計算する
        /// 演習相手は二隻ともLv180
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAnyShipWithAsahi()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-any-asahi_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : だいいちかんたい"
                + "\n獲得経験値 : 915\nS勝利 : 1098");
        }

        /// <summary>
        /// 他の艦と錬巡朝日香取で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAnyShipWithAsahiKatori()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-any-asahi-katori_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 鹿島香取のサービス編成"
                + "\n獲得経験値 : 991\nS勝利 : 1189");
        }

        /// <summary>
        /// 他の艦と錬巡朝日香取鹿島で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAnyShipWithAsahiKatoriKashima()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-any-asahi-katori-kashima_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 演習"
                + "\n獲得経験値 : 761\nS勝利 : 913");
        }

        /// <summary>
        /// 錬巡朝日と他の艦で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAsahiWithAnyShip()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-asahi-any_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 日課"
                + "\n獲得経験値(水上艦) : 374\nS勝利 : 449"
                + "\n獲得経験値(潜水艦) : 812\nS勝利 : 974");
        }

        /// <summary>
        /// 錬巡朝日香取と他の艦で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAsahiWithKatori()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-asahi-katori-any_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 第1艦隊"
                + "\n獲得経験値(水上艦) : 675\nS勝利 : 809"
                + "\n獲得経験値(潜水艦) : 932\nS勝利 : 1117");
        }

        /// <summary>
        /// 香取錬巡朝日と他の艦で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointKatoriWithAsahi()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-katori-asahi-any_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 第1艦隊"
                + "\n獲得経験値(水上艦) : 665\nS勝利 : 798"
                + "\n獲得経験値(潜水艦) : 918\nS勝利 : 1102");
        }

        /// <summary>
        /// 鹿島香取錬巡朝日と他の艦で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointKashimaWithKatoriAsahi()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-kashima-katori-asahi_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 第1艦隊"
                + "\n獲得経験値(水上艦) : 988\nS勝利 : 1185"
                + "\n獲得経験値(潜水艦) : 1365\nS勝利 : 1637");
        }

        /// <summary>
        /// 錬巡朝日鹿島香取と他の艦で演習の獲得経験値を計算する
        /// </summary>
        [TestMethod]
        public void PracticeExpPointAsahiWithKashimaKatori()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice-asahi-kashima-katori_101");
            PAssert.That(() => sniffer.MiscText == "[演習情報]\n敵艦隊名 : 演習艦隊"
                + "\n獲得経験値(水上艦) : 678\nS勝利 : 813"
                + "\n獲得経験値(潜水艦) : 936\nS勝利 : 1123");
        }
    }
}
