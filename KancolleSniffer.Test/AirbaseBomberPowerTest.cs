// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class AirbaseBomberPowerTest : ApiStart2
    {
        [TestMethod]
        public void 陸偵補正と制空()
        {
            var corps = new AirBase.AirCorpsInfo();
            var rikutei = NewPlaneInfo(二式陸上偵察機());
            var rikuteijukuren = NewPlaneInfo(二式陸上偵察機熟練());

            var nonakatai = NewPlaneInfo(一式陸攻野中隊());
            corps.Planes = [nonakatai];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 12, "制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 12, "対基地制空補正なし");
            PAssert.That(() => nonakatai.FighterPower.AirCombat.Min == 12);
            PAssert.That(() => nonakatai.FighterPower.Interception.Min == 12);
            PAssert.That(() => Math.Round(nonakatai.ShipBomberPower[0].BomberPower, 1) == 133.2, "対艦補正なし");
            PAssert.That(() => nonakatai.ShipBomberPower[0].Accuracy == 102);
            PAssert.That(() => Math.Round(nonakatai.LandBomberPower.BomberPower, 1) == 142.2, "対地補正なし");
            PAssert.That(() => Math.Round(nonakatai.LandBomberPower.IsolatedIsland, 1) == 284.4, "離島補正なし");
            PAssert.That(() => nonakatai.LandBomberPower.SupplyDepot == 478.8, "集積補正なし");
            PAssert.That(() => nonakatai.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => nonakatai.AswBomberPower.Max == 0, "対潜不可");
            PAssert.That(() => corps.CostForSortie.Fuel == 27);
            PAssert.That(() => corps.CostForSortie.Bull == 12);

            corps.Planes = [nonakatai, rikutei];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 20, "制空補正1.15");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 21, "対基地制空補正1.18");
            PAssert.That(() => nonakatai.FighterPower.AirCombat.Min == 12);
            PAssert.That(() => nonakatai.FighterPower.Interception.Min == 12);
            PAssert.That(() => Math.Round(nonakatai.ShipBomberPower[0].BomberPower, 2) == 149.85, "対艦補正1.125");
            PAssert.That(() => nonakatai.ShipBomberPower[0].Accuracy == 102);
            PAssert.That(() => Math.Round(nonakatai.LandBomberPower.BomberPower, 3) == 159.975, "対地補正1.125");
            PAssert.That(() => Math.Round(nonakatai.LandBomberPower.IsolatedIsland, 2) == 319.95, "離島補正1.125");
            PAssert.That(() => nonakatai.LandBomberPower.SupplyDepot == 538.65, "集積補正1.125");
            PAssert.That(() => nonakatai.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => nonakatai.AswBomberPower.Max == 0, "対潜不可");
            PAssert.That(() => corps.CostForSortie.Fuel == 31);
            PAssert.That(() => corps.CostForSortie.Bull == 15);

            corps.Planes = [nonakatai, rikuteijukuren];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 21, "制空補正1.18");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 22, "対基地制空補正1.23");
            PAssert.That(() => nonakatai.FighterPower.AirCombat.Min == 12);
            PAssert.That(() => nonakatai.FighterPower.Interception.Min == 12);
            PAssert.That(() => nonakatai.ShipBomberPower[0].BomberPower == 153.18, "対艦補正1.15");
            PAssert.That(() => nonakatai.ShipBomberPower[0].Accuracy == 102);
            PAssert.That(() => nonakatai.LandBomberPower.BomberPower == 163.53, "対地補正1.15");
            PAssert.That(() => nonakatai.LandBomberPower.IsolatedIsland == 327.06, "離島補正1.15");
            PAssert.That(() => nonakatai.LandBomberPower.SupplyDepot == 550.62, "集積補正1.15");
            PAssert.That(() => nonakatai.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => nonakatai.AswBomberPower.Max == 0, "対潜不可");
            PAssert.That(() => corps.CostForSortie.Fuel == 31);
            PAssert.That(() => corps.CostForSortie.Bull == 15);

            corps.Planes = [nonakatai, rikuteijukuren, rikutei];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 28, "複数種類の陸偵でも倍率は上限");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 29, "複数種類の陸偵でも倍率は上限");
            PAssert.That(() => nonakatai.FighterPower.AirCombat.Min == 12);
            PAssert.That(() => nonakatai.FighterPower.Interception.Min == 12);
            PAssert.That(() => nonakatai.ShipBomberPower[0].BomberPower == 153.18, "複数種類の陸偵でも倍率は上限");
            PAssert.That(() => nonakatai.ShipBomberPower[0].Accuracy == 102);
            PAssert.That(() => nonakatai.LandBomberPower.BomberPower == 163.53, "複数種類の陸偵でも倍率は上限");
            PAssert.That(() => nonakatai.LandBomberPower.IsolatedIsland == 327.06, "複数種類の陸偵でも倍率は上限");
            PAssert.That(() => nonakatai.LandBomberPower.SupplyDepot == 550.62, "複数種類の陸偵でも倍率は上限");
            PAssert.That(() => nonakatai.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => nonakatai.AswBomberPower.Max == 0, "対潜不可");
            PAssert.That(() => corps.CostForSortie.Fuel == 35);
            PAssert.That(() => corps.CostForSortie.Bull == 18);


            var tokai = NewPlaneInfo(試製東海());
            corps.Planes = [tokai];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 0, "制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 0, "対基地制空補正なし");
            PAssert.That(() => tokai.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => tokai.FighterPower.Interception.Min == 0);
            PAssert.That(() => tokai.ShipBomberPower[0].BomberPower == 36, "雷装0補正なし");
            PAssert.That(() => tokai.ShipBomberPower[0].Accuracy == 95);
            PAssert.That(() => tokai.LandBomberPower.BomberPower == 52.2);
            PAssert.That(() => Math.Round(tokai.LandBomberPower.IsolatedIsland, 2) == 102.6);
            PAssert.That(() => tokai.LandBomberPower.SupplyDepot == 289.8);
            PAssert.That(() => Math.Round(tokai.AswBomberPower.Min, 2) == 102.6, "対潜下限補正なし");
            PAssert.That(() => tokai.AswBomberPower.Max == 145.8, "対潜上限補正なし");
            PAssert.That(() => corps.CostForSortie.Fuel == 27);
            PAssert.That(() => corps.CostForSortie.Bull == 12);

            corps.Planes = [tokai, rikutei];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 6, "制空補正1.15");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 7, "対基地制空補正1.18");
            PAssert.That(() => tokai.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => tokai.FighterPower.Interception.Min == 0);
            PAssert.That(() => tokai.ShipBomberPower[0].BomberPower == 40.5, "雷装0補正1.125");
            PAssert.That(() => tokai.ShipBomberPower[0].Accuracy == 95);
            PAssert.That(() => tokai.LandBomberPower.BomberPower == 58.725);
            PAssert.That(() => Math.Round(tokai.LandBomberPower.IsolatedIsland, 3) == 115.425);
            PAssert.That(() => Math.Round(tokai.LandBomberPower.SupplyDepot, 3) == 326.025);
            PAssert.That(() => Math.Round(tokai.AswBomberPower.Min, 3) == 115.425, "対潜下限補正1.125");
            PAssert.That(() => tokai.AswBomberPower.Max == 164.025, "対潜上限補正1.125");
            PAssert.That(() => corps.CostForSortie.Fuel == 31);
            PAssert.That(() => corps.CostForSortie.Bull == 15);


            var hayabusa64 = NewPlaneInfo(一式戦隼II型64戦隊());
            corps.Planes = [hayabusa64];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 78, "制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 76, "対基地制空補正なし");
            PAssert.That(() => hayabusa64.FighterPower.AirCombat.Min == 78);
            PAssert.That(() => hayabusa64.FighterPower.Interception.Min == 76);
            PAssert.That(() => corps.CostForSortie.Fuel == 18);
            PAssert.That(() => corps.CostForSortie.Bull == 11);

            corps.Planes = [hayabusa64, rikutei];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 96, "制空補正1.15");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 96, "対基地制空補正1.18");
            PAssert.That(() => hayabusa64.FighterPower.AirCombat.Min == 78);
            PAssert.That(() => hayabusa64.FighterPower.Interception.Min == 76);
            PAssert.That(() => corps.CostForSortie.Fuel == 22);
            PAssert.That(() => corps.CostForSortie.Bull == 14);

            corps.Planes = [hayabusa64, rikuteijukuren];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 99, "制空補正1.18");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 101, "対基地制空補正1.23");
            PAssert.That(() => hayabusa64.FighterPower.AirCombat.Min == 78);
            PAssert.That(() => hayabusa64.FighterPower.Interception.Min == 76);
            PAssert.That(() => corps.CostForSortie.Fuel == 22);
            PAssert.That(() => corps.CostForSortie.Bull == 14);

            var suiteiotsu = NewPlaneInfo(零式水上偵察機11型乙());
            corps.Planes = [hayabusa64, suiteiotsu];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 80, "水偵は制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 85, "対基地制空補正1.1");
            PAssert.That(() => hayabusa64.FighterPower.AirCombat.Min == 78);
            PAssert.That(() => hayabusa64.FighterPower.Interception.Min == 76);
            PAssert.That(() => corps.CostForSortie.Fuel == 22);
            PAssert.That(() => corps.CostForSortie.Bull == 14);

            var saiun = NewPlaneInfo(彩雲());
            corps.Planes = [hayabusa64, saiun];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 78, "艦偵は制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 98, "対基地制空補正1.3");
            PAssert.That(() => hayabusa64.FighterPower.AirCombat.Min == 78);
            PAssert.That(() => hayabusa64.FighterPower.Interception.Min == 76);
            PAssert.That(() => corps.CostForSortie.Fuel == 22);
            PAssert.That(() => corps.CostForSortie.Bull == 14);


            var keiun = NewPlaneInfo(噴式景雲改(18));
            corps.Planes = [keiun];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 25, "制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 25, "対基地制空補正なし");
            PAssert.That(() => keiun.FighterPower.AirCombat.Min == 25);
            PAssert.That(() => keiun.FighterPower.Interception.Min == 25);
            PAssert.That(() => keiun.ShipBomberPower[0].Target == "噴式強襲");
            PAssert.That(() => Math.Round(keiun.ShipBomberPower[0].BomberPower, 2) == 88.64, "噴式強襲補正なし");
            PAssert.That(() => keiun.ShipBomberPower[0].Accuracy == 95, "噴式強襲は命中固定");
            PAssert.That(() => keiun.ShipBomberPower[1].Target == "対艦");
            PAssert.That(() => keiun.ShipBomberPower[1].BomberPower == 78, "対艦補正なし");
            PAssert.That(() => keiun.ShipBomberPower[1].Accuracy == 102);
            PAssert.That(() => keiun.LandBomberPower.BomberPower == 78, "対地補正なし");
            PAssert.That(() => keiun.LandBomberPower.IsolatedIsland == 156, "離島補正なし");
            PAssert.That(() => keiun.LandBomberPower.SupplyDepot == 263, "集積補正なし");
            PAssert.That(() => keiun.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => keiun.AswBomberPower.Max == 0, "対潜不可");
            PAssert.That(() => keiun.Item.SteelCost == 50);

            corps.Planes = [keiun, rikuteijukuren];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 36, "制空補正1.18");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 38, "対基地制空補正1.18");
            PAssert.That(() => keiun.FighterPower.AirCombat.Min == 25);
            PAssert.That(() => keiun.FighterPower.Interception.Min == 25);
            PAssert.That(() => keiun.ShipBomberPower[0].Target == "噴式強襲");
            PAssert.That(() => Math.Round(keiun.ShipBomberPower[0].BomberPower, 2) == 88.64, "噴式強襲は補正無効");
            PAssert.That(() => keiun.ShipBomberPower[0].Accuracy == 95, "噴式強襲は命中固定");
            PAssert.That(() => keiun.ShipBomberPower[1].Target == "対艦");
            PAssert.That(() => Math.Round(keiun.ShipBomberPower[1].BomberPower, 2) == 89.7, "対艦補正1.15");
            PAssert.That(() => keiun.ShipBomberPower[1].Accuracy == 102);
            PAssert.That(() => Math.Round(keiun.LandBomberPower.BomberPower, 2) == 89.7, "対地補正1.15");
            PAssert.That(() => Math.Round(keiun.LandBomberPower.IsolatedIsland, 1) == 179.4, "離島補正1.15");
            PAssert.That(() => keiun.LandBomberPower.SupplyDepot == 302.45, "集積補正1.15");
            PAssert.That(() => keiun.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => keiun.AswBomberPower.Max == 0, "対潜不可");
            PAssert.That(() => keiun.Item.SteelCost == 50);


            var shinzan = NewPlaneInfo(深山());
            corps.Planes = [shinzan];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 3, "制空補正なし");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 3, "対基地制空補正なし");
            PAssert.That(() => shinzan.FighterPower.AirCombat.Min == 3);
            PAssert.That(() => shinzan.FighterPower.Interception.Min == 3);
            PAssert.That(() => shinzan.ShipBomberPower[0].BomberPower == 73, "対艦補正なし");
            PAssert.That(() => shinzan.ShipBomberPower[0].Accuracy == 95);
            PAssert.That(() => shinzan.LandBomberPower.BomberPower == 93, "対地補正なし");
            PAssert.That(() => shinzan.LandBomberPower.IsolatedIsland == 187, "離島補正なし");
            PAssert.That(() => shinzan.LandBomberPower.SupplyDepot == 296, "集積補正なし");
            PAssert.That(() => shinzan.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => shinzan.AswBomberPower.Max == 0, "対潜不可");

            corps.Planes = [shinzan, rikutei];
            corps.CalcLandBasedReconModifier();
            PAssert.That(() => corps.CalcFighterPower().AirCombat.Min == 10, "制空補正1.15");
            PAssert.That(() => corps.CalcFighterPower().Interception.Min == 10, "対基地制空補正1.15");
            PAssert.That(() => shinzan.FighterPower.AirCombat.Min == 3);
            PAssert.That(() => shinzan.FighterPower.Interception.Min == 3);
            PAssert.That(() => shinzan.ShipBomberPower[0].BomberPower == 82.125, "対艦補正なし");
            PAssert.That(() => shinzan.ShipBomberPower[0].Accuracy == 95);
            PAssert.That(() => shinzan.LandBomberPower.BomberPower == 104.625, "対地補正なし");
            PAssert.That(() => shinzan.LandBomberPower.IsolatedIsland == 210.375, "離島補正なし");
            PAssert.That(() => shinzan.LandBomberPower.SupplyDepot == 333, "集積補正なし");
            PAssert.That(() => shinzan.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => shinzan.AswBomberPower.Max == 0, "対潜不可");


            var isshiki34 = NewPlaneInfo(一式陸攻三四型());
            PAssert.That(() => isshiki34.FighterPower.AirCombat.Min == 16);
            PAssert.That(() => isshiki34.FighterPower.Interception.Min == 16);
            PAssert.That(() => isshiki34.ShipBomberPower[0].BomberPower == 126);
            PAssert.That(() => isshiki34.ShipBomberPower[0].Accuracy == 102);

            isshiki34.Item.Alv = 7;
            PAssert.That(() => isshiki34.FighterPower.AirCombat.Min == 20, "一式陸攻三四型は熟練度最大で制空+4");
            PAssert.That(() => isshiki34.FighterPower.Interception.Min == 20, "一式陸攻三四型は熟練度最大で制空+4");

            isshiki34.Item.Alv = 0;
            isshiki34.Item.Level = 10;
            PAssert.That(() => isshiki34.FighterPower.AirCombat.Min == 23, "改修で制空上昇");
            PAssert.That(() => isshiki34.FighterPower.Interception.Min == 23, "改修で制空上昇");
            PAssert.That(() => isshiki34.ShipBomberPower[0].BomberPower == 144, "改修で対艦攻撃力上昇");
            PAssert.That(() => isshiki34.ShipBomberPower[0].Accuracy == 102);

            var hayabusa65 = NewPlaneInfo(爆装一式戦隼III型改65戦隊());
            PAssert.That(() => hayabusa65.FighterPower.AirCombat.Min == 25);
            PAssert.That(() => hayabusa65.FighterPower.Interception.Min == 25);
            PAssert.That(() => hayabusa65.ShipBomberPower[0].BomberPower == 36, "雷装0でも対艦攻撃可能");
            PAssert.That(() => hayabusa65.ShipBomberPower[0].Accuracy == 123);

            hayabusa65.Item.Level = 10;
            PAssert.That(() => hayabusa65.FighterPower.AirCombat.Min == 32);
            PAssert.That(() => hayabusa65.FighterPower.Interception.Min == 32);
            PAssert.That(() => hayabusa65.ShipBomberPower[0].BomberPower == 54, "雷装0でも改修で対艦攻撃力上昇");
            PAssert.That(() => hayabusa65.ShipBomberPower[0].Accuracy == 123);

            var spitfire = NewPlaneInfo(SpitfireMkV());
            PAssert.That(() => spitfire.FighterPower.AirCombat.Min == 50);
            PAssert.That(() => spitfire.FighterPower.Interception.Min == 72);

            spitfire.Item.Alv = 7;
            PAssert.That(() => spitfire.FighterPower.AirCombat.Min == 76, "熟練度最大で制空上昇");
            PAssert.That(() => spitfire.FighterPower.Interception.Min == 97, "熟練度最大で制空上昇");

            spitfire.Item.Alv = 0;
            spitfire.Item.Level = 10;
            PAssert.That(() => spitfire.FighterPower.AirCombat.Min == 59, "改修で制空上昇");
            PAssert.That(() => spitfire.FighterPower.Interception.Min == 80, "改修で制空上昇");
        }

        [TestMethod]
        public void 高高度防空()
        {
            var corps1 = new AirBase.AirCorpsInfo();
            var corps2 = new AirBase.AirCorpsInfo();
            corps1.Action = 2;
            corps2.Action = 2;
            var airBase = new AirBase.BaseInfo
            {
                AirCorps = [corps1, corps2],
                AreaId = 50
            };

            var spitfire = NewPlaneInfo(SpitfireMkV());
            corps1.Planes = [spitfire];
            corps1.CalcLandBasedReconModifier();
            var hayabusa64 = NewPlaneInfo(一式戦隼II型64戦隊());
            corps2.Planes = [hayabusa64];
            corps2.CalcLandBasedReconModifier();
            PAssert.That(() => airBase.CalcInterceptionFighterPower(3, true).Min == 74, "ロケット機がないので補正0.5");

            var shusui = NewPlaneInfo(秋水());
            corps1.Planes = [spitfire, shusui];
            corps1.CalcLandBasedReconModifier();
            PAssert.That(() => airBase.CalcInterceptionFighterPower(3, true).Min == 189, "ロケット機1なので補正0.8");

            corps2.Planes = [hayabusa64, shusui];
            corps2.CalcLandBasedReconModifier();
            PAssert.That(() => airBase.CalcInterceptionFighterPower(3, true).Min == 358, "ロケット機1なので補正1.1");

            var saiun = NewPlaneInfo(彩雲());
            corps1.Planes = [spitfire, shusui, saiun];
            corps1.CalcLandBasedReconModifier();
            PAssert.That(() => airBase.CalcInterceptionFighterPower(3, true).Min == 411, "彩雲配備でさらに1.3");
        }

        [TestMethod]
        public void 艦載機配備()
        {
            var ryusei1 = NewPlaneInfo(流星改一航戦());
            PAssert.That(() => ryusei1.FighterPower.AirCombat.Min == 8);
            PAssert.That(() => ryusei1.FighterPower.Interception.Min == 8);
            PAssert.That(() => ryusei1.ShipBomberPower[0].BomberPower == 104);
            PAssert.That(() => ryusei1.ShipBomberPower[0].Accuracy == 102);
            PAssert.That(() => ryusei1.LandBomberPower.BomberPower == 104);
            PAssert.That(() => ryusei1.LandBomberPower.IsolatedIsland == 123);
            PAssert.That(() => ryusei1.LandBomberPower.SupplyDepot == 204);
            PAssert.That(() => ryusei1.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => ryusei1.AswBomberPower.Max == 0, "対潜不可");

            var ryusei1juku = NewPlaneInfo(流星改一航戦熟練());
            PAssert.That(() => ryusei1juku.FighterPower.AirCombat.Min == 12);
            PAssert.That(() => ryusei1juku.FighterPower.Interception.Min == 12);
            PAssert.That(() => ryusei1juku.ShipBomberPower[0].BomberPower == 110);
            PAssert.That(() => ryusei1juku.ShipBomberPower[0].Accuracy == 109);
            PAssert.That(() => ryusei1juku.LandBomberPower.BomberPower == 110);
            PAssert.That(() => ryusei1juku.LandBomberPower.IsolatedIsland == 130);
            PAssert.That(() => ryusei1juku.LandBomberPower.SupplyDepot == 210);
            PAssert.That(() => ryusei1juku.AswBomberPower.Min == 22, "対潜下限");
            PAssert.That(() => ryusei1juku.AswBomberPower.Max == 51, "対潜上限");

            var ryusei1juku0 = NewPlaneInfo(流星改一航戦熟練(0));
            PAssert.That(() => ryusei1juku0.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => ryusei1juku0.FighterPower.Interception.Min == 0);
            PAssert.That(() => ryusei1juku0.ShipBomberPower.Length == 0);
            PAssert.That(() => ryusei1juku0.LandBomberPower.BomberPower == 0);
            PAssert.That(() => ryusei1juku0.LandBomberPower.IsolatedIsland == 0);
            PAssert.That(() => ryusei1juku0.LandBomberPower.SupplyDepot == 0);
            PAssert.That(() => ryusei1juku0.AswBomberPower.Min == 0, "対潜下限");
            PAssert.That(() => ryusei1juku0.AswBomberPower.Max == 0, "対潜上限");

            var keiun0 = NewPlaneInfo(噴式景雲改(0));
            PAssert.That(() => keiun0.ShipBomberPower.Length == 0);

            var sb2c5 = NewPlaneInfo(SB2C5());
            PAssert.That(() => sb2c5.FighterPower.AirCombat.Min == 8);
            PAssert.That(() => sb2c5.FighterPower.Interception.Min == 8);
            PAssert.That(() => sb2c5.ShipBomberPower[0].BomberPower == 93);
            PAssert.That(() => sb2c5.ShipBomberPower[0].Accuracy == 109);
            PAssert.That(() => sb2c5.LandBomberPower.BomberPower == 93);
            PAssert.That(() => sb2c5.LandBomberPower.IsolatedIsland == 187);
            PAssert.That(() => sb2c5.LandBomberPower.SupplyDepot == 295);
            PAssert.That(() => sb2c5.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => sb2c5.AswBomberPower.Max == 0, "対潜不可");

            var zero64kmx = NewPlaneInfo(零式艦戦64型複座KMX搭載機());
            PAssert.That(() => zero64kmx.FighterPower.AirCombat.Min == 16);
            PAssert.That(() => zero64kmx.FighterPower.Interception.Min == 16);
            PAssert.That(() => zero64kmx.ShipBomberPower[0].BomberPower == 42);
            PAssert.That(() => zero64kmx.ShipBomberPower[0].Accuracy == 95);
            PAssert.That(() => zero64kmx.LandBomberPower.BomberPower == 42);
            PAssert.That(() => zero64kmx.LandBomberPower.IsolatedIsland == 83);
            PAssert.That(() => zero64kmx.LandBomberPower.SupplyDepot == 188);
            PAssert.That(() => zero64kmx.AswBomberPower.Min == 24, "対潜下限");
            PAssert.That(() => zero64kmx.AswBomberPower.Max == 56, "対潜上限");

            var zuiun2 = NewPlaneInfo(瑞雲改二());
            PAssert.That(() => zuiun2.FighterPower.AirCombat.Min == 16);
            PAssert.That(() => zuiun2.FighterPower.Interception.Min == 16);
            PAssert.That(() => zuiun2.ShipBomberPower[0].BomberPower == 81);
            PAssert.That(() => zuiun2.ShipBomberPower[0].Accuracy == 109);
            PAssert.That(() => zuiun2.LandBomberPower.BomberPower == 81);
            PAssert.That(() => zuiun2.LandBomberPower.IsolatedIsland == 163);
            PAssert.That(() => zuiun2.LandBomberPower.SupplyDepot == 272);
            PAssert.That(() => zuiun2.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => zuiun2.AswBomberPower.Max == 0, "対潜不可");

            var zuiun2juku = NewPlaneInfo(瑞雲改二熟練());
            PAssert.That(() => zuiun2juku.FighterPower.AirCombat.Min == 21);
            PAssert.That(() => zuiun2juku.FighterPower.Interception.Min == 21);
            PAssert.That(() => zuiun2juku.ShipBomberPower[0].BomberPower == 87);
            PAssert.That(() => zuiun2juku.ShipBomberPower[0].Accuracy == 116);
            PAssert.That(() => zuiun2juku.LandBomberPower.BomberPower == 87);
            PAssert.That(() => zuiun2juku.LandBomberPower.IsolatedIsland == 175);
            PAssert.That(() => zuiun2juku.LandBomberPower.SupplyDepot == 283);
            PAssert.That(() => zuiun2juku.AswBomberPower.Min == 22, "対潜下限");
            PAssert.That(() => zuiun2juku.AswBomberPower.Max == 51, "対潜上限");

            var yateiotsu = NewPlaneInfo(零式水上偵察機11型乙改夜偵());
            PAssert.That(() => yateiotsu.FighterPower.AirCombat.Min == 2);
            PAssert.That(() => yateiotsu.FighterPower.Interception.Min == 2);
            PAssert.That(() => yateiotsu.ShipBomberPower.Length == 0);
            PAssert.That(() => yateiotsu.LandBomberPower.BomberPower == 0);
            PAssert.That(() => yateiotsu.LandBomberPower.IsolatedIsland == 0);
            PAssert.That(() => yateiotsu.LandBomberPower.SupplyDepot == 0);
            PAssert.That(() => yateiotsu.AswBomberPower.Min == 0, "対潜不可");
            PAssert.That(() => yateiotsu.AswBomberPower.Max == 0, "対潜不可");

            var suiteiotsu = NewPlaneInfo(零式水上偵察機11型乙());
            PAssert.That(() => suiteiotsu.FighterPower.AirCombat.Min == 2);
            PAssert.That(() => suiteiotsu.FighterPower.Interception.Min == 2);
            PAssert.That(() => suiteiotsu.ShipBomberPower.Length == 0);
            PAssert.That(() => suiteiotsu.LandBomberPower.BomberPower == 0);
            PAssert.That(() => suiteiotsu.AswBomberPower.Min == 15, "対潜下限");
            PAssert.That(() => suiteiotsu.AswBomberPower.Max == 35, "対潜上限");

            var kagou = NewPlaneInfo(カ号観測機());
            PAssert.That(() => kagou.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => kagou.FighterPower.Interception.Min == 0);
            PAssert.That(() => kagou.ShipBomberPower.Length == 0);
            PAssert.That(() => kagou.LandBomberPower.BomberPower == 0);
            PAssert.That(() => kagou.AswBomberPower.Min == 26, "対潜下限");
            PAssert.That(() => kagou.AswBomberPower.Max == 60, "対潜上限");

            var s51jkai = NewPlaneInfo(S51J改());
            PAssert.That(() => s51jkai.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => s51jkai.FighterPower.Interception.Min == 0);
            PAssert.That(() => s51jkai.ShipBomberPower.Length == 0);
            PAssert.That(() => s51jkai.LandBomberPower.BomberPower == 0);
            PAssert.That(() => s51jkai.AswBomberPower.Min == 69, "対潜下限");
            PAssert.That(() => s51jkai.AswBomberPower.Max == 98, "対潜上限");

            var sanshiki = NewPlaneInfo(三式指揮連絡機対潜());
            PAssert.That(() => sanshiki.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => sanshiki.FighterPower.Interception.Min == 0);
            PAssert.That(() => sanshiki.ShipBomberPower.Length == 0);
            PAssert.That(() => sanshiki.LandBomberPower.BomberPower == 0);
            PAssert.That(() => sanshiki.AswBomberPower.Min == 22, "対潜下限");
            PAssert.That(() => sanshiki.AswBomberPower.Max == 51, "対潜上限");

            var hayabusa20juku = NewPlaneInfo(一式戦隼III型改熟練20戦隊(alv: 7));
            PAssert.That(() => hayabusa20juku.FighterPower.AirCombat.Min == 54, "熟練度補正が戦闘機と同じ");
            PAssert.That(() => hayabusa20juku.FighterPower.Interception.Min == 54, "熟練度補正が戦闘機と同じ");
            PAssert.That(() => hayabusa20juku.ShipBomberPower[0].BomberPower == 53, "対艦爆撃可能");
            PAssert.That(() => hayabusa20juku.ShipBomberPower[0].Accuracy == 114);
            PAssert.That(() => hayabusa20juku.LandBomberPower.BomberPower == 53, "対地爆撃可能");
            PAssert.That(() => hayabusa20juku.LandBomberPower.IsolatedIsland == 107, "離島補正は有効と仮定");
            PAssert.That(() => hayabusa20juku.LandBomberPower.SupplyDepot == 212, "修正補正は有効と仮定");
            PAssert.That(() => hayabusa20juku.AswBomberPower.Min == 24, "対潜下限");
            PAssert.That(() => hayabusa20juku.AswBomberPower.Max == 56, "対潜上限");
        }

        [TestMethod]
        public void 特定艦種特効()
        {
            var nonakatai = NewPlaneInfo(一式陸攻野中隊());
            PAssert.That(() => nonakatai.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => Math.Round(nonakatai.ShipBomberPower[1].BomberPower, 1) == 133.2);
            PAssert.That(() => nonakatai.ShipBomberPower[1].Accuracy == 107);

            var nonakataiAlv = NewPlaneInfo(一式陸攻野中隊(alv: 1));
            PAssert.That(() => nonakataiAlv.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => Math.Round(nonakataiAlv.ShipBomberPower[1].BomberPower, 1) == 133.2);
            PAssert.That(() => nonakataiAlv.ShipBomberPower[1].Accuracy == 108);

            var nonakatai0 = NewPlaneInfo(一式陸攻野中隊(0));
            PAssert.That(() => nonakatai0.ShipBomberPower.Length == 0);

            var gingaegusa = NewPlaneInfo(銀河江草隊());
            PAssert.That(() => gingaegusa.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => gingaegusa.ShipBomberPower[1].BomberPower == 158.4);
            PAssert.That(() => gingaegusa.ShipBomberPower[1].Accuracy == 121);

            var hs293 = NewPlaneInfo(Do217E5Hs293初期型());
            PAssert.That(() => hs293.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => hs293.ShipBomberPower[1].BomberPower == 153);
            PAssert.That(() => hs293.ShipBomberPower[1].Accuracy == 109);

            var fritzx = NewPlaneInfo(Do217K2FritzX());
            PAssert.That(() => fritzx.ShipBomberPower[1].Target == "戦艦");
            PAssert.That(() => Math.Round(fritzx.ShipBomberPower[1].BomberPower, 1) == 232.2);
            PAssert.That(() => fritzx.ShipBomberPower[1].Accuracy == 109);

            var hiryumissile = NewPlaneInfo(四式重爆飛龍イ号一型甲誘導弾());
            PAssert.That(() => hiryumissile.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => hiryumissile.ShipBomberPower[1].BomberPower == 176.4);
            PAssert.That(() => hiryumissile.ShipBomberPower[1].Accuracy == 95);
            PAssert.That(() => hiryumissile.ShipBomberPower[2].Target == "軽巡");
            PAssert.That(() => hiryumissile.ShipBomberPower[2].BomberPower == 176.4);
            PAssert.That(() => hiryumissile.ShipBomberPower[2].Accuracy == 109);
            PAssert.That(() => hiryumissile.ShipBomberPower[3].Target == "重巡軽空戦艦");
            PAssert.That(() => hiryumissile.ShipBomberPower[3].BomberPower == 176.4);
            PAssert.That(() => hiryumissile.ShipBomberPower[3].Accuracy == 102);

            var hiryumissileskilled = NewPlaneInfo(四式重爆飛龍熟練イ号一型甲誘導弾());
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[1].BomberPower == 194.4);
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[1].Accuracy == 116);
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[2].Target == "軽巡");
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[2].BomberPower == 194.4);
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[2].Accuracy == 123);
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[3].Target == "重巡軽空戦艦");
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[3].BomberPower == 194.4);
            PAssert.That(() => hiryumissileskilled.ShipBomberPower[3].Accuracy == 116);

            var ki102 = NewPlaneInfo(キ102乙());
            PAssert.That(() => ki102.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => ki102.ShipBomberPower[1].BomberPower == 126);
            PAssert.That(() => ki102.ShipBomberPower[1].Accuracy == 130);

            var ki102missile = NewPlaneInfo(キ102乙改イ号一型乙誘導弾());
            PAssert.That(() => ki102missile.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => ki102missile.ShipBomberPower[1].BomberPower == 167.4);
            PAssert.That(() => ki102missile.ShipBomberPower[1].Accuracy == 101);
            PAssert.That(() => ki102missile.ShipBomberPower[2].Target == "軽巡");
            PAssert.That(() => ki102missile.ShipBomberPower[2].BomberPower == 167.4);
            PAssert.That(() => ki102missile.ShipBomberPower[2].Accuracy == 123);
            PAssert.That(() => ki102missile.ShipBomberPower[3].Target == "重巡軽空戦艦");
            PAssert.That(() => ki102missile.ShipBomberPower[3].BomberPower == 167.4);
            PAssert.That(() => ki102missile.ShipBomberPower[3].Accuracy == 116);

            var b25 = NewPlaneInfo(B25());
            PAssert.That(() => b25.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => Math.Round(b25.ShipBomberPower[1].BomberPower, 1) == 196.2);
            PAssert.That(() => b25.ShipBomberPower[1].Accuracy == 116);
            PAssert.That(() => b25.ShipBomberPower[2].Target == "軽巡");
            PAssert.That(() => b25.ShipBomberPower[2].BomberPower == 181.8);
            PAssert.That(() => b25.ShipBomberPower[2].Accuracy == 116);
            PAssert.That(() => b25.ShipBomberPower[3].Target == "重巡");
            PAssert.That(() => b25.ShipBomberPower[3].BomberPower == 165.6);
            PAssert.That(() => b25.ShipBomberPower[3].Accuracy == 116);
            PAssert.That(() => b25.ShipBomberPower[4].Target == "軽空戦艦補給");
            PAssert.That(() => b25.ShipBomberPower[4].BomberPower == 129.6);
            PAssert.That(() => b25.ShipBomberPower[4].Accuracy == 95);

            var hayabusa65 = NewPlaneInfo(爆装一式戦隼III型改65戦隊());
            PAssert.That(() => hayabusa65.ShipBomberPower[1].Target == "駆逐PT");
            PAssert.That(() => hayabusa65.ShipBomberPower[1].BomberPower == 239.4);
            PAssert.That(() => hayabusa65.ShipBomberPower[1].Accuracy == 123);

            var hayabusa20juku = NewPlaneInfo(一式戦隼III型改熟練20戦隊());
            PAssert.That(() => hayabusa20juku.ShipBomberPower[1].Target == "駆逐");
            PAssert.That(() => hayabusa20juku.ShipBomberPower[1].BomberPower == 195);
            PAssert.That(() => hayabusa20juku.ShipBomberPower[1].Accuracy == 102);
        }

        [TestMethod]
        public void 基地航空でも対潜機で対潜可能かつ一式戦隼II型改20戦隊のみ爆撃可能()
        {
            var airBasePlane = NewPlaneInfo(カ号観測機(18));
            PAssert.That(() => airBasePlane.ShipBomberPower.Length == 0);
            PAssert.That(() => airBasePlane.LandBomberPower.BomberPower == 0);
            PAssert.That(() => airBasePlane.AswBomberPower.Max == 60);
            airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18));
            PAssert.That(() => airBasePlane.ShipBomberPower.Length == 0);
            PAssert.That(() => airBasePlane.LandBomberPower.BomberPower == 0);
            PAssert.That(() => airBasePlane.AswBomberPower.Max == 51);
            airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18));
            PAssert.That(() => airBasePlane.ShipBomberPower[0].BomberPower == 47);
            PAssert.That(() => airBasePlane.LandBomberPower.BomberPower == 47);
            PAssert.That(() => airBasePlane.AswBomberPower.Max == 56);

            airBasePlane = NewPlaneInfo(カ号観測機(18, level: 10));
            PAssert.That(() => airBasePlane.ShipBomberPower.Length == 0);
            PAssert.That(() => airBasePlane.LandBomberPower.BomberPower == 0);
            PAssert.That(() => airBasePlane.AswBomberPower.Max == 70);
            airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18, level: 10));
            PAssert.That(() => airBasePlane.ShipBomberPower.Length == 0);
            PAssert.That(() => airBasePlane.LandBomberPower.BomberPower == 0);
            PAssert.That(() => airBasePlane.AswBomberPower.Max == 61);
            airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18, level: 10));
            PAssert.That(() => airBasePlane.ShipBomberPower[0].BomberPower == 47);
            PAssert.That(() => airBasePlane.LandBomberPower.BomberPower == 47);
            PAssert.That(() => airBasePlane.AswBomberPower.Max == 66);
        }
    }
}
