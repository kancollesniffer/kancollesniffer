// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ShipApiTest
    {
        /// <summary>
        /// 編成で空き番号を使ったローテートを正しく反映する
        /// </summary>
        [TestMethod]
        public void RotateFleetMember()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("deck_001");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(756, 17204, 6156, 28806, 1069, -1));
        }

        /// <summary>
        /// ドラッグ＆ドロップで離れた空き番号を使って編成をローテートする
        /// </summary>
        [TestMethod]
        public void RotateFleetMemberWithDragAndDrop()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("deck_005");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(57391, 50, 24475, 113, -1, -1));
        }

        /// <summary>
        /// 編成で艦隊に配置ずみの艦娘を交換する
        /// </summary>
        [TestMethod]
        public void ExchangeFleetMember()
        {
            var sniffer = new TestingSniffer();

            sniffer.SniffLogFile("deck_002");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(1069, 6156, 756, 3223, -1, -1), "編成で艦隊内で艦娘と交換する");

            sniffer.SniffLogFile("deck_003");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(1069, 6156, 14258, 3223, -1, -1), "002に続いて艦隊をまたがって交換する");
            PAssert.That(() => sniffer.Fleets[1].Deck.ParamsEqual(101, 4487, 756, 14613, 28806, -1), "002に続いて艦隊をまたがって交換する");

            sniffer.SniffLogFile("deck_004");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(1069, 6156, 14258, 3223, 756, -1), "003に続いて空き番号にほかの艦隊の艦娘を配置する");
            PAssert.That(() => sniffer.Fleets[1].Deck.ParamsEqual(101, 4487, 14613, 28806, -1, -1), "003に続いて空き番号にほかの艦隊の艦娘を配置する");
        }

        /// <summary>
        /// 随伴艦一括解除を実行する
        /// </summary>
        [TestMethod]
        public void WithdrawAccompanyingShipsAtOnce()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("deck_006");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(135, -1, -1, -1, -1, -1));
        }

        /// <summary>
        /// 編成展開を正しく反映する
        /// </summary>
        [TestMethod]
        public void PresetSelect()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("preset_001");
            PAssert.That(() => sniffer.Fleets[0].Deck.ParamsEqual(50510, 632, 39843, 113, 478, 47422));
        }

        /// <summary>
        /// 編成を開いて母港に戻った後と、戦闘後の一覧ウィンドウのプリセットにある編成艦隊番号を維持する
        /// </summary>
        [TestMethod]
        public void PresetListWindow()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("preset_101");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(3).Ships[1].Fleet.Number == 0, "編成を開く");
            sniffer.SniffLogFile("preset_102");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(3).Ships[1].Fleet.Number == 0, "母港に戻る");
            sniffer.SniffLogFile("preset_103");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(3).Ships[1].Fleet.Number == 0, "戦闘後の更新");
        }

        /// <summary>
        /// プリセットの順番を入れ替える
        /// </summary>
        [TestMethod]
        public void PresetOrderChange()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("preset_order_change-101");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(0).Deck.ParamsEqual(82, 27, 107, 29, 1898, 11064, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(1).Deck.ParamsEqual(10015, 120, 35758, 39396, 23640, 48612, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(4).Deck.ParamsEqual(212232, 212126, 207275, 211248, 206602, 108681, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(5).Deck.ParamsEqual(56, 158951, 7844, 12490, 39215, 9005, -1));

            sniffer.SniffLogFile("preset_order_change-102");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(4).Deck.ParamsEqual(56, 158951, 7844, 12490, 39215, 9005, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(5).Deck.ParamsEqual(212232, 212126, 207275, 211248, 206602, 108681, -1));
            sniffer.SniffLogFile("preset_order_change-103");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(4).Deck.ParamsEqual(212232, 212126, 207275, 211248, 206602, 108681, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(5).Deck.ParamsEqual(56, 158951, 7844, 12490, 39215, 9005, -1));

            sniffer.SniffLogFile("preset_order_change-104");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(0).Deck.ParamsEqual(10015, 120, 35758, 39396, 23640, 48612, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(1).Deck.ParamsEqual(82, 27, 107, 29, 1898, 11064, -1));
            sniffer.SniffLogFile("preset_order_change-105");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(0).Deck.ParamsEqual(82, 27, 107, 29, 1898, 11064, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(1).Deck.ParamsEqual(10015, 120, 35758, 39396, 23640, 48612, -1));

            sniffer.SniffLogFile("preset_order_change-106");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(0).Deck.ParamsEqual(56, 158951, 7844, 12490, 39215, 9005, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(5).Deck.ParamsEqual(82, 27, 107, 29, 1898, 11064, -1));
            sniffer.SniffLogFile("preset_order_change-107");
            PAssert.That(() => sniffer.PresetDecks.ElementAt(0).Deck.ParamsEqual(82, 27, 107, 29, 1898, 11064, -1));
            PAssert.That(() => sniffer.PresetDecks.ElementAt(5).Deck.ParamsEqual(56, 158951, 7844, 12490, 39215, 9005, -1));
        }

        /// <summary>
        /// 近代化改修の結果をすぐに反映する
        /// </summary>
        [TestMethod]
        public void PowerUpResult()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("powerup_001");
            PAssert.That(() => EffectiveShelling.Calc(sniffer.Fleets[0].Ships[0]).ShellingPower == 30);
        }

        /// <summary>
        /// 近代化改修による艦娘数と装備数の変化
        /// </summary>
        [TestMethod]
        public void PowerUpCount()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("powerup_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 204);
            PAssert.That(() => sniffer.ItemCounter.Now == 886);
        }

        /// <summary>
        /// 近代化改修が二重に行われた場合に対応する
        /// </summary>
        [TestMethod]
        public void DuplicatedPowerUp()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("powerup_002");
            PAssert.That(() => sniffer.ShipCounter.Now == 218);
        }

        /// <summary>
        /// 装備解除後の近代化改修
        /// </summary>
        [TestMethod]
        public void PowerUpDetachItem()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("powerup_003");
            PAssert.That(() => sniffer.ShipCounter.Now == 317);
            PAssert.That(() => sniffer.ItemCounter.Now == 1326);
        }

        /// <summary>
        /// 南瓜を使う近代化改修
        /// </summary>
        [TestMethod]
        public void PowerupPumpkin()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("powerup_pumpkin_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(35).ToString() == "南瓜 x2");

            sniffer.SniffLogFile("powerup_pumpkin_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(35).ToString() == "南瓜 x1");
        }

        /// <summary>
        /// 特殊な素材を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingSpecialMaterial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodeling_special_material_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2965");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x3000");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 48837).Spec.Id == 390);

            sniffer.SniffLogFile("remodeling_special_material_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2955");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2980");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x299");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 48837).Spec.Id == 903);
        }

        /// <summary>
        /// 新型砲熕兵装資材を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelGunMount()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodeling_special_material_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2989");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x63");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 10).Spec.Id == 206);

            sniffer.SniffLogFile("remodeling_special_material_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2961");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x324");
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "新型砲熕兵装資材 x62");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 10).Spec.Id == 666);
        }

        /// <summary>
        /// 新型高温高圧缶を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelBoiler()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodeling_special_material_201");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x3");
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x66");
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 87) == 12); // id:12 新型高温高圧缶
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 27212).Spec.Id == 136);

            sniffer.SniffLogFile("remodeling_special_material_202");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x332");
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "新型砲熕兵装資材 x63");
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 87) == 10); // id:12 新型高温高圧缶
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 27212).Spec.Id == 911);
        }

        /// <summary>
        /// 海外艦最新技術を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelTechCount()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodeling_special_material-301");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 40);
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(39).ToString() == "海外艦最新技術 x2");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 49013).Spec.Id == 392);

            sniffer.SniffLogFile("remodeling_special_material-302");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 38);
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "給糧艦「伊良湖」 x488");
            PAssert.That(() => sniffer.UseItems.ElementAt(37).ToString() == "白たすき x3");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 49013).Spec.Id == 969);
        }

        /// <summary>
        /// 新型砲熕兵装資材を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelGunMount2()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodeling_special_material-401");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 39);
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2839");
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "改装設計図 x2");
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型砲熕兵装資材 x84");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 2309).Spec.Id == 152);

            sniffer.SniffLogFile("remodeling_special_material-402");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 38);
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2439");
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "給糧艦「伊良湖」 x499");
            PAssert.That(() => sniffer.UseItems.ElementAt(26).ToString() == "新型砲熕兵装資材 x82");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 2309).Spec.Id == 694);
        }

        /// <summary>
        /// 新型砲熕兵装資材を消費する改装の結果を反映する
        /// </summary>
        [TestMethod]
        public void RemodelingNewModelGunMount3()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("remodeling_special_material-501");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 39);
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型砲熕兵装資材 x83");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 4).Spec.Id == 202);

            sniffer.SniffLogFile("remodeling_special_material-502");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 39);
            PAssert.That(() => sniffer.UseItems.ElementAt(27).ToString() == "新型砲熕兵装資材 x82");
            PAssert.That(() => sniffer.ShipList.First(ship => ship.Id == 4).Spec.Id == 986);
        }

        /// <summary>
        /// 補強増設使用の結果を反映する
        /// </summary>
        [TestMethod]
        public void OpenExslot()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("open_exslot_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(21).ToString() == "補強増設 x9");
            var slotex = sniffer.ShipList.First(ship => ship.Id == 7863).SlotEx;
            PAssert.That(() => slotex == 0);

            sniffer.SniffLogFile("open_exslot_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(21).ToString() == "補強増設 x8");
            slotex = sniffer.ShipList.First(ship => ship.Id == 7863).SlotEx;
            PAssert.That(() => slotex == -1);
        }

        /// <summary>
        /// ship2を待たずにケッコンの結果を反映する
        /// </summary>
        [TestMethod]
        public void MarriageResult()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("marriage_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 33);
            PAssert.That(() => sniffer.UseItems.ElementAt(14).ToString() == "書類一式＆指輪 x1");
            PAssert.That(() => sniffer.Fleets[0].Ships[2].Level == 99);

            sniffer.SniffLogFile("marriage_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 32);
            PAssert.That(() => sniffer.UseItems.ElementAt(14).ToString() == "艦娘からのチョコ x2");
            PAssert.That(() => sniffer.Fleets[0].Ships[2].Level == 100);
        }

        /// <summary>
        /// 修復時間が1分以内の艦娘が入渠する
        /// </summary>
        [TestMethod]
        public void NyukyoLessThanOrEqualTo1Min()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("nyukyo_001");
            PAssert.That(() => sniffer.RepairList.Length == 1);
        }

        /// <summary>
        /// 建造してすぐ消費アイテムを一覧に反映する
        /// </summary>
        [TestMethod]
        public void CreateShip()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("createship_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x350000");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2867");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2991");

            sniffer.SniffLogFile("createship_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x349970");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2867");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2990");

            sniffer.SniffLogFile("createship_103");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x349939");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2866");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2989");

            sniffer.SniffLogFile("createship_104");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x348939");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2866");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2988");

            sniffer.SniffLogFile("createship_105");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x347929");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2856");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2968");

            sniffer.SniffLogFile("createship_106");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x347929");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2855");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2968");

            sniffer.SniffLogFile("createship_107");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x347929");
            PAssert.That(() => sniffer.UseItems.ElementAt(5).ToString() == "高速建造材 x2845");
            PAssert.That(() => sniffer.UseItems.ElementAt(6).ToString() == "開発資材 x2968");
        }

        /// <summary>
        /// 一括解体する(装備保管なしとあり)
        /// </summary>
        [TestMethod]
        public void DestroyShip()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("destroyship_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 250);
            PAssert.That(() => sniffer.ItemCounter.Now == 1055);
            PAssert.That(() => sniffer.Material.Current.Take(4).ParamsEqual(285615, 286250, 291010, 284744));
        }

        /// <summary>
        /// ship2がリクエストで指定した艦娘のデータしか返さない
        /// </summary>
        [TestMethod]
        public void Ship2ReturnShipSpecifiedByRequest()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("ship2_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 243);
        }

        /// <summary>
        /// 艦娘数を数える
        /// </summary>
        [TestMethod]
        public void CountShips()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("ship_count_001");
            PAssert.That(() => sniffer.ShipCounter.Now == 267 && sniffer.ShipCounter.Alarm, "ログイン");
            sniffer.SniffLogFile("ship_count_002");
            PAssert.That(() => sniffer.ShipCounter.Now == 266 && sniffer.ShipCounter.Alarm, "建造");
            sniffer.SniffLogFile("ship_count_003");
            PAssert.That(() => sniffer.ShipCounter.Now == 266 && sniffer.ShipCounter.Alarm, "ドロップ");
        }
    }
}
