// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class BadlyDamageTest
    {
        /// <summary>
        /// 出撃時に大破している艦娘がいたら警告する
        /// </summary>
        [TestMethod]
        public void DamagedShipWarningOnMapStart()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("mapstart_001");
            PAssert.That(() => sniffer.BadlyDamagedShips.ParamsEqual("大潮"));
        }

        /// <summary>
        /// 連合艦隊に大破艦がいる状態で第3艦隊が出撃したときに警告しない
        /// </summary>
        [TestMethod]
        public void NotWarnDamagedShipInCombinedFleetOnMapStart()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("mapstart_002");
            PAssert.That(() => !sniffer.BadlyDamagedShips.Any());
        }

        /// <summary>
        /// 連合艦隊の第二旗艦の大破を警告しない
        /// </summary>
        [TestMethod]
        public void NotWarnDamaged1StShipInGuardFleet()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("combined_battle_004");
            PAssert.That(() => !sniffer.BadlyDamagedShips.Any());
        }

        /// <summary>
        /// ダメコン進撃する
        /// </summary>
        [TestMethod]
        public void NotWarnDamagedShipWithDamageControl()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("damecon_advance_001");
            PAssert.That(() => sniffer.BadlyDamagedShips.Length == 0);
        }

        /// <summary>
        /// 大破進撃しても平気なマスでの大破警告抑制
        /// </summary>
        [TestMethod]
        public void IgnoreDamagedShips()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("endpoint_001");
            PAssert.That(() => sniffer.BadlyDamagedShips.Length == 0);
        }

        /// <summary>
        /// 演習でダメコンを発動させない
        /// </summary>
        [TestMethod]
        public void NotTriggerDameConInPractice()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice_002");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 昼戦でダメコンを消費する
        /// https://www.youtube.com/watch?v=GllfzhmMm0I&t=753s
        /// </summary>
        [TestMethod]
        public void ConsumeDameconInDay()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("damecon_day_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 42);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 13);

            // 戦闘開始時には、メイン画面では撃沈前未消費
            sniffer.SniffLogFile("damecon_day_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 42);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 13);

            // 戦況画面では撃沈済み消費済み
            var shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 3);
            PAssert.That(() => shipResult.Items.Count == 1);
            PAssert.That(() => shipResult.Items[0].Spec.Id == 2);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            sniffer.SniffLogFile("damecon_day_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 3);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 13);

            // 帰投後にダメコン所持数を更新
            sniffer.SniffLogFile("damecon_day_104");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 3);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 42) == 12);
        }

        /// <summary>
        /// 昼戦で女神を消費する
        /// https://www.youtube.com/watch?v=GllfzhmMm0I&t=365s
        /// </summary>
        [TestMethod]
        public void ConsumeMegamiInDay()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("megami_day_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 24);

            // 戦闘開始時には、メイン画面では撃沈前未消費
            sniffer.SniffLogFile("megami_day_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 24);

            // 戦況画面では撃沈済み消費済み
            var shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 16);
            PAssert.That(() => shipResult.Items.Count == 1);
            PAssert.That(() => shipResult.Items[0].Spec.Id == 2);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            sniffer.SniffLogFile("megami_day_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 24);

            // 帰投後にダメコン所持数を更新
            sniffer.SniffLogFile("megami_day_104");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);
        }

        /// <summary>
        /// 夜戦で女神カウンター
        /// https://www.youtube.com/watch?v=GllfzhmMm0I&t=1275s
        /// </summary>
        [TestMethod]
        public void MegamiCounterInNight()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("megami_night_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 4);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 昼戦では被弾回避
            sniffer.SniffLogFile("megami_night_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 4);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 戦況画面でも状況変わらず
            var dayResult = sniffer.Battle.Result;
            var shipDayResult = dayResult.Friend.Main[1];
            PAssert.That(() => shipDayResult.NowHp == 4);
            PAssert.That(() => shipDayResult.Items.Count == 2);
            PAssert.That(() => shipDayResult.Items[0].Id == 213820);
            PAssert.That(() => shipDayResult.Items[0].Spec.Id == 2);
            PAssert.That(() => shipDayResult.Items[1].Id == 73455);
            PAssert.That(() => shipDayResult.Items[1].Spec.Id == 43);

            // 夜戦開始時には、メイン画面では撃沈前未消費
            sniffer.SniffLogFile("megami_night_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 4);
            PAssert.That(() => ship.Items.Count == 2);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => ship.Items[1].Spec.Id == 43);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 戦況画面では撃沈済み消費済み
            var nightResult = sniffer.Battle.Result;
            var shipNightResult = nightResult.Friend.Main[1];
            PAssert.That(() => shipNightResult.NowHp == 16);
            PAssert.That(() => shipNightResult.Items.Count == 1);
            PAssert.That(() => shipNightResult.Items[0].Id == 213820);
            PAssert.That(() => shipNightResult.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.P);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            sniffer.SniffLogFile("megami_night_104");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 23);

            // 帰投後にダメコン所持数を更新
            sniffer.SniffLogFile("megami_night_105");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 16);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 2);
            PAssert.That(() => sniffer.SlotItems.Count(item => item.Spec.Id == 43) == 22);

            // 帰投後の戦況昼戦では撃沈前未消費
            shipDayResult = dayResult.Friend.Main[1];
            PAssert.That(() => shipDayResult.NowHp == 4);
            PAssert.That(() => shipDayResult.Items.Count == 2);
            PAssert.That(() => shipDayResult.Items[0].Id == 213820);
            PAssert.That(() => shipDayResult.Items[0].Spec.Id == 2);
            PAssert.That(() => shipDayResult.Items[1].Id == 73455);
            PAssert.That(() => shipDayResult.Items[1].Spec.Id == 43);

            // 帰投後の戦況夜戦では撃沈済み消費済み
            shipNightResult = nightResult.Friend.Main[1];
            PAssert.That(() => shipNightResult.NowHp == 16);
            PAssert.That(() => shipNightResult.Items.Count == 1);
            PAssert.That(() => shipNightResult.Items[0].Id == 213820);
            PAssert.That(() => shipNightResult.Items[0].Spec.Id == 2);
        }

        /// <summary>
        /// 補強増設の女神を消費する
        /// </summary>
        [TestMethod]
        public void ConsumeSlotExMegami()
        {
            var sniffer = new TestingSniffer();
            // 戦闘開始時には、メイン画面では撃沈前未消費
            sniffer.SniffLogFile("megami_slotex_101");
            var ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 1);
            PAssert.That(() => ship.Items.Count == 1);
            PAssert.That(() => ship.Items[0].Spec.Id == 43);

            // 戦況画面では撃沈済み消費済み
            var shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 32);
            PAssert.That(() => shipResult.Items.Count == 0);

            // 戦闘終了後には、メイン画面でも撃沈済み消費済み
            sniffer.SniffLogFile("megami_slotex_102");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 32);
            PAssert.That(() => ship.Items.Count == 0);

            // 次の戦闘で戦闘結果エラーが出ない
            sniffer.SniffLogFile("megami_slotex_103");
            ship = sniffer.Fleets[0].Ships[1];
            PAssert.That(() => ship.NowHp == 32);
            PAssert.That(() => ship.Items.Count == 0);

            shipResult = sniffer.Battle.Result.Friend.Main[1];
            PAssert.That(() => shipResult.NowHp == 23);
            PAssert.That(() => shipResult.Items.Count == 0);
            PAssert.That(() => !sniffer.IsBattleResultError);
        }
    }
}
