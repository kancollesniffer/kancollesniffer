// Copyright (C) 2016 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ExpressionToCodeLib;
using KancolleSniffer.View;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ShipLabelTest
    {
        [TestClass]
        public class TruncateTest
        {
            [TestMethod]
            public void TestTruncate()
            {
                var name = "大発動艇(八九式中戦車&陸戦隊)";
                var level = "";
                PAssert.That(() => Scaler.Truncate(name, level, 180, null) + level == name);
                level = "★10";
                PAssert.That(() => Scaler.Truncate(name, level, 180, null) + level == "大発動艇(八九式中戦車&陸戦★10");

                name = "零式艦戦53型(岩本隊)";
                level = "";
                PAssert.That(() => Scaler.Truncate(name, level, 132, null) + level == name);
                level = "★10";
                PAssert.That(() => Scaler.Truncate(name, level, 132, null) + level == "零式艦戦53型(岩本★10");
            }

            /// <summary>
            /// 文字数制限がない
            /// </summary>
            [TestMethod]
            public void Unlimited()
            {
                var label = new ShipLabel.Name(Point.Empty, ShipNameWidth.Max) {Parent = new Panel()};
                var str = "一二三四五六七八九〇一二三四五六七八九〇一二三四五六七八九〇";
                label.SetName(str);
                PAssert.That(() => label.Text == str);
            }

            /// <summary>
            /// 明石タイマー表示中の艦娘の名前を縮める
            /// </summary>
            [TestMethod]
            public void ForAkashiTimer()
            {
                var data = new Dictionary<int, (string, string)[]>()
                {
                    { 100, [
                        ("夕立改二", "夕立改二"),
                        ("朝潮改二丁", "朝潮改二"),
                        ("千代田航改", "千代田航"),
                        ("千代田航改二", "千代田航"),
                        ("Bismarck改", "Bismarck"),
                        ("Bismarck zwei", "Bismarck"),
                        ("Bismarck drei", "Bismarck"),
                        ("Prinz Eugen", "Prinz Eug"),
                        ("Prinz Eugen改", "Prinz Eug"),
                        ("Graf Zeppelin", "Graf Zep"),
                        ("Graf Zeppelin改", "Graf Zep"),
                        ("Libeccio改", "Libeccio")
                    ]},
                    { 125, [
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("", ""),
                        ("Graf Zeppelin", "Graf Zepp"),
                        ("Graf Zeppelin改", "Graf Zepp"),
                        ("Libeccio改", "Libeccio改")
                    ]}
                };
                TestTruncate(data, ShipNameWidth.AkashiTimer);
            }

            /// <summary>
            /// 入渠中の艦娘名の名前を縮める
            /// </summary>
            [TestMethod]
            public void ForNDock()
            {
                var data = new Dictionary<int, (string, string)[]>()
                {
                    { 100, [("千歳航改二", "千歳航改二"), ("千代田航改二", "千代田航改"), ("Graf Zeppelin", "Graf Zeppeli"), ("Graf Zeppelin改", "Graf Zeppeli")]},
                    { 125, [("", ""), ("", ""), ("Graf Zeppelin", "Graf Zeppelin"), ("Graf Zeppelin改", "Graf Zeppelin")]}
                };
                TestTruncate(data, ShipNameWidth.NDock);
            }

            /// <summary>
            /// 一覧ウィンドウの要修復一覧の艦娘の名前を縮める
            /// </summary>
            [TestMethod]
            public void ForRepairListFull()
            {
                var data = new Dictionary<int, (string, string)[]>()
                {
                    { 100, [("Graf Zeppelin", "Graf Zeppelin"), ("Graf Zeppelin改", "Graf Zeppelin"), ("千代田航改二", "千代田航改")]},
                    { 125, [("", ""), ("", ""), ("千代田航改二", "千代田航改二")]}
                };
                TestTruncate(data, ShipNameWidth.RepairListFull);
            }

            /// <summary>
            /// メインパネルの艦娘の名前を縮める
            /// </summary>
            [TestMethod]
            public void ForMainPanel()
            {
                var data = new Dictionary<int, (string, string)[]>()
                {
                    { 100, [("Commandant Teste", "Commandant Tes")]},
                    { 125, [("Commandant Teste", "Commandant Test")]}
                };
                TestTruncate(data, ShipNameWidth.MainPanel);
            }

            /// <summary>
            /// 一覧ウィンドウの艦娘一覧の名前を縮める
            /// </summary>
            [TestMethod]
            public void ForShipList()
            {
                var data = new Dictionary<int, (string, string)[]>()
                {
                    { 100, [("Commandant Teste", "Commandant T"), ("Graf Zeppelin改", "Graf Zeppelin")]},
                    { 125, [("", ""), ("Graf Zeppelin改", "Graf Zeppelin改")]}
                };
                TestTruncate(data, ShipNameWidth.ShipList);
            }

            private static void TestTruncate(Dictionary<int, (string, string)[]> data, ShipNameWidth width)
            {
                foreach (var zoom in data.Keys)
                {
                    SetScaleFactor(zoom);
                    var label = new ShipLabel.Name(Point.Empty, width) {Font = ShipLabel.Name.BaseFont};
                    for (var i = 0; i < data[zoom].Length; i++)
                    {
                        var entry = data[zoom][i];
                        if (string.IsNullOrEmpty(entry.Item1))
                            entry = data[100][i];
                        label.SetName(entry.Item1);
                        PAssert.That(() => label.Text == entry.Item2, $"{entry.Item1}: scale {zoom}");
                    }
                }
            }

            private static void SetScaleFactor(int zoom)
            {
                var form = new Form {AutoScaleMode = AutoScaleMode.Font};
                if (zoom == 100)
                {
                    Scaler.Factor = new SizeF(1, 1);
                    ShipLabel.Name.BaseFont = form.Font;
                    ShipLabel.Name.LatinFont = LatinFont(100);
                    return;
                }
                var prev = form.CurrentAutoScaleDimensions;
                form.Font = new Font(form.Font.FontFamily, form.Font.Size * zoom / 100);
                ShipLabel.Name.BaseFont = form.Font;
                ShipLabel.Name.LatinFont = LatinFont(zoom);
                var cur = form.CurrentAutoScaleDimensions;
                Scaler.Factor = new SizeF(cur.Width / prev.Width, cur.Height / prev.Height);
            }

            private static Font LatinFont(int zoom)
            {
                return new Font("Tahoma", 8f * zoom / 100);
            }
        }

        /// <summary>
        /// %表示の小数部を切り捨てる
        /// </summary>
        [TestMethod]
        public void RoundOffFractionOfPercent()
        {
            var label = new ShipLabel.Hp { Parent = new Panel() };
            label.SetHp(104, 105);
            label.ToggleHpPercent();
            PAssert.That(() => label.Text == "99%");
        }
    }
}
