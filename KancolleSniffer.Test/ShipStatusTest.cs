// Copyright (C) 2013-2021 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static KancolleSniffer.Model.ShipTypeCode;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ShipStatusTest : ApiStart2
    {
        [TestMethod]
        public void GotlandのSwordfishは雷装ボーナスなし()
        {
            var ship = NewShip(574);
            ship.SetItems(SwordfishMkIII改水上機型(2));
            ship.ResolveTbBonus(AdditionalData);

            PAssert.That(() => ship.Items[0].TbBonus == 0, "Swordfishに雷装ボーナスがつかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 34.9, "Swordfishの爆装ボーナスが航空戦につかない");
        }

        [TestMethod]
        public void Gotland改二のSwordfishに雷装ボーナス()
        {
            var ship = NewShip(630);
            ship.SetItems(SwordfishMkIII改水上機型(2));
            ship.ResolveTbBonus(AdditionalData);

            PAssert.That(() => ship.Items[0].TbBonus == 2, "Swordfishに雷装ボーナスがつく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 37.73, "Swordfishの爆装ボーナスが航空戦につく");
        }

        [TestMethod]
        public void Gotland改二の雷装ボーナス付加は爆装で判定()
        {
            var ship = NewShip(630);
            ship.SetItems(瑞雲六三四空(2), SwordfishMkIII改水上機型(2));
            ship.ResolveTbBonus(AdditionalData);

            PAssert.That(() => ship.Items[0].TbBonus == 0, "Swordfishの雷装ボーナスは瑞雲につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 33.49, "雷装ボーナスは瑞雲の航空戦につかない");
            PAssert.That(() => ship.Items[1].TbBonus == 2, "Swordfishの雷装ボーナスはSwordfishにつく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 37.73, "雷装ボーナスはSwordfishの航空戦につく");
        }

        [TestMethod]
        public void Gotland改二の瑞雲に雷装ボーナス()
        {
            var ship = NewShip(630);
            ship.SetItems(瑞雲12型(2), SwordfishMkIII改水上機型(2));
            ship.ResolveTbBonus(AdditionalData);

            PAssert.That(() => ship.Items[0].TbBonus == 2, "Swordfishの雷装ボーナスは瑞雲につく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 37.73, "雷装ボーナスは瑞雲の航空戦につく");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "Swordfishの雷装ボーナスはSwordfishにつかない");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 34.9, "雷装ボーナスはSwordfishの航空戦につかない");
        }

        [TestMethod]
        public void 通常の駆逐軽巡重順は初期状態で雷撃可能()
        {
            var ship = NewShip(1);
            ship.Torpedo = 18;
            ship.ImprovedTorpedo = 0;
            PAssert.That(() => ship.EffectiveTorpedo == 23);

            var ship2 = NewShip(51);
            ship2.Torpedo = 18;
            ship2.ImprovedTorpedo = 0;
            PAssert.That(() => ship2.EffectiveTorpedo == 23);

            var ship3 = NewShip(59);
            ship3.Torpedo = 12;
            ship3.ImprovedTorpedo = 0;
            PAssert.That(() => ship3.EffectiveTorpedo == 17);
        }

        [TestMethod]
        public void 雷装初期値0のままでは雷撃不可()
        {
            var ship = NewShip(183);
            ship.Torpedo = 0;
            ship.ImprovedTorpedo = 0;
            PAssert.That(() => ship.EffectiveTorpedo == 0);

            ship.SetItems(_61cm三連装魚雷());
            ship.Torpedo = ship.Items[0].Spec.Torpedo;
            PAssert.That(() => ship.EffectiveTorpedo == 0);

            ship.Torpedo = 1;
            ship.ImprovedTorpedo = 1;
            ship.SetItems();
            PAssert.That(() => ship.EffectiveTorpedo == 6);
        }

        [TestMethod]
        public void 空母水母速吸改神威改はどんな装備でも雷撃不可()
        {
            var ship = NewShip(89);
            ship.Torpedo = 13;
            ship.ImprovedTorpedo = 0;
            ship.SetItems(流星改(8));
            PAssert.That(() => ship.EffectiveTorpedo == 0);

            var ship2 = NewShip(352);
            ship2.Torpedo = 13;
            ship2.ImprovedTorpedo = 0;
            ship2.SetItems(流星改(6));
            PAssert.That(() => ship2.EffectiveTorpedo == 0);

            var ship3 = NewShip(499);
            ship3.Torpedo = 12;
            ship3.ImprovedTorpedo = 0;
            ship3.SetItems(甲標的甲型());
            PAssert.That(() => ship3.EffectiveTorpedo == 0);

            var ship4 = NewShip(451);
            ship4.Torpedo = 4;
            ship4.ImprovedTorpedo = 0;
            ship4.SetItems(瑞雲(12));
            PAssert.That(() => ship4.EffectiveTorpedo == 0);
        }

        /// <summary>
        /// 敵艦名とランク省略
        /// </summary>
        [TestMethod]
        public void EnemyShortenName()
        {
            PAssert.That(() => NewShip(1501).Spec.Name             == "駆逐イ級");
            PAssert.That(() => NewShip(1501).Spec.EnemyShortenName == "駆逐イ級");
            PAssert.That(() => NewShip(1514).Spec.Name             == "駆逐イ級(elite)");
            PAssert.That(() => NewShip(1514).Spec.EnemyShortenName == "駆逐イ級(e)");
            PAssert.That(() => NewShip(1527).Spec.Name             == "重巡リ級(flagship)");
            PAssert.That(() => NewShip(1527).Spec.EnemyShortenName == "重巡リ級(f)");
            PAssert.That(() => NewShip(1530).Spec.Name             == "潜水カ級");
            PAssert.That(() => NewShip(1530).Spec.EnemyShortenName == "潜水カ級");
        }

        /// <summary>
        /// 特殊砲撃、ダメコン、退避の状況を調べる
        /// </summary>
        [TestMethod]
        public void ShipLabelPrefix()
        {
            var ship = new ShipStatus();
            ship.SpecialAttack.Trigger(100);
            PAssert.That(() => ship.ShipLabelPrefix   == "+", "ネルソンタッチ");
            PAssert.That(() => ship.FleetSourcePrefix == "", "ネルソンタッチ");

            ship.SpecialAttack.Fire = false;
            PAssert.That(() => ship.ShipLabelPrefix   == "-", "ネルソンタッチ実行後");
            PAssert.That(() => ship.FleetSourcePrefix == "", "ネルソンタッチ実行後");

            ship.SpecialAttack.Count = 0;
            ship.SpecialAttack.Trigger(104);
            PAssert.That(() => ship.ShipLabelPrefix   == "+", "金剛僚艦夜戦突撃1回目");
            PAssert.That(() => ship.FleetSourcePrefix == "", "金剛僚艦夜戦突撃1回目");

            ship.SpecialAttack.Fire = false;
            PAssert.That(() => ship.ShipLabelPrefix   == ">", "金剛僚艦夜戦突撃1回目実行後");
            PAssert.That(() => ship.FleetSourcePrefix == "", "金剛僚艦夜戦突撃1回目実行後");

            ship.SpecialAttack.Trigger(104);
            PAssert.That(() => ship.ShipLabelPrefix   == "+", "金剛僚艦夜戦突撃2回目");
            PAssert.That(() => ship.FleetSourcePrefix == "", "金剛僚艦夜戦突撃2回目");

            ship.SpecialAttack.Fire = false;
            PAssert.That(() => ship.ShipLabelPrefix   == "-", "金剛僚艦夜戦突撃2回目実行後");
            PAssert.That(() => ship.FleetSourcePrefix == "", "金剛僚艦夜戦突撃2回目実行後");

            ship.Escaped = true;
            PAssert.That(() => ship.ShipLabelPrefix   == "[避]-", "退避");
            PAssert.That(() => ship.FleetSourcePrefix == "[避]", "退避");

            ship.Escaped = false;
            ship.SpecialAttack.Count = 0;
            ship.Items.Add(new ItemStatus { Spec = new ItemSpec { Id = 42 } });
            PAssert.That(() => ship.ShipLabelPrefix   == "", "ダメコン装備、耐久全快");
            PAssert.That(() => ship.FleetSourcePrefix == "", "ダメコン装備、耐久全快");

            ship.MaxHp = 32;
            ship.NowHp = 8;
            PAssert.That(() => ship.ShipLabelPrefix   == "[ダ]", "ダメコン装備大破");
            PAssert.That(() => ship.FleetSourcePrefix == "", "ダメコン装備大破");

            ship.Items.Clear();
            ship.Items.Add(new ItemStatus { Spec = new ItemSpec { Id = 43 } });
            PAssert.That(() => ship.ShipLabelPrefix   == "[メ]", "女神装備大破");
            PAssert.That(() => ship.FleetSourcePrefix == "", "女神装備大破");

            ship.Escaped = true;
            PAssert.That(() => ship.ShipLabelPrefix   == "[避]", "ダメコンより退避優先");
            PAssert.That(() => ship.FleetSourcePrefix == "[避]", "ダメコンより退避優先");
        }

        /// <summary>
        /// 損傷状態の配色を調べる
        /// </summary>
        [TestMethod]
        public void DamageColor()
        {
            var initColor = Control.DefaultBackColor;
            var ship = new ShipStatus { NowHp = 31, MaxHp = 32 };
            PAssert.That(() => ship.DamageColor(initColor) == Control.DefaultBackColor);

            ship.InNDock = true;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.LightGreen);

            ship.InNDock = false;
            ship.NowHp = 24;
            PAssert.That(() => ship.DamageColor(initColor) == Color.FromArgb(240, 240, 0));

            ship.InNDock = true;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.LightGreen);

            ship.InNDock = false;
            ship.NowHp = 16;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.Orange);

            ship.InNDock = true;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.LightGreen);

            ship.InNDock = false;
            ship.NowHp = 8;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.Red);

            ship.InNDock = true;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.LightGreen);

            ship.InNDock = false;
            ship.NowHp = 0;
            PAssert.That(() => ship.DamageColor(initColor) == Color.CornflowerBlue);

            ship.InNDock = true;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.LightGreen);


            ship.InNDock = false;
            ship.Escaped = true;
            ship.NowHp = 31;
            PAssert.That(() => ship.DamageColor(initColor) == Control.DefaultBackColor);

            ship.NowHp = 8;
            PAssert.That(() => ship.DamageColor(initColor) == CUDColors.LightGray);
        }

        /// <summary>
        /// cond状態の配色を調べる
        /// </summary>
        [TestMethod]
        public void CondColor()
        {
            var initColor = Control.DefaultBackColor;
            var ship = new ShipStatus { Cond = 49 };
            PAssert.That(() => ship.CondColor(initColor) == Control.DefaultBackColor);

            ship.Cond = 29;
            PAssert.That(() => ship.CondColor(initColor) == CUDColors.Orange);

            ship.Cond = 19;
            PAssert.That(() => ship.CondColor(initColor) == CUDColors.Red);

            ship.Cond = 50;
            PAssert.That(() => ship.CondColor(initColor) == CUDColors.Yellow);
        }

        /// <summary>
        /// 装備スロットの状況を調べる
        /// </summary>
        [TestMethod]
        public void SlotStatus()
        {
            var ship = new ShipStatus
            {
                Id = 1,
                Spec = new ShipSpec {SlotNum = 3}
            };
            ship.SetItems([-1, -1, -1]);
            PAssert.That(() => (int)ship.GetSlotStatus() == 2, "NormalEmpty");
            ship.SetItems([-1, -1, -1], -1);
            PAssert.That(() => (int)ship.GetSlotStatus() == (2 | 4), "| ExtraEmpty");
            ship.SetItems([-1, -1, -1], 1);
            PAssert.That(() => (int)ship.GetSlotStatus() == 2, "NormalEmpty");
            ship.SetItems([1, 1, 1], 1);
            PAssert.That(() => ship.GetSlotStatus() == 0, "Equipped");
            ship.SetItems([1, 1, -1], 1);
            PAssert.That(() => (int)ship.GetSlotStatus() == 1, "SemiEquipped");
            ship.Spec.SlotNum = 2;
            PAssert.That(() => ship.GetSlotStatus() == 0, "Equipped");
            ship.Spec.SlotNum = 0;
            PAssert.That(() => ship.GetSlotStatus() == 0, "Equipped (まるゆ)");
        }

        /// <summary>
        /// 修復所要時間を調べる
        /// </summary>
        [TestMethod]
        public void DisplayRepairTime()
        {
            var ship = new ShipStatus { Spec = new ShipSpec { ShipType = SS }, Level = 1, NowHp = 6, MaxHp = 7 };
            PAssert.That(() => ship.DisplayRepairTime() == "00:00:35");

            ship = new ShipStatus { Spec = new ShipSpec { ShipType = DD }, Level = 50, NowHp = 24, MaxHp = 32 };
            PAssert.That(() => ship.DisplayRepairTime() == "00:48:30");

            ship = new ShipStatus { Spec = new ShipSpec { ShipType = CVL }, Level = 99, NowHp = 30, MaxHp = 60 };
            PAssert.That(() => ship.DisplayRepairTime() == "07:56:45");

            ship = new ShipStatus { Spec = new ShipSpec { ShipType = BB }, Level = 150, NowHp = 1, MaxHp = 99 };
            PAssert.That(() => ship.DisplayRepairTime() == "49:33:10");
        }

        [TestMethod]
        public void CalcSmokeScreenTriggerRates()
        {
            var shipInfo = NewShipInfo();
            var ship = NewShip(1);
            var ship0 = NewShip(2);
            ship.Lucky = 10;
            ship0.Lucky = 50;
            shipInfo.Fleets[0].SetShips(ship0.Id, ship.Id);
            ship.SetItems(SmokeGenerator());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.6, 0.4, 0.0, 0.0),
                "旗艦運50随伴煙幕★0で一重のみ");
            ship.Escaped = true;
            shipInfo.Fleets[0].State = FleetState.Sortie;
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).ParamsEqual(), "退避艦が装備したものは無効");

            ship.Escaped = false;
            shipInfo.Fleets[0].State = FleetState.Port;
            shipInfo.Fleets[0].SetShips(ship.Id);
            ship.SetItems(SmokeGenerator(9));
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).ParamsEqual(), "運10煙幕★9で不発");
            ship.SetItems(SmokeGenerator(10));
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.8, 0.2, 0.0, 0.0),
                "運10煙幕★10で一重のみ");

            ship.SetItems(SmokeGenerator());
            ship.Lucky = 36;
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).ParamsEqual(), "運36煙幕★0で不発");
            ship.Lucky = 37;
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.8, 0.2, 0.0, 0.0),
                "運37煙幕★0で一重のみ");

            ship.Lucky = 10;
            ship.SetItems(SmokeGenerator());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).ParamsEqual(), "運10煙幕★0で不発");
            ship.SetItems(SmokeGeneratorK());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.4, 0.414, 0.186, 0.0),
                "運10煙幕改★0で二重可能");
            ship.SetItems(SmokeGenerator(), SmokeGenerator());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.4, 0.414, 0.186, 0.0),
                "運10煙幕★0二個で二重可能");

            ship.SetItems(SmokeGeneratorK(), SmokeGenerator());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.0, 0.54, 0.30, 0.16),
                "運10煙幕★0煙幕改★0で三重可能");
            ship.SetItems(SmokeGeneratorK(), SmokeGeneratorK());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.0, 0.39, 0.30, 0.31),
                "運10煙幕改★0二個で三重可能");

            ship.SetItems(SmokeGenerator(8), SmokeGenerator(9));
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.0, 0.54, 0.30, 0.16),
                "運10煙幕二個★17で三重可能");
            ship.Lucky = 63;
            ship.SetItems(SmokeGeneratorK(), SmokeGeneratorK(2), SmokeGenerator(10));
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.0, 0.0, 0.21, 0.79),
                "雪風改二に煙幕最大搭載");


            var ship1 = NewShip(1);
            var ship2 = NewShip(2);
            var ship3 = NewShip(3);
            ship1.Lucky = 9;
            ship2.Lucky = 16;
            ship3.Lucky = 25;
            shipInfo.Fleets[0].CombinedType = shipInfo.Fleets[1].CombinedType = CombinedType.Transport;
            shipInfo.Fleets[0].SetShips(ship1.Id, ship2.Id);
            shipInfo.Fleets[1].SetShips(ship3.Id);

            ship3.SetItems(SmokeGeneratorK());
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.6, 0.276, 0.124, 0.0),
                "運9連合第二に煙幕改★0で二重可能");
            ship2.SetItems(SmokeGenerator(10));
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.0, 0.45, 0.30, 0.25),
                "運9連合第一に煙幕★10、第二に煙幕改★0で二重可能");
            ship2.Escaped = true;
            shipInfo.Fleets[0].State = shipInfo.Fleets[1].State = FleetState.Sortie;
            PAssert.That(() => shipInfo.CalcSmokeScreenTriggerRates(0).Select(rate => Math.Round(rate, 3)).ParamsEqual(0.6, 0.276, 0.124, 0.0),
                "退避艦が装備したものは無効");
        }

        [TestMethod]
        public void PreparedDamageControl()
        {
            var ship = NewShip(38);
            ship.MaxHp = 16;
            ship.SetItems(三式水中探信儀(), Damecon());

            ship.NowHp = 5;
            PAssert.That(() => ship.PreparedDamageControl == -1, "大破してない");

            ship.NowHp = 4;
            PAssert.That(() => ship.PreparedDamageControl == 42, "応急修理要員が選択される");

            ship.SetItems(三式水中探信儀(), 三式水中探信儀());
            PAssert.That(() => ship.PreparedDamageControl == -1, "ダメコンを装備してない");

            ship.SetItems(Megami(), Damecon());
            PAssert.That(() => ship.PreparedDamageControl == 43, "応急修理女神が選択される");

            ship.SetItems([三式水中探信儀(), Megami()], Damecon());
            PAssert.That(() => ship.PreparedDamageControl == 42, "応急修理要員が選択される");
        }
    }
}
