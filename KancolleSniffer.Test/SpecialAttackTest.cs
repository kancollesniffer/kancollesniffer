// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class SpecialAttackTest
    {
        /// <summary>
        /// Nelson Touchに対応する
        /// </summary>
        [TestMethod]
        public void NelsonTouch()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("nelsontouch_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 100);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 100);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // ship_deckでフラグを引き継ぐ
            sniffer.SniffLogFile("nelsontouch_002");
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 100);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // 夜戦
            var night = new TestingSniffer();
            night.SniffLogFile("nelsontouch_003");
            PAssert.That(() => night.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => night.Battle.Result.Friend.Main[0].SpecialAttack.Type == 100);
            PAssert.That(() => night.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            // 海戦をまたがってフラグを引き継ぐ
            var fired = new TestingSniffer();
            fired.SniffLogFile("nelsontouch_004");
            PAssert.That(() => !fired.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => fired.Fleets[0].Ships[0].SpecialAttack.Type == 100);
            PAssert.That(() => fired.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 長門改二の一斉射に対応する
        /// </summary>
        [TestMethod]
        public void NagatoSpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("nagatospecial_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 101);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 101);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 陸奥改二の一斉射に対応する
        /// </summary>
        [TestMethod]
        public void MutsuSpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("mutsuspecial_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 102);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 102);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// Coloradoの一斉射に対応する
        /// </summary>
        [TestMethod]
        public void ColoradoSpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("coloradospecial_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 103);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 103);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 連合艦隊時の僚艦夜戦突撃に対応する
        /// </summary>
        [TestMethod]
        public void KongoSpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("kongospecial_101");
            PAssert.That(() => !sniffer.Fleets[1].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Type == 0);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Count == 0);

            var dayResult = sniffer.Battle.Result;
            PAssert.That(() => !dayResult.Friend.Guard[0].SpecialAttack.Fire);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Type == 0);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Count == 0);

            sniffer.SniffLogFile("kongospecial_102");
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Count == 1);

            PAssert.That(() => !dayResult.Friend.Guard[0].SpecialAttack.Fire);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Type == 0);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Count == 0);
            var nightResult = sniffer.Battle.Result;
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Fire);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Type == 104);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Count == 1);

            sniffer.SniffLogFile("kongospecial_103");
            PAssert.That(() => !sniffer.Fleets[1].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[1].Ships[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.IsBattleResultError);

            PAssert.That(() => !dayResult.Friend.Guard[0].SpecialAttack.Fire);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Type == 0);
            PAssert.That(() => dayResult.Friend.Guard[0].SpecialAttack.Count == 0);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Fire);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Type == 104);
            PAssert.That(() => nightResult.Friend.Guard[0].SpecialAttack.Count == 1);
            var result = sniffer.Battle.Result;
            PAssert.That(() => result.Friend.Guard[0].SpecialAttack.Fire);
            PAssert.That(() => result.Friend.Guard[0].SpecialAttack.Type == 104);
            PAssert.That(() => result.Friend.Guard[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 連合艦隊時の僚艦夜戦突撃2発目に対応する
        /// </summary>
        [TestMethod]
        public void KongoSpecial2()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("kongospecial_002_1");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // ship_deckでフラグを引き継ぐ
            sniffer.SniffLogFile("kongospecial_002_2");
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
            // 2発目
            sniffer.SniffLogFile("kongospecial_002_3");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 2);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 104);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 2);
        }

        /// <summary>
        /// 大和型改二の特殊砲撃(2隻)に対応する
        /// </summary>
        [TestMethod]
        public void YamatoSpecial2Ships()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("yamatospecial_2ships_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 401);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 401);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 大和型改二の特殊砲撃(3隻)に対応する
        /// </summary>
        [TestMethod]
        public void YamatoSpecial3Ships()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("yamatospecial_3ships_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 400);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 400);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// リシュリュー級特殊砲撃を確認する
        /// </summary>
        [TestMethod]
        public void RichelieuSpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("richelieu_special-101");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 105);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 105);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// QE級特殊砲撃を確認する
        /// </summary>
        [TestMethod]
        public void QESpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("qe_special-101");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Type == 106);
            PAssert.That(() => sniffer.Battle.Result.Friend.Main[0].SpecialAttack.Count == 1);
            PAssert.That(() => !sniffer.Fleets[0].Ships[0].SpecialAttack.Fire);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Type == 106);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].SpecialAttack.Count == 1);
        }

        /// <summary>
        /// 潜水艦特殊攻撃で潜水艦補給物資を消費する
        /// </summary>
        [TestMethod]
        public void SubmarineSpecial()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("submarine_special_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(34).ToString() == "潜水艦補給物資 x18");

            sniffer.SniffLogFile("submarine_special_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(34).ToString() == "潜水艦補給物資 x17");
        }
    }
}
