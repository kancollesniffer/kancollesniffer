// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class AirContactRatesTest : ApiStart2
    {
        [TestMethod]
        public void 合計夜間接触確率()
        {
            var fleet = NewFleet();
            var ship1 = NewShip(662);
            ship1.Level = 99;
            var ship2 = NewShip(662);
            ship2.Level = 60;

            ship1.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(2));
            ship2.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1));
            fleet.SetShips(ship1.Id, ship2.Id);
            PAssert.That(() => AirContactRates.Calc(fleet, null, false).Night.Select5 * 100 == 84.64, "2隻に夜偵1スロットずつ");

            ship1.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(2));
            ship2.SetItems(_15_5cm三連装副砲());
            PAssert.That(() => AirContactRates.Calc(fleet, null, false).Night.Select5 * 100 == 68, "1隻目だけに夜偵1スロット");

            ship1.SetItems(_15_5cm三連装副砲());
            ship2.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1));
            PAssert.That(() => AirContactRates.Calc(fleet, null, false).Night.Select5 * 100 == 52, "2隻目だけに夜偵1スロット");

            ship1.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1), 九八式水上偵察機夜偵(1));
            ship2.SetItems(_15_5cm三連装副砲());
            PAssert.That(() => AirContactRates.Calc(fleet, null, false).Night.Select5 * 100 == 89.76, "1隻目だけに夜偵2スロット");

            ship1.SetItems(_15_5cm三連装副砲(), 零式水上偵察機11型乙改夜偵(1));
            ship2.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1));
            var contactRate = AirContactRates.Calc(fleet);
            PAssert.That(() => contactRate.Night.Select9 * 100 == 0, "夜偵+9は未搭載");
            PAssert.That(() => contactRate.Night.Select7 * 100 == 88, "夜偵+7と");
            PAssert.That(() => Math.Round(contactRate.Night.Select5 * 100, 2) == 6.24, "夜偵+5の混在");
            PAssert.That(() => contactRate.Night.Fail * 100 == 5.76, "+7と+5と不発率の合計は100");

            ship1.SetItems(_15_5cm三連装副砲(), 零式水上偵察機11型乙改夜偵(1), Loire130M改熟練(1));
            ship2.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1));
            contactRate = AirContactRates.Calc(fleet);
            PAssert.That(() => contactRate.Night.Select9 * 100 == 88, "夜偵+9と");
            PAssert.That(() => contactRate.Night.Select7 * 100 == 10.56, "夜偵+7と");
            PAssert.That(() => Math.Round(contactRate.Night.Select5 * 100, 2) == 0.75, "夜偵+5の混在");
            PAssert.That(() => Math.Round(contactRate.Night.Fail * 100, 2) == 0.69, "+9と+7と+5と不発率の合計は100");

            ship1.Level = 175;
            ship1.SetItems(_15_5cm三連装副砲(), 零式水上偵察機11型乙改夜偵(1));
            ship2.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1));
            contactRate = AirContactRates.Calc(fleet);
            PAssert.That(() => contactRate.Night.Select9 * 100 == 0, "夜偵+9は未搭載");
            PAssert.That(() => contactRate.Night.Select7 * 100 == 100, "Lv125以上でも表示は100%で止める");
            PAssert.That(() => contactRate.Night.Select5 * 100 == 0, "11型夜偵ですでに100%なので、九八式夜偵は0%");

            ship1.SetItems(_15_5cm三連装副砲(), 零式水上偵察機11型乙改夜偵(1), Loire130M改熟練(1));
            ship2.SetItems(_15_5cm三連装副砲(), 九八式水上偵察機夜偵(1));
            contactRate = AirContactRates.Calc(fleet);
            PAssert.That(() => contactRate.Night.Select9 * 100 == 100, "Lv125以上でも表示は100%で止める");
            PAssert.That(() => contactRate.Night.Select7 * 100 == 0, "Loire130M改熟練ですでに100%なので、11型夜偵は0%");
            PAssert.That(() => contactRate.Night.Select5 * 100 == 0, "Loire130M改熟練ですでに100%なので、九八式夜偵は0%");

            ship1.Level = 99;
            fleet.SetShips(ship1.Id);
            fleet.CombinedType = CombinedType.Carrier;
            var fleet2 = NewFleet(1);
            fleet2.SetShips(ship2.Id);
            fleet2.CombinedType = CombinedType.Carrier;
            PAssert.That(() => AirContactRates.Calc(fleet, fleet2, false).Night.Select5 * 100 == 52, "連合艦隊では護衛の夜偵のみ有効");
        }

        [TestMethod]
        public void 触接()
        {
            var ship1 = NewShip(321);
            ship1.SetItems(零式水上偵察機11型乙(1));
            var fleet1 = NewFleet();
            fleet1.SetShips(ship1.Id);
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 28.0/17.5/12.7"
                        + "\n触接17%: 12.0/6.5/4.2");

            ship1.SetItems(零式水上偵察機11型乙(2));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 36.0/22.5/16.3"
                        + "\n触接17%: 15.4/8.4/5.4"
                        , "機数で開始率上昇");

            ship1.SetItems(零式水上偵察機11型乙(2), 零式水上偵察機11型乙(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 60.0/37.5/27.2"
                        + "\n触接17%: 40.4/22.8/15.1"
                        , "複数スロットで選択率上昇");

            ship1.SetItems(零式水上偵察機11型乙(20));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 108.0/67.5/49.0"
                        + "\n触接17%: 42.8/25.3/16.3"
                        , "確保で開始率100%超え");

            ship1.SetItems(零式水上偵察機11型乙(30));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 132.0/82.5/60.0"
                        + "\n触接17%: 42.8/30.9/19.9"
                        , "確保で開始率100%超えからさらに上昇しても、選択率は変わらず");

            ship1.SetItems(零式水上偵察機11型乙(0));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString() == "触接開始: 0.0");

            ship1.SetItems(九七式艦攻(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 4.0/2.5/1.8"
                        + "\n触接12%: 0.2/0.1/0.1"
                        , "艦攻だけでも低確率で触接可能");

            ship1.SetItems(九七式艦攻(2));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 4.0/2.5/1.8"
                        + "\n触接12%: 0.2/0.1/0.1"
                        , "艦攻は機数が増えても開始率は上昇しない");

            ship1.SetItems(九七式艦攻(2), 九七式艦攻(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 4.0/2.5/1.8"
                        + "\n触接12%: 0.5/0.3/0.1"
                        , "艦攻でも複数スロットで選択率は上昇");

            ship1.SetItems(九七式艦攻(0));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString() == "触接開始: 0.0");

            ship1.SetItems(Ju87C改二KMX搭載機(1), 瑞雲(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString() == "触接開始: 0.0", "艦爆瑞雲は触接不可");

            ship1.SetItems(零式水上偵察機11型乙(1), Ju87C改二KMX搭載機(1), 瑞雲(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 28.0/17.5/12.7"
                        + "\n触接17%: 12.0/6.5/4.2"
                        , "艦爆瑞雲は選択も不可");


            ship1.SetItems(零式水上偵察機11型乙(1), 九八式水上偵察機夜偵(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 40.0/25.0/18.1"
                        + "\n触接17%: 17.1/9.3/6.0"
                        + "\n触接12%: 4.8/2.9/2.0"
                        + "\n触接失敗: 77.9/87.6/91.9"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "触接効果が複数の場合は失敗率を表示");

            ship1.SetItems(零式水上偵察機11型乙(1), 九八式水上偵察機夜偵(1), TBM3W3S(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 40.0/25.0/18.1"
                        + "\n触接20%: 28.5/15.6/10.1"
                        + "\n触接17%: 4.8/3.5/2.6"
                        + "\n触接12%: 1.3/1.0/0.8"
                        + "\n触接失敗: 65.1/79.7/86.3"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "触接効果3種全表示");

            ship1.SetItems(零式水上偵察機11型乙(1));
            var ship2 = NewShip(321);
            ship2.SetItems(九八式水上偵察機夜偵(1), TBM3W3S(1));
            var fleet2 = NewFleet(1);
            fleet2.SetShips(ship2.Id);
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, false).ToString()
                        == "触接開始: 28.0/17.5/12.7"
                        + "\n触接17%: 12.0/6.5/4.2"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "連合で敵通常の場合は本隊のみ有効");
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, true).ToString()
                        == "触接開始: 40.0/25.0/18.1"
                        + "\n触接20%: 28.5/15.6/10.1"
                        + "\n触接17%: 4.8/3.5/2.6"
                        + "\n触接12%: 1.3/1.0/0.8"
                        + "\n触接失敗: 65.1/79.7/86.3"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "連合で敵連合の場合は護衛も有効");

            ship1.SetItems(九八式水上偵察機夜偵(1), TBM3W3S(1));
            ship2.SetItems(零式水上偵察機11型乙(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, false).ToString()
                        == "触接開始: 16.0/10.0/7.2"
                        + "\n触接20%: 11.4/6.2/4.0"
                        + "\n触接12%: 0.9/0.7/0.5"
                        + "\n触接失敗: 87.5/93.0/95.4"
                        , "連合では護衛の夜偵のみ有効");
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, true).ToString()
                        == "触接開始: 40.0/25.0/18.1"
                        + "\n触接20%: 28.5/15.6/10.1"
                        + "\n触接17%: 4.8/3.5/2.6"
                        + "\n触接12%: 1.3/1.0/0.8"
                        + "\n触接失敗: 65.1/79.7/86.3"
                        , "連合では敵連合でも護衛の夜偵のみ有効");

            ship1.SetItems();
            ship2.SetItems(九八式水上偵察機夜偵(1), TBM3W3S(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, false).ToString()
                        == "触接開始: 0.0"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "連合で敵通常または開幕夜戦のときは夜偵を表示");
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, true).ToString()
                        == "触接開始: 16.0/10.0/7.2"
                        + "\n触接20%: 11.4/6.2/4.0"
                        + "\n触接12%: 0.9/0.7/0.5"
                        + "\n触接失敗: 87.5/93.0/95.4"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "連合かつ敵連合でようやく触接可能");


            var itemId = 零式水上偵察機11型乙(20);
            var item = ItemInventory[itemId];
            item.Spec = new ItemSpec
            {
                Type = 10,
                LoS = 14,
                Accuracy = 3
            };
            ship1.SetItems(itemId, 零式水上偵察機11型乙(1));
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 276.0/172.5/125.4"
                        + "\n触接20%: 100.0/87.5/77.7"
                        + "\n触接17%: 0.0/4.6/7.4"
                        + "\n触接失敗: 0.0/7.8/14.8"
                        , "確保で選択率100%なら以降は0%表示");

            item.Spec.LoS = 18;
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 348.0/217.4/158.1"
                        + "\n触接20%: 100.0/100.0/100.0"
                        , "劣勢でも選択率100%ならそこで表示終了");

            item.Spec.Accuracy = 0;
            PAssert.That(() => AirContactRates.Calc(fleet1, null, false).ToString()
                        == "触接開始: 348.0/217.4/158.1"
                        + "\n触接17%: 42.8/37.5/33.3"
                        + "\n触接12%: 57.1/62.5/66.6"
                        , "劣勢でも選択率合計100%ならそこで表示終了し、触接失敗を表示しない");

            ship1.Escaped = true;
            ship2.Escaped = false;
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, true).ToString()
                        == "触接開始: 16.0/10.0/7.2"
                        + "\n触接20%: 11.4/6.2/4.0"
                        + "\n触接12%: 0.9/0.7/0.5"
                        + "\n触接失敗: 87.5/93.0/95.4"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "退避艦の機体での触接は無効");

            ship1.Escaped = false;
            ship2.Escaped = true;
            PAssert.That(() => AirContactRates.Calc(fleet1, fleet2, true).ToString()
                        == "触接開始: 348.0/217.4/158.1"
                        + "\n触接17%: 42.8/37.5/33.3"
                        + "\n触接12%: 57.1/62.5/66.6"
                        + "\n夜偵: 0.0/0.0/68.0/31.9"
                        , "退避艦の夜偵はなぜか有効");
        }
    }
}
