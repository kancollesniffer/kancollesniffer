// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestFleetCheckerSortie3Test : QuestCounterTester
    {
        [TestInitialize]
        public void Initialize()
        {
            battleInfo.InjectDeck(1);
        }

        /// <summary>
        /// 1005: 精強「第七駆逐隊」緊急出動！
        /// </summary>
        [TestMethod]
        public void BattleResult_1005()
        {
            var count = InjectQuest(1005);
            var oboro     = NewShip(93);
            var oboro1    = NewShip(230);
            var akebono   = NewShip(15);
            var akebono1  = NewShip(231);
            var akebono2  = NewShip(665);
            var sazanami  = NewShip(94);
            var sazanami1 = NewShip(232);
            var ushio     = NewShip(16);
            var ushio1    = NewShip(233);
            var ushio2    = NewShip(407);

            battleInfo.InjectFleet(oboro1, sazanami1, akebono2, ushio2);
            InjectMapNext(12, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "B勝はカウントしない");

            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "誤出撃はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0), "1-2");

            battleInfo.InjectFleet(oboro1, sazanami1, akebono1, ushio2);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "1-3");

            battleInfo.InjectFleet(oboro1, sazanami1, akebono2, ushio1);
            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0), "1-5");

            battleInfo.InjectFleet(oboro1, sazanami1, akebono1, ushio1);
            InjectMapNext(32, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "3-2");

            battleInfo.InjectFleet(oboro, sazanami1, akebono1, ushio1);
            InjectMapNext(32, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "未改はカウントしない");

            battleInfo.InjectFleet(oboro1, sazanami, akebono1, ushio1);
            InjectMapNext(32, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "未改はカウントしない");

            battleInfo.InjectFleet(oboro1, sazanami1, akebono, ushio1);
            InjectMapNext(32, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "未改はカウントしない");

            battleInfo.InjectFleet(oboro1, sazanami1, akebono1, ushio);
            InjectMapNext(32, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1), "未改はカウントしない");
        }

        /// <summary>
        /// 1010: 【期間限定任務】対潜掃討作戦
        /// </summary>
        [TestMethod]
        public void BattleResult_1010()
        {
            var count = InjectQuest(1010);
            var shimushu  = NewShip(517);
            var kunashiri = NewShip(518);
            var etorofu   = NewShip(524);
            var kamikaze  = NewShip(471);
            var harukaze  = NewShip(473);
            var hatakaze  = NewShip(475);
            var tenryu    = NewShip(51);

            battleInfo.InjectFleet(shimushu, kunashiri, etorofu);
            InjectMapNext(15, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0), "ボス以外はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0), "A勝はカウントしない");

            InjectMapNext(14, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0), "誤出撃はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0), "1-5");

            InjectMapNext(16, 8);
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1), "1-6");

            battleInfo.InjectFleet(kamikaze, harukaze, hatakaze);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(2, 1), "1-5");

            battleInfo.InjectFleet(tenryu, kamikaze, shimushu, kunashiri);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1), "1-5");

            battleInfo.InjectFleet(kamikaze, etorofu, tenryu);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1), "指定艦不足");

            battleInfo.InjectFleet(shimushu, kunashiri, tenryu);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1), "指定艦不足");

            battleInfo.InjectFleet(harukaze, hatakaze, tenryu);
            InjectMapNext(15, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(3, 1), "指定艦不足");
        }

        /// <summary>
        /// 1011: 【期間限定任務】精強海防艦、緊急近海防衛！
        /// </summary>
        [TestMethod]
        public void BattleResult_1011()
        {
            var count = InjectQuest(1011);
            var shimushu  = NewShip(517);
            var kunashiri = NewShip(518);
            var etorofu   = NewShip(524);
            var kamikaze  = NewShip(471);
            var tenryu    = NewShip(51);

            battleInfo.InjectFleet(shimushu, kunashiri, etorofu);
            InjectMapNext(11, 4);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(11, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "B勝はカウントしない");

            InjectMapNext(14, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0, 0), "誤出撃はカウントしない");

            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0, 0), "1-1");

            battleInfo.InjectFleet(etorofu, shimushu, kunashiri, kamikaze);
            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0, 0), "1-2");

            battleInfo.InjectFleet(etorofu, tenryu, shimushu, kunashiri);
            InjectMapNext(13, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0, 0), "1-3");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 0), "1-5");

            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "2-1");

            battleInfo.InjectFleet(kamikaze, etorofu, shimushu, kunashiri);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "旗艦指定間違い");

            battleInfo.InjectFleet(etorofu, kunashiri, kamikaze);
            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1, 1), "指定艦不足");
        }

        /// <summary>
        /// 1012: // 鵜来型海防艦、静かな海を防衛せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_1012()
        {
            var count = InjectQuest(1012);
            var ukuru     = NewShip(921);
            var shimushu  = NewShip(517);
            var kunashiri = NewShip(518);
            var etorofu   = NewShip(524);
            var matsuwa   = NewShip(525);
            var kamikaze  = NewShip(471);

            battleInfo.InjectFleet(ukuru, shimushu);
            InjectMapNext(11, 4);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "ボス以外はカウントしない");

            InjectMapNext(11, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "A勝はカウントしない");

            InjectMapNext(13, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0), "誤出撃はカウントしない");

            InjectMapNext(11, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "1-1");

            InjectMapNext(15, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0), "B勝はカウントしない");

            InjectMapNext(15, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0), "1-5");

            InjectMapNext(12, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0), "B勝はカウントしない");

            InjectMapNext(12, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "1-2");

            battleInfo.InjectFleet(etorofu, shimushu, kunashiri);
            InjectMapNext(11, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "旗艦指定間違い");

            battleInfo.InjectFleet(ukuru);
            InjectMapNext(11, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "指定艦不足");

            battleInfo.InjectFleet(ukuru, etorofu, shimushu, kunashiri, matsuwa);
            InjectMapNext(11, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "指定艦過剰");

            battleInfo.InjectFleet(ukuru, shimushu, kamikaze);
            InjectMapNext(11, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1), "海防艦以外が含まれる");
        }

        /// <summary>
        /// 1018: 「第三戦隊」第二小隊、鉄底海峡へ！
        /// </summary>
        [TestMethod]
        public void BattleResult_1018()
        {
            var count = InjectQuest(1018);
            var hiei      = NewShip(86);
            var kirishima = NewShip(85);
            var escort    = NewShip(517);
            var destroyer = NewShip(1);

            battleInfo.InjectFleet(hiei, kirishima, destroyer);
            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "駆逐が不足");

            battleInfo.InjectFleet(hiei, kirishima, destroyer, escort);
            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "海防は無関係");

            battleInfo.InjectFleet(kirishima, destroyer, destroyer);
            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "比叡不在");

            battleInfo.InjectFleet(hiei, destroyer, destroyer);
            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "霧島不在");

            battleInfo.InjectFleet(hiei, kirishima, destroyer, destroyer);
            InjectMapNext(51, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "A勝が取れなかった");

            InjectMapNext(52, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "5-2は関係なし");

            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0));

            battleInfo.InjectFleet(escort, hiei, kirishima, destroyer, destroyer);
            InjectMapNext(53, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0), "旗艦指定はなし");

            InjectMapNext(54, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            InjectMapNext(55, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }

        /// <summary>
        /// 1022: 【期間限定任務】「三十二駆」月次戦闘哨戒！
        /// </summary>
        [TestMethod]
        public void BattleResult_1022()
        {
            var count = InjectQuest(1022);
            var tamanami  = NewShip(674);
            var suzunami  = NewShip(675);
            var fujinami  = NewShip(485);
            var hayanami  = NewShip(528);
            var hamanami  = NewShip(484);

            var makinami  = NewShip(671);
            var kishinami = NewShip(527);

            battleInfo.InjectFleet(tamanami, suzunami);
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "指定艦が不足");

            battleInfo.InjectFleet(tamanami, suzunami, fujinami);
            InjectMapNext(23, 5);
            InjectBattleResult("B");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "B勝は不可");

            InjectMapNext(21, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "2-1は無関係");

            battleInfo.InjectFleet(hamanami, tamanami, makinami);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "巻波は艦違い");

            battleInfo.InjectFleet(suzunami, fujinami, kishinami);
            InjectMapNext(23, 5);
            InjectBattleResult("S");
            PAssert.That(() => count.NowArray.ParamsEqual(0, 0, 0, 0), "岸波は艦違い");

            battleInfo.InjectFleet(tamanami, suzunami, fujinami);
            InjectMapNext(23, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 0, 0, 0));

            battleInfo.InjectFleet(hayanami, hamanami, fujinami);
            InjectMapNext(41, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 0, 0));

            battleInfo.InjectFleet(hayanami, hamanami, tamanami);
            InjectMapNext(51, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 0));

            battleInfo.InjectFleet(suzunami, hamanami, tamanami);
            InjectMapNext(71, 5);
            InjectBattleResult("A");
            PAssert.That(() => count.NowArray.ParamsEqual(1, 1, 1, 1));
        }
    }
}
