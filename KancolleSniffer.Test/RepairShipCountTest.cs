// Copyright (C) 2019 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.View.MainWindow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class RepairShipCountTest
    {
        [TestMethod]
        public void TestCreateRepairShipCountString()
        {
            ShipStatus[] repairList = [];
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "なし");

            const int max = 31;
            const int minor = 30;
            const int small = (int)(max * 0.75);
            const int half  = (int)(max * 0.5);
            const int badly = (int)(max * 0.25);

            repairList =
            [
                new ShipStatus {NowHp = minor, MaxHp = max},
                new ShipStatus {NowHp = minor, MaxHp = max},
                new ShipStatus {NowHp = minor, MaxHp = max},
                new ShipStatus {NowHp = minor, MaxHp = max}
            ];
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "軽微 4");

            repairList[0].NowHp = small;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "軽微 3\n小破 1\n　計 4");

            repairList[1].NowHp = half;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "軽微 2\n小破 1\n　計 3\n中破 1\n　計 1");

            repairList[2].NowHp = badly;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "軽微 1\n小破 1\n　計 2\n中破 1\n大破 1\n　計 2");

            repairList[3].NowHp = small;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "小破 2\n　計 2\n中破 1\n大破 1\n　計 2");

            repairList[1].NowHp = badly;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "小破 2\n大破 2");

            repairList[0].NowHp = half;
            repairList[3].NowHp = half;
            repairList[1].NowHp = badly;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "中破 2\n大破 2\n　計 4");

            repairList[0].NowHp = badly;
            repairList[3].NowHp = badly;
            PAssert.That(() => new RepairShipCount(repairList).ToString() == "大破 4");
        }
    }
}
