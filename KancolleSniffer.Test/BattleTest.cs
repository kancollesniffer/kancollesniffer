// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class BattleTest
    {
        /// <summary>
        /// 4-2-1で開幕対潜雷撃を含む戦闘を行う
        /// </summary>
        [TestMethod]
        public void NormalBattleWithVariousTypesOfAttack()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("battle_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.A);
            PAssert.That(() => sniffer.Fleets[0].Ships.Select(s => s.NowHp).ParamsEqual(57, 66, 50, 65, 40, 42));
            PAssert.That(() => sniffer.Battle.Result.Enemy.Main.Select(s => s.NowHp).ParamsEqual(34, 5, 0, 0, 0, 0));
        }

        /// <summary>
        /// 開幕夜戦で潜水艦同士がお見合いする
        /// </summary>
        [TestMethod]
        public void SpMidnightWithoutBattle()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("sp_midnight_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.D);
        }

        /// <summary>
        /// 夜戦で戦艦が攻撃すると一回で三発分のデータが来る
        /// そのうち存在しない攻撃はターゲット、ダメージともに-1になる
        /// </summary>
        [TestMethod]
        public void BattleShipAttackInMidnight()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("midnight_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.S);
        }

        /// <summary>
        /// 7隻編成の戦闘で7隻目が攻撃される
        /// </summary>
        [TestMethod]
        public void Ship7Battle()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("ship7battle_001");
            PAssert.That(() => sniffer.Battle.ResultRank == BattleResultRank.P);
        }

        /// <summary>
        /// 友軍航空戦によるダメージを戦果ランクの計算に反映させる
        /// </summary>
        [TestMethod]
        public void FriendlyKouku()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("friendly_kouku_001");
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 航空支援が全機撃墜されてもエラーにならない
        /// </summary>
        [TestMethod]
        public void SupportAiratackAllShootdown()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("support_airatack_stage3_null_101");
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 潜水航空戦でのダメージを戦果ランクの計算に反映させる
        /// </summary>
        [TestMethod]
        public void AswAirBattle()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("asw-airbattle_101");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.Result.Enemy.Main[4].NowHp == -1);
        }

        /// <summary>
        /// 演習のあとのportで戦闘結果の検証を行わない
        /// </summary>
        [TestMethod]
        public void NotVerifyBattleResultAfterPractice()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 演習中の艦を要修復リストに載せない
        /// </summary>
        [TestMethod]
        public void DamagedShipListNotShowShipInPractice()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("practice_003");
            PAssert.That(() => sniffer.RepairList.Select(s => s.Spec.Name).ParamsEqual("飛龍改二", "翔鶴改二"));
        }

        /// <summary>
        /// 連合艦隊が開幕雷撃で被弾する
        /// </summary>
        [TestMethod]
        public void OpeningTorpedoInCombinedBattle()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("combined_battle_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 連合艦隊が閉幕雷撃で被弾する
        /// </summary>
        [TestMethod]
        public void ClosingTorpedoInCombinedBattle()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("combined_battle_002");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 第一が6隻未満の連合艦隊で戦闘する
        /// </summary>
        [TestMethod]
        public void SmallCombinedFleetBattle()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("combined_battle_003");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.CellInfo.Current == "E3 １戦目");
        }

        /// <summary>
        /// 護衛退避する
        /// </summary>
        [TestMethod]
        public void EscapeWithEscort()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("escape_001");
            var fleets = sniffer.Fleets;
            PAssert.That(() => fleets[0].Ships[5].Escaped);
            PAssert.That(() => fleets[1].Ships[2].Escaped);
        }

        /// <summary>
        /// 開幕夜戦に支援が来る
        /// </summary>
        [TestMethod]
        public void SpMidnightSupportAttack()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("sp_midnight_002");
            PAssert.That(() => !sniffer.IsBattleResultError);
            PAssert.That(() => sniffer.Battle.SupportType == 2);
        }

        /// <summary>
        /// 払暁戦を行う
        /// </summary>
        [TestMethod]
        public void NightToDay()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("nighttoday_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 単艦退避する
        /// </summary>
        [TestMethod]
        public void EscapeWithoutEscort()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("escape_002");
            PAssert.That(() => sniffer.Fleets[2].Ships[1].Escaped);
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 装甲破砕を検知する
        /// </summary>
        [TestMethod]
        public void DetectDebuffed()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("detect_debuffed-101");
            PAssert.That(() => !sniffer.Battle.Debuffed, "装甲未破砕");
            sniffer.SniffLogFile("detect_debuffed-201");
            PAssert.That(() => sniffer.Battle.Debuffed, "装甲破砕済");
        }

        /// <summary>
        /// 敵護衛の生存スコアを計算する
        /// </summary>
        [TestMethod]
        public void EnemySurvivalScore()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("night_zuiun-friendly_battle-101");
            PAssert.That(() => sniffer.Battle.Result.Enemy.SurvivalScore == 2.0, "夜戦前");
            sniffer.SniffLogFile("night_zuiun-friendly_battle-102");
            PAssert.That(() => sniffer.Battle.Result.Enemy.SurvivalScore == 0.7, "夜戦友軍後");

            sniffer.SniffLogFile("survival_score_failed-101");
            PAssert.That(() => sniffer.Battle.Result.Enemy.SurvivalScore == 3.0, "夜戦前");
            sniffer.SniffLogFile("survival_score_failed-102");
            PAssert.That(() => sniffer.Battle.Result.Enemy.SurvivalScore == 3.0, "夜戦友軍でも敵護衛が片付かなかった");
        }

        /// <summary>
        /// 敵生存スコアの配色を調べる
        /// </summary>
        [TestMethod]
        public void SurvivalScoreColor()
        {
            var fleet = new BattleInfo.BattleResult.Combined { SurvivalScore = 0.0 };
            PAssert.That(() => fleet.SurvivalScoreColor == Color.Black);

            fleet = new BattleInfo.BattleResult.Combined { SurvivalScore = 2.7 };
            PAssert.That(() => fleet.SurvivalScoreColor == Color.Black);

            fleet = new BattleInfo.BattleResult.Combined { SurvivalScore = 3.0 };
            PAssert.That(() => fleet.SurvivalScoreColor == CUDColors.Red);
        }

        /// <summary>
        /// レーダー射撃戦に対応する
        /// </summary>
        [TestMethod]
        public void LdShooting()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("ld_shooting_001");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 友軍の夜戦瑞雲が敵の本隊と護衛に分散して攻撃する
        /// </summary>
        [TestMethod]
        public void NightZuiunFriendlySplit()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("night_zuiun-friendly_battle-101");
            sniffer.SniffLogFile("night_zuiun-friendly_battle-102");
            sniffer.SniffLogFile("night_zuiun-friendly_battle-103");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// 敵連合艦隊の護衛の装備を正しく読み取る
        /// </summary>
        [TestMethod]
        public void EnemyGuardSlot()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("enemy_combined_001");
            PAssert.That(() => sniffer.Battle.Result.Enemy.Guard[0].Items[0].Spec.Id == 506);
        }

        /// <summary>
        /// 対空砲火のパラメータを計算する
        /// </summary>
        [TestMethod]
        public void AntiAirFire()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("antiairfire_101");
            var fleet = sniffer.Fleets[0];
            var ships = fleet.Ships;
            PAssert.That(() => fleet.AntiAir == 22.0);
            PAssert.That(() => fleet.CombinedAntiAir == 56.0);
            PAssert.That(() => Math.Round(fleet.EffectiveAntiAir, 2) == 86.15);
            PAssert.That(() => ships.Select(ship => Math.Round(ship.PropShootdown, 2)).ParamsEqual(18.4, 19.2, 16.8, 7.2, 19.6, 21.2));
            PAssert.That(() => ships.Select(ship => Math.Round(ship.FixedShootdown, 2)).ParamsEqual(14.25, 14.65, 13.65, 9.77, 14.73, 15.37));

            var guardFleet = sniffer.Fleets[1];
            var guardShips = guardFleet.Ships;
            PAssert.That(() => guardFleet.AntiAir == 34.0);
            PAssert.That(() => guardFleet.CombinedAntiAir == 56.0);
            PAssert.That(() => Math.Round(guardFleet.EffectiveAntiAir, 2) == 86.15);
            PAssert.That(() => guardShips.Select(ship => Math.Round(ship.PropShootdown, 2)).ParamsEqual(14.88, 31.44, 7.44, 9.6, 0.0, 10.8));
            PAssert.That(() => guardShips.Select(ship => Math.Round(ship.FixedShootdown, 2)).ParamsEqual(10.21, 16.88, 7.11, 8.02, 4.14, 8.65));

            // 敵は通常艦隊
            var enemyFleet = sniffer.Battle.EnemyFleet;
            var enemies = enemyFleet.Ships;
            PAssert.That(() => enemyFleet.AntiAir == 24.0);
            PAssert.That(() => enemyFleet.CombinedAntiAir == 24.0);
            PAssert.That(() => Math.Round(enemyFleet.EffectiveAntiAir, 2) == 76.0);
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.PropShootdown, 2)).ParamsEqual(5.0, 5.0, 5.0, 23.5, 14.75, 14.75));
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.FixedShootdown, 2)).ParamsEqual(9.0, 9.0, 9.0, 15.94, 12.66, 12.66));

            sniffer.SniffLogFile("antiairfire_102");
            // 強力な対空装備を搭載した艦が退避したら艦隊防空が落ちるパターンを追加したい

            // 敵は連合艦隊
            enemyFleet = sniffer.Battle.EnemyFleet;
            enemies = enemyFleet.Ships;
            PAssert.That(() => enemyFleet.AntiAir == 33.0);
            PAssert.That(() => enemyFleet.CombinedAntiAir == 62.0);
            PAssert.That(() => Math.Round(enemyFleet.EffectiveAntiAir, 2) == 186.0);
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.PropShootdown, 2)).ParamsEqual(6.0, 5.6, 4.0, 15.8, 15.8, 18.8));
            PAssert.That(() => enemies.Select(enemy => Math.Round(enemy.FixedShootdown, 2)).ParamsEqual(16.2, 16.05, 15.45, 19.88, 19.88, 21.0));

            var enemyGuardFleet = sniffer.Battle.EnemyGuardFleet;
            var guardEnemies = enemyGuardFleet.Ships;
            PAssert.That(() => enemyGuardFleet.AntiAir == 29.0);
            PAssert.That(() => enemyGuardFleet.CombinedAntiAir == 62.0);
            PAssert.That(() => Math.Round(enemyGuardFleet.EffectiveAntiAir, 2) == 186.0);
            PAssert.That(() => guardEnemies.Select(enemy => Math.Round(enemy.PropShootdown, 2)).ParamsEqual(1.44, 16.2, 7.08, 7.08, 7.08, 7.08));
            PAssert.That(() => guardEnemies.Select(enemy => Math.Round(enemy.FixedShootdown, 2)).ParamsEqual(8.91, 14.44, 11.02, 11.02, 11.02, 11.02));
        }

        /// <summary>
        /// 空戦2回マスでの戦況初期表示
        /// </summary>
        [TestMethod]
        public void ResultPanelInitIndexAirbattle2Times()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airbattle_2times_101");
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }
    }
}
