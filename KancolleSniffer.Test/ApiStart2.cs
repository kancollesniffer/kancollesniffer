// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using KancolleSniffer.Model;

namespace KancolleSniffer.Test
{
    public class ApiStart2
    {
        private static readonly TestingSniffer sniffer = new(apiStart2: true);
        protected virtual TestingSniffer GetSniffer() => sniffer;

        protected ShipMaster ShipMaster => GetSniffer().ShipMaster;
        protected ShipInventory ShipInventory => GetSniffer().ShipInventory;
        protected ItemMaster ItemMaster => GetSniffer().ItemMaster;
        protected ItemInventory ItemInventory => GetSniffer().ItemInventory;
        protected AdditionalData AdditionalData => GetSniffer().AdditionalData;

        protected ShipInfo NewShipInfo() => new(ShipMaster, ShipInventory, ItemInventory);

        protected Fleet NewFleet(int number = 0, CombinedType combinedType = CombinedType.None, int hqLevel = 120) =>
            GetSniffer().NewFleet(number, combinedType, hqLevel);
        protected Fleet NewTransportFleet1() => NewFleet(0, CombinedType.Transport);
        protected Fleet NewTransportFleet2() => NewFleet(1, CombinedType.Transport);

        protected ShipStatus NewShip(int specId, int level, int nowHp, int maxHp, int lucky)
        {
            lock (ShipInventory)
            {
                var ship = new ShipStatus(ShipInventory.MaxId + 1)
                {
                    Spec = ShipMaster.GetSpec(specId),
                    Level = level,
                    NowHp = nowHp,
                    MaxHp = maxHp,
                    Lucky = lucky,
                    Fleet = GetSniffer().NormalFleet,
                    GetItem = itemId => ItemInventory[itemId]
                };
                ShipInventory.Add(ship);
                return ship;
            }
        }

        protected int NewItem(int specId, int onSlot = 0, int level = 0, int alv = 0)
        {
            lock (ItemInventory)
            {
                var item = new ItemStatus(ItemInventory.MaxId + 1)
                {
                    Spec = ItemMaster[specId],
                    OnSlot = onSlot,
                    MaxEq = onSlot,
                    Level = level,
                    Alv = alv
                };
                ItemInventory.Add(item);
                return item.Id;
            }
        }

        protected AirBase.PlaneInfo NewPlaneInfo(int itemId) =>
            new() { Item = ItemInventory[itemId], State = 1, Cond = 1, EffectiveLandBasedReconModifier = 1.0 };


        private const int defaultLevel = 99;
        private const int defaultNowHp = 4;
        private const int defaultMaxHp = 4;
        private const int defaultLucky = 16;

        protected ShipStatus NewShip(int specId) => NewShip(specId, defaultLevel, defaultNowHp, defaultMaxHp, defaultLucky);
        protected ShipStatus NewShip(int specId, int level) => NewShip(specId, level, defaultNowHp, defaultMaxHp, defaultLucky);
        protected ShipStatus NewShip(int specId, int nowHp, int maxHp) => NewShip(specId, defaultLevel, nowHp, maxHp, defaultLucky);

        protected int _12_7cm連装砲(int level = 0) =>
            NewItem(2, level: level);

        protected int _12_7cm連装砲B型改二(int level = 0) =>
            NewItem(63, level: level);


        protected int _10cm連装高角砲高射装置(int level = 0) =>
            NewItem(122, level: level);

        protected int A12_7cm単装高角砲改二() =>
            NewItem(379);

        protected int _14cm単装砲() =>
            NewItem(4);

        protected int _15_5cm三連装砲() =>
            NewItem(5);

        protected int _20_3cm連装砲(int level = 0) =>
            NewItem(6, level: level);

        protected int _20_3cm3号連装砲(int level = 0) =>
            NewItem(50, level: level);

        protected int _15_2cm単装砲() =>
            NewItem(11);

        protected int _15_2cm連装砲() =>
            NewItem(65);

        protected int _203mm53連装砲() =>
            NewItem(162);

        protected int _6inch連装速射砲MkXXI() =>
            NewItem(359);

        protected int _6inchMkXXIII三連装砲() =>
            NewItem(399);

        protected int _5inch連装両用砲集中配備() =>
            NewItem(362);

        protected int _8inch三連装砲Mk9() =>
            NewItem(356);

        protected int _41cm連装砲(int level = 0) =>
            NewItem(8, level: level);

        protected int _152cm単装砲(int level = 0) =>
            NewItem(11, level: level);

        protected int _15_5cm三連装副砲(int level = 0) =>
            NewItem(12, level: level);

        protected int OTO152mm三連装速射砲(int level = 0) =>
            NewItem(134, level: level);

        protected int _90mm単装高角砲(int level = 0) =>
            NewItem(135, level: level);

        protected int _10cm連装高角砲群集中配備(int level = 0) =>
            NewItem(464, level: level);

        protected int _5inch連装砲副砲配置集中配備(int level = 0) =>
            NewItem(467, level: level);

        protected int _15_2cm三連装砲(int level = 0) =>
            NewItem(247, level: level);

        protected int _61cm三連装魚雷() =>
            NewItem(13);

        protected int _61cm五連装酸素魚雷() =>
            NewItem(58);

        protected int 試製61cm六連装酸素魚雷() =>
            NewItem(179);

        protected int 甲標的甲型() =>
            NewItem(41);

        protected int 甲標的丙型(int level = 0) =>
            NewItem(309, level: level);

        protected int 零式艦戦52型熟練(int onSlot, int level = 0, int alv = 0) =>
            NewItem(152, onSlot, level, alv);

        protected int 零式艦戦53型岩本隊(int onSlot, int level = 0, int alv = 0) =>
            NewItem(157, onSlot, level, alv);

        protected int 紫電改四(int onSlot, int alv = 0) =>
            NewItem(271, onSlot, alv: alv);

        protected int 試製烈風後期型(int onSlot, int alv = 0) =>
            NewItem(22, onSlot, alv: alv);

        protected int 烈風一一型(int onSlot, int alv = 0) =>
            NewItem(53, onSlot, alv: alv);

        protected int 烈風六〇一空(int onSlot, int alv = 0) =>
            NewItem(110, onSlot, alv: alv);

        protected int 烈風改二戊型一航戦熟練(int onSlot) =>
            NewItem(339, onSlot);

        protected int Re2005改(int onSlot, int level = 0, int alv = 0) =>
            NewItem(189, onSlot, level, alv);

        protected int F6F3N(int onSlot) =>
            NewItem(254, onSlot);

        protected int F6F5N(int onSlot) =>
            NewItem(255, onSlot);

        protected int 九九式艦爆(int onSlot) =>
            NewItem(23, onSlot);

        protected int 九九式練爆二二型改夜間装備実験機(int onSlot) =>
            NewItem(552, onSlot);

        protected int 彗星一二型甲(int onSlot) =>
            NewItem(57, onSlot);

        protected int 彗星一二型三一号光電管爆弾搭載機(int onSlot = 1) =>
            NewItem(320, onSlot);

        protected int 彗星江草隊(int onSlot, int alv = 0) =>
            NewItem(100, onSlot, alv: alv);

        protected int 零戦62型爆戦岩井隊(int onSlot, int level = 0) =>
            NewItem(154, onSlot, level);

        protected int 零式艦戦64型複座KMX搭載機(int onSlot = 18) =>
            NewItem(447, onSlot);

        protected int Ju87C改二KMX搭載機(int onSlot) =>
            NewItem(305, onSlot);

        protected int SB2C5(int onSlot = 18) =>
            NewItem(421, onSlot);

        protected int 九七式艦攻(int onSlot) =>
            NewItem(16, onSlot);

        protected int 九七式艦攻九三一空(int onSlot) =>
            NewItem(82, onSlot);

        protected int 天山一二型甲(int onSlot) =>
            NewItem(372, onSlot);

        protected int 天山一二型甲改空六号電探改装備機(int onSlot) =>
            NewItem(373, onSlot);

        protected int 天山一二型甲改熟練空六号電探改装備機(int onSlot) =>
            NewItem(374, onSlot);

        protected int 天山一二型村田隊(int onSlot, int alv = 0) =>
            NewItem(144, onSlot, alv: alv);

        protected int 流星改(int onSlot) =>
            NewItem(52, onSlot);

        protected int 流星改一航戦(int onSlot = 18) =>
            NewItem(342, onSlot);

        protected int 流星改一航戦熟練(int onSlot = 18) =>
            NewItem(343, onSlot);

        protected int Re2001G改() =>
            NewItem(188, 1);

        protected int Swordfish(int onSlot, int level = 0) =>
            NewItem(242, onSlot, level);

        protected int SwordfishMkIII熟練(int onSlot) =>
            NewItem(244, onSlot);

        protected int BarracudaMkII(int onSlot, int level = 0) =>
            NewItem(424, onSlot, level);

        protected int TBF(int onSlot) =>
            NewItem(256, onSlot);

        protected int TBM3D(int onSlot) =>
            NewItem(257, onSlot);

        protected int TBM3W3S(int onSlot) =>
            NewItem(389, onSlot);

        protected int 彩雲(int onSlot = 4) =>
            NewItem(54, onSlot);

        protected int 彩雲東カロリン空(int onSlot = 4) =>
            NewItem(212, onSlot);

        protected int 噴式景雲改(int onSlot = 18) =>
            NewItem(199, onSlot);

        protected int 橘花改(int onSlot) =>
            NewItem(200, onSlot);

        protected int 九八式水上偵察機夜偵(int onSlot) =>
            NewItem(102, onSlot);

        protected int 零式水上偵察機11型乙(int onSlot = 4) =>
            NewItem(238, onSlot);

        protected int 零式水上偵察機11型乙熟練(int onSlot = 4) =>
            NewItem(239, onSlot);

        protected int 零式水上偵察機11型乙改夜偵(int onSlot = 4) =>
            NewItem(469, onSlot);

        protected int 紫雲(int onSlot = 4) =>
            NewItem(118, onSlot);

        protected int SwordfishMkIII改水上機型(int onSlot) =>
            NewItem(368, onSlot);

        protected int Loire130M改熟練(int onSlot = 4) =>
            NewItem(538, onSlot);

        protected int 瑞雲(int onSlot, int alv = 0) =>
            NewItem(26, onSlot, alv: alv);

        protected int 瑞雲六三四空(int onSlot) =>
            NewItem(79, onSlot);

        protected int 瑞雲12型(int onSlot, int alv = 0) =>
            NewItem(80, onSlot, alv: alv);

        protected int 瑞雲改二(int onSlot = 18) =>
            NewItem(322, onSlot);

        protected int 瑞雲改二熟練(int onSlot = 18) =>
            NewItem(323, onSlot);

        protected int 試製晴嵐(int onSlot, int alv = 0) =>
            NewItem(62, onSlot, alv: alv);

        protected int _22号対水上電探改四(int level = 0) =>
            NewItem(88, level: level);

        protected int _33号対水上電探(int level = 0) =>
            NewItem(29, level: level);

        protected int _13号対空電探改(int level = 0) =>
            NewItem(106, level: level);

        protected int _21号対空電探改(int level = 0) =>
            NewItem(89, level: level);

        protected int FuMO25レーダー(int level = 0) =>
            NewItem(124, level: level);

        protected int SKレーダー() =>
            NewItem(278);

        protected int SGレーダー初期型() =>
            NewItem(315);

        protected int 九三式水中聴音機(int level = 0) =>
            NewItem(46, level: level);

        protected int 三式水中探信儀() =>
            NewItem(47);

        protected int 水中聴音機零式() =>
            NewItem(132);

        protected int 三式爆雷投射機() =>
            NewItem(45);

        protected int 九五式爆雷() =>
            NewItem(226);

        protected int 二式12cm迫撃砲改() =>
            NewItem(346);

        protected int 九一式徹甲弾(int level = 0) =>
            NewItem(36, level: level);

        protected int A12cm30連装噴進砲改二() =>
            NewItem(274);

        protected int A25mm三連装機銃集中配備() =>
            NewItem(131);

        protected int Bofors40mm四連装機関砲() =>
            NewItem(173);

        protected int QF2ポンド8連装ポンポン砲() =>
            NewItem(191);

        protected int カ号観測機(int onSlot = 18, int level = 0, int alv = 0) =>
            NewItem(69, onSlot, level, alv);

        protected int S51J(int onSlot) =>
            NewItem(326, onSlot);

        protected int S51J改(int onSlot = 18) =>
            NewItem(327, onSlot);

        protected int 三式指揮連絡機対潜(int onSlot = 18, int level = 0, int alv = 0) =>
            NewItem(70, onSlot, level, alv);

        protected int 一式戦隼II型改20戦隊(int onSlot, int level = 0, int alv = 0) =>
            NewItem(489, onSlot, level, alv);

        protected int 一式戦隼III型改熟練20戦隊(int onSlot = 18, int level = 0, int alv = 0) =>
            NewItem(491, onSlot, level, alv);

        protected int 二式大艇(int onSlot, int level = 0, int alv = 0) =>
            NewItem(138, onSlot, level, alv);

        protected int 夜間作戦航空要員() =>
            NewItem(259);

        protected int 熟練甲板要員航空整備員(int level = 0) =>
            NewItem(478, level: level);

        protected int 探照灯(int level = 0) =>
            NewItem(74, level: level);

        protected int RepairFacility() =>
            NewItem(86);

        protected int Daihatsu() =>
            NewItem(68);

        protected int Tokudaihatsu() =>
            NewItem(193);

        protected int DaihatsuMax() =>
            NewItem(68, level: 10);

        protected int TokudaihatsuMax() =>
            NewItem(193, level: 10);

        protected int Daihatsu89Max() =>
            NewItem(166, level: 10);

        protected int Type2TankMax() =>
            NewItem(167, level: 10);

        protected int Daihatu11th() =>
            NewItem(230);

        protected int M4A1DD() =>
            NewItem(355);

        protected int ArmoredBoatMax() =>
            NewItem(408, level: 10);

        protected int ArmedDaihatsu() =>
            NewItem(409);

        protected int NorthAfrica() =>
            NewItem(436);

        protected int 艦隊司令部施設() =>
            NewItem(107);

        protected int 熟練見張員() =>
            NewItem(129);

        protected int 二式水戦改熟練(int onSlot, int level = 0, int alv = 0) =>
            NewItem(216, onSlot, level, alv);

        protected int SmokeGenerator(int level = 0) =>
            NewItem(500, level: level);

        protected int SmokeGeneratorK(int level = 0) =>
            NewItem(501, level: level);

        protected int CombatRation() =>
            NewItem(145);

        protected int Damecon() =>
            NewItem(42);

        protected int Megami() =>
            NewItem(43);

        protected int 一式陸攻三四型(int onSlot = 18) =>
            NewItem(186, onSlot);

        protected int 一式陸攻野中隊(int onSlot = 18, int alv = 0) =>
            NewItem(170, onSlot, alv: alv);

        protected int 銀河江草隊(int onSlot = 18) =>
            NewItem(388, onSlot);

        protected int Do217E5Hs293初期型(int onSlot = 18) =>
            NewItem(405, onSlot);

        protected int Do217K2FritzX(int onSlot = 18) =>
            NewItem(406, onSlot);

        protected int 四式重爆飛龍イ号一型甲誘導弾(int onSlot = 18) =>
            NewItem(444, onSlot);

        protected int 四式重爆飛龍熟練イ号一型甲誘導弾(int onSlot = 18) =>
            NewItem(484, onSlot);

        protected int キ102乙(int onSlot = 18) =>
            NewItem(453, onSlot);

        protected int キ102乙改イ号一型乙誘導弾(int onSlot = 18) =>
            NewItem(454, onSlot);

        protected int B25(int onSlot = 18) =>
            NewItem(459, onSlot);

        protected int 爆装一式戦隼III型改65戦隊(int onSlot = 18) =>
            NewItem(224, onSlot);

        protected int 試製東海(int onSlot = 18) =>
            NewItem(269, onSlot);

        protected int 深山(int onSlot = 9) =>
            NewItem(395, onSlot);

        protected int 一式戦隼II型64戦隊(int onSlot = 18) =>
            NewItem(225, onSlot);

        protected int SpitfireMkV(int onSlot = 18) =>
            NewItem(251, onSlot);

        protected int 秋水(int onSlot = 18) =>
            NewItem(352, onSlot);

        protected int 二式陸上偵察機(int onSlot = 4) =>
            NewItem(311, onSlot);

        protected int 二式陸上偵察機熟練(int onSlot = 4, int level = 0, int alv = 0) =>
            NewItem(312, onSlot, level, alv);
    }
}
