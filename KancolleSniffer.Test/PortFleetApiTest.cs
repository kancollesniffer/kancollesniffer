// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class PortFleetApiTest
    {
        /// <summary>
        /// 一つもアイテムがない場合のrequire_info
        /// </summary>
        [TestMethod]
        public void NoUseItemRequireInfo()
        {
            var sniffer = new TestingSniffer(true);
            sniffer.SniffLogFile("require_info_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 0);
        }

        /// <summary>
        /// 一つもアイテムがない場合のuseitem
        /// </summary>
        [TestMethod]
        public void NoUseItem()
        {
            var sniffer = new TestingSniffer(true);
            sniffer.SniffLogFile("useitem_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 0);
        }

        /// <summary>
        /// 基地航空隊の飛行場拡張と整備レベル強化で設営隊を消費
        /// </summary>
        [TestMethod]
        public void ExpandAirbase()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airbase_expand_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "設営隊 x2");
            PAssert.That(() => sniffer.AirBase[0].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[0].AirCorps.Length == 3);
            PAssert.That(() => sniffer.AirBase[1].MaintenanceLevel == 0);
            PAssert.That(() => sniffer.AirBase[1].AirCorps.Length == 1);

            sniffer.SniffLogFile("airbase_expand_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.AirBase[0].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[0].AirCorps.Length == 3);
            PAssert.That(() => sniffer.AirBase[1].MaintenanceLevel == 0);
            PAssert.That(() => sniffer.AirBase[1].AirCorps.Length == 2);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].ActionName == "待機");
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Distance.ToString() == "0");
            var fp = sniffer.AirBase[1].AirCorps[1].CalcFighterPower();
            PAssert.That(() => fp.AirCombat.RangeString == "0～0");
            PAssert.That(() => fp.Interception.RangeString == "0～0");

            sniffer.SniffLogFile("airbase_expand_003");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "新型航空機設計図 x6");
            PAssert.That(() => sniffer.AirBase[0].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[0].AirCorps.Length == 3);
            PAssert.That(() => sniffer.AirBase[1].MaintenanceLevel == 1);
            PAssert.That(() => sniffer.AirBase[1].AirCorps.Length == 2);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].ActionName == "待機");
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Distance.ToString() == "0");
            fp = sniffer.AirBase[1].AirCorps[1].CalcFighterPower();
            PAssert.That(() => fp.AirCombat.RangeString == "0～0");
            PAssert.That(() => fp.Interception.RangeString == "0～0");
        }

        /// <summary>
        /// 平時にイベント海域の整備レベルを強化してもエラーにならない
        /// </summary>
        [TestMethod]
        public void ExpandEventAreaMaintenanceLevelWhenClose()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airbase_expand_001");
            sniffer.SniffLogFile("airbase_expand_004");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(25).ToString() == "設営隊 x1");
        }

        /// <summary>
        /// 航空特別増加食を使って基地航空隊の疲労を回復する
        /// </summary>
        [TestMethod]
        public void AirCorpsCondRecovery()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("air_corps-cond_recovery-101");
            PAssert.That(() => sniffer.AirBase[1].AreaId == 7);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Planes[0].Cond == 3);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Planes[1].Cond == 2);
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 41);
            PAssert.That(() => sniffer.UseItems.ElementAt(40).ToString() == "航空特別増加食 x3");

            sniffer.SniffLogFile("air_corps-cond_recovery-102");
            PAssert.That(() => sniffer.AirBase[1].AreaId == 7);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Planes[0].Cond == 0);
            PAssert.That(() => sniffer.AirBase[1].AirCorps[1].Planes[1].Cond == 0);
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 41);
            PAssert.That(() => sniffer.UseItems.ElementAt(40).ToString() == "航空特別増加食 x2");
        }

        /// <summary>
        /// 拡張した編成記録枠にすぐに記録してもエラーにならず、ドック開放キーを消費する
        /// </summary>
        [TestMethod]
        public void PresetExpand()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("preset_201");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "ドック開放キー x1");
            sniffer.SniffLogFile("preset_202");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");
        }

        /// <summary>
        /// 拡張した装備記録枠にすぐに記録してもエラーにならず、ドック開放キーを消費する
        /// </summary>
        [TestMethod]
        public void PresetSlotExpand()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("preset_slot_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "ドック開放キー x1");
            sniffer.SniffLogFile("preset_slot_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");
        }

        /// <summary>
        /// 任務報酬で装備運用枠増加をすぐに反映する
        /// </summary>
        [TestMethod]
        public void ClearitemgetMaxSlotitem()
        {
            var sniffer = new TestingSniffer();
            // ログイン
            sniffer.SniffLogFile("clearitemget_max_slotitem_101");
            PAssert.That(() => sniffer.ItemCounter.Max == 2607);

            // 報酬受け取り時点で装備数を更新
            sniffer.SniffLogFile("clearitemget_max_slotitem_102");
            PAssert.That(() => sniffer.ItemCounter.Max == 2610);

            // 母港表示で装備数が更新されない
            sniffer.SniffLogFile("clearitemget_max_slotitem_103");
            PAssert.That(() => sniffer.ItemCounter.Max == 2610);
        }

        /// <summary>
        /// 2024/07/26の任務報酬受け取りnullの確認
        /// </summary>
        [TestMethod]
        public void ClearitemgetBounusNull()
        {
            var sniffer = new TestingSniffer();
            // ログイン
            sniffer.SniffLogFile("clearitemget-bounus-null_101");
            PAssert.That(() => sniffer.UseItems.ElementAt(2).ToString() == "鋼材 x349984");

            // 報酬受け取りでエラーにならない
            sniffer.SniffLogFile("clearitemget-bounus-null_102");
            PAssert.That(() => sniffer.UseItems.ElementAt(2).ToString() == "鋼材 x350000");
        }

        /// <summary>
        /// 資材の変動を正しく反映する
        /// </summary>
        [TestMethod]
        public void MaterialChanges()
        {
            var sniffer1 = new TestingSniffer();
            var result1 = new List<int[]>();
            sniffer1.SniffLogFile("material_001", sn =>
            {
                var cur = sn.Material.Current;
                if (result1.Count == 0)
                {
                    result1.Add(cur);
                }
                else
                {
                    if (!result1.Last().SequenceEqual(cur))
                        result1.Add(cur);
                }
            });
            int[][] expected1 = [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [26178, 26742, 21196, 33750, 1426, 1574, 2185, 10],
                [26178, 26842, 21226, 33750, 1426, 1574, 2185, 10],
                [28951, 29493, 24945, 35580, 1426, 1574, 2185, 10],
                [27451, 27993, 22945, 34580, 1426, 1574, 2184, 10],
                [26074, 26616, 21068, 33700, 1426, 1572, 2183, 10],
                [26171, 26721, 21175, 33750, 1426, 1574, 2185, 10],
                [27023, 27829, 28136, 42404, 1404, 1521, 2142, 15],
                [31208, 29819, 29714, 42345, 1407, 1530, 2155, 13],
                [24595, 25353, 18900, 32025, 1427, 1576, 2187, 10],
                [24515, 25353, 18749, 32025, 1427, 1575, 2187, 10],
                [23463, 24964, 17284, 31765, 1427, 1572, 2187, 10],
                [23463, 25064, 17314, 31765, 1427, 1572, 2187, 10]
            ];
            PAssert.That(() => SequenceOfSequenceEqual(expected1, result1));

            var sniffer2 = new TestingSniffer();
            var result2 = new List<int[]>();
            sniffer2.SniffLogFile("material_002", sn =>
            {
                var cur = sn.Material.Current;
                if (result2.Count == 0)
                {
                    result2.Add(cur);
                }
                else
                {
                    if (!result2.Last().SequenceEqual(cur))
                        result2.Add(cur);
                }
            });
            int[][] expected2 = [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [201649, 189713, 261490, 123227, 2743, 2828, 3000, 44],
                [201649, 189714, 261491, 123227, 2743, 2828, 3000, 44],
                [201650, 189718, 261500, 123227, 2743, 2828, 3000, 44]
            ];
            PAssert.That(() => SequenceOfSequenceEqual(expected2, result2));
        }

        /// <summary>
        /// 基地航空隊における資材の変動を反映する
        /// </summary>
        [TestMethod]
        public void MaterialChangesInAirCorps()
        {
            var sniffer3 = new TestingSniffer();
            var result3 = new List<int[]>();
            sniffer3.SniffLogFile("material_003", sn =>
            {
                var cur = sn.Material.Current;
                if (result3.Count == 0)
                {
                    result3.Add(cur);
                }
                else
                {
                    if (!result3.Last().SequenceEqual(cur))
                        result3.Add(cur);
                }
            });
            int[][] expected3 = [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [288194, 282623, 299496, 295958, 3000, 2968, 2997, 7],
                [288185, 282623, 299496, 295943, 3000, 2968, 2997, 7],
                [288161, 282623, 299496, 295903, 3000, 2968, 2997, 7]
            ];
            PAssert.That(() => SequenceOfSequenceEqual(expected3, result3), "航空機の補充");

            var sniffer4 = new TestingSniffer();
            var result4 = new List<int[]>();
            sniffer4.SniffLogFile("material_004", sn =>
            {
                var cur = sn.Material.Current;
                if (result4.Count == 0)
                {
                    result4.Add(cur);
                }
                else
                {
                    if (!result4.Last().SequenceEqual(cur))
                        result4.Add(cur);
                }
            });
            int[][] expected4 = [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [261012, 252252, 298492, 279622, 3000, 2842, 3000, 22],
                [261012, 252252, 298492, 279538, 3000, 2842, 3000, 22],
                [261012, 252252, 298492, 279454, 3000, 2842, 3000, 22]
            ];
            PAssert.That(() => SequenceOfSequenceEqual(expected4, result4), "航空機の配備");
        }

        private bool SequenceOfSequenceEqual<T>(IEnumerable<IEnumerable<T>> a, IEnumerable<IEnumerable<T>> b)
        {
            var aa = a.ToArray();
            var bb = b.ToArray();
            if (aa.Length != bb.Length)
                return false;
            return aa.Zip(bb, (x, y) => x.SequenceEqual(y)).All(x => x);
        }

        /// <summary>
        /// 第2艦隊までしか解放していなくてもエラーにならないようにする
        /// </summary>
        [TestMethod]
        public void TwoFleets()
        {
            var sniffer = new TestingSniffer(true);
            sniffer.SniffLogFile("twofleets_001");
            PAssert.That(() => sniffer.Fleets.Select(f => f.ChargeStatus.Fuel).ParamsEqual(5, 5, 0, 0));
        }

        /// <summary>
        /// 購入済みアイテムと保有アイテムを一覧に表示する
        /// </summary>
        [TestMethod]
        public void ShowPayitemUseitem()
        {
            var sniffer = new TestingSniffer();
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 0);
            PAssert.That(() => sniffer.PayItems == null);

            sniffer.SniffLogFile("payitem_useitem_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(3).ToString() == "ボーキサイト x350000");
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x91");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x690");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x139");
            PAssert.That(() => sniffer.UseItems.ElementAt(30).ToString() == "捷号章 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x48");
            PAssert.That(() => sniffer.PayItems == null);

            sniffer.SniffLogFile("payitem_useitem_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 8);
        }

        /// <summary>
        /// アイテムを購入してすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void BuyItem()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("payitem_useitem_buy_001");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");

            sniffer.SniffLogFile("payitem_useitem_buy_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");
            PAssert.That(() => sniffer.PayItems.ElementAt(1).ToString() == "設営隊 x1");
        }

        /// <summary>
        /// 南瓜と交換した一式徹甲弾改をすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void ExchangeItem()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("itemuse_getitem-101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 39);
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "給糧艦「伊良湖」 x416");
            PAssert.That(() => sniffer.UseItems.ElementAt(36).ToString() == "南瓜 x6");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 365).Count() == 1);

            sniffer.SniffLogFile("itemuse_getitem-102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 38);
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "給糧艦「伊良湖」 x418");
            PAssert.That(() => sniffer.UseItems.ElementAt(36).ToString() == "海色リボン x3");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 365).Count() == 2);
        }

        /// <summary>
        /// 購入したアイテムを開封してすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void UnboxItem()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("payitem_useitem_unbox_001");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x8");
            PAssert.That(() => sniffer.PayItems.ElementAt(1).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(24).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 8);
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -1).Count() == 23);

            sniffer.SniffLogFile("payitem_useitem_unbox_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x7");
            PAssert.That(() => sniffer.PayItems.ElementAt(1).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(24).ToString() == "設営隊 x1");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 7);
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -1).Count() == 24);

            sniffer.SniffLogFile("payitem_useitem_unbox_003");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x7");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(24).ToString() == "設営隊 x2");
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -2).Count() == 7);
            PAssert.That(() => sniffer.ItemList.Where(item => item.Spec.Id == 43 && item.Holder.Id == -1).Count() == 24);
        }

        /// <summary>
        /// 購入したアイテムを開封するときに、アイテムあふれ警告の時点では開封しない
        /// </summary>
        [TestMethod]
        public void UnboxItemCaution()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("payitemuse_caution_001");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "ドック増設セット x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");

            sniffer.SniffLogFile("payitemuse_caution_002");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 2);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "ドック増設セット x1");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 34);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");

            sniffer.SniffLogFile("payitemuse_caution_003");
            PAssert.That(() => sniffer.PayItems != null);
            PAssert.That(() => sniffer.PayItems.Count() == 1);
            PAssert.That(() => sniffer.PayItems.ElementAt(0).ToString() == "応急修理女神 x7");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "ドック開放キー x1");
        }

        /// <summary>
        /// プレゼント箱、勲章、家具箱を使用してすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void UseItem()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("payitem_useitem_use_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x87");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x2690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x486");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x139");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x299");
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "プレゼント箱 x113");

            sniffer.SniffLogFile("payitem_useitem_use_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x88");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x2690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x486");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x139");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "給糧艦「伊良湖」 x299");
            PAssert.That(() => sniffer.UseItems.ElementAt(17).ToString() == "プレゼント箱 x112");

            sniffer.SniffLogFile("payitem_useitem_use_003");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x88");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x2690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x486");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x135");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(18).ToString() == "プレゼント箱 x112");

            sniffer.SniffLogFile("payitem_useitem_use_004");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 36);
            PAssert.That(() => sniffer.UseItems.ElementAt(7).ToString() == "改修資材 x88");
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x4690");
            PAssert.That(() => sniffer.UseItems.ElementAt(9).ToString() == "家具箱（小） x476");
            PAssert.That(() => sniffer.UseItems.ElementAt(15).ToString() == "勲章 x135");
            PAssert.That(() => sniffer.UseItems.ElementAt(16).ToString() == "改装設計図 x1");
            PAssert.That(() => sniffer.UseItems.ElementAt(18).ToString() == "プレゼント箱 x112");
        }

        /// <summary>
        /// 家具を購入してアイテム消費をすぐ一覧に反映する
        /// </summary>
        [TestMethod]
        public void BuyFurniture()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("buy_furniture_001");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x30690");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x17");

            sniffer.SniffLogFile("buy_furniture_002");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x28747");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x17");

            sniffer.SniffLogFile("buy_furniture_003");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x9317");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x16");

            sniffer.SniffLogFile("buy_furniture_004");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x1517");
            PAssert.That(() => sniffer.UseItems.ElementAt(12).ToString() == "特注家具職人 x15");
        }

        /// <summary>
        /// 20万を超える家具を割引購入してコイン消費量を正しく計算する
        /// </summary>
        [TestMethod]
        public void Furniturebuy200kDiscount()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("furniturebuy_200k_discount_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 38);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x13370");
            PAssert.That(() => sniffer.UseItems.ElementAt(13).ToString() == "特注家具職人 x19");

            sniffer.SniffLogFile("furniturebuy_200k_discount_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 38);
            PAssert.That(() => sniffer.UseItems.ElementAt(8).ToString() == "家具コイン x1070");
            PAssert.That(() => sniffer.UseItems.ElementAt(13).ToString() == "特注家具職人 x18");
        }
    }
}
