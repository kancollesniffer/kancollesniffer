// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class AkashiTimerTest : ApiStart2
    {
        private static readonly TestingSniffer sniffer = new(apiStart2: true);
        protected override TestingSniffer GetSniffer() => sniffer;

        /// <summary>
        /// 泊地修理数
        /// </summary>
        [TestMethod]
        public void RepairReach()
        {
            var akitsushima = NewShip(450, 36, 36);
            akitsushima.SetItems(RepairFacility(), RepairFacility(), RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akitsushima) == 0);

            var asahiCT = NewShip(953, 36, 36);
            asahiCT.SetItems(RepairFacility(), RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiCT) == 0);

            var asahiAR = NewShip(958, 37, 37);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR) == 0);

            asahiAR.SetItems(RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR) == 1);

            asahiAR.SetItems(RepairFacility(), RepairFacility(), RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR) == 3);

            var akashi = NewShip(187, 45, 45);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi) == 2);

            akashi.SetItems(RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi) == 3);

            akashi.SetItems(RepairFacility(), RepairFacility(), RepairFacility(), RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi) == 6);

            akashi.NowHp = 22;
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi) == 0);

            akashi.NowHp = akashi.MaxHp;
            akashi.InNDock = true;
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi) == 0);
        }

        /// <summary>
        /// 工作艦2隻での泊地修理数
        /// </summary>
        [TestMethod]
        public void RepairReach2AR()
        {
            var akitsushima = NewShip(450, 36, 36);
            var asahiAR = NewShip(958, 19, 37);
            var akashi = NewShip(187, 23, 45);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR, akashi) == 0);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, asahiAR) == 2);

            asahiAR.SetItems(RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR, akashi) == 3);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, asahiAR) == 3);

            akashi.SetItems(RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR, akashi) == 4);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, asahiAR) == 4);

            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR, akitsushima, akashi) == 1);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, akitsushima, asahiAR) == 3);

            asahiAR.SetItems(RepairFacility(), RepairFacility(), RepairFacility());
            akashi.SetItems(RepairFacility(), RepairFacility());
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(asahiAR, akashi) == 7);
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, asahiAR) == 7);

            asahiAR.NowHp = 18;
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, asahiAR) == 4);

            asahiAR.NowHp = asahiAR.MaxHp;
            asahiAR.InNDock = true;
            PAssert.That(() => sniffer.AkashiTimer.RepairReach(akashi, asahiAR) == 4);
        }

        /// <summary>
        /// 修理対象艦
        /// </summary>
        [TestMethod]
        public void RepairTarget()
        {
            var akashi = NewShip(187, 45, 45);
            akashi.SetItems(RepairFacility(), RepairFacility());
            var inazuma = NewShip(237, 27, 30);
            var mogami = NewShip(73, 48, 50);
            var nagato = NewShip(80, 78, 80);
            var i168 = NewShip(398, 8, 15);

            var repairTargets = sniffer.AkashiTimer.RepairTarget(akashi, inazuma, mogami, nagato, i168);
            PAssert.That(() => repairTargets.Length == 4);
            PAssert.That(() => repairTargets[0].NowHp == akashi.NowHp);
            PAssert.That(() => repairTargets[1].NowHp == inazuma.NowHp);
            PAssert.That(() => repairTargets[2].NowHp == mogami.NowHp);
            PAssert.That(() => repairTargets[3].NowHp == nagato.NowHp);

            inazuma.NowHp = 15;
            mogami.InNDock = true;
            repairTargets = sniffer.AkashiTimer.RepairTarget(akashi, inazuma, mogami, nagato, i168);
            PAssert.That(() => repairTargets.Length == 4);
            PAssert.That(() => repairTargets[0].NowHp == akashi.NowHp);
            PAssert.That(() => repairTargets[1].NowHp == 0);
            PAssert.That(() => repairTargets[2].NowHp == 50);
            PAssert.That(() => repairTargets[3].NowHp == nagato.NowHp);

            repairTargets = sniffer.AkashiTimer.RepairTarget(nagato, akashi, inazuma, mogami, i168);
            PAssert.That(() => repairTargets.Length == 0);
        }

        /// <summary>
        /// 母港
        /// </summary>
        private void Port()
        {
            sniffer.AkashiTimer.NowFunc = () => new DateTime(2018, 1, 1, 0, 0, 0);
            sniffer.AkashiTimer.Port();
            PAssert.That(() => sniffer.AkashiTimer.GetPresetDeckTimer(new DateTime(2018, 1, 1, 0, 1, 0)) == TimeSpan.FromMinutes(19), "母港で開始");
        }

        /// <summary>
        /// 20分経過
        /// </summary>
        [TestMethod]
        public void After20Nimutes()
        {
            var akashi = NewShip(187, 45, 45);
            akashi.SetItems(RepairFacility(), RepairFacility());
            var inazuma = NewShip(237, 16, 30);
            var mogami = NewShip(73, 26, 50);
            var nagato = NewShip(80, 41, 80);
            var fleet = sniffer.Fleets[0];
            fleet.SetShips(akashi.Id, inazuma.Id, mogami.Id, nagato.Id);

            Port();
            var spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 20, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.FromMinutes(2));
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(12));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(23));

            spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 22, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.FromMinutes(10));
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(10));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(21));

            spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 32, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.FromMinutes(11));
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(16));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(11));

            spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 43, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.FromMinutes(11));
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(5));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(21));
        }

        /// <summary>
        /// 工作艦2隻で20分経過
        /// </summary>
        [TestMethod]
        public void After20Nimutes2AR()
        {
            var akashi = NewShip(187, 45, 45);
            akashi.SetItems(RepairFacility(), RepairFacility(), RepairFacility(), RepairFacility());
            var asahiAR = NewShip(958, 37, 37);
            asahiAR.SetItems(RepairFacility(), RepairFacility(), RepairFacility());
            var inazuma = NewShip(237, 16, 30);
            var mogami = NewShip(73, 26, 50);
            var nagato = NewShip(80, 41, 80);
            nagato.Level = 80;
            var mutsu = NewShip(80, 41, 80);
            mutsu.Level  = 79;
            var fleet = sniffer.Fleets[0];
            fleet.SetShips(akashi.Id, asahiAR.Id, inazuma.Id, mogami.Id, nagato.Id, mutsu.Id);

            Port();
            var spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 20, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(7));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(7));
            PAssert.That(() => spans[4].Span == TimeSpan.FromMinutes(10));
            PAssert.That(() => spans[5].Span == TimeSpan.FromMinutes(10));

            spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 27, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(9));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(13));
            PAssert.That(() => spans[4].Span == TimeSpan.FromMinutes(3));
            PAssert.That(() => spans[5].Span == TimeSpan.FromMinutes(3));

            spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 30, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(6));
            PAssert.That(() => spans[3].Span == TimeSpan.FromMinutes(10));
            PAssert.That(() => spans[4].Span == TimeSpan.FromMinutes(15));
            PAssert.That(() => spans[5].Span == TimeSpan.FromMinutes(14));
        }

        /// <summary>
        /// 工作艦2隻修理施設なしで20分経過
        /// </summary>
        [TestMethod]
        public void After20Nimutes2ARWithoutRepairFacility()
        {
            var akashi = NewShip(187, 45, 45);
            akashi.SetItems(RepairFacility());
            var asahiAR = NewShip(958, 37, 37);
            var inazuma = NewShip(237, 16, 30);
            var mogami = NewShip(73, 26, 50);
            var fleet = sniffer.Fleets[0];
            fleet.SetShips(akashi.Id, asahiAR.Id, inazuma.Id, mogami.Id);

            Port();
            var spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 20, 0));
            PAssert.That(() => spans[0].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[1].Span == TimeSpan.MinValue);
            PAssert.That(() => spans[2].Span == TimeSpan.FromMinutes(2));

            fleet.SetShips(asahiAR.Id, akashi.Id, inazuma.Id, mogami.Id);
            Port();
            spans = sniffer.AkashiTimer.GetTimers(0, new DateTime(2018, 1, 1, 0, 20, 0));
            PAssert.That(() => spans.Length == 0);
        }

        /// <summary>
        /// 二番艦を外してリセット
        /// </summary>
        [TestMethod]
        public void WithdrawShip()
        {
            var akashi = NewShip(187, 45, 45);
            akashi.SetItems(RepairFacility());
            var inazuma = NewShip(237, 30, 30);
            var fleet = sniffer.Fleets[0];
            fleet.SetShips(akashi.Id, inazuma.Id);

            Port();
            sniffer.AkashiTimer.NowFunc = () => new DateTime(2018, 1, 1, 0, 2, 0);
            fleet.SetShips(akashi.Id);
            sniffer.AkashiTimer.InspectChange("api_id=1&api_ship_idx=0&api_ship_id=-1");
            PAssert.That(() => sniffer.AkashiTimer.GetPresetDeckTimer(new DateTime(2018, 1, 1, 0, 3, 0)) == TimeSpan.FromMinutes(19));
        }

        /// <summary>
        /// 随伴艦一括解除でリセットしない
        /// </summary>
        [TestMethod]
        public void WithdrawAccompanyingShips()
        {
            var akashi = NewShip(187, 45, 45);
            akashi.SetItems(RepairFacility());
            var inazuma = NewShip(237, 30, 30);
            var fleet = sniffer.Fleets[0];
            fleet.SetShips(akashi.Id, inazuma.Id);

            Port();
            sniffer.AkashiTimer.NowFunc = () => new DateTime(2018, 1, 1, 0, 2, 0);
            fleet.SetShips(akashi.Id);
            sniffer.AkashiTimer.InspectChange("api_id=1&api_ship_idx=0&api_ship_id=-2");
            PAssert.That(() => sniffer.AkashiTimer.GetPresetDeckTimer(new DateTime(2018, 1, 1, 0, 3, 0)) == TimeSpan.FromMinutes(17));
        }
    }
}
