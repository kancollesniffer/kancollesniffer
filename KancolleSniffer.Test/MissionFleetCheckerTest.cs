// Copyright (C) 2025 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class MissionFleetCheckerTest : ApiStart2
    {
        [TestMethod]
        public void MiSendan1()
        {
            var cve = NewShip(560);
            var cl = NewShip(51);
            var ct = NewShip(154);
            var dd = NewShip(1);
            var de = NewShip(517);
            var ap = NewShip(1513);
            cl.Firepower = dd.Firepower = 500;
            cl.AntiAir = dd.AntiAir = 500;
            cl.ShownAsw = dd.ShownAsw = 500;
            cl.LoS = dd.LoS = 500;
            var fleet = NewFleet().SetShips(cl.Id, dd.Id, dd.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(42));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(42));

            fleet.SetShips(cl.Id, de.Id, de.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(42));

            fleet.SetShips(cl.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(42));

            fleet.SetShips(ct.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(42));

            fleet.SetShips(cve.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(42));

            fleet.SetShips(cve.Id, dd.Id, dd.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(42));
        }

        [TestMethod]
        public void MiSendan2()
        {
            var cvl = NewShip(89);
            var cve = NewShip(560);
            var cl = NewShip(51);
            var ct = NewShip(154);
            var dd = NewShip(1);
            var de = NewShip(517);
            var ap = NewShip(1513);
            var kasugamaru = NewShip(521);
            cvl.Firepower = cve.Firepower = cl.Firepower = ct.Firepower = dd.Firepower = de.Firepower = kasugamaru.Firepower = 500;
            cvl.AntiAir = cve.AntiAir = cl.AntiAir = ct.AntiAir = dd.AntiAir = de.AntiAir = kasugamaru.AntiAir = 500;
            cvl.ShownAsw = cve.ShownAsw = cl.ShownAsw = ct.ShownAsw = dd.ShownAsw = de.ShownAsw = kasugamaru.ShownAsw = 500;
            cvl.LoS = cve.LoS = cl.LoS = ct.LoS = dd.LoS = de.LoS = kasugamaru.LoS = 500;

            var fleet = NewFleet().SetShips(cve.Id, dd.Id, dd.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(cve.Id, de.Id, de.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(cve.Id, dd.Id, de.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, de.Id, de.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(cvl.Id, cl.Id, dd.Id, dd.Id, dd.Id, dd.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));


            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, dd.Id, de.Id, de.Id, de.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, dd.Id, de.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, cl.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, ct.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, cve.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));

            fleet.SetShips(kasugamaru.Id, cve.Id, dd.Id, dd.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(43));
        }

        [TestMethod]
        public void SouthWest()
        {
            var cve = NewShip(560);
            var cl = NewShip(51);
            var ct = NewShip(154);
            var dd = NewShip(1);
            var de = NewShip(517);
            var ap = NewShip(1513);
            cl.Firepower = dd.Firepower = de.Firepower = 500;
            cl.AntiAir = dd.AntiAir = de.AntiAir = 500;
            cl.ShownAsw = dd.ShownAsw = de.ShownAsw = 500;
            cl.LoS = dd.LoS = de.LoS = 500;
            var fleet = NewFleet().SetShips(cl.Id, dd.Id, dd.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(103));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(103));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(103));

            fleet.SetShips(cl.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(103));

            fleet.SetShips(ct.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(103));

            fleet.SetShips(cve.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(103));

            fleet.SetShips(cve.Id, dd.Id, dd.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(103));
        }

        [TestMethod]
        public void Ogasawara1()
        {
            var cve = NewShip(560);
            var cl = NewShip(51);
            var ct = NewShip(154);
            var dd = NewShip(1);
            var de = NewShip(517);
            var ap = NewShip(1513);
            cl.Firepower = dd.Firepower = de.Firepower = 500;
            cl.AntiAir = dd.AntiAir = de.AntiAir = 500;
            cl.ShownAsw = dd.ShownAsw = de.ShownAsw = 500;
            cl.LoS = dd.LoS = de.LoS = 500;
            var fleet = NewFleet().SetShips(cl.Id, dd.Id, dd.Id, dd.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(104));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(104));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(104));

            fleet.SetShips(cl.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(104));

            fleet.SetShips(ct.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(104));

            fleet.SetShips(cve.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(104));

            fleet.SetShips(cve.Id, dd.Id, dd.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(104));
        }

        [TestMethod]
        public void Ogasawara2()
        {
            var cve = NewShip(560);
            var cl = NewShip(51);
            var ct = NewShip(154);
            var dd = NewShip(1);
            var de = NewShip(517);
            var ap = NewShip(1513);
            cl.Firepower = dd.Firepower = de.Firepower = 500;
            cl.AntiAir = dd.AntiAir = de.AntiAir = 500;
            cl.ShownAsw = dd.ShownAsw = de.ShownAsw = 500;
            cl.LoS = dd.LoS = de.LoS = 500;
            var fleet = NewFleet().SetShips(cl.Id, dd.Id, dd.Id, dd.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(105));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id, ap.Id);
            PAssert.That(() => !new MissionFleetChecker(fleet).Check(105));

            fleet.SetShips(dd.Id, de.Id, de.Id, de.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(105));

            fleet.SetShips(cl.Id, de.Id, de.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(105));

            fleet.SetShips(ct.Id, de.Id, de.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(105));

            fleet.SetShips(cve.Id, de.Id, de.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(105));

            fleet.SetShips(cve.Id, dd.Id, dd.Id, ap.Id, ap.Id, ap.Id);
            PAssert.That(() => new MissionFleetChecker(fleet).Check(105));
        }

        [TestMethod]
        public void MissionStatusNoneAircraft()
        {
            var helena = NewShip(620).SetItems(零式水上偵察機11型乙熟練(0), _21号対空電探改(6), _10cm連装高角砲高射装置(4), 九三式水中聴音機(8));
            helena.Firepower = 78 + helena.Items.Sum(item => item.Spec.Firepower);
            helena.AntiAir   = 86 + helena.Items.Sum(item => item.Spec.AntiAir);
            helena.ShownAsw  = 32 + helena.Items.Sum(item => item.Spec.Asw);
            helena.LoS       = 66 + helena.Items.Sum(item => item.Spec.LoS);
            var fleet = NewFleet().SetShips(helena.Id);
            var total = TotalParams.Calc(fleet);

            PAssert.That(() => total.ViewFirePower == 84);
            PAssert.That(() => total.ViewAntiAir == 103);
            PAssert.That(() => total.ViewAsw == 40);
            PAssert.That(() => total.ViewLoS == 74);
        }
    }
}
