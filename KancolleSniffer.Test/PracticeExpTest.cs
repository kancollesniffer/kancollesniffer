// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class PracticeExpTest : ApiStart2
    {
        [TestMethod]
        public void BaseExp()
        {
            PAssert.That(() => PracticeExp.GetExp(1, 1) == 0);
            PAssert.That(() => PracticeExp.GetExp(32, 1) == 496);
            PAssert.That(() => PracticeExp.GetExp(33, 1) == 505);
            PAssert.That(() => PracticeExp.GetExp(1, 55) == 498);
            PAssert.That(() => PracticeExp.GetExp(1, 56) == 504);
            PAssert.That(() => PracticeExp.GetExp(28, 27) == 495);
            PAssert.That(() => PracticeExp.GetExp(29, 27) == 504);
            PAssert.That(() => PracticeExp.GetExp(28, 28) == 502);
            PAssert.That(() => PracticeExp.GetExp(99, 99) == 613);
            PAssert.That(() => PracticeExp.GetExp(150, 150) == 740);
            PAssert.That(() => PracticeExp.GetExp(175, 175) == 881);
            PAssert.That(() => PracticeExp.GetExp(180, 180) == 915);
        }

        [TestMethod]
        public void TrainingCruiserBonus()
        {
            var ship       = NewShip(1, 1);
            var katori     = NewShip(154, 1);
            var kashima    = NewShip(465, 1);
            var katori10   = NewShip(154, 10);
            var kashima10  = NewShip(465, 10);
            var katori30   = NewShip(154, 30);
            var kashima30  = NewShip(465, 30);
            var katori60   = NewShip(343, 60);
            var kashima60  = NewShip(356, 60);
            var katori100  = NewShip(343, 100);
            var kashima100 = NewShip(356, 100);

            ShipStatus[] ships = [ship];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.0);

            ships = [ship, katori];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.03);
            ships = [ship, katori, kashima];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.04);
            ships = [ship, katori10];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.05);
            ships = [ship, katori10, kashima];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.06);
            ships = [ship, katori, kashima10];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.06);
            ships = [ship, kashima30];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.07);
            ships = [ship, kashima30, katori];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.08);
            ships = [ship, kashima, katori30];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.08);
            ships = [ship, kashima60];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.1);
            ships = [ship, kashima60, katori];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.12);
            ships = [ship, kashima, katori60];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.12);
            ships = [ship, kashima100];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.15);
            ships = [ship, kashima100, katori];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.175);
            ships = [ship, kashima, katori100];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.175);

            ships = [kashima];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.05);
            ships = [kashima, katori100];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.1);
            ships = [kashima10];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.08);
            ships = [kashima10, katori100];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.13);
            ships = [katori30];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.12);
            ships = [katori30, kashima100];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.16);
            ships = [katori60];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.15);
            ships = [katori60, kashima10];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.2);
            ships = [katori100];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.20);
            ships = [katori100, kashima10];
            PAssert.That(() => PracticeExp.TrainingCruiserBonus(ships) == 1.25);
        }

        [TestMethod]
        public void AsashiCTBonus()
        {
            var ship    = NewShip(1, 1);
            var asahiCT = NewShip(953, 1);
            var asahiAR = NewShip(958, 1);
            var katori  = NewShip(154, 1);
            var kashima = NewShip(465, 1);

            var (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([ship]);
            PAssert.That(() => otherShipRate == 1.0);
            PAssert.That(() => submarineRate == 1.0);
            PAssert.That(() => actualShips.ParamsEqual(ship));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([asahiAR]);
            PAssert.That(() => otherShipRate == 1.0);
            PAssert.That(() => submarineRate == 1.0);
            PAssert.That(() => actualShips.ParamsEqual(asahiAR));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([ship, asahiCT]);
            PAssert.That(() => otherShipRate == 1.0);
            PAssert.That(() => submarineRate == 1.0);
            PAssert.That(() => actualShips.ParamsEqual(ship));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([ship, asahiCT, katori]);
            PAssert.That(() => otherShipRate == 1.0);
            PAssert.That(() => submarineRate == 1.0);
            PAssert.That(() => actualShips.ParamsEqual(ship, katori));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([ship, asahiCT, katori, kashima]);
            PAssert.That(() => otherShipRate == 1.0);
            PAssert.That(() => submarineRate == 1.0);
            PAssert.That(() => actualShips.ParamsEqual(ship, katori, kashima));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([asahiCT]);
            PAssert.That(() => otherShipRate == 0.6);
            PAssert.That(() => submarineRate == 1.3);
            PAssert.That(() => actualShips.ParamsEqual(asahiCT));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([asahiCT, ship, ship]);
            PAssert.That(() => otherShipRate == 0.6);
            PAssert.That(() => submarineRate == 1.3);
            PAssert.That(() => actualShips.ParamsEqual(asahiCT, ship, ship));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([asahiCT, katori, ship]);
            PAssert.That(() => otherShipRate == 1.05);
            PAssert.That(() => submarineRate == 1.45);
            PAssert.That(() => actualShips.ParamsEqual(asahiCT));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([katori, asahiCT, ship]);
            PAssert.That(() => otherShipRate == 1.05);
            PAssert.That(() => submarineRate == 1.45);
            PAssert.That(() => actualShips.ParamsEqual(katori));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([kashima, katori, asahiCT]);
            PAssert.That(() => otherShipRate == 1.05);
            PAssert.That(() => submarineRate == 1.45);
            PAssert.That(() => actualShips.ParamsEqual(kashima));

            (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus([asahiCT, kashima, katori, ship]);
            PAssert.That(() => otherShipRate == 1.05);
            PAssert.That(() => submarineRate == 1.45);
            PAssert.That(() => actualShips.ParamsEqual(asahiCT, kashima, katori, ship));
        }
    }
}
