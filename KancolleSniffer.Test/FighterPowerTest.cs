// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class FighterPowerTest : ApiStart2
    {
        /// <summary>
        /// 熟練度込みの制空値を正しく計算する
        /// </summary>
        [TestMethod]
        public void FighterPowerWithBonus()
        {
            var taihou = NewShip(156).SetItems(天山一二型村田隊(30, 7), 彗星江草隊(24, 7), 烈風一一型(24, 7), 試製烈風後期型(8, 0));
            var chiyoda = NewShip(107).SetItems(試製晴嵐(12, 7), 瑞雲12型(6, 7), 瑞雲(6, 0));
            var fleet = NewFleet().SetShips(taihou.Id, chiyoda.Id);

            PAssert.That(() => fleet.FighterPower.Min == 156);
            PAssert.That(() => fleet.FighterPower.Max == 159);

            chiyoda.SetItems(試製晴嵐(12, 7), 瑞雲12型(0, 7), 瑞雲(6, 0));
            PAssert.That(() => fleet.FighterPower.Min == 140);
            PAssert.That(() => fleet.FighterPower.Max == 143);
        }

        [TestMethod]
        public void 対潜機は熟練度ありでも制空なしだが一式戦隼II型改20戦隊は制空あり()
        {
            var ship = NewShip(717);
            ship.Firepower = 24;
            ship.Torpedo = 0;

            ship.SetItems(カ号観測機(8), 三式指揮連絡機対潜(6), 一式戦隼II型改20戦隊(2));
            PAssert.That(() => ship.Items[0].CalcFighterPower().Max == 0);
            PAssert.That(() => ship.Items[1].CalcFighterPower().Max == 0);
            PAssert.That(() => ship.Items[2].CalcFighterPower().Max == 9);

            ship.SetItems(カ号観測機(8, level: 10), 三式指揮連絡機対潜(6, level: 10), 一式戦隼II型改20戦隊(2, level: 10));
            PAssert.That(() => ship.Items[0].CalcFighterPower().Max == 0);
            PAssert.That(() => ship.Items[1].CalcFighterPower().Max == 0);
            PAssert.That(() => ship.Items[2].CalcFighterPower().Max == 9);

            ship.SetItems(カ号観測機(8, alv: 7), 三式指揮連絡機対潜(6, alv: 7), 一式戦隼II型改20戦隊(2, alv: 7));
            PAssert.That(() => ship.Items[0].CalcFighterPower().Max == 0);
            PAssert.That(() => ship.Items[1].CalcFighterPower().Max == 0);
            PAssert.That(() => ship.Items[2].CalcFighterPower().Max == 33);
        }

        [TestMethod]
        public void 基地航空でも対潜機は熟練度ありでも制空なしだが一式戦隼II型改20戦隊は制空あり()
        {
            var airBasePlane = NewPlaneInfo(カ号観測機(18));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);
            airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);
            airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 26);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 26);

            airBasePlane = NewPlaneInfo(カ号観測機(18, level: 10));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);
            airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18, level: 10));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);
            airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18, level: 10));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 26);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 26);

            airBasePlane = NewPlaneInfo(カ号観測機(18, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);
            airBasePlane = NewPlaneInfo(三式指揮連絡機対潜(18, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);
            airBasePlane = NewPlaneInfo(一式戦隼II型改20戦隊(18, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 50);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 50);
        }

        [TestMethod]
        public void 二式大艇の改修値による基地航空での制空上昇()
        {
            var airBasePlane = NewPlaneInfo(二式大艇(4));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 0);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);

            airBasePlane = NewPlaneInfo(二式大艇(4, level: 3));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 0);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);

            airBasePlane = NewPlaneInfo(二式大艇(4, level: 4));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 1);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 1);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 1);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 1);

            airBasePlane = NewPlaneInfo(二式大艇(3, level: 4));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 0);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 0);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 0);

            airBasePlane = NewPlaneInfo(二式大艇(4, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 3);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 3);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 3);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 3);

            var ship = NewShip(450);
            ship.SetItems(二式大艇(4, level: 4, alv: 7));
            PAssert.That(() => ship.Items[0].CalcFighterPower().Min == 0, "艦載時は制空なし");
            PAssert.That(() => ship.Items[0].CalcFighterPower().Max == 0, "艦載時は制空なし");
        }

        [TestMethod]
        public void 陸上偵察機は改修2かつ内部熟練度最大で制空上昇()
        {
            var airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(4, level: 2));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 6);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 6);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 6);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 6);

            airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(4, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 9);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 9);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 9);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 9);

            airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(4, level: 2, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 9);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 9);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 10, "内部熟練度最大時のみ");
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 10, "内部熟練度最大時のみ");

            airBasePlane = NewPlaneInfo(二式陸上偵察機熟練(3, level: 2, alv: 7));
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Min == 8);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Min == 8);
            PAssert.That(() => airBasePlane.FighterPower.AirCombat.Max == 9);
            PAssert.That(() => airBasePlane.FighterPower.Interception.Max == 9);
        }

        [TestMethod]
        public void _23春e2乙最終基地空襲制空()
        {
            var ships = new List<ShipStatus>();
            var ship1 = NewShip(2045);
            ship1.SetItems(NewItem(1617), NewItem(1618), NewItem(1619), NewItem(1619));
            ship1.FillOnSlotMaxEq(ship1.Spec.MaxEq);
            ships.Add(ship1);

            var ship2 = NewShip(2106);
            ship2.SetItems(NewItem(1581), NewItem(1574), NewItem(1582), NewItem(1583));
            ship2.FillOnSlotMaxEq(ship2.Spec.MaxEq);
            ships.Add(ship2);

            var ship34 = NewShip(2103);
            ship34.SetItems(NewItem(1547), NewItem(1574), NewItem(1574), NewItem(1583));
            ship34.FillOnSlotMaxEq(ship34.Spec.MaxEq);
            ships.Add(ship34);
            ships.Add(ship34);

            PAssert.That(() => ships.Sum(e => e.EnemyInterception().FighterPower) == 627);
        }

        [TestMethod]
        public void _23春e3_3甲前哨対基地制空()
        {
            var ships = new List<ShipStatus>();
            var ship1 = NewShip(2148);
            ship1.SetItems(NewItem(1617), NewItem(1618), NewItem(1619), NewItem(1633), NewItem(1608));
            ship1.FillOnSlotMaxEq(ship1.Spec.MaxEq);
            ships.Add(ship1);

            var ship2 = NewShip(2107);
            ship2.SetItems(NewItem(1581), NewItem(1574), NewItem(1582), NewItem(1583));
            ship2.FillOnSlotMaxEq(ship2.Spec.MaxEq);
            ships.Add(ship2);

            var ship34 = NewShip(2103);
            ship34.SetItems(NewItem(1547), NewItem(1574), NewItem(1574), NewItem(1583));
            ship34.FillOnSlotMaxEq(ship34.Spec.MaxEq);
            ships.Add(ship34);
            ships.Add(ship34);

            var ship5 = NewShip(1792);
            ship5.SetItems(NewItem(1584), NewItem(1584), NewItem(1584), NewItem(1525));
            ship5.FillOnSlotMaxEq(ship5.Spec.MaxEq);
            ships.Add(ship5);

            var ship6 = NewShip(1543);
            ship6.SetItems(NewItem(1509), NewItem(1509), NewItem(1525), NewItem(1529));
            ship6.FillOnSlotMaxEq(ship6.Spec.MaxEq);
            ships.Add(ship6);

            var ship7 = NewShip(1592);
            ship7.SetItems(NewItem(1550), NewItem(1550), NewItem(1545), NewItem(1525));
            ship7.FillOnSlotMaxEq(ship7.Spec.MaxEq);
            ships.Add(ship7);

            PAssert.That(() => ships.Sum(e => e.EnemyInterception().FighterPower) == 703);
        }
    }
}
