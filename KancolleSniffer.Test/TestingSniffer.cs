// Copyright (C) 2014 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using DynaJson;
using KancolleSniffer.Model;

namespace KancolleSniffer.Test
{
    public class TestingSniffer : Sniffer
    {
        public readonly Fleet NormalFleet;

        public TestingSniffer(bool start = false, bool apiStart2 = false) : base(start)
        {
            AdditionalData.UseNumEquipsFile = false;
            NormalFleet = NewFleet();
            if (apiStart2)
                SniffLogFile("api_start2");
        }

        public Fleet NewFleet(int number = 0, CombinedType combinedType = CombinedType.None, int hqLevel = 120) =>
            new(ShipInventory, number, () => hqLevel) { CombinedType = combinedType };

        public void SniffLogFile(string name, Action<TestingSniffer> action = null)
        {
            var dir = Path.GetDirectoryName(Path.GetDirectoryName(Environment.CurrentDirectory));
            var path = Path.Combine(dir, Path.Combine("logs", name + ".log.gz"));
            using var stream = new StreamReader(new GZipStream(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read), CompressionMode.Decompress));

            var ln = 0;
            while (!stream.EndOfStream)
            {
                var triple = new List<string>();
                foreach (var s in Main.LogHeaders)
                {
                    string line;
                    do
                    {
                        line = stream.ReadLine();
                        ln++;
                        if (line == null)
                            throw new Exception($"ログの内容がそろっていません: {ln:d}行目");
                    } while (!line.StartsWith(s));
                    triple.Add(line.Substring(s.Length));
                }
                var json = JsonObject.Parse(triple[2]);
                Sniff(triple[0], triple[1], json);
                action?.Invoke(this);
            }
        }
    }
}
