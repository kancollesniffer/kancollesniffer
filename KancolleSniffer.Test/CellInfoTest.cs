// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class CellInfoTest
    {
        /// <summary>
        /// 第二期の開幕夜戦のセル情報を表示する
        /// </summary>
        [TestMethod]
        public void SpMidnightIn2ndSequence()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("sp_midnight_003");
            PAssert.That(() => sniffer.CellInfo.Current == "5-3 １戦目(夜戦)");
        }

        /// <summary>
        /// 緊急泊地修理
        /// 退避後に泊地修理する
        /// 泊地修理で緊急修理資材を消費する
        /// </summary>
        [TestMethod]
        public void AnchorageRepair()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("anchorage_repair_101");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x49");

            sniffer.SniffLogFile("anchorage_repair_102");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x48");
            PAssert.That(() => sniffer.Fleets[1].Ships[2].Escaped);
            PAssert.That(() => sniffer.Fleets[1].Ships[4].Escaped);

            sniffer.SniffLogFile("anchorage_repair_103");
            PAssert.That(() => sniffer.UseItems != null);
            PAssert.That(() => sniffer.UseItems.Count() == 35);
            PAssert.That(() => sniffer.UseItems.ElementAt(31).ToString() == "緊急修理資材 x48");
            PAssert.That(() => !sniffer.IsBattleResultError);
        }

        /// <summary>
        /// うずしおで弾薬消費
        /// </summary>
        [TestMethod]
        public void UzushioConsumeBull()
        {
            var sniffer = new TestingSniffer();
            // うずしお前の残量
            sniffer.SniffLogFile("uzushio_bull_101");
            PAssert.That(() => sniffer.CellInfo.Current == "4-3 １戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "4-3 次資源");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 4);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Fuel == 12);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Bull == 16);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            sniffer.SniffLogFile("uzushio_bull_102");
            PAssert.That(() => sniffer.CellInfo.Current == "4-3 資源");
            PAssert.That(() => sniffer.CellInfo.Next == "4-3 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 4);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Fuel == 12);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Bull == 16);

            // うずしお通過、ここで減らす
            sniffer.SniffLogFile("uzushio_bull_103");
            PAssert.That(() => sniffer.CellInfo.Current == "4-3 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "4-3 次２戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 2);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Fuel == 12);
            PAssert.That(() => sniffer.Fleets[0].Ships[3].Bull == 11);
        }

        /// <summary>
        /// 出撃直後にうずしお
        /// </summary>
        [TestMethod]
        public void UzushioAfterStart()
        {
            var sniffer = new TestingSniffer();
            // うずしお前の残量
            sniffer.SniffLogFile("uzushio_after_start_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            sniffer.SniffLogFile("uzushio_after_start_102");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // うずしお通過、ここで減らす
            sniffer.SniffLogFile("uzushio_after_start_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-2 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 次気のせい");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 14);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);
        }

        /// <summary>
        /// 無戦闘マスのあとにうずしお
        /// </summary>
        [TestMethod]
        public void UzushioAfterEmpty()
        {
            var sniffer = new TestingSniffer();
            // うずしお前の残量
            sniffer.SniffLogFile("uzushio_after_empty_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "5-4 次気のせい");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 10);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            sniffer.SniffLogFile("uzushio_after_empty_102");
            PAssert.That(() => sniffer.CellInfo.Current == "5-4 気のせい");
            PAssert.That(() => sniffer.CellInfo.Next == "5-4 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 10);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // うずしお通過、ここで減らす
            sniffer.SniffLogFile("uzushio_after_empty_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-4 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-4 次１戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 7);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);
        }

        /// <summary>
        /// 能動分岐のあとにうずしお
        /// </summary>
        [TestMethod]
        public void UzushioAfterSelectRoute()
        {
            var sniffer = new TestingSniffer();
            // うずしお前の残量
            sniffer.SniffLogFile("uzushio_after_select_route_101");
            PAssert.That(() => sniffer.CellInfo.Current == "5-5 １戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "5-5 次能動分岐");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 9);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            sniffer.SniffLogFile("uzushio_after_select_route_102");
            PAssert.That(() => sniffer.CellInfo.Current == "5-5 能動分岐");
            PAssert.That(() => sniffer.CellInfo.Next == "5-5 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 9);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);

            // うずしお通過、ここで減らす
            sniffer.SniffLogFile("uzushio_after_select_route_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-5 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-5 次２戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 6);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 10);
        }

        /// <summary>
        /// うずしおのあと進撃続行
        /// </summary>
        [TestMethod]
        public void UzushioToNextBattle()
        {
            var sniffer = new TestingSniffer();
            // うずしお前の残量
            sniffer.SniffLogFile("uzushio_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次１戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 85);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 120);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 25);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            sniffer.SniffLogFile("uzushio_102");
            PAssert.That(() => sniffer.CellInfo.Current == "3-3 １戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 68);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 96);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 16);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 20);

            // うずしお通過、ここで減らす
            sniffer.SniffLogFile("uzushio_103");
            PAssert.That(() => sniffer.CellInfo.Current == "3-3 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次２戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 59);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 96);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 13);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 20);

            // 戦闘後の進路指示で残量が矛盾しない
            sniffer.SniffLogFile("uzushio_104");
            PAssert.That(() => sniffer.CellInfo.Current == "3-3 ２戦目");
            PAssert.That(() => sniffer.CellInfo.Next == "3-3 次ボス戦");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 42);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 72);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Fuel == 10);
            PAssert.That(() => sniffer.Fleets[0].Ships[5].Bull == 15);
        }

        /// <summary>
        /// うずしお消費が表示される前にブラウザ再読み込み
        /// </summary>
        [TestMethod]
        public void UzushioToBrowserReload()
        {
            var sniffer = new TestingSniffer();
            // うずしお前の残量
            sniffer.SniffLogFile("uzushio_reload_101");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // 次うずしお、データはすでにここで来てるが、ここでは減らさない
            sniffer.SniffLogFile("uzushio_reload_102");
            PAssert.That(() => sniffer.CellInfo.Current == "");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 次渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 20);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // うずしお通過前にブラウザ再読み込み、減ってる
            sniffer.SniffLogFile("uzushio_reload_103");
            PAssert.That(() => sniffer.CellInfo.Current == "5-2 渦潮");
            PAssert.That(() => sniffer.CellInfo.Next == "5-2 渦潮");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 14);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);

            // そのまま出撃しても戦闘まで減らない
            sniffer.SniffLogFile("uzushio_reload_104");
            PAssert.That(() => sniffer.CellInfo.Current == "4-5 能動分岐");
            PAssert.That(() => sniffer.CellInfo.Next == "4-5 次１戦目");
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Fuel == 14);
            PAssert.That(() => sniffer.Fleets[0].Ships[0].Bull == 20);
        }
    }
}
