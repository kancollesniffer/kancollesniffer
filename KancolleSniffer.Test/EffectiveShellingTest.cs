// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class EffectiveShellingTest : ApiStart2
    {
        [TestMethod]
        public void Log_firepower_001()
        {
            var ship = NewShip(218);
            ship.Firepower = 85;
            ship.SetItems(NewItem(134, level: 9), NewItem(220, level: 1), NewItem(12, level: 1));
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 93.5);

            var ship2 = NewShip(221);
            ship2.Firepower = 77;
            ship2.SetItems(NewItem(247, level: 1), NewItem(275, level: 1));
            PAssert.That(() => EffectiveShelling.Calc(ship2).ShellingPower == 82.5);
        }

        [TestMethod]
        public void 副砲改修火力個別検証()
        {
            var ship = NewShip(541);
            ship.SetItems(_152cm単装砲(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items[0].Spec.Firepower;
            var effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 100, "Math.Sqrt(Level)");
            PAssert.That(() => effectiveShelling.SupportPower == 97);

            ship.SetItems(OTO152mm三連装速射砲(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items[0].Spec.Firepower;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 106, "Math.Sqrt(Level)");
            PAssert.That(() => effectiveShelling.SupportPower == 103);

            ship.SetItems(_90mm単装高角砲(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items[0].Spec.Firepower;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 99, "Math.Sqrt(Level)");
            PAssert.That(() => effectiveShelling.SupportPower == 96);

            ship.SetItems(_10cm連装高角砲群集中配備(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items[0].Spec.Firepower;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 99.8, "0.2 * Level");
            PAssert.That(() => effectiveShelling.SupportPower == 98);

            ship.SetItems(_5inch連装砲副砲配置集中配備(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items[0].Spec.Firepower;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 102.2, "0.3 * Level");
            PAssert.That(() => effectiveShelling.SupportPower == 100);

            ship.SetItems(_15_2cm三連装砲(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items[0].Spec.Firepower;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 103.2, "0.3 * Level");
            PAssert.That(() => effectiveShelling.SupportPower == 101);
        }

        [TestMethod]
        public void 軽巡に軽巡砲を装備して火力命中補正()
        {
            var ship = NewShip(52);
            ship.SetItems(_20_3cm連装砲(), _14cm単装砲());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 27, "軽巡に火力補正");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 113.9, "命中はマイナス補正");

            var ship2 = NewShip(58);
            ship2.SetItems(_20_3cm連装砲(), _15_2cm連装砲());
            ship2.Firepower = ship2.Spec.MinFirepower + ship2.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship2).ShellingPower == 28, "雷巡に火力補正");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship2).ShellingAccuracy, 2) == 115.9, "命中はマイナス補正");

            var ship3 = NewShip(343);
            ship3.SetItems(_14cm単装砲(), _15_2cm連装砲(), _15_2cm単装砲(), _6inch連装速射砲MkXXI());
            ship3.Firepower = ship3.Spec.MinFirepower + ship3.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship3).ShellingPower, 2) == 39.24, "練巡に火力補正");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship3).ShellingAccuracy, 2) == 122.9, "命中はマイナス補正");

            var ship4 = NewShip(262);
            ship4.SetItems(_14cm単装砲(), _15_2cm連装砲(), _15_2cm単装砲(), _6inch連装速射砲MkXXI());
            ship4.Firepower = ship4.Spec.MinFirepower + ship4.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship4).ShellingPower == 55, "重巡は補正なし");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship4).ShellingAccuracy, 2) == 122.9, "命中も補正なし");
        }

        [TestMethod]
        public void 軽巡中口径砲命中補正()
        {
            var ship    = NewShip(52);
            var agano   = NewShip(137);
            var atlanta = NewShip(597);
            var ooyodo  = NewShip(183);

            ship.Level = 99;
            ship.SetItems(_14cm単装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9, "Lv99では補正なし");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 118.0, "Lv100で加算補正");
            ship.SetItems(_14cm単装砲(), _14cm単装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 120.0, "加算補正2本分");

            ship.Level = 99;
            ship.SetItems(_15_2cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 118.9, "Lv99では補正なし");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 120.0, "Lv100で加算補正");
            ship.SetItems(_15_2cm連装砲(), _15_2cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 124.0, "加算補正2本分");

            agano.Level = 99;
            agano.SetItems(_6inch連装速射砲MkXXI());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(agano).ShellingAccuracy, 2) == 122.9, "阿賀野型は加算補正");
            agano.SetItems(_6inch連装速射砲MkXXI(), _6inch連装速射砲MkXXI());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(agano).ShellingAccuracy, 2) == 126.97, "加算補正2本分");
            agano.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(agano).ShellingAccuracy, 2) == 128.49, "Lv100で加算補正");

            ship.Level = 99;
            ship.SetItems(_6inchMkXXIII三連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 113.9, "減算補正");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 115.2, "Lv100で減算補正軽減");
            ship.SetItems(_6inchMkXXIII三連装砲(), _6inchMkXXIII三連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 114.4, "減算補正2本分");

            ship.Level = 99;
            ship.SetItems(_5inch連装両用砲集中配備());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 114.9, "減算補正");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.2, "Lv100で減算補正軽減");
            ship.SetItems(_5inch連装両用砲集中配備(), _5inch連装両用砲集中配備());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.4, "減算補正2本分");

            atlanta.Level = 99;
            atlanta.SetItems(_5inch連装両用砲集中配備());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(atlanta).ShellingAccuracy, 2) == 117.9, "アトランタ級は補正なし");
            atlanta.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(atlanta).ShellingAccuracy, 2) == 119.5, "Lv100で加算補正");
            atlanta.SetItems(_5inch連装両用砲集中配備(), _5inch連装両用砲集中配備());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(atlanta).ShellingAccuracy, 2) == 123.0, "加算補正2本分");

            ship.Level = 99;
            ship.SetItems(_15_5cm三連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 114.4, "減算補正");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.5, "Lv100で減算補正軽減");
            ship.SetItems(_15_5cm三連装砲(), _15_5cm三連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 117.0, "減算補正2本分");

            ooyodo.Level = 99;
            ooyodo.SetItems(_15_5cm三連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ooyodo).ShellingAccuracy, 2) == 115.9, "阿賀野型は減算補正軽減");
            ooyodo.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ooyodo).ShellingAccuracy, 2) == 116.4, "Lv100で減算補正軽減");
            ooyodo.SetItems(_15_5cm三連装砲(), _15_5cm三連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ooyodo).ShellingAccuracy, 2) == 116.8, "減算補正2本分");

            ship.Level = 99;
            ship.SetItems(_20_3cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 112.9, "減算補正");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 114.2, "Lv100で減算補正軽減");
            ship.SetItems(_20_3cm連装砲(), _20_3cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 112.4, "減算補正2本分");

            agano.Level = 99;
            agano.SetItems(_20_3cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(agano).ShellingAccuracy, 2) == 111.9, "阿賀野型は強減算");
            agano.SetItems(_20_3cm連装砲(), _20_3cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(agano).ShellingAccuracy, 2) == 107.9, "強減算2本分");
            agano.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(agano).ShellingAccuracy, 2) == 110.0, "Lv100で強減算軽減");

            ooyodo.Level = 99;
            ooyodo.SetItems(_20_3cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ooyodo).ShellingAccuracy, 2) == 110.9, "大淀型は強減算");
            ooyodo.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ooyodo).ShellingAccuracy, 2) == 113.0, "Lv100で強減算軽減");
            ooyodo.SetItems(_20_3cm連装砲(), _20_3cm連装砲());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ooyodo).ShellingAccuracy, 2) == 110.0, "強減算2本分");

            ship.Level = 99;
            ship.SetItems(_8inch三連装砲Mk9());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 104.9, "減算補正");
            ship.Level = 100;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 107.2, "Lv100で減算補正軽減");
            ship.SetItems(_8inch三連装砲Mk9(), _8inch三連装砲Mk9());
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 98.4, "減算補正2本分");
        }

        [TestMethod]
        public void イタリア重巡にイタリア重巡砲を装備して火力命中補正()
        {
            var ship = NewShip(448);
            ship.SetItems(_20_3cm連装砲(), _203mm53連装砲());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 59);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 114.9);

            var ship2 = NewShip(361);
            ship2.SetItems(_203mm53連装砲(), _203mm53連装砲());
            ship2.Firepower = ship2.Spec.MinFirepower + ship2.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship2).ShellingPower, 2) == 65.41);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship2).ShellingAccuracy, 2) == 113.9);

            var ship3 = NewShip(59);
            ship3.SetItems(_203mm53連装砲(), _203mm53連装砲());
            ship3.Firepower = ship3.Spec.MinFirepower + ship3.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship3).ShellingPower == 53, "イタリア以外の重巡は補正なし");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship3).ShellingAccuracy, 2) == 111.9, "命中も補正なし");
        }

        [TestMethod]
        public void 砲火力と命中()
        {
            var ship = NewShip(80);
            ship.SetItems(_20_3cm連装砲());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);

            ship.Level = 1;
            ship.Lucky = 20;
            var effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 95);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 98.71);
            PAssert.That(() => effectiveShelling.SupportPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 72.71);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            ship.Level = 99;
            ship.Lucky = 20;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 95);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 116.61);
            PAssert.That(() => effectiveShelling.SupportPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 90.61);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            ship.Level = 1;
            ship.Lucky = 79;
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 95);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 105.33);
            PAssert.That(() => effectiveShelling.SupportPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 79.33);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            var shipT1 = NewShip(80);
            shipT1.SetItems(_20_3cm連装砲());
            shipT1.Firepower = shipT1.Spec.MinFirepower + shipT1.Items.Sum(item => item.Spec.Firepower);
            shipT1.Fleet = NewTransportFleet1();
            shipT1.Level = 1;
            shipT1.Lucky = 20;
            effectiveShelling = EffectiveShelling.Calc(shipT1);
            PAssert.That(() => effectiveShelling.ShellingPower == 90);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 62.71);
            PAssert.That(() => effectiveShelling.SupportPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 72.71);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            var shipT2 = NewShip(80);
            shipT2.SetItems(_20_3cm連装砲());
            shipT2.Firepower = shipT2.Spec.MinFirepower + shipT2.Items.Sum(item => item.Spec.Firepower);
            shipT2.Fleet = NewTransportFleet2();
            shipT2.Level = 1;
            shipT2.Lucky = 20;
            effectiveShelling = EffectiveShelling.Calc(shipT2);
            PAssert.That(() => effectiveShelling.ShellingPower == 105);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 53.71);
            PAssert.That(() => effectiveShelling.SupportPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 72.71);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            ship.Level = 1;
            ship.Lucky = 20;
            ship.SetItems(_15_5cm三連装砲());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 99.71);
            PAssert.That(() => effectiveShelling.SupportPower == 93);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 73.71);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            ship.SetItems(_20_3cm連装砲(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 97);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 100.71);
            PAssert.That(() => effectiveShelling.SupportPower == 94);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 72.71);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            ship.SetItems(_41cm連装砲(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 110);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 100.71);
            PAssert.That(() => effectiveShelling.SupportPower == 106);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 72.71);
            PAssert.That(() => effectiveShelling.APShellPower == 0);
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 0);

            ship.SetItems(_20_3cm連装砲(), 九一式徹甲弾());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 103);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 99.71);
            PAssert.That(() => effectiveShelling.SupportPower == 102);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 73.71);
            PAssert.That(() => Math.Round(effectiveShelling.APShellPower, 2) == 111.24, "1.08");
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 109.68, "1.1");

            ship.SetItems(_20_3cm連装砲(), 九一式徹甲弾(4));
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 105);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 101.71);
            PAssert.That(() => effectiveShelling.SupportPower == 102);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 73.71);
            PAssert.That(() => Math.Round(effectiveShelling.APShellPower, 2) == 113.4, "1.08");
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 111.88, "1.1");

            ship.SetItems(_20_3cm連装砲(), 九一式徹甲弾(), _90mm単装高角砲());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 104);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 100.71);
            PAssert.That(() => effectiveShelling.SupportPower == 103);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 74.71);
            PAssert.That(() => Math.Round(effectiveShelling.APShellPower, 2) == 119.6, "1.15");
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 120.85, "1.2");

            ship.SetItems(_20_3cm連装砲(), 九一式徹甲弾(), SKレーダー());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 103);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 100.71);
            PAssert.That(() => effectiveShelling.SupportPower == 102);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 74.71);
            PAssert.That(() => Math.Round(effectiveShelling.APShellPower, 2) == 113.3, "1.1");
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 125.89, "1.25");

            ship.SetItems(_20_3cm連装砲(), 九一式徹甲弾(), _90mm単装高角砲(), SKレーダー());
            ship.Firepower = ship.Spec.MinFirepower + ship.Items.Sum(item => item.Spec.Firepower);
            effectiveShelling = EffectiveShelling.Calc(ship);
            PAssert.That(() => effectiveShelling.ShellingPower == 104);
            PAssert.That(() => Math.Round(effectiveShelling.ShellingAccuracy, 2) == 101.71);
            PAssert.That(() => effectiveShelling.SupportPower == 103);
            PAssert.That(() => Math.Round(effectiveShelling.SupportAccuracy, 2) == 75.71);
            PAssert.That(() => Math.Round(effectiveShelling.APShellPower, 2) == 119.6, "1.15");
            PAssert.That(() => Math.Round(effectiveShelling.APShellAccuracy, 2) == 132.22, "1.3");
        }

        [TestMethod]
        public void 空母に艦攻0機で昼攻撃不可()
        {
            var ship = NewShip(545);
            ship.SetItems(流星改(0));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 68 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 0);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 0);
        }

        [TestMethod]
        public void 空母に艦爆0機で昼攻撃不可()
        {
            var ship = NewShip(545);
            ship.SetItems(九九式艦爆(0));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 68 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 0);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 0);
        }

        [TestMethod]
        public void 空母に艦攻0機艦爆0機熟練甲板要員航空整備員で昼攻撃不可()
        {
            var ship = NewShip(545);
            ship.SetItems(流星改(0), 九九式艦爆(0), 熟練甲板要員航空整備員(10));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            ship.Firepower = 68 + 3 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 0);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 0);
            PAssert.That(() => ship.Items[0].ApBonus == 1);
            PAssert.That(() => ship.Items[1].ApBonus == 1);
        }

        [TestMethod]
        public void 空母に艦攻0機艦爆0機一式戦隼II型改20戦隊で昼攻撃不可()
        {
            var ship = NewShip(529);
            ship.SetItems(流星改(0), 九九式艦爆(0), 一式戦隼II型改20戦隊(10));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            ship.Firepower = 39 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 0);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 0);
        }

        [TestMethod]
        public void 速吸改に艦攻装備で空母と同じ昼爆撃()
        {
            var ship = NewShip(352);
            ship.SetItems(流星改(6));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 36 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 128);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 115.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 40);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);

            ship.Items[0].Alv = 1;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9, "支援には熟練度命中無効");
        }

        [TestMethod]
        public void 速吸改に瑞雲装備で通常の砲撃()
        {
            var ship = NewShip(352);
            ship.SetItems(瑞雲(6));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 36 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 41);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 40);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);

            ship.Items[0].Alv = 1;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9, "砲撃には熟練度命中無効");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);
        }

        [TestMethod]
        public void 山汐丸改に艦爆装備で空母と同じ昼爆撃()
        {
            var ship = NewShip(717);
            ship.SetItems(零式艦戦64型複座KMX搭載機(8));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 23 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 94);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 115.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 27);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);

            ship.Items[0].Alv = 1;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9, "支援には熟練度命中無効");
        }

        [TestMethod]
        public void 山汐丸改に対潜機装備で通常の砲撃()
        {
            var ship = NewShip(717);
            ship.SetItems(カ号観測機(8));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 23 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 28);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 27);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);

            ship.Items[0].Alv = 1;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9, "対潜機には熟練度命中無効");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);
        }

        [TestMethod]
        public void 速吸改に艦攻と水上機装備で水上機の攻撃力が加算()
        {
            var ship = NewShip(352);
            ship.SetItems(流星改(6), 瑞雲(1));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 36 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 136);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 40);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);

            ship.Items[1].Alv = 4;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 118.9, "水上機の熟練度で命中上昇");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);
        }

        [TestMethod]
        public void 速吸改に艦攻0機で昼攻撃不可()
        {
            var ship = NewShip(352);
            ship.SetItems(流星改(0));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 36 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 0);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 40);
        }

        [TestMethod]
        public void 速吸改に艦攻0機と水上機でも昼攻撃不可()
        {
            var ship = NewShip(352);
            ship.SetItems(流星改(0), 瑞雲(1));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 36 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 0);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 40);
        }

        [TestMethod]
        public void 山汐丸改に艦爆と一式戦隼II型改20戦隊で空母と同じ昼爆撃に加算する()
        {
            var ship = NewShip(717);
            ship.SetItems(零式艦戦64型複座KMX搭載機(8), 一式戦隼II型改20戦隊(6));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 23 + 1 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 107);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 115.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 30);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);
            var bomberPower = ship.Items[1].CalcBomberPower();
            PAssert.That(() => bomberPower.Length == 1);
            PAssert.That(() => Math.Round(bomberPower[0], 2) == 25, "爆装は0で計算");

            ship.Items[0].Alv = 1;
            ship.Items[1].Alv = 4;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 117.9, "一式戦の熟練度で命中上昇");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);


            ship.SetItems(零式艦戦64型複座KMX搭載機(8), 一式戦隼II型改20戦隊(6, 8));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 23 + 1 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 107, "改修値は昼爆撃に影響しない");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 30);
            bomberPower = ship.Items[1].CalcBomberPower();
            PAssert.That(() => bomberPower.Length == 1);
            PAssert.That(() => Math.Round(bomberPower[0], 2) == 25, "改修値は空戦に影響しない");
        }

        [TestMethod]
        public void 山汐丸改に対潜機装備と一式戦隼II型改20戦隊で通常の砲撃に加算しない()
        {
            var ship = NewShip(717);
            ship.SetItems(カ号観測機(8), 一式戦隼II型改20戦隊(6));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 23 + 1 + ship.Items.Sum(item => item.Spec.Firepower);
            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 31);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 30);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);

            ship.Items[0].Alv = 1;
            ship.Items[1].Alv = 3;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 116.9, "砲撃には熟練度命中無効");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 90.9);
        }

        [TestMethod]
        public void 不明な空母には雷装ボーナスなし()
        {
            var ship = NewShip(197);
            ship.SetItems(天山一二型甲(27));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 163, "雷装ボーナスは砲撃戦につかない");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 161);
            PAssert.That(() => ship.Items[0].TbBonus == 0, "天山に雷装ボーナスがつかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 57.41, "天山の雷装ボーナスが航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 107.65, "天山の雷装ボーナスが航空戦につかない");
        }

        [TestMethod]
        public void 翔鶴改二の天山に雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.SetItems(天山一二型甲(27));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 166, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 163);
            PAssert.That(() => ship.Items[0].TbBonus == 1, "天山に雷装ボーナスがつく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 61.57, "天山の雷装ボーナスが航空戦につく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 115.44, "天山の雷装ボーナスが航空戦につく");
        }

        [TestMethod]
        public void 翔鶴改二の流星改に雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.SetItems(天山一二型甲(27), 流星改(27));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 185, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 115.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 182);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);
            PAssert.That(() => ship.Items[0].TbBonus == 0, "天山の雷装ボーナスは天山につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 57.41, "雷装ボーナスは天山の航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 107.65, "雷装ボーナスは天山の航空戦につかない");
            PAssert.That(() => ship.Items[1].TbBonus == 1, "天山の雷装ボーナスは流星改につく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 78.2, "雷装ボーナスは流星改の航空戦につく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[1], 2) == 146.62, "雷装ボーナスは流星改の航空戦につく");

            ship.Items[0].Alv = 4;
            ship.Items[1].Alv = 2;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 119.9);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9, "支援には熟練度命中無効");
        }

        [TestMethod]
        public void 翔鶴改二の搭載0の流星改には雷装ボーナスつかない()
        {
            var ship = NewShip(461);
            ship.SetItems(天山一二型甲(27), 流星改(0));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 185, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 115.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 182);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);
            PAssert.That(() => ship.Items[0].TbBonus == 1, "天山の雷装ボーナスは天山につく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 61.57, "雷装ボーナスは天山の航空戦につく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 115.44, "雷装ボーナスは天山の航空戦につく");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "天山の雷装ボーナスは流星改につかない");
            PAssert.That(() => ship.Items[1].CalcBomberPower().Length == 0, "雷装ボーナスは流星改の航空戦につかない");

            ship.Items[0].Alv = 4;
            ship.Items[1].Alv = 2;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 120.9, "0機の機体は熟練度命中対象外");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 89.9);
        }

        [TestMethod]
        public void 翔鶴改二の機数が多いJu87Cに雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.SetItems(夜間作戦航空要員(), 天山一二型甲(26), Ju87C改二KMX搭載機(27));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 187, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 184);
            PAssert.That(() => ship.Items[1].TbBonus == 0, "天山の雷装ボーナスは天山につかない");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 56.71, "雷装ボーナスは天山の航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[1], 2) == 106.34, "雷装ボーナスは天山の航空戦につかない");
            PAssert.That(() => ship.Items[2].TbBonus == 1, "天山の雷装ボーナスはJu87Cにつく");
            PAssert.That(() => Math.Round(ship.Items[2].CalcBomberPower()[0], 2) == 76.96, "雷装ボーナスはJu87Cの航空戦につく");
        }

        [TestMethod]
        public void 翔鶴改二のスロットが上にある橘花改に雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.SetItems(夜間作戦航空要員(), 橘花改(27), 天山一二型甲改空六号電探改装備機(27));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + 2 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 197, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 118.9);
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 193);
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 92.9);
            PAssert.That(() => ship.Items[1].TbBonus == 2, "夜攻天山の雷装ボーナスは橘花改につく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 82.16, "雷装ボーナスは通常の航空戦のみ");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[1], 2) == 65.44, "雷装ボーナスは通常の航空戦のみ");
            PAssert.That(() => ship.Items[2].TbBonus == 0, "夜攻天山の雷装ボーナスは夜攻天山につかない");
            PAssert.That(() => Math.Round(ship.Items[2].CalcBomberPower()[0], 2) == 65.73, "雷装ボーナスは夜攻天山の航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[2].CalcBomberPower()[1], 2) == 123.24, "雷装ボーナスは夜攻天山の航空戦につかない");

            ship.Items[1].Alv = 2;
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).ShellingAccuracy, 2) == 119.9, "噴式機も熟練度命中有効");
            PAssert.That(() => Math.Round(EffectiveShelling.Calc(ship).SupportAccuracy, 2) == 92.9);
        }

        [TestMethod]
        public void 翔鶴改二の夜攻天山に雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.SetItems(天山一二型甲(27), 天山一二型甲改空六号電探改装備機(27));
            ship.ResolveTbBonus(AdditionalData);
            ship.Firepower = 63 + 2 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 185, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 182);
            PAssert.That(() => ship.Items[0].TbBonus == 0, "天山の雷装ボーナスは天山につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 57.41, "雷装ボーナスは天山の航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 107.65, "雷装ボーナスは天山の航空戦につかない");
            PAssert.That(() => ship.Items[1].TbBonus == 1, "天山の雷装ボーナスは夜攻天山につく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 69.88, "雷装ボーナスは夜攻天山の航空戦につく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[1], 2) == 131.03, "雷装ボーナスは夜攻天山の航空戦につく");
        }

        [TestMethod]
        public void 空母に熟練甲板要員航空整備員()
        {
            var ship = NewShip(197);
            ship.SetItems(天山一二型甲(27), 九九式艦爆(27), 熟練甲板要員航空整備員());
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 187, "雷装ボーナスは砲撃戦につかない");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 185);
            PAssert.That(() => ship.Items[0].TbBonus == 0, "天山に雷装ボーナスがつかない");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "九九式艦爆に雷装ボーナスがつかない");
            PAssert.That(() => ship.Items[0].ApBonus == 0, "天山に航空要員ボーナスがつかない");
            PAssert.That(() => ship.Items[1].ApBonus == 0, "九九式艦爆に航空要員ボーナスがつかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 57.41, "天山のボーナスが航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 107.65, "天山のボーナスが航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 50.98, "九九式艦爆のボーナスが航空戦につかない");
        }

        [TestMethod]
        public void 空母に熟練甲板要員航空整備員ボーナス()
        {
            var ship = NewShip(197);
            ship.SetItems(天山一二型甲(27), 九九式艦爆(27), 熟練甲板要員航空整備員(4));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            ship.Firepower = 63 + 1 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 193, "雷装ボーナスは砲撃戦につかない");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 188);
            PAssert.That(() => ship.Items[0].TbBonus == 0, "天山に雷装ボーナスがつかない");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "九九式艦爆に雷装ボーナスがつかない");
            PAssert.That(() => ship.Items[0].ApBonus == 0, "天山に航空要員ボーナスがつかない");
            PAssert.That(() => ship.Items[1].ApBonus == 1, "九九式艦爆に航空要員ボーナスがつく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 57.41, "天山のボーナスが航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 107.65, "天山のボーナスが航空戦につかない");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 56.18, "九九式艦爆の雷装ボーナスが航空戦につかないが、航空要員ボーナスはつく");
        }

        [TestMethod]
        public void 空母に熟練甲板要員航空整備員ボーナスと雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.SetItems(天山一二型甲(27), 九九式艦爆(27), 熟練甲板要員航空整備員(7), 熟練甲板要員航空整備員(4));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            ship.Firepower = 63 + 1 + 2 + ship.Items.Sum(item => item.Spec.Firepower);

            PAssert.That(() => EffectiveShelling.Calc(ship).ShellingPower == 216, "雷装ボーナスは砲撃戦にもつく");
            PAssert.That(() => EffectiveShelling.Calc(ship).SupportPower == 206);
            PAssert.That(() => ship.Items[0].TbBonus == 1, "天山に雷装ボーナスがつく");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "九九式艦爆に雷装ボーナスがつかない");
            PAssert.That(() => ship.Items[0].ApBonus == 1, "天山に航空要員ボーナスがつく");
            PAssert.That(() => ship.Items[1].ApBonus == 1, "九九式艦爆に航空要員ボーナスがつく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[0], 2) == 65.73, "天山の雷装ボーナスと航空要員ボーナスが航空戦につく");
            PAssert.That(() => Math.Round(ship.Items[0].CalcBomberPower()[1], 2) == 123.24, "天山の雷装ボーナスと航空要員ボーナスが航空戦につく");
            PAssert.That(() => Math.Round(ship.Items[1].CalcBomberPower()[0], 2) == 56.18, "九九式艦爆の雷装ボーナスが航空戦につかないが、航空要員ボーナスはつく");
        }
    }
}
