// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class AntiAirTest : ApiStart2
    {
        // https://twitter.com/nishikkuma/status/1555193708475486210
        [TestMethod]
        public void 装備ボーナス大和改二重()
        {
            var fleet = NewFleet();
            var ship = NewShip(916);
            ship.SetItems(_10cm連装高角砲群集中配備(), _10cm連装高角砲群集中配備(), A12cm30連装噴進砲改二());
            ship.AntiAir = 112 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 112 - ship.Spec.MinAntiAir;
            fleet.SetShips(ship.Id);
            fleet.CalcAntiAir();
            PAssert.That(() => fleet.AntiAir == 9);
            PAssert.That(() => ship.EffectiveAntiAirForShip == 256);
            PAssert.That(() => ship.PropShootdown == 64.0);
            PAssert.That(() => Math.Round(ship.FixedShootdown, 2) == 26.98);

            ship.AntiAir += 10;
            PAssert.That(() => ship.EffectiveAntiAirForShip == 262);
            PAssert.That(() => ship.PropShootdown == 65.5);
            PAssert.That(() => Math.Round(ship.FixedShootdown, 2) == 28.08);
        }

        // https://twitter.com/noro_006/status/1555206653586571264
        [TestMethod]
        public void 装備ボーナス由良改二()
        {
            var fleet = NewFleet();
            var ship = NewShip(488);
            ship.SetItems(A12_7cm単装高角砲改二());
            ship.AntiAir = 88 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 88 - ship.Spec.MinAntiAir;
            fleet.SetShips(ship.Id);
            fleet.CalcAntiAir();
            PAssert.That(() => fleet.AntiAir == 1);
            PAssert.That(() => ship.EffectiveAntiAirForShip == 108);
            PAssert.That(() => ship.PropShootdown == 27.0);
            PAssert.That(() => Math.Round(ship.FixedShootdown, 2) == 10.95);

            ship.AntiAir += 4;
            PAssert.That(() => ship.EffectiveAntiAirForShip == 110);
            PAssert.That(() => ship.PropShootdown == 27.5);
            PAssert.That(() => Math.Round(ship.FixedShootdown, 2) == 11.35);
        }

        [TestMethod]
        public void 噴進砲改二なし()
        {
            var ship = NewShip(73);
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => ship.AntiAirPropellantBarrageChance == 0);
        }

        [TestMethod]
        public void 噴進砲改二1つ()
        {
            var ship = NewShip(286);
            ship.SetItems(A12cm30連装噴進砲改二());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 61.71);
        }

        [TestMethod]
        public void 補強増設に噴進砲改二()
        {
            var ship = NewShip(83);
            ship.SetItems([], A12cm30連装噴進砲改二());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 61.71);
        }

        [TestMethod]
        public void 噴進砲改二2つ()
        {
            var ship = NewShip(89);
            ship.SetItems(A12cm30連装噴進砲改二(), A12cm30連装噴進砲改二());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 93.79);
        }

        [TestMethod]
        public void 噴進砲改二2つと機銃()
        {
            var ship = NewShip(102);
            ship.SetItems(A12cm30連装噴進砲改二(), A12cm30連装噴進砲改二(), A25mm三連装機銃集中配備());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 113.01, "噴進砲改二2+機銃");
        }

        [TestMethod]
        public void 伊勢型()
        {
            var ship = NewShip(82);
            ship.SetItems(A12cm30連装噴進砲改二());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 86.71);
        }

        [TestMethod]
        public void 噴進砲改二に装備ボーナス()
        {
            var ship = NewShip(73);
            ship.SetItems(A12cm30連装噴進砲改二(), A25mm三連装機銃集中配備());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 80.93);

            ship.AntiAir += 12;
            PAssert.That(() => Math.Round(ship.AntiAirPropellantBarrageChance, 2) == 84.48);
        }

        [TestMethod]
        public void 空母水母航戦航巡以外は噴進弾幕不可()
        {
            var ship = NewShip(78);
            ship.SetItems(A12cm30連装噴進砲改二());
            ship.AntiAir = 85 + ship.Items.Sum(item => item.Spec.AntiAir);
            ship.ImprovedAntiAir = 85 - ship.Spec.MinAntiAir;
            ship.Lucky = 46;
            PAssert.That(() => ship.AntiAirPropellantBarrageChance == 0);
        }
    }
}
