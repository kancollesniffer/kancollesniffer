// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using DynaJson;
using KancolleSniffer.Model;

namespace KancolleSniffer.Test
{
    public class QuestCounterTester : ApiStart2
    {
        protected readonly BattleInfo battleInfo = new(null, null, null);
        protected readonly QuestInfo questInfo = new(new QuestCountList(), () => new DateTime(2015, 1, 1));
        protected readonly QuestCounter questCounter;

        public QuestCounterTester()
        {
            questCounter = new QuestCounter(questInfo, ItemInventory, ShipInventory, battleInfo);
        }

        protected dynamic CreateQuestList(int[] ids) => new JsonObject(new
        {
            api_list = ids.Select(id => new
            {
                api_no = id,
                api_category = id / 100,
                api_type = 1,
                api_state = 2,
                api_title = "",
                api_detail = "",
                api_get_material = new int[0],
                api_progress_flag = 0
            })
        });

        protected QuestCount InjectQuest(int id)
        {
            InjectQuestList(id);
            return questInfo.Quests[0].Count;
        }

        protected void InjectQuestList(params int[] ids)
        {
            questInfo.InspectQuestList("api_tab_id=0", CreateQuestList(ids));
        }

        protected void InjectMapStart(int map, int eventId)
        {
            questCounter.InspectMapStart(CreateMap(map, eventId));
        }

        protected void InjectMapNext(int map, int eventId, int cell = 0)
        {
            questCounter.InspectMapNext(CreateMap(map, eventId, cell));
        }

        protected dynamic CreateMap(int map, int eventId, int cell = 0) => new JsonObject(new
        {
            api_maparea_id = map / 10,
            api_mapinfo_no = map % 10,
            api_no = cell,
            api_event_id = eventId
        });

        protected void InjectBattleResult(string result)
        {
            questCounter.InspectBattleResult(CreateWinRank(result));
        }

        protected void InjectPracticeResult(string result)
        {
            questCounter.InspectPracticeResult(CreateWinRank(result));
        }

        protected dynamic CreateWinRank(string result) => new JsonObject(new { api_win_rank = result });
    }
}
