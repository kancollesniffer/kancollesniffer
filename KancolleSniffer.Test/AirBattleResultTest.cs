// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class AirBattleResultTest
    {
        /// <summary>
        /// 敵艦隊の制空値を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPower()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("enemy_combined_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            PAssert.That(() => fp.Interception.Aircrafts == 277);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 264);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 61);
            PAssert.That(() => fp.Interception.FighterPower == 215);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 209);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 102);
        }

        /// <summary>
        /// 噴式機で敵空母を撃沈したあとの制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerAfterJetBomber()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("jetbomber_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == 109);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 108);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 54);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.FighterPower == 17);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 16);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 8);
            PAssert.That(() => fp.AirCombatAircraftsOffset == 54);
            PAssert.That(() => fp.InterceptionAircraftsOffset == 54);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 基地航空隊の噴式機のあとの制空値と搭載数を通常の基地航空戦で計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerAfterAirBaseJetBomber()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airbase_jetbomber_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.Aircrafts == 179);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 176);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 98);
            PAssert.That(() => fp.Interception.FighterPower == 47);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 46);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 36);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 演習での噴式機対噴式機の空戦の制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerJetBomberVsJetBomber()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("jet_vs_jet_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == abr.First.Stage1.EnemyCount + abr.EnemyLostJets);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 58);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 48);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 230);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 214);
        }

        /// <summary>
        /// 迎撃機ありの基地空襲戦
        /// </summary>
        [TestMethod]
        public void AirRaidBattleWithInterceptor()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airraid_battle_001");
            var battle = sniffer.Battle;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => battle.BattleState == BattleState.AirRaid);
            PAssert.That(() => battle.AirControlLevel == 2);
            PAssert.That(() => battle.FighterPower.Min == 425);
            PAssert.That(() => battle.EnemyFighterPower.Interception.FighterPower == 231);
            PAssert.That(() => battle.EnemyFighterPower.LoggerAirCombat.FighterPower == 231);
            PAssert.That(() => battle.EnemyFighterPower.ActualAirCombat.FighterPower == 231);
            PAssert.That(() => battle.ResultRank == BattleResultRank.S);
            var ships = battle.Result.Friend.Main;
            PAssert.That(() => ships.Select(ship => ship.NowHp).ParamsEqual(200, 200, 200));
            PAssert.That(() => ships.Select(ship => ship.Spec.Name).ParamsEqual("基地航空隊1", "基地航空隊2", "基地航空隊3"));
            PAssert.That(() => ships[2].Items.Select(item => item.Spec.Name).ParamsEqual("烈風改(三五二空/熟練)", "雷電", "雷電", "烈風改"));
            PAssert.That(() => ships[2].Items.Select(item => item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "空襲");
        }

        /// <summary>
        /// 迎撃機なしの基地航空戦
        /// </summary>
        [TestMethod]
        public void AirRaidBattleWithoutInterceptor()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airraid_battle_002");
            var battle = sniffer.Battle;
            PAssert.That(() => battle.BattleState == BattleState.AirRaid);
            PAssert.That(() => battle.AirControlLevel == 4);
            PAssert.That(() => battle.FighterPower.Min == 0);
            PAssert.That(() => battle.EnemyFighterPower.Interception.FighterPower == 231);
            PAssert.That(() => battle.EnemyFighterPower.LoggerAirCombat.FighterPower == 231);
            PAssert.That(() => battle.EnemyFighterPower.ActualAirCombat.FighterPower == 231);
            PAssert.That(() => battle.ResultRank == BattleResultRank.B);
            var ships = battle.Result.Friend.Main;
            PAssert.That(() => ships.Select(ship => ship.NowHp).ParamsEqual(82, 174, 147));
        }

        /// <summary>
        /// 基地航空戦直後のボス戦
        /// </summary>
        [TestMethod]
        public void AirRaidBattleBeforeBoss()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airraid_battle_004");
            var battle = sniffer.Battle;
            PAssert.That(() => battle.BattleState == BattleState.Result);
            PAssert.That(() => sniffer.BadlyDamagedShips.Length == 0);
        }

        /// <summary>
        /// 22冬e-5の超重爆迎撃Good判定
        /// </summary>
        [TestMethod]
        public void AirRaidBattleWithUserInput()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("airraid-input_good_101");
            var battle = sniffer.Battle;
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 53);
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => battle.FighterPower.Min == 356);
            PAssert.That(() => battle.FighterPower.Min == airbase.CalcInterceptionFighterPower(2, true).Min);
            PAssert.That(() => abr.Result[2].Stage1.FriendCount == 47);
            PAssert.That(() => abr.Result[2].Stage1.FriendCount
                == airbase.AirCorps.Take(2).SelectMany(aircorps => aircorps.Planes).Where(plane => plane.Deploying).Sum(plane => plane.Item.OnSlot));
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "空襲3");

            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.State).ParamsEqual(0, 0, 1, 1));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.State).ParamsEqual(0, 0, 1, 1));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.State).ParamsEqual(0, 0, 1, 1));

            PAssert.That(() => airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv).ParamsEqual(4, 2));
            PAssert.That(() => airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv).ParamsEqual(7, 7));
            PAssert.That(() => airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv).ParamsEqual(7, 7));
            PAssert.That(() => airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot).ParamsEqual(11, 12));
            PAssert.That(() => airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot).ParamsEqual(12, 12));
            PAssert.That(() => airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18));

            PAssert.That(() => battle.Result.Friend.Main[0].Items.Select(item => item.Alv)
                .SequenceEqual(airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            PAssert.That(() => battle.Result.Friend.Main[1].Items.Select(item => item.Alv)
                .SequenceEqual(airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            PAssert.That(() => battle.Result.Friend.Main[2].Items.Select(item => item.Alv)
                .SequenceEqual(airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Alv)));
            PAssert.That(() => battle.Result.Friend.Main[0].Items.Select(item => item.OnSlot)
                .SequenceEqual(airbase.AirCorps[0].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
            PAssert.That(() => battle.Result.Friend.Main[1].Items.Select(item => item.OnSlot)
                .SequenceEqual(airbase.AirCorps[1].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
            PAssert.That(() => battle.Result.Friend.Main[2].Items.Select(item => item.OnSlot)
                .SequenceEqual(airbase.AirCorps[2].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.OnSlot)));
        }

        /// <summary>
        /// 基地航空隊分散出撃時の残機数変動
        /// </summary>
        [TestMethod]
        public void AirbaseOnSlotInSortie()
        {
            var sniffer = new TestingSniffer();
            // 出撃前
            sniffer.SniffLogFile("strike_point_split_101");
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.State).ParamsEqual(1, 1, 1, 1));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.State).ParamsEqual(1, 1, 1, 1));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.State).ParamsEqual(1, 1, 1, 1));

            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv).ParamsEqual(5, 5, 0, 7));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv).ParamsEqual(3, 4, 3, 7));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv).ParamsEqual(7, 7, 7, 7));
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(1, 18, 5, 4));
            PAssert.That(() => airbase.AirCorps[0].CalcFighterPower().AirCombat.Min == 134);
            PAssert.That(() => airbase.AirCorps[1].CalcFighterPower().AirCombat.Min == 108);
            PAssert.That(() => airbase.AirCorps[2].CalcFighterPower().Interception.Min == 263);

            // 基地空襲、防空あり、地上撃破あり
            sniffer.SniffLogFile("strike_point_split_102");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv).ParamsEqual(5, 5, 0, 7));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv).ParamsEqual(3, 4, 3, 7));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv).ParamsEqual(7, 7, 7, 7));
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(1, 18, 5, 4));
            PAssert.That(() => airbase.AirCorps[0].CalcFighterPower().AirCombat.Min == 134);
            PAssert.That(() => airbase.AirCorps[1].CalcFighterPower().AirCombat.Min == 108);
            PAssert.That(() => airbase.AirCorps[2].CalcFighterPower().Interception.Min == 263);

            // 基地航空隊出撃1 (対艦)
            sniffer.SniffLogFile("strike_point_split_103");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv).ParamsEqual(5, 5, 0, 7));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv).ParamsEqual(3, 4, 3, 7));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv).ParamsEqual(7, 7, 7, 7));
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(1, 18, 5, 4));
            PAssert.That(() => airbase.AirCorps[0].CalcFighterPower().AirCombat.Min == 134);
            PAssert.That(() => airbase.AirCorps[1].CalcFighterPower().AirCombat.Min == 108);
            PAssert.That(() => airbase.AirCorps[2].CalcFighterPower().Interception.Min == 263);
            var battle = sniffer.Battle;
            PAssert.That(() => airbase.AirCorps[1].Planes.Sum(plane => plane.Item.OnSlot) == battle.AirBattleResult.Result[0].Stage1.FriendCount);

            // 基地航空隊出撃2 (ボス)
            sniffer.SniffLogFile("strike_point_split_104");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 6);
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.Alv).ParamsEqual(5, 5, 0, 7));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.Alv).ParamsEqual(3, 4, 3, 7));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.Alv).ParamsEqual(7, 7, 7, 7));
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(17, 18, 18, 18));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(16, 16, 17, 17));
            PAssert.That(() => airbase.AirCorps[2].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(1, 18, 5, 4));
            PAssert.That(() => airbase.AirCorps[0].CalcFighterPower().AirCombat.Min == 133);
            PAssert.That(() => airbase.AirCorps[1].CalcFighterPower().AirCombat.Min == 106);
            PAssert.That(() => airbase.AirCorps[2].CalcFighterPower().Interception.Min == 263);
            battle = sniffer.Battle;
            PAssert.That(() => airbase.AirCorps[0].Planes.Sum(plane => plane.Item.OnSlot) == battle.AirBattleResult.Result[0].Stage1.FriendCount);
            PAssert.That(() => airbase.AirCorps[1].Planes.Sum(plane => plane.Item.OnSlot) == battle.AirBattleResult.Result[2].Stage1.FriendCount);
        }

        /// <summary>
        /// 配置転換中でも戦闘時に残機数0が送られてくる
        /// </summary>
        [TestMethod]
        public void AirbaseOnSlotInSortieWhenChanging()
        {
            var sniffer = new TestingSniffer();
            // 出撃前
            sniffer.SniffLogFile("airbase_onslot_slip_101");
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 56);
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.State).ParamsEqual(2, 1, 2, 1));
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(0, 9, 0, 4));

            // 出動地点到着
            sniffer.SniffLogFile("airbase_onslot_slip_102");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 56);
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.State).ParamsEqual(2, 1, 2, 1));
            PAssert.That(() => airbase.AirCorps[0].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(0, 9, 0, 4));
        }

        /// <summary>
        /// 配置転換中では防空時に残機数0の情報は送られない
        /// </summary>
        [TestMethod]
        public void AirbaseOnSlotInAirRaidWhenChanging()
        {
            var sniffer = new TestingSniffer();
            // 出撃前
            sniffer.SniffLogFile("airraid_changing-101");
            var airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 59);
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.State).ParamsEqual(1, 1, 2, 0));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 0, 0));

            // 出動地点到着
            sniffer.SniffLogFile("airraid_changing-102");
            airbase = sniffer.AirBase.FirstOrDefault(b => b.AreaId == 59);
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.State).ParamsEqual(1, 1, 2, 0));
            PAssert.That(() => airbase.AirCorps[1].Planes.Select(plane => plane.Item.OnSlot).ParamsEqual(18, 18, 0, 0));
        }

        /// <summary>
        /// 通常対敵連合での基地航空戦の制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerOnAirBase()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("aircraft_count_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.Aircrafts == 227);
            PAssert.That(() => fp.LoggerAirCombat.Aircrafts == 216);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 45);
            PAssert.That(() => fp.Interception.FighterPower == 206);
            PAssert.That(() => fp.LoggerAirCombat.FighterPower == 201);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 72);
            PAssert.That(() => abr.Result[abr.ResultPanelInitIndex()].PhaseName == "航空戦");
        }

        /// <summary>
        /// 連合対敵通常、連合対敵連合での基地航空戦の制空値と搭載数を計算する
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerOnAirBaseAndCombined()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("aircraft_count_combined_001");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.Interception.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.Interception.Aircrafts == 15);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 0);
            PAssert.That(() => fp.Interception.FighterPower == 3);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 0);

            sniffer.SniffLogFile("aircraft_count_combined_002");
            var fp2 = sniffer.Battle.EnemyFighterPower;
            var abr2 = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp2.Interception.Aircrafts == abr2.First.Stage1.EnemyCount);
            PAssert.That(() => fp2.Interception.Aircrafts == 291);
            PAssert.That(() => fp2.LoggerAirCombat.Aircrafts == 286);
            PAssert.That(() => fp2.ActualAirCombat.Aircrafts == 184);
            PAssert.That(() => fp2.Interception.FighterPower == 331);
            PAssert.That(() => fp2.LoggerAirCombat.FighterPower == 329);
            PAssert.That(() => fp2.ActualAirCombat.FighterPower == 262);
        }

        /// <summary>
        /// 敵連合護衛空母の制空値
        /// </summary>
        [TestMethod]
        public void EnemyFighterPowerGuardAircraftCarrier()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("enemy_combined_carrier-101");
            var fp = sniffer.Battle.EnemyFighterPower;
            var abr = sniffer.Battle.AirBattleResult;
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == abr.First.Stage1.EnemyCount);
            PAssert.That(() => fp.ActualAirCombat.Aircrafts == 366);
            PAssert.That(() => fp.ActualAirCombat.FighterPower == 286);
        }
    }
}
