// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class ExMapInfoTest
    {
        /// <summary>
        /// EOの海域ゲージを消費できなかった
        /// </summary>
        [TestMethod]
        public void EOMaphpFail()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 8, 28, 41, 467);
            sniffer.SniffLogFile("eo35-maphp-fail_101");
            var gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 3/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "13日 13:31:18");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 8, 34, 55, 219);
            sniffer.SniffLogFile("eo35-maphp-fail_102");
            gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 3/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "13日 13:25:04");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 8, 35, 2, 179);
            sniffer.SniffLogFile("eo35-maphp-fail_103");
            gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 3/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "13日 13:24:57");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// EOの海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EOMaphpDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 18, 10, 20, 41, 321);
            sniffer.SniffLogFile("eo35-maphp-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 3/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "12日 11:39:18");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 18, 10, 25, 51, 321);
            sniffer.SniffLogFile("eo35-maphp-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 2/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "12日 11:34:08");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 18, 10, 25, 59, 916);
            sniffer.SniffLogFile("eo35-maphp-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 2/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "12日 11:34:00");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// EOの海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EOMaphpSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 19, 8, 50, 18, 167);
            sniffer.SniffLogFile("eo35-maphp-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => !sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "残り 1/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "11日 13:09:41");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 19, 8, 57, 36, 816);
            sniffer.SniffLogFile("eo35-maphp-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 19, 8, 57, 43, 282);
            sniffer.SniffLogFile("eo35-maphp-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[35];
            PAssert.That(() => sniffer.ExMap.Cleared(35));
            PAssert.That(() => gauge.MapName == "3-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");
        }

        /// <summary>
        /// 破壊後のEOに再出撃し敵旗艦撃沈
        /// </summary>
        [TestMethod]
        public void EOMaphpCleared()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 2, 51, 819);
            sniffer.SniffLogFile("eo15-maphp-cleared_101");
            var gauge = sniffer.ExMap.GaugeStatus[15];
            PAssert.That(() => sniffer.ExMap.Cleared(15));
            PAssert.That(() => gauge.MapName == "1-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 6, 28, 37);
            sniffer.SniffLogFile("eo15-maphp-cleared_102");
            gauge = sniffer.ExMap.GaugeStatus[15];
            PAssert.That(() => sniffer.ExMap.Cleared(15));
            PAssert.That(() => gauge.MapName == "1-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 6, 37, 189);
            sniffer.SniffLogFile("eo15-maphp-cleared_103");
            gauge = sniffer.ExMap.GaugeStatus[15];
            PAssert.That(() => sniffer.ExMap.Cleared(15));
            PAssert.That(() => gauge.MapName == "1-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");
        }

        /// <summary>
        /// 多段階EO1段階目の海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EO751MaphpDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 31, 19, 358);
            sniffer.SniffLogFile("eo751-maphp-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-1");
            PAssert.That(() => gauge.GaugeText == "残り 2/2");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:28:40");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 37, 51, 217);
            sniffer.SniffLogFile("eo751-maphp-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-1");
            PAssert.That(() => gauge.GaugeText == "残り 1/2");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:22:08");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 37, 58, 763);
            sniffer.SniffLogFile("eo751-maphp-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-1");
            PAssert.That(() => gauge.GaugeText == "残り 1/2");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:22:01");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO1段階目の海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EO751MaphpSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 47, 51, 19);
            sniffer.SniffLogFile("eo751-maphp-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-1");
            PAssert.That(() => gauge.GaugeText == "残り 1/2");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:12:08");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 53, 18, 189);
            sniffer.SniffLogFile("eo751-maphp-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-1");
            PAssert.That(() => gauge.GaugeText == "残り 0/2");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:06:41");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 54, 3, 478);
            sniffer.SniffLogFile("eo751-maphp-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:05:56");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO2段階目の状態で1段階目の敵旗艦を撃沈
        /// </summary>
        [TestMethod]
        public void EO751MaphpCleared()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 15, 58, 39, 59);
            sniffer.SniffLogFile("eo751-maphp-cleared_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 06:01:20");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 16, 4, 41, 395);
            sniffer.SniffLogFile("eo751-maphp-cleared_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 05:55:18");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 16, 4, 53, 770);
            sniffer.SniffLogFile("eo751-maphp-cleared_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 05:55:06");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO2段階目の海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EO752MaphpDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 16, 47, 8, 658);
            sniffer.SniffLogFile("eo752-maphp-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 05:12:51");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 16, 54, 34, 380);
            sniffer.SniffLogFile("eo752-maphp-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 2/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 05:05:25");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 16, 55, 7, 461);
            sniffer.SniffLogFile("eo752-maphp-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 2/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 05:04:52");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO2段階目の海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EO752MaphpSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 17, 22, 21, 156);
            sniffer.SniffLogFile("eo752-maphp-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 1/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 04:37:38");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 17, 29, 45, 866);
            sniffer.SniffLogFile("eo752-maphp-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-2");
            PAssert.That(() => gauge.GaugeText == "残り 0/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 04:30:14");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 17, 30, 9, 569);
            sniffer.SniffLogFile("eo752-maphp-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 04:29:50");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO3段階目の状態で2段階目の敵旗艦を撃沈
        /// </summary>
        [TestMethod]
        public void EO752MaphpCleared()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 17, 31, 29, 861);
            sniffer.SniffLogFile("eo752-maphp-cleared_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 04:28:30");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 17, 38, 1, 38);
            sniffer.SniffLogFile("eo752-maphp-cleared_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 04:21:58");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 17, 39, 12, 219);
            sniffer.SniffLogFile("eo752-maphp-cleared_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 04:20:47");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO3段階目の海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EO753MaphpDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 18, 24, 34, 816);
            sniffer.SniffLogFile("eo753-maphp-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 3/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 03:35:25");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 18, 31, 43, 654);
            sniffer.SniffLogFile("eo753-maphp-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 2/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 03:28:16");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 18, 31, 54, 439);
            sniffer.SniffLogFile("eo753-maphp-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 2/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 03:28:05");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 多段階EO3段階目の海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EO753MaphpSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 18, 53, 21, 120);
            sniffer.SniffLogFile("eo753-maphp-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => !sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5-3");
            PAssert.That(() => gauge.GaugeText == "残り 1/3");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "10日 03:06:38");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 19, 2, 52, 827);
            sniffer.SniffLogFile("eo753-maphp-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 20, 18, 53, 21, 120);
            sniffer.SniffLogFile("eo753-maphp-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[75];
            PAssert.That(() => sniffer.ExMap.Cleared(75));
            PAssert.That(() => gauge.MapName == "7-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");
        }

        /// <summary>
        /// 6-5の敵連合編成海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EOMaphpCombinedFleetDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 21, 55, 34, 379);
            sniffer.SniffLogFile("eo65-maphp-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[65];
            PAssert.That(() => !sniffer.ExMap.Cleared(65));
            PAssert.That(() => gauge.MapName == "6-5");
            PAssert.That(() => gauge.GaugeText == "残り 6/6");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "13日 00:04:25");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 22, 3, 49, 521);
            sniffer.SniffLogFile("eo65-maphp-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[65];
            PAssert.That(() => !sniffer.ExMap.Cleared(65));
            PAssert.That(() => gauge.MapName == "6-5");
            PAssert.That(() => gauge.GaugeText == "残り 5/6");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "12日 23:56:10");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 22, 4, 8, 230);
            sniffer.SniffLogFile("eo65-maphp-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[65];
            PAssert.That(() => !sniffer.ExMap.Cleared(65));
            PAssert.That(() => gauge.MapName == "6-5");
            PAssert.That(() => gauge.GaugeText == "残り 5/6");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "12日 23:55:51");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 時間切れの後にEOの海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EOMaphpLimitoverSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 5, 31, 21, 59, 5, 182);
            sniffer.SniffLogFile("eo25-maphp-limitover-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 1/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "00:00:54");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "05/31 22:00");

            // 時間経過で自動的に時間切れ表示
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 5, 31, 22, 12, 42, 468);
            gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 1/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            // 時間切れの後にEOの海域ゲージを破壊する
            sniffer.SniffLogFile("eo25-maphp-limitover-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 0/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("eo25-maphp-limitover-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 0/0");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");
        }

        /// <summary>
        /// 1-6の海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EO16MaphpDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 15, 9, 1, 49, 194);
            sniffer.SniffLogFile("eo16-maphp-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 3/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "15日 12:58:10");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 終点手前の戦闘終了
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 15, 9, 5, 48, 188);
            sniffer.SniffLogFile("eo16-maphp-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 3/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "15日 12:54:11");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 15, 9, 6, 2, 953);
            sniffer.SniffLogFile("eo16-maphp-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 2/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "15日 12:53:57");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 15, 9, 6, 8, 654);
            sniffer.SniffLogFile("eo16-maphp-damage_104");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 2/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "15日 12:53:51");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");
        }

        /// <summary>
        /// 1-6の海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EO16MaphpSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 15, 17, 274);
            sniffer.SniffLogFile("eo16-maphp-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 1/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "13日 01:44:42");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "04/30 22:00");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 19, 11, 744);
            sniffer.SniffLogFile("eo16-maphp-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 19, 19, 168);
            sniffer.SniffLogFile("eo16-maphp-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");
        }

        /// <summary>
        /// 輸送済みの1-6に再出撃し終点に到着
        /// </summary>
        [TestMethod]
        public void EO16MaphpCleared()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 21, 7, 8);
            sniffer.SniffLogFile("eo16-maphp-cleared_101");
            var gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 25, 56, 875);
            sniffer.SniffLogFile("eo16-maphp-cleared_102");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 帰投後、ゲージを再確認
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 17, 20, 26, 4, 561);
            sniffer.SniffLogFile("eo16-maphp-cleared_103");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");
        }

        /// <summary>
        /// 時間切れの後に1-6の海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void EO16MaphpLimitoverDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 7, 31, 21, 59, 5, 182);
            sniffer.SniffLogFile("eo16_maphp_limitover_damage-101");
            var gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 2/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "00:00:54");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "07/31 22:00");

            // 時間経過で自動的に時間切れ表示
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 7, 31, 22, 1, 42, 468);
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 2/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            sniffer.SniffLogFile("eo16_maphp_limitover_damage-102");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 1/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("eo16_maphp_limitover_damage-103");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 1/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");
        }

        /// <summary>
        /// 時間切れの後に1-6の海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void EO16MaphpLimitoverSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 7, 31, 21, 59, 5, 182);
            sniffer.SniffLogFile("eo16_maphp_limitover_sunk-101");
            var gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 1/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "00:00:54");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "07/31 22:00");

            // 時間経過で自動的に時間切れ表示
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 7, 31, 22, 8, 42, 468);
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 1/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            sniffer.SniffLogFile("eo16_maphp_limitover_sunk-102");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 0/7");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("eo16_maphp_limitover_sunk-103");
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "残り 0/0");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");
        }

        /// <summary>
        /// 月末日22時以降に海域情報を更新する
        /// </summary>
        [TestMethod]
        public void ReloadEOMaphpAfterLimittime()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 5, 31, 22, 29, 1);
            sniffer.ExMap.InjectLastReset(sniffer.ExMap.NowFunc());
            sniffer.ExMap.InjectCleared(15);
            sniffer.SniffLogFile("eo25-maphp-limitover-cleared_101");

            // 時間切れ前に割った状態で再ログイン
            var gauge = sniffer.ExMap.GaugeStatus[15];
            PAssert.That(() => sniffer.ExMap.Cleared(15));
            PAssert.That(() => gauge.MapName == "1-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 時間切れ後に割った状態で再ログイン
            gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 0/0");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            // 時間切れの後にゲージを破壊したEO海域に再出撃
            sniffer.SniffLogFile("eo25-maphp-limitover-cleared_102");
            gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 0/0");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("eo25-maphp-limitover-cleared_103");
            gauge = sniffer.ExMap.GaugeStatus[25];
            PAssert.That(() => !sniffer.ExMap.Cleared(25));
            PAssert.That(() => gauge.MapName == "2-5");
            PAssert.That(() => gauge.GaugeText == "残り 0/0");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "時間切れ");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "時間切れ");
        }

        /// <summary>
        /// 起動したまま翌月を迎えて海域情報を更新する
        /// </summary>
        [TestMethod]
        public void ReloadEOMaphpNextMonth()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 5, 31, 22, 29, 1);
            sniffer.ExMap.InjectLastReset(sniffer.ExMap.NowFunc());
            sniffer.ExMap.InjectCleared(15);
            sniffer.ExMap.InjectCleared(16);
            sniffer.SniffLogFile("eo25-maphp-limitover-cleared_101");

            // 前月1-5は達成済
            var gauge = sniffer.ExMap.GaugeStatus[15];
            PAssert.That(() => sniffer.ExMap.Cleared(15));
            PAssert.That(() => gauge.MapName == "1-5");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 前月1-6も達成済
            gauge = sniffer.ExMap.GaugeStatus[16];
            PAssert.That(() => sniffer.ExMap.Cleared(16));
            PAssert.That(() => gauge.MapName == "1-6");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "達成済");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "達成済");

            // 翌月再ログインで1-5は消去
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 6, 1, 9, 32, 1);
            sniffer.SniffLogFile("eo-refresh_101");
            PAssert.That(() => !sniffer.ExMap.Cleared(15));
            PAssert.That(() => !sniffer.ExMap.GaugeStatus.ContainsKey(15));

            // 翌月再ログインで1-6も消去
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => !sniffer.ExMap.GaugeStatus.ContainsKey(16));

            // 海域情報を更新すると1-5は初期化
            sniffer.SniffLogFile("eo-refresh_102");
            gauge = sniffer.ExMap.GaugeStatus[15];
            PAssert.That(() => !sniffer.ExMap.Cleared(15));
            PAssert.That(() => gauge.MapName == "1-5");
            PAssert.That(() => gauge.GaugeText == "残り 4/4");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "29日 12:27:59");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "06/30 22:00");

            // 海域情報を更新しても1-6は情報なし
            PAssert.That(() => !sniffer.ExMap.Cleared(16));
            PAssert.That(() => !sniffer.ExMap.GaugeStatus.ContainsKey(16));
        }

        /// <summary>
        /// 常設の海域ゲージを1回消費する
        /// </summary>
        [TestMethod]
        public void MaphpFirstDamage()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 30, 22, 5, 1);
            sniffer.SniffLogFile("map74-maphp-first-damage_101");
            var gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "残り 5/5");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");

            sniffer.SniffLogFile("map74-maphp-first-damage_102");
            gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "残り 4/5");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("map74-maphp-first-damage_103");
            gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "残り 4/5");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");
        }

        /// <summary>
        /// 常設の海域ゲージを破壊する
        /// </summary>
        [TestMethod]
        public void MaphpFirstSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 30, 22, 5, 1);
            sniffer.SniffLogFile("map74-maphp-first-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "残り 1/5");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");

            sniffer.SniffLogFile("map74-maphp-first-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("map74-maphp-first-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");
        }

        /// <summary>
        /// 突破済みの常設の海域ゲージを再度破壊する
        /// </summary>
        [TestMethod]
        public void MaphpClearedSunk()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 4, 30, 22, 5, 1);
            sniffer.SniffLogFile("map74-maphp-cleared-sunk_101");
            var gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");

            sniffer.SniffLogFile("map74-maphp-cleared-sunk_102");
            gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("map74-maphp-cleared-sunk_103");
            gauge = sniffer.ExMap.GaugeStatus[74];
            PAssert.That(() => gauge.MapName == "7-4");
            PAssert.That(() => gauge.GaugeText == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), false) == "");
            PAssert.That(() => gauge.ShowStatus(sniffer.ExMap.NowFunc(), true) == "");
        }

        /// <summary>
        /// 未着手のイベント海域の難易度を選択する
        /// </summary>
        [TestMethod]
        public void SelectEventmapRankStart()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 3, 31, 22, 5, 1);
            sniffer.SniffLogFile("select_eventmap_rank-start-101");
            var gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1");
            PAssert.That(() => gauge.GaugeText == "未出撃");

            sniffer.SniffLogFile("select_eventmap_rank-start-102");
            gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1-1丁");
            PAssert.That(() => gauge.GaugeText == "HP 280/280");
        }

        /// <summary>
        /// 未着手の輸送イベント海域の難易度を選択する
        /// </summary>
        [TestMethod]
        public void SelectEventmapRankStartTp()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 3, 31, 22, 5, 1);
            sniffer.SniffLogFile("select_eventmap_rank-starttp-101");
            // クリア済みゲージは表示しない
            PAssert.That(() => sniffer.ExMap.GaugeStatus[561].GaugeText == "");
            PAssert.That(() => sniffer.ExMap.GaugeStatus[562].GaugeText == "");

            var gauge = sniffer.ExMap.GaugeStatus[563];
            PAssert.That(() => gauge.MapName == "E3");
            PAssert.That(() => gauge.GaugeText == "未出撃");

            sniffer.SniffLogFile("select_eventmap_rank-starttp-102");
            gauge = sniffer.ExMap.GaugeStatus[563];
            PAssert.That(() => gauge.MapName == "E3-1甲");
            PAssert.That(() => gauge.GaugeText == "TP 800/800");
        }

        /// <summary>
        /// イベント海域の削り途中で難易度を下げる
        /// </summary>
        [TestMethod]
        public void SelectEventmapRankInprogress()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 3, 31, 22, 5, 1);
            sniffer.SniffLogFile("select_eventmap_rank-inprogress-101");
            var gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1-1丙");
            PAssert.That(() => gauge.GaugeText == "HP 140/280");

            sniffer.SniffLogFile("select_eventmap_rank-inprogress-102");
            gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1-1丁");
            PAssert.That(() => gauge.GaugeText == "HP 210/280");
        }

        /// <summary>
        /// イベント海域の1段階目攻略後に難易度を上げる
        /// </summary>
        [TestMethod]
        public void SelectEventmapRankReset()
        {
            var sniffer = new TestingSniffer();
            sniffer.ExMap.NowFunc = () => new DateTime(2024, 3, 31, 22, 5, 1);
            sniffer.SniffLogFile("select_eventmap_rank-reset-101");
            var gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1-2丁");
            PAssert.That(() => gauge.GaugeText == "HP 390/390");

            sniffer.SniffLogFile("select_eventmap_rank-reset-102");
            gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1-1甲");
            PAssert.That(() => gauge.GaugeText == "HP 470/470");

            // 帰投後、ゲージを再確認
            sniffer.SniffLogFile("select_eventmap_rank-reset-103");
            gauge = sniffer.ExMap.GaugeStatus[561];
            PAssert.That(() => gauge.MapName == "E1-1甲");
            PAssert.That(() => gauge.GaugeText == "HP 470/470");
        }

        /// <summary>
        /// 月次遠征に出撃する
        /// </summary>
        [TestMethod]
        public void MonthlyMissionStart()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("monthly-mission-start_101");
            var now = new DateTime(2024, 04, 06, 18, 05, 0, 332);
            var mission = sniffer.MonthlyMissionStatus[105];
            PAssert.That(() => mission.ShowStatus(new DateTime(2024, 04, 15, 8, 25, 50, 332), false) == "00:04:09");
            PAssert.That(() => mission.ShowStatus(new DateTime(2024, 04, 15, 9, 25, 50, 332), false) == "時間切れ");

            PAssert.That(() => mission.Spec.Name == "小笠原沖戦闘哨戒");
            PAssert.That(() => mission.ShowStatus(now, false) == "8日 14:24:59");
            PAssert.That(() => mission.ShowStatus(now, true) == "04/15 08:30");
            PAssert.That(() => mission.Fleet == 0);

            sniffer.SniffLogFile("monthly-mission-start_102");
            mission = sniffer.MonthlyMissionStatus[105];
            PAssert.That(() => mission.ShowStatus(now, false) == "遠征中");
            PAssert.That(() => mission.ShowStatus(now, true) == "遠征中");
            PAssert.That(() => mission.Fleet == 1);
        }

        /// <summary>
        /// 月次遠征に成功する
        /// </summary>
        [TestMethod]
        public void MonthlyMissionResult()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("monthly-mission-result_101");
            var now = new DateTime(2024, 04, 06, 15, 41, 0);
            var mission = sniffer.MonthlyMissionStatus[142];
            PAssert.That(() => mission.Spec.Name == "強行鼠輸送作戦");
            PAssert.That(() => mission.ShowStatus(now, false) == "遠征中");
            PAssert.That(() => mission.ShowStatus(now, true) == "遠征中");
            PAssert.That(() => mission.Fleet == 1);

            sniffer.SniffLogFile("monthly-mission-result_102");
            mission = sniffer.MonthlyMissionStatus[142];
            PAssert.That(() => mission.ShowStatus(now, false) == "達成済");
            PAssert.That(() => mission.ShowStatus(now, true) == "達成済");
            PAssert.That(() => mission.Fleet == 0);

            sniffer.SniffLogFile("monthly-mission-result_103");
            mission = sniffer.MonthlyMissionStatus[142];
            PAssert.That(() => mission.ShowStatus(now, false) == "達成済");
            PAssert.That(() => mission.ShowStatus(now, true) == "達成済");
            PAssert.That(() => mission.Fleet == 0);
        }

        /// <summary>
        /// 月次遠征に失敗する
        /// </summary>
        [TestMethod]
        public void MonthlyMissionFailed()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("monthly-mission-failed_101");
            var now = new DateTime(2024, 04, 06, 21, 34, 0, 889);
            var mission = sniffer.MonthlyMissionStatus[105];
            PAssert.That(() => mission.Spec.Name == "小笠原沖戦闘哨戒");
            PAssert.That(() => mission.ShowStatus(now, false) == "遠征中");
            PAssert.That(() => mission.ShowStatus(now, true) == "遠征中");
            PAssert.That(() => mission.Fleet == 1);

            sniffer.SniffLogFile("monthly-mission-failed_102");
            mission = sniffer.MonthlyMissionStatus[105];
            PAssert.That(() => mission.ShowStatus(now, false) == "8日 10:55:59");
            PAssert.That(() => mission.ShowStatus(now, true) == "04/15 08:30");
            PAssert.That(() => mission.Fleet == 0);

            sniffer.SniffLogFile("monthly-mission-failed_103");
            mission = sniffer.MonthlyMissionStatus[105];
            PAssert.That(() => mission.ShowStatus(now, false) == "8日 10:55:59");
            PAssert.That(() => mission.ShowStatus(now, true) == "04/15 08:30");
            PAssert.That(() => mission.Fleet == 0);
        }

        /// <summary>
        /// 月次遠征を中断する
        /// </summary>
        [TestMethod]
        public void MonthlyMissionReturnInstruction()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("monthly-mission-return_instruction-fleet4_101");
            var now = new DateTime(2024, 05, 01, 21, 40, 18, 235);
            var mission = sniffer.MonthlyMissionStatus[111];
            var timer = sniffer.MissionTimers[2];
            PAssert.That(() => mission.Spec.Name == "敵泊地強襲反撃作戦");
            PAssert.That(() => mission.ShowStatus(now, false) == "遠征中");
            PAssert.That(() => mission.ShowStatus(now, true) == "遠征中");
            PAssert.That(() => mission.Fleet == 3);
            PAssert.That(() => !mission.Canceled);
            PAssert.That(() => timer.Timer.ToString(now, false) == "08:33:23");
            PAssert.That(() => timer.Timer.ToString(now, true) == "02 06:13");

            sniffer.SniffLogFile("monthly-mission-return_instruction-fleet4_102");
            mission = sniffer.MonthlyMissionStatus[111];
            timer = sniffer.MissionTimers[2];
            PAssert.That(() => mission.ShowStatus(now, false) == "13日 05:39:41");
            PAssert.That(() => mission.ShowStatus(now, true) == "05/15 03:20");
            PAssert.That(() => mission.Fleet == 3);
            PAssert.That(() => mission.Canceled);
            PAssert.That(() => timer.Timer.ToString(now, false) == "-00:02:11");
            PAssert.That(() => timer.Timer.ToString(now, true) == "01 21:42");

            sniffer.SniffLogFile("monthly-mission-return_instruction-fleet4_103");
            mission = sniffer.MonthlyMissionStatus[111];
            timer = sniffer.MissionTimers[2];
            PAssert.That(() => mission.ShowStatus(now, false) == "13日 05:39:41");
            PAssert.That(() => mission.ShowStatus(now, true) == "05/15 03:20");
            PAssert.That(() => mission.Fleet == 3);
            PAssert.That(() => mission.Canceled);
            PAssert.That(() => timer.Timer.ToString(now, false) == "-00:02:11");
            PAssert.That(() => timer.Timer.ToString(now, true) == "01 21:42");

            sniffer.SniffLogFile("monthly-mission-return_instruction-fleet4_104");
            mission = sniffer.MonthlyMissionStatus[111];
            timer = sniffer.MissionTimers[2];
            PAssert.That(() => mission.ShowStatus(now, false) == "13日 05:39:41");
            PAssert.That(() => mission.ShowStatus(now, true) == "05/15 03:20");
            PAssert.That(() => mission.Fleet == 0);
            PAssert.That(() => !mission.Canceled);
            PAssert.That(() => timer.Timer.ToString(now, false) == "");
            PAssert.That(() => timer.Timer.ToString(now, true) == "");
        }

        /// <summary>
        /// 各月15日12時で月次遠征が更新される
        /// </summary>
        [TestMethod]
        public void MonthlyMissionLimitOver()
        {
            var sniffer = new TestingSniffer();
            sniffer.SniffLogFile("monthly-mission-limitover_101");
            var now = new DateTime(2024, 04, 15, 11, 50, 0, 235);
            var mission = sniffer.MonthlyMissionStatus[42];
            PAssert.That(() => mission.Spec.Name == "ミ船団護衛(一号船団)");
            PAssert.That(() => mission.ShowStatus(now, false) == "達成済");
            PAssert.That(() => mission.ShowStatus(now, true) == "達成済");
            PAssert.That(() => mission.Fleet == 0);
            PAssert.That(() => sniffer.MonthlyMissionStatus.Count == 16);

            sniffer.SniffLogFile("monthly-mission-limitover_102");
            now = new DateTime(2024, 04, 15, 12, 1, 0, 657);
            mission = sniffer.MonthlyMissionStatus[42];
            PAssert.That(() => mission.ShowStatus(now, false) == "29日 15:58:59");
            PAssert.That(() => mission.ShowStatus(now, true) == "05/15 04:00");
            PAssert.That(() => mission.Fleet == 0);
            PAssert.That(() => sniffer.MonthlyMissionStatus.Count == 7);
        }
    }
}
