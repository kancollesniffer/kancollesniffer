// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using ExpressionToCodeLib;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class QuestFleetCheckerTest : QuestCounterTester
    {
        [TestInitialize]
        public void Initialize()
        {
            battleInfo.InjectDeck(1);
        }

        /// <summary>
        /// 201: 敵艦隊を撃滅せよ！
        /// 210: 敵艦隊を10回邀撃せよ！
        /// 214: あ号
        /// 216: 敵艦隊主力を撃滅せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_201_216_210_214()
        {
            InjectQuestList(201, 216, 210, 214);
            var counts = questInfo.Quests.Select(q => q.Count).ToArray();

            // 出撃カウント
            InjectMapStart(11, 4);
            PAssert.That(() => counts[2].Id == 214);
            PAssert.That(() => counts[2].NowArray[0] == 1);

            // 道中S勝利
            InjectBattleResult("S");
            PAssert.That(() => counts.Select(c => new { c.Id, c.Now }).ParamsEqual(
                new { Id = 201, Now = 1 }, new { Id = 210, Now = 1 },
                new { Id = 214, Now = 0 }, new { Id = 216, Now = 1 }
            ));
            PAssert.That(() => counts[2].NowArray.ParamsEqual(1, 1, 0, 0));

            // ボスB勝利
            InjectMapNext(11, 5);
            InjectBattleResult("B");
            PAssert.That(() => counts.Select(c => new { c.Id, c.Now }).ParamsEqual(
                new { Id = 201, Now = 2 }, new { Id = 210, Now = 2 },
                new { Id = 214, Now = 0 }, new { Id = 216, Now = 2 }
            ));

            // ボス敗北
            PAssert.That(() => counts[2].NowArray.ParamsEqual(1, 1, 1, 1));
            InjectBattleResult("C");
            PAssert.That(() => counts.Select(c => new { c.Id, c.Now }).ParamsEqual(
                new { Id = 201, Now = 2 }, new { Id = 210, Now = 3 },
                new { Id = 214, Now = 0 }, new { Id = 216, Now = 2 }
            ));
            PAssert.That(() => counts[2].NowArray.ParamsEqual(1, 1, 2, 1));

            // 1-6 ゴール
            InjectMapNext(16, 8);
            PAssert.That(() => counts[0].Now == 2);
        }

        /// <summary>
        /// 211: 敵空母を3隻撃沈せよ！
        /// 212: 敵輸送船団を叩け！
        /// 213: 海上通商破壊作戦
        /// 218: 敵補給艦を3隻撃沈せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_211_212_213_218_220_221()
        {
            InjectQuestList(211, 212, 213, 218, 220, 221);
            // 補給艦1隻と空母2隻
            battleInfo.InjectEnemy(
                new ShipStatus { NowHp = 0, MaxHp = 130, Spec = ShipMaster.GetSpec(1558) },
                new ShipStatus { NowHp = 0, MaxHp = 90, Spec = ShipMaster.GetSpec(1543) },
                new ShipStatus { NowHp = 0, MaxHp = 90, Spec = ShipMaster.GetSpec(1543) },
                new ShipStatus { NowHp = 0, MaxHp = 96, Spec = ShipMaster.GetSpec(1528) },
                new ShipStatus { NowHp = 0, MaxHp = 70, Spec = ShipMaster.GetSpec(1523) },
                new ShipStatus { NowHp = 1, MaxHp = 70, Spec = ShipMaster.GetSpec(1523) }
            );
            InjectBattleResult("A");
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(
                new { Id = 211, Now = 2 }, new { Id = 212, Now = 1 }, new { Id = 213, Now = 1 },
                new { Id = 218, Now = 1 }, new { Id = 220, Now = 2 }, new { Id = 221, Now = 1 }
            ));
        }

        /// <summary>
        /// 228: 海上護衛戦
        /// 230: 敵潜水艦を制圧せよ！
        /// </summary>
        [TestMethod]
        public void BattleResult_228_230()
        {
            InjectQuestList(228, 230);
            // 潜水艦3
            battleInfo.InjectEnemy(
                new ShipStatus { NowHp = 0, MaxHp = 27, Spec = ShipMaster.GetSpec(1532) },
                new ShipStatus { NowHp = 0, MaxHp = 19, Spec = ShipMaster.GetSpec(1530) },
                new ShipStatus { NowHp = 0, MaxHp = 19, Spec = ShipMaster.GetSpec(1530) }
            );
            InjectBattleResult("S");
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(new { Id = 228, Now = 3 }, new { Id = 230, Now = 3 }));
        }

        /// <summary>
        /// 潜水航空戦での支援軽空は撃沈判定にならない
        /// 211: 敵空母を3隻撃沈せよ！
        /// 230: 敵潜水艦を制圧せよ！
        /// </summary>
        [TestMethod]
        public void BattleResultAswAirBattle()
        {
            InjectQuestList(211, 230);
            // 潜水4 支援軽空1
            battleInfo.InjectEnemy(
                new ShipStatus { NowHp = 0, MaxHp = 19, Spec = ShipMaster.GetSpec(1530) },
                new ShipStatus { NowHp = 0, MaxHp = 19, Spec = ShipMaster.GetSpec(1530) },
                new ShipStatus { NowHp = 0, MaxHp = 19, Spec = ShipMaster.GetSpec(1530) },
                new ShipStatus { NowHp = 0, MaxHp = 19, Spec = ShipMaster.GetSpec(1530) },
                new ShipStatus { NowHp = -1, MaxHp = -1, Spec = ShipMaster.GetSpec(1510) }
            );
            InjectBattleResult("S");
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(new { Id = 211, Now = 0 }, new { Id = 230, Now = 4 }));
            PAssert.That(() => battleInfo.Result.Enemy.Main[4].SupportShip);
        }

        /// <summary>
        /// 280と854以降を同時に遂行していると854以降がカウントされないことがある
        /// </summary>
        [TestMethod]
        public void BattleResult_280_854()
        {
            InjectQuestList(280, 854);
            var escort = NewShip(517);

            battleInfo.InjectFleet(escort, escort, escort, escort, escort, escort);
            InjectMapNext(24, 5);
            InjectBattleResult("S");
            PAssert.That(() => questInfo.Quests[1].Count.NowArray[0] == 1);
        }

        /// <summary>
        /// 888と893以降を同時に遂行していると893以降がカウントされないことがある
        /// </summary>
        [TestMethod]
        public void BattleResult_888_893()
        {
            InjectQuestList(888, 893);
            var escort = NewShip(517);

            battleInfo.InjectFleet(escort, escort, escort, escort, escort, escort);
            InjectMapNext(71, 5);
            InjectBattleResult("S");
            PAssert.That(() => questInfo.Quests[1].Count.NowArray[1] == 1);
        }

        /// <summary>
        /// 302: 大規模演習
        /// 303: 「演習」で練度向上！
        /// 304: 「演習」で他提督を圧倒せよ！
        /// 311: 精鋭艦隊演習
        /// 313: 秋季大演習
        /// 314: 冬季大演習
        /// 315: 春季大演習
        /// 326: 夏季大演習
        /// </summary>
        [TestMethod]
        public void PracticeResult()
        {
            InjectQuestList(302, 303, 304, 311, 313, 314, 315, 326);
            var destroyer    = NewShip(1);
            var lightcruiser = NewShip(51);

            battleInfo.InjectFleet(destroyer, lightcruiser);
            InjectPracticeResult("C");
            InjectPracticeResult("A");
            PAssert.That(() => questInfo.Quests.Select(q => new { q.Id, q.Count.Now }).ParamsEqual(
                new { Id = 302, Now = 1 }, new { Id = 303, Now = 2 }, new { Id = 304, Now = 1 },
                new { Id = 311, Now = 1 }, new { Id = 313, Now = 1 }, new { Id = 314, Now = 1 },
                new { Id = 315, Now = 1 }, new { Id = 326, Now = 1 }
            ));
        }
    }
}
