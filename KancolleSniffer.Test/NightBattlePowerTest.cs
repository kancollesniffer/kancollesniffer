// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using ExpressionToCodeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KancolleSniffer.Test
{
    [TestClass]
    public class NightBattlePowerTest : ApiStart2
    {
        [TestMethod]
        public void 甲標的の改修効果()
        {
            var ship = NewShip(58);
            ship.Torpedo = 102;
            ship.SetItems(甲標的丙型(4));
            PAssert.That(() => ship.NightBattlePower == 104);
        }

        [TestMethod]
        public void LuigiTorelliの夜戦判定()
        {
            var ship = NewShip(535);
            ship.Firepower = 10;
            ship.Torpedo = 62;
            PAssert.That(() => ship.NightBattlePower == 72, "未改は夜戦可能");

            ship = NewShip(605);
            ship.Firepower = 8;
            ship.Torpedo = 32;
            PAssert.That(() => ship.NightBattlePower == 0, "改は夜戦不可");
        }

        [TestMethod]
        public void 宗谷の夜戦判定()
        {
            var ship = NewShip(699);
            ship.Firepower = 9;
            PAssert.That(() => ship.NightBattlePower == 9, "特務は夜戦可能");

            ship = NewShip(645);
            ship.Firepower = 1;
            PAssert.That(() => ship.NightBattlePower == 0, "灯台は夜戦不可");

            ship = NewShip(650);
            ship.Firepower = 1;
            PAssert.That(() => ship.NightBattlePower == 0, "南極は夜戦不可");
        }

        [TestMethod]
        public void Langleyの夜戦判定()
        {
            var ship = NewShip(925);
            ship.Firepower = 32;
            PAssert.That(() => ship.NightBattlePower == 0, "未改は夜戦不可");

            ship = NewShip(930);
            ship.Firepower = 40;
            PAssert.That(() => ship.NightBattlePower == 0, "改も夜戦不可");
        }

        [TestMethod]
        public void GambierBayの夜戦判定()
        {
            var ship = NewShip(396);
            ship.Firepower = 32;
            PAssert.That(() => ship.NightBattlePower == 0, "改は夜戦不可");

            ship = NewShip(707);
            ship.Firepower = 55;
            PAssert.That(() => ship.NightBattlePower == 0, "Mk.IIも夜戦不可");
        }

        [TestMethod]
        public void 大鷹型の夜戦判定()
        {
            var ship = NewShip(380);
            ship.Firepower = 23;
            PAssert.That(() => ship.NightBattlePower == 0, "大鷹改は夜戦不可");

            ship = NewShip(529);
            ship.Firepower = 39;
            PAssert.That(() => ship.NightBattlePower == 39, "大鷹改二は夜戦可能");

            ship = NewShip(381);
            ship.Firepower = 24;
            PAssert.That(() => ship.NightBattlePower == 0, "神鷹改は夜戦不可");

            ship = NewShip(536);
            ship.Firepower = 37;
            PAssert.That(() => ship.NightBattlePower == 37, "神鷹改二は夜戦可能");

            ship = NewShip(381);
            ship.Firepower = 24;
            PAssert.That(() => ship.NightBattlePower == 0, "雲鷹改は夜戦不可");

            ship = NewShip(536);
            ship.Firepower = 39;
            PAssert.That(() => ship.NightBattlePower == 39, "雲鷹改二は夜戦可能");
        }

        [TestMethod]
        public void GrafZeppelinの夜戦判定()
        {
            var ship = NewShip(432);
            ship.Firepower = 40;
            PAssert.That(() => ship.NightBattlePower == 40, "未改は夜戦可能");

            ship = NewShip(353);
            ship.Firepower = 52;
            PAssert.That(() => ship.NightBattlePower == 52, "改も夜戦可能");
        }

        [TestMethod]
        public void Saratogaの夜戦判定()
        {
            var ship = NewShip(433);
            ship.Firepower = 45;
            PAssert.That(() => ship.NightBattlePower == 45, "未改は夜戦可能");

            ship = NewShip(438);
            ship.Firepower = 53;
            PAssert.That(() => ship.NightBattlePower == 0, "改は夜戦不可");
        }

        [TestMethod]
        public void 加賀の夜戦判定()
        {
            var ship = NewShip(698);
            ship.Firepower = 56;
            PAssert.That(() => ship.NightBattlePower == 0, "改二は夜戦不可");

            ship = NewShip(610);
            ship.Firepower = 62;
            PAssert.That(() => ship.NightBattlePower == 0, "改二戊も夜戦不可");

            ship = NewShip(646);
            ship.Firepower = 60;
            PAssert.That(() => ship.NightBattlePower == 60, "改二護は夜戦可能");
        }

        [TestMethod]
        public void 夜戦不可空母()
        {
            var ship = NewShip(461);
            ship.Firepower = 68 + 2 + 7 + 3;
            ship.Torpedo = 9 + 1 + 1;
            ship.SetItems(TBM3D(32), F6F3N(24), 熟練甲板要員航空整備員(10));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            PAssert.That(() => ship.NightBattlePower == 0);
        }

        [TestMethod]
        public void 速吸改に夜戦装備しても砲撃()
        {
            var ship = NewShip(352);
            ship.Firepower = 36 + 2;
            ship.Torpedo = 9;
            ship.SetItems(TBM3D(6), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 47);
        }

        [TestMethod]
        public void 日向改二に夜戦装備しても砲撃()
        {
            var ship = NewShip(554);
            ship.Firepower = 86;
            ship.Torpedo = 0;
            ship.SetItems(F6F3N(2), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 86);
        }

        // 空母夜戦火力はwiki参考 https://wikiwiki.jp/kancolle/%E5%A4%9C%E6%88%A6#x345rgc6
        [TestMethod]
        public void サラトガmk2にTBM3D()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 2;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(32));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 2 + 9 + 144.37);
        }

        public void サラトガmk2にTBM3Dを0機()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 2;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(0));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 0);
        }

        [TestMethod]
        public void サラトガmk2にF6F3N()
        {
            var ship = NewShip(545);
            ship.Firepower = 68;
            ship.Torpedo = 0;
            ship.ImprovedFirepower = 68;
            ship.SetItems(F6F3N(32));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 106.18);
        }

        [TestMethod]
        public void サラトガmk2にF6F3Nを0機()
        {
            var ship = NewShip(545);
            ship.Firepower = 68;
            ship.Torpedo = 0;
            ship.ImprovedFirepower = 68;
            ship.SetItems(F6F3N(0));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 0);
        }

        [TestMethod]
        public void サラトガmk2にTBM3DとF6F3N()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 2;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(32), F6F3N(24));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 2 + 9 + 144.36 + 80.82);
        }

        [TestMethod]
        public void サラトガmk2にTBM3Dを0機とF6F3N()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 2;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(0), F6F3N(24));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 80.82);
        }

        [TestMethod]
        public void サラトガmk2にTBM3DとF6F3Nを0機()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 2;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(32), F6F3N(0));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 2 + 9 + 144.37);
        }

        [TestMethod]
        public void サラトガmk2にTBM3DとF6F3Nを0機とSwordfishと熟練甲板要員航空整備員()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 2 + 2;
            ship.Torpedo = 9 + 3;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(0), F6F3N(0), Swordfish(18), 熟練甲板要員航空整備員(10));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            PAssert.That(() => ship.NightBattlePower == 0);
        }

        [TestMethod]
        public void 夜戦不可空母に夜間作戦航空要員()
        {
            var ship = NewShip(461);
            ship.Firepower = 68 + 2 + 3;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(32), F6F3N(24), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 2 + 9 + 144.36 + 80.82);
        }

        [TestMethod]
        public void 夜戦不可空母に夜間作戦航空要員と熟練甲板要員航空整備員()
        {
            var ship = NewShip(461);
            ship.Firepower = 68 + 2 + 3 + 7;
            ship.Torpedo = 9 + 1;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(32), F6F3N(24), 夜間作戦航空要員(), 熟練甲板要員航空整備員());
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 2 + 9 + 144.36 + 80.82);
        }

        [TestMethod]
        public void 夜戦不可空母に夜間作戦航空要員と熟練甲板要員航空整備員ボーナス()
        {
            var ship = NewShip(461);
            ship.Firepower = 68 + 2 + 3 + 7 + 1;
            ship.Torpedo = 9 + 1 + 1;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3D(32), F6F3N(24), 夜間作戦航空要員(), 熟練甲板要員航空整備員(4));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 2 + 9 + 2 + 144.36 + 80.82);
        }

        [TestMethod]
        public void 赤城改二戊に天山一二型甲改熟練空六号電探改装備機と零戦62型爆戦岩井隊と九九式艦爆()
        {
            var ship = NewShip(599);
            ship.Firepower = 67;
            ship.Torpedo = 13;
            ship.ImprovedFirepower = 67;
            ship.SetItems(天山一二型甲改熟練空六号電探改装備機(40), 零戦62型爆戦岩井隊(16, 10), 九九式艦爆(4));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 67 + 13 + 4 + Math.Round(176.92 + 11.56, 2));
        }

        [TestMethod]
        public void 加賀改二戊にTBM3W3Sと彗星一二型三一号光電管爆弾搭載機と九七式艦攻()
        {
            var ship = NewShip(610);
            ship.Firepower = 62 + 3;
            ship.Torpedo = 10 + 5;
            ship.ImprovedFirepower = 62;
            ship.SetItems(TBM3W3S(40), 彗星一二型三一号光電管爆弾搭載機(18), 九七式艦攻(8));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == Math.Round(62 + 3 + 10 + 11 + 5 + 208.92 + 14.00, 2));
        }

        [TestMethod]
        public void 龍鳳改二戊に烈風改二戊型一航戦熟練とSwordfishMkIII熟練とカ号観測機()
        {
            var ship = NewShip(883);
            ship.Firepower = 45 + 2 + 4;
            ship.Torpedo = 8;
            ship.ImprovedFirepower = 45;
            ship.SetItems(烈風改二戊型一航戦熟練(21), SwordfishMkIII熟練(12), カ号観測機(3));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 45 + 2 + 4 + 8 + 67.13 + 22.86);
        }

        [TestMethod]
        public void サラトガmk2にTBM3W3Sと零戦62型爆戦岩井隊とSwordfishMkIII熟練とF6F5N()
        {
            var ship = NewShip(545);
            ship.Firepower = 68 + 3 + 4;
            ship.Torpedo = 10 + 8;
            ship.ImprovedFirepower = 68;
            ship.SetItems(TBM3W3S(32), 零戦62型爆戦岩井隊(24, 10), SwordfishMkIII熟練(18), F6F5N(6));
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 68 + 3 + 4 + 10 + 4 + 8 + Math.Round(180.00 + 13.45 + 28.00 + 23.52, 2));
        }

        [TestMethod]
        public void 龍鳳改二戊に烈風改二戊型一航戦熟練と九九式練爆二二型改夜間装備実験機()
        {
            var ship = NewShip(883).SetItems(烈風改二戊型一航戦熟練(21), 九九式練爆二二型改夜間装備実験機(21));
            ship.Firepower = 45 + 3 + ship.Items.Sum(item => item.Spec.Firepower);
            ship.Torpedo = ship.Items.Sum(item => item.Spec.Torpedo);
            ship.ImprovedFirepower = 45;
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 207.87);
        }

        [TestMethod]
        public void 不明な空母の夜戦に雷装ボーナスなし()
        {
            var ship = NewShip(197);
            ship.Firepower = 63;
            ship.Torpedo = 0;
            ship.ImprovedFirepower = 63;
            ship.SetItems(天山一二型甲改空六号電探改装備機(27), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 63 + 131.75, "雷装ボーナスは夜戦につかない");
            PAssert.That(() => ship.Items[0].TbBonus == 0, "夜攻天山に雷装ボーナスがつかない");
        }

        [TestMethod]
        public void 翔鶴改二の夜戦に雷装ボーナス()
        {
            var ship = NewShip(461);
            ship.Firepower = 63;
            ship.Torpedo = 0;
            ship.ImprovedFirepower = 63;
            ship.SetItems(天山一二型甲改空六号電探改装備機(27), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 63 + 2 + 131.75, "雷装ボーナスは夜戦にもつく");
            PAssert.That(() => ship.Items[0].TbBonus == 2, "夜攻天山に雷装ボーナスがつく");

            ship.SetItems(TBM3D(27), 天山一二型甲(27), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 63 + 1 + 136.43, "夜攻ではない機体の雷装ボーナスが夜戦にもつく");
            PAssert.That(() => ship.Items[0].TbBonus == 1, "天山の雷装ボーナスはTBM3Dにつく");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "天山の雷装ボーナスは天山につかない");

            ship.SetItems(天山一二型甲改空六号電探改装備機(27), 流星改(27), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 63 + 2 + 131.75, "夜攻ではない機体についた雷装ボーナスが夜戦にもつく");
            PAssert.That(() => ship.Items[0].TbBonus == 0, "夜攻天山の雷装ボーナスは夜攻天山につかない");
            PAssert.That(() => ship.Items[1].TbBonus == 2, "夜攻天山の雷装ボーナスは流星改につく");
        }

        [TestMethod]
        public void 翔鶴改二の夜戦に雷装ボーナスと熟練甲板要員航空整備員ボーナス()
        {
            var ship = NewShip(461);
            ship.Firepower = 63 + 2 + 1 + 7 + 2;
            ship.Torpedo = 0 + 2 + 1 + 1;
            ship.ImprovedFirepower = 63;
            ship.SetItems(天山一二型甲改空六号電探改装備機(27), 流星改(27), 夜間作戦航空要員(), 熟練甲板要員航空整備員(7), 熟練甲板要員航空整備員(4));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 63 + 2 + 4 + 131.75, "夜攻ではない機体についた雷装ボーナスが夜戦にもつく");
            PAssert.That(() => ship.Items[0].TbBonus == 0, "夜攻天山の雷装ボーナスは夜攻天山につかない");
            PAssert.That(() => ship.Items[1].TbBonus == 2, "夜攻天山の雷装ボーナスは流星改につく");
        }

        [TestMethod]
        public void GambierBayMkIIに夜間作戦航空要員()
        {
            var ship = NewShip(707);
            ship.Firepower = 55 + 2 + 3;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 52;
            ship.SetItems(TBM3D(24), F6F3N(20), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 52 + 3 + 2 + 9 + 113.89 + 68.05);
        }

        [TestMethod]
        public void GambierBayMkIIは夜戦不可()
        {
            var ship = NewShip(707);
            ship.Firepower = 55;
            ship.Torpedo = 13 + 13;
            ship.ImprovedFirepower = 52;
            ship.SetItems(流星改(24), 流星改(20), _15_5cm三連装副砲(), _15_5cm三連装副砲());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 0);
        }

        [TestMethod]
        public void グラーフツェッペリンに夜間作戦航空要員()
        {
            var ship = NewShip(432);
            ship.Firepower = 10 + 2 + 3;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 0;
            ship.SetItems(TBM3D(20), F6F3N(13), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => Math.Round(ship.NightBattlePower, 2) == 10 + 2 + 9 + 98.24 + 45.49);
        }

        [TestMethod]
        public void グラーフツェッペリンに流星改と15_5cm三連装副砲2スロずつ()
        {
            var ship = NewShip(432);
            ship.Firepower = 10 + 7 + 7;
            ship.Torpedo = 13 + 13;
            ship.ImprovedFirepower = 0;
            ship.SetItems(流星改(20), 流星改(13), _15_5cm三連装副砲(), _15_5cm三連装副砲());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 50);
        }

        [TestMethod]
        public void グラーフツェッペリンに流星改0機と15_5cm三連装副砲2スロずつ()
        {
            var ship = NewShip(432);
            ship.Firepower = 10 + 7 + 7;
            ship.Torpedo = 13 + 13;
            ship.ImprovedFirepower = 0;
            ship.SetItems(流星改(0), 流星改(0), _15_5cm三連装副砲(), _15_5cm三連装副砲());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 50);
        }

        [TestMethod]
        public void アークロイヤル改に夜間作戦航空要員()
        {
            var ship = NewShip(393);
            ship.Firepower = 50 + 2 + 3;
            ship.Torpedo = 9;
            ship.ImprovedFirepower = 50;
            ship.SetItems(TBM3D(24), F6F3N(30), 夜間作戦航空要員());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => (int)(ship.NightBattlePower * 100) == (50 + 2) * 100 + 9 * 100 + 11388 + 9986);
        }

        [TestMethod]
        public void アークロイヤル改にSwordfishを2スロットとBarracudaMkIIと15_5cm三連装副砲()
        {
            var ship = NewShip(393);
            ship.Firepower = 50 + 2 + 2 + 7 + 7;
            ship.Torpedo = 3 + 3;
            ship.ImprovedFirepower = 50;
            ship.SetItems(Swordfish(24, 9), Swordfish(30), BarracudaMkII(12, 10), _15_5cm三連装副砲());
            ship.ResolveTbBonus(AdditionalData);

            PAssert.That(() => ship.Items[0].TbBonus == 0, "Barracudaの雷装ボーナスはSwordfishにつかない");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "Barracudaの雷装ボーナスはSwordfishにつかない");
            PAssert.That(() => ship.Items[2].TbBonus == 3, "Barracudaの雷装ボーナスはBarracudaにつく");
            PAssert.That(() => ship.NightBattlePower == 66, "改修値はSwordfishのみ、雷装ボーナスは夜間機同様に有効");
        }

        [TestMethod]
        public void アークロイヤル改にSwordfishを2スロットとBarracudaMkIIと熟練甲板要員航空整備員()
        {
            var ship = NewShip(393);
            ship.Firepower = 50 + 2 + 2 + 7 + 7 + 3;
            ship.Torpedo = 3 + 3 + 7 + 1 + 1;
            ship.ImprovedFirepower = 50;
            ship.SetItems(Swordfish(24, 9), Swordfish(30), BarracudaMkII(12, 10), 熟練甲板要員航空整備員(10));
            ship.ResolveTbBonus(AdditionalData).ResolveApBonus();

            PAssert.That(() => ship.Items[0].TbBonus == 0, "Barracudaの雷装ボーナスはSwordfishにつかない");
            PAssert.That(() => ship.Items[1].TbBonus == 0, "Barracudaの雷装ボーナスはSwordfishにつかない");
            PAssert.That(() => ship.Items[2].TbBonus == 3, "Barracudaの雷装ボーナスはBarracudaにつく");
            PAssert.That(() => ship.NightBattlePower == 71, "改修値はSwordfishのみ、雷装ボーナスは夜間機同様に有効");
        }

        [TestMethod]
        public void アークロイヤル改にSwordfish0機と15_5cm三連装副砲2スロずつ()
        {
            var ship = NewShip(393);
            ship.Firepower = 50 + 2 + 2 + 7 + 7;
            ship.Torpedo = 3 + 3;
            ship.ImprovedFirepower = 50;
            ship.SetItems(Swordfish(0), Swordfish(0), _15_5cm三連装副砲(), _15_5cm三連装副砲());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 0);
        }

        [TestMethod]
        public void アークロイヤル改に九七式艦攻と15_5cm三連装副砲2スロずつ()
        {
            var ship = NewShip(393);
            ship.Firepower = 50 + 7 + 7;
            ship.Torpedo = 5 + 5;
            ship.ImprovedFirepower = 50;
            ship.SetItems(流星改(24), 流星改(30), _15_5cm三連装副砲(), _15_5cm三連装副砲());
            ship.ResolveTbBonus(AdditionalData);
            PAssert.That(() => ship.NightBattlePower == 0);
        }
    }
}
