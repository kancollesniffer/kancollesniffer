// Copyright (C) 2016 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using KancolleSniffer.Forms;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.View.ListWindow
{
    public class AntiAirPanel : PanelWithToolTip, IPanelResize
    {
        private const int LineHeight = 16;
        private readonly List<AntiAirLabels> _labelList = [];
        private readonly List<Record> _table = [];

        private class AntiAirLabels : ControlsArranger
        {
            public ShipLabel.Fleet Fleet { get; } = new ShipLabel.Fleet(new Point(1, 3));
            public ShipLabel.Name Name { get; } = new ShipLabel.Name(new Point(1, 3), ShipNameWidth.Max);
            public Label Rate { get; } = new Label { Location = new Point(18, 3), AutoSize = true };
            public Label Diff { get; } = new Label { Location = new Point(83, 3), AutoSize = true };
            public GrowLeftLabel Spec { get; } = new GrowLeftLabel
            {
                Location = new Point(217, 3),
                GrowLeft = true,
                Anchor = AnchorStyles.Right | AnchorStyles.Top
            };

            // 後ろの要素ほど奥に隠れるため順番が重要
            public override Control[] Controls => [Spec, Fleet, Name, Rate, Diff];
        }

        public void Update(Sniffer sniffer)
        {
            CreateTable(sniffer);
            SuspendLayout();
            CreateLabels();
            ResizeLabels();
            SetRecords();
            ResumeLayout();
        }

        private class Record
        {
            public string Fleet { get; set; }
            public string Ship { get; set; }
            public string ShipTooltip { get; set; }
            public int Id { get; set; }
            public string Rate { get; set; }
            public string Diff { get; set; }
            public string Spec { get; set; }

            public Record()
            {
                Fleet = Ship = ShipTooltip = Rate = Diff = Spec = "";
            }
        }

        private static readonly double[] FormationModifiers         = [1.0, 1.2, 1.6];
        private static readonly double[] FormationModifiersCombined = [1.0, 1.1, 1.5];
        private void CreateTable(Sniffer sniffer)
        {
            _table.Clear();
            foreach (var fleet in sniffer.Fleets)
            {
                var formationModifiers = fleet.IsCombined ? FormationModifiersCombined : FormationModifiers;
                _table.Add(new Record
                {
                    Fleet = Const.FleetNamesLong[fleet.Number],
                    Diff  = "防空" + formationModifiers.Select(formationModifier => ToS(fleet.CalcEffectiveAntiAir(formationModifier))).Join("/")
                });
                foreach (var ship in fleet.Ships)
                {
                    _table.Add(CreateShipRecord(ship));
                    _table.Add(CreateAntiAirRecord(ship, formationModifiers));
                }
            }
        }

        private Record CreateShipRecord(ShipStatus ship)
        {
            return new Record
            {
                Ship = ship.Spec.Name + " Lv" + ship.Level,
                ShipTooltip = ship.GetToolTipString(),
                Spec = Cat(AntiAirPropellantBarrageChance(ship), " ", $"加重{ship.EffectiveAntiAirForShip}%"),
                Id = ship.Id
            };
        }

        private Record CreateAntiAirRecord(ShipStatus ship, double[] formationModifiers)
        {
            return new Record
            {
                Rate = "割合" + ToS(ship.PropShootdown) + "% ",
                Diff = "固定" + formationModifiers.Select(formationModifier => ToS(ship.CalcFixedShootdown(formationModifier))).Join("/")
            };
        }

        private static string AntiAirPropellantBarrageChance(ShipStatus ship)
        {
            var chance = ship.AntiAirPropellantBarrageChance;
            return chance == 0 ? "" : $"弾幕{ToS(chance)}%";
        }

        private void CreateLabels()
        {
            for (var i = _labelList.Count; i < _table.Count; i++)
                CreateLabels(i);
        }

        private void CreateLabels(int i)
        {
            var y = 1 + LineHeight * i;
            var labels = new AntiAirLabels
            {
                BackPanel = new Panel
                {
                    Location = new Point(0, y),
                    Size = new Size(ListForm.PanelWidth, LineHeight),
                }
            };
            _labelList.Add(labels);
            labels.Arrange(this, CustomColors.ColumnColors.BrightFirst(i));
            labels.Scale();
            labels.Move(AutoScrollPosition);
        }

        public void ApplyResize()
        {
            SuspendLayout();
            ResizeLabels();
            ResumeLayout();
        }

        private void ResizeLabels()
        {
            var width = Width - SystemInformation.VerticalScrollBarWidth - 2;
            foreach (var labels in _labelList)
            {
                labels.BackPanel.Width = width;
                labels.Spec.AdjustLocation();
            }
        }

        private void SetRecords()
        {
            for (var i = 0; i < _table.Count; i++)
                SetRecord(i);
            for (var i = _table.Count; i < _labelList.Count; i++)
                _labelList[i].BackPanel.Visible = false;
        }

        private void SetRecord(int i)
        {
            var lbp = _labelList[i].BackPanel;
            var column = _table[i];
            var labels = _labelList[i];
            labels.Fleet.Text = column.Fleet;
            labels.Name.SetName(column.Ship);
            ToolTip.SetToolTip(labels.Name, column.ShipTooltip);
            labels.Rate.Text = column.Rate;
            labels.Diff.Text = column.Diff;

            // 他の項目にかぶさるので少し余白が欲しいけど、何もないときは余白もいらない
            labels.Spec.Text = (" " + column.Spec).TrimEnd(); ;
            lbp.Visible = true;
        }

        public void ShowShip(int id)
        {
            var i = _table.FindIndex(e => e.Id == id);
            if (i == -1)
                return;
            var y = Scaler.ScaleHeight(LineHeight * i);
            AutoScrollPosition = new Point(0, y);
        }
    }
}
