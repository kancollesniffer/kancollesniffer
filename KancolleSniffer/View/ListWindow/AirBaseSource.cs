// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;
using Clipboard = KancolleSniffer.Util.Clipboard;

namespace KancolleSniffer.View.ListWindow
{
    public class AirBaseSource : CompositionPanel<AirBaseRow>.ISource
    {
        public List<AirBaseData> Data { get; } = [];

        public class AirBaseData
        {
            public string AirBase { get; set; } = "";
            public string AirBaseTooltip { get; set; } = "";
            public string AirCorps { get; set; } = "";
            public string AirCorpsTooltip { get; set; } = "";
            public string PlaneState { get; set; } = "";
            public ItemSpec ItemSpec { get; set; } = new ItemSpec();
            public int ItemLevel { get; set; } = 0;
            public string ItemTooltip { get; set; } = "";
            public string Spec { get; set; } = "";
            public string SpecTooltip { get; set; } = "";
            public Color BackColor { get; set; } = Color.Empty;

            public string PlaneText => ItemSpec.Empty ? PlaneState : ((ItemLevel > 0 ? "★" + ItemLevel : "") + " " + ItemSpec.Name).Trim();

            public string ClipboardText()
            {
                var text = new StringBuilder(AirBase);
                text.Cat("\n", AirBaseTooltip);
                text.Cat("\n", AirCorps);
                text.Cat("\n", AirCorpsTooltip);
                text.Cat("\n", PlaneText);
                text.Cat("\n", ItemTooltip);
                text.Cat("\n", Spec);
                text.Cat("\n", SpecTooltip);
                return text.ToString();
            }
        }

        public void CreateData(Sniffer sniffer)
        {
            Data.Clear();
            if (sniffer.AirBase == null)
            {
                Data.Add(new AirBaseData { AirBase = "出撃海域選択画面に進むと更新されます。" });
                return;
            }

            foreach (var baseInfo in sniffer.AirBase.OrderBy(baseInfo => baseInfo.ListWindowSortKey))
            {
                var ifp = baseInfo.CalcInterceptionFighterPower(highAltitude: false);
                var specTooltip = "";
                if (baseInfo.HighAltitudeArea && baseInfo.CanIntercept())
                {
                    var highAltitudeModifier = baseInfo.HighAltitudeModifier();
                    var rawIfp = ifp;
                    ifp *= highAltitudeModifier;
                    if (ifp.Diff)
                        specTooltip = $"防空:{ifp.RangeString}\n";
                    specTooltip += $"高高度補正:{highAltitudeModifier}\n通常防空:{RangeString(rawIfp)}";
                }
                else if (ifp.Diff)
                {
                    specTooltip = ifp.RangeString;
                }

                Data.Add(new AirBaseData
                {
                    AirBase = $"{baseInfo.AreaNameShort} 整備Lv{baseInfo.MaintenanceLevel}",
                    Spec = $"防空:{ifp.Min}",
                    SpecTooltip = specTooltip
                });

                var i = 0;
                foreach (var airCorps in baseInfo.AirCorps)
                {
                    Data.Add(CreateAirCorpsRecord(airCorps, i++));
                    Data.AddRange(airCorps.Planes.Select(CreatePlaneRecord));
                }
            }
        }

        public static AirBaseData CreateAirCorpsRecord(AirBase.AirCorpsInfo airCorps, int number)
        {
            var corpsFp = airCorps.CalcFighterPower();
            string spec;
            string specTooltip;
            if (airCorps.Action == 2)
            {
                spec = $"制空:{corpsFp.Interception.Min} 距離:{airCorps.Distance.Total}";
                specTooltip = $"制空:{RangeString(corpsFp.Interception)} 距離:{airCorps.Distance}";
            }
            else
            {
                spec = $"制空:{corpsFp.AirCombat.Min} 距離:{airCorps.Distance.Total}";
                specTooltip = $"制空:{RangeString(corpsFp.AirCombat)} 距離:{airCorps.Distance}";
            }

            return new AirBaseData
            {
                AirCorps = $"{Const.AirCorpsNames[number]} {airCorps.ActionName} {airCorps.CondName}".Trim(),
                AirCorpsTooltip = airCorps.CostForSortie.ToString(),
                Spec = spec,
                SpecTooltip = specTooltip,
                BackColor = airCorps.BackColor
            };
        }

        public static AirBaseData CreatePlaneRecord(AirBase.PlaneInfo plane)
        {
            if (plane.Deploying)
            {
                var itemTooltip = new StringBuilder();
                var shipBomberPower = plane.ShipBomberPower;
                if (shipBomberPower.Length > 0)
                    itemTooltip.Cat("\n", shipBomberPower.Join("\n"));
                var landBomberPower = plane.LandBomberPower;
                if (landBomberPower.BomberPower > 0)
                    itemTooltip.Cat("\n", landBomberPower.ToString());
                var aswBomberPower = plane.AswBomberPower;
                if (aswBomberPower.Min > 0)
                    itemTooltip.Cat("\n", $"対潜:{aswBomberPower.RangeString}");
                var steelCost = plane.Item.SteelCost;
                if (steelCost > 0)
                    itemTooltip.Cat("\n", $"鋼材消費:{steelCost}");

                return new AirBaseData
                {
                    ItemSpec    = plane.Item.Spec,
                    ItemLevel   = plane.Item.Level,
                    ItemTooltip = itemTooltip.ToString(),
                    Spec = $"{plane.Item.OnSlot}/{plane.Item.MaxEq} +{plane.Item.Alv}"
                };
            }
            else
            {
                return new AirBaseData { PlaneState = plane.StateName };
            }
        }

        private static string RangeString(Range range) => range.Diff ? range.RangeString : range.Min.ToString();

        public AirBaseRow CreateRow() => new AirBaseRow().SetEventHandler();

        public int DataCount() => Data.Count;

        public void Assign(int i, AirBaseRow row, ToolTip tooltip) => row.Assign(Data[i], tooltip);
    }

    public class AirBaseRow : CompositionRow
    {
        public Label AirBase { get; } = new Label { Location = new Point(1, 2), AutoSize = true };
        public Label AirCorps { get; } = new Label { Location = new Point(1, 2), AutoSize = true };
        public Label PlaneLevel { get; } = new Label
        {
            Location = new Point(1, 2),
            AutoSize = true,
            Anchor = AnchorStyles.Left | AnchorStyles.Top,
            ForeColor = Color.FromArgb(69, 169, 165)
        };
        public Label PlaneColor { get; } = new Label
        {
            Location = new Point(18, 2),
            Size = new Size(4, LabelHeight - 2)
        };
        public Label Plane { get; } = new Label
        {
            Location = new Point(21, 2),
            AutoSize = true,
            Anchor = AnchorStyles.Left | AnchorStyles.Top
        };
        public GrowLeftLabel Spec { get; } = new GrowLeftLabel
        {
            Location = new Point(217, 2),
            GrowLeft = true,
            Anchor = AnchorStyles.Right | AnchorStyles.Top
        };
        public string ClipboardText { get; private set; } = "";

        public override void AdjustLocation() => Spec.AdjustLocation();

        public override bool MoveToTopLevel(string fn) => AirBase.Text.StartsWith(fn);

        public override bool MoveToSecondLevel(int id) => false;

        // 後ろの要素ほど奥に隠れるため順番が重要
        public override Control[] Controls => [Spec, AirBase, AirCorps, PlaneLevel, Plane, PlaneColor];

        public override Control[] BackColorControls => [Spec, AirBase, AirCorps, PlaneLevel, Plane, BackPanel];

        public AirBaseRow SetEventHandler()
        {
            foreach (var control in Controls)
                control.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
            return this;
        }

        public void Assign(AirBaseSource.AirBaseData data, ToolTip tooltip)
        {
            AirBase.Text = data.AirBase;
            tooltip.SetToolTip(AirBase, data.AirBaseTooltip);

            AirCorps.Text = data.AirCorps;
            tooltip.SetToolTip(AirCorps, data.AirCorpsTooltip);

            PlaneLevel.Text = Const.ItemLevelChars.ElementAtOrDefault(data.ItemLevel) ?? "";
            PlaneColor.BackColor = data.ItemSpec.Empty ? DefaultBackColor : data.ItemSpec.Color;
            Plane.Text = data.ItemSpec.Empty ? data.PlaneState : data.ItemSpec.Name;
            tooltip.SetToolTip(Plane, data.ItemTooltip);

            // 他の項目にかぶさるので少し余白が欲しいけど、何もないときは余白もいらない
            Spec.Text = (" " + data.Spec).TrimEnd();
            tooltip.SetToolTip(Spec, data.SpecTooltip);

            ClipboardText = data.ClipboardText();
            CurrentBackColor = data.BackColor == Color.Empty ? DefaultBackColor : data.BackColor;
        }
    }
}
