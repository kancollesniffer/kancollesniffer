// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;
using Clipboard = KancolleSniffer.Util.Clipboard;

namespace KancolleSniffer.View.ListWindow
{
    public class FleetSource : CompositionPanel<FleetRow>.ISource
    {
        public List<FleetData> Data { get; } = [];

        public class FleetData
        {
            public string Fleet { get; set; } = "";
            public string FleetTooltip { get; set; } = "";
            public Color FleetForeColor { get; set; } = Control.DefaultForeColor;
            public string Ship { get; set; } = "";
            public string ShipTooltip { get; set; } = "";
            public int ShipId { get; set; } = -1;
            public ItemSpec ItemSpec { get; set; } = new ItemSpec();
            public int ItemLevel { get; set; } = 0;
            public string ItemTooltip { get; set; } = "";
            public string Spec { get; set; } = "";
            public string SpecTooltip { get; set; } = "";

            public string EquipText => ((ItemLevel > 0 ? "★" + ItemLevel : "") + " " + ItemSpec.Name).Trim();

            public string ClipboardText()
            {
                var text = new StringBuilder(Fleet);
                text.Cat("\n", FleetTooltip);
                text.Cat("\n", Ship);
                text.Cat("\n", ShipTooltip);
                text.Cat("\n", EquipText);
                text.Cat("\n", ItemTooltip);
                text.Cat("\n", Spec);
                text.Cat("\n", SpecTooltip);
                return text.ToString();
            }
        }

        public void CreateData(Sniffer sniffer)
        {
            Data.Clear();
            foreach (var fleet in sniffer.Fleets)
            {
                Data.Add(CreateFleetRecord(sniffer, fleet.Number));
                foreach (var ship in fleet.Ships)
                {
                    Data.Add(CreateShipRecord(ship));
                    Data.AddRange(ship.Items.Select(CreateEquipRecord));
                }
            }
        }

        public static FleetData CreateFleetRecord(Sniffer sniffer, int number)
        {
            var fleet = sniffer.Fleets[number];
            var total = TotalParams.Calc(fleet);
            return new FleetData
            {
                Fleet = Const.FleetNames[number] + " " + ShipStatus.SpeedName(fleet.Speed) + "   " + SpecTotal(total),
                FleetTooltip = Cat(Cat(FleetParams(total, fleet.DaihatsuBonus), "\n", GetSmokeScreen(sniffer, number)), "\n",
                    Cat(GetTp(sniffer, number), " ", GetRadarShips(fleet))),
                FleetForeColor = total.HasNoneAircraft ? CUDColors.Red : Control.DefaultForeColor
            };
        }

        private static string GetSmokeScreen(Sniffer sniffer, int number)
        {
            double[] triggerRates = sniffer.CalcSmokeScreenTriggerRates(number);
            return triggerRates.Length > 0 ? $"煙幕:{triggerRates.Reverse().Select(rate => ToS(rate * 100.0)).Join("/")}" : "";
        }

        private static string GetTp(Sniffer sniffer, int number)
        {
            double tp = sniffer.CalcTransportPoint(number);
            return tp > 0 ? $"TP:S{(int)tp} A{(int)(tp * 0.7)}" : "";
        }

        private static string GetRadarShips(Fleet fleet) => HideIfZero("電探:", fleet.RadarShips, "隻");

        private static string SpecTotal(TotalParams total) => $"火{total.ViewFirePower} 空{total.ViewAntiAir} 潜{total.ViewAsw} 索{total.ViewLoS}";

        private static string FleetParams(TotalParams total, double daihatsuBonus) =>
            (total.HasNoneAircraft ? "搭載数0の機体があります\n遠征成功条件を満たせてない可能性があります\n" : "")
            + $"計: Lv{total.Level}" + HideIfZero(" ド", total.Drum) + HideIfZero("(", total.DrumShips, "隻)") + HideIfZero(" 大", daihatsuBonus, "%")
            + $"\n戦闘:燃{total.Fuel / 5}弾{total.Bull / 5} 支援:燃{total.Fuel / 2}弾{(int)(total.Bull * 0.8)}";

        public static FleetData CreateShipRecord(ShipStatus ship)
        {
            var effectiveShelling = EffectiveShelling.Calc(ship);
            var spec = Cat(effectiveShelling.ShellingString(), " ", EffectiveAsw.Calc(ship).ToString());
            var specTooltip = new StringBuilder(HideIfZero("雷", ship.EffectiveTorpedo));
            specTooltip.Cat(" ", HideIfZero("夜", ship.NightBattlePower));
            if (string.IsNullOrWhiteSpace(spec))
            {
                spec = specTooltip.ToString();
                specTooltip.Clear();
            }
            specTooltip.Cat("\n", effectiveShelling.APShellString());
            specTooltip.Cat("\n", effectiveShelling.SupportString());

            return new FleetData
            {
                Ship = $"{ship.FleetSourcePrefix}{ship.Spec.Name} Lv{ship.Level}",
                ShipTooltip = $"燃{ship.EffectiveFuelMax} 弾{ship.EffectiveBullMax}",
                ShipId = ship.Id,
                Spec = spec,
                SpecTooltip = specTooltip.ToString()
            };
        }

        public static FleetData CreateEquipRecord(ItemStatus item)
        {
            var data = new FleetData { ItemSpec = item.Spec, ItemLevel = item.Level };
            if (item.Spec.IsAircraft)
                data.Spec = $"{item.OnSlot}/{item.MaxEq} +{item.Alv}";

            var itemTooltip = new StringBuilder();
            var bomberPower = item.CalcBomberPower();
            if (bomberPower.Length > 0)
                itemTooltip.Append($"航空戦:{bomberPower.Select(power => ToS(power)).Join("/")}");
            var airSupport = item.CalcAirSupportPower();
            if (airSupport.Length > 0)
                itemTooltip.Cat("\n", $"航空支援:{airSupport.Select(power => ToS(power)).Join("/")}");
            var aswSupport = item.CalcAswSupportPower();
            if (aswSupport.Length > 0)
                itemTooltip.Cat("\n", $"対潜支援:{aswSupport.Select(power => ToS(power)).Join("/")}");
            var steelCost = item.SteelCost;
            if (steelCost > 0)
                itemTooltip.Cat("\n", $"鋼材消費:{steelCost}");
            data.ItemTooltip = itemTooltip.ToString();
            return data;
        }

        private static string HideIfZero(string name, double value, string suffix = "") => value > 0 ? name + ToS(value) + suffix : "";

        private static string HideIfZero(string name, int value, string suffix = "") => value > 0 ? name + value + suffix : "";

        public FleetRow CreateRow() => new FleetRow().SetEventHandler();

        public int DataCount() => Data.Count;

        public void Assign(int i, FleetRow row, ToolTip tooltip) => row.Assign(Data[i], tooltip);
    }

    public class FleetRow : CompositionRow
    {
        public Label Fleet { get; } = new Label { Location = new Point(1, 2), AutoSize = true };
        // 海外艦の名前が間延びしないよう、ただのLabelではなくShipLabel.Nameを使う
        public ShipLabel.Name Ship { get; } = new ShipLabel.Name(new Point(1, 2), ShipNameWidth.Max);
        public Label EquipLevel { get; } = new Label
        {
            Location = new Point(1, 2),
            AutoSize = true,
            Anchor = AnchorStyles.Left | AnchorStyles.Top,
            ForeColor = Color.FromArgb(69, 169, 165)
        };
        public Label EquipColor { get; } = new Label
        {
            Location = new Point(18, 2),
            Size = new Size(4, LabelHeight - 2)
        };
        public Label Equip { get; } = new Label
        {
            Location = new Point(21, 2),
            AutoSize = true,
            Anchor = AnchorStyles.Left | AnchorStyles.Top
        };
        public GrowLeftLabel Spec { get; } = new GrowLeftLabel
        {
            Location = new Point(217, 2),
            GrowLeft = true,
            Anchor = AnchorStyles.Right | AnchorStyles.Top
        };
        public string ClipboardText { get; private set; } = "";
        public int ShipId { get; private set; } = -1;

        public override void AdjustLocation() => Spec.AdjustLocation();

        public override bool MoveToTopLevel(string fn) => Fleet.Text.StartsWith(fn);

        public override bool MoveToSecondLevel(int id) => ShipId == id;

        // 後ろの要素ほど奥に隠れるため順番が重要
        public override Control[] Controls => [Spec, Fleet, Ship, EquipLevel, Equip, EquipColor];

        public override Control[] BackColorControls => [Spec, Fleet, Ship, EquipLevel, Equip, BackPanel];

        public FleetRow SetEventHandler()
        {
            foreach (var control in Controls)
                control.DoubleClick += (obj, ev) => Clipboard.SetText(ClipboardText);
            return this;
        }

        public void Assign(FleetSource.FleetData data, ToolTip tooltip)
        {
            Fleet.Text = data.Fleet;
            tooltip.SetToolTip(Fleet, data.FleetTooltip);
            Fleet.ForeColor = data.FleetForeColor;

            Ship.SetName(data.Ship);
            tooltip.SetToolTip(Ship, data.ShipTooltip);
            ShipId = data.ShipId;

            EquipLevel.Text = Const.ItemLevelChars.ElementAtOrDefault(data.ItemLevel) ?? "";
            EquipColor.BackColor = data.ItemSpec.Empty ? DefaultBackColor : data.ItemSpec.Color;
            Equip.Text = data.ItemSpec.Name;
            tooltip.SetToolTip(Equip, data.ItemTooltip);

            // 他の項目にかぶさるので少し余白が欲しいけど、何もないときは余白もいらない
            Spec.Text = (" " + data.Spec).TrimEnd();
            tooltip.SetToolTip(Spec, data.SpecTooltip);

            ClipboardText = data.ClipboardText();
        }
    }
}
