// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using KancolleSniffer.Forms;
using KancolleSniffer.Model;
using KancolleSniffer.Util;

namespace KancolleSniffer.View.ListWindow
{
    public class MonthlyMissionPanel : PanelWithToolTip, IPanelResize
    {
        private const int LineHeight = 16;
        private readonly List<Labels> _labelList = [];
        private readonly List<Record> _records = [];
        private MissionFleetChecker[] _checkers;
        private int _currentFleet;
        private int _expandMission;
        public bool ShowLimit { get; set; }
        public Func<TimeStep> GetStep { private get; set; }

        private class Labels : ControlsArranger
        {
            public Label Name { get; } = new Label { Location = new Point(1, 3), AutoSize = true };

            public Label Gauge { get; } = new Label { Location = new Point(57, 3), AutoSize = true };

            public GrowLeftLabel Status { get; } = new GrowLeftLabel
            {
                Location = new Point(217, 3),
                GrowLeft = true,
                Anchor = AnchorStyles.Right | AnchorStyles.Top
            };

            // 後ろの要素ほど奥に隠れるため順番が重要
            public override Control[] Controls => [Name, Gauge, Status];
        }

        private class Record
        {
            public string Title { get; set; }
            public GaugeStatus OnceGauge { get; set; }
            public GaugeStatus EOGauge { get; set; }
            public MissionStatus Mission { get; set; }
            public int ExpandMission { get; set; }
        }

        private void SetRecords(Sniffer sniffer)
        {
            _records.Clear();
            var onceGauges = sniffer.ExMap.GaugeStatus.Values.Where(e => e.IsOnce).OrderBy(e => e.ListWindowSortKey).Select(e => e.Clone()).ToArray();
            if (onceGauges.Any())
            {
                _records.Add(new() { Title = "単発海域" });
                foreach (var onceGauge in onceGauges)
                    _records.Add(new() { OnceGauge = onceGauge });
            }
            _records.Add(new() { Title = "EO" });
            foreach (var eoGauge in sniffer.ExMap.GaugeStatus.Values.Where(e => e.IsEO).OrderBy(e => e.Id).Select(e => e.Clone()))
                _records.Add(new() { EOGauge = eoGauge });
            _records.Add(new() { Title = "遠征  (ダブルクリックで条件を展開)" });
            foreach (var mission in sniffer.MonthlyMissionStatus.Values.OrderBy(e => e.Spec.DispNo).Select(e => e.Clone()))
                _records.Add(new() { Mission = mission });
        }

        private void InsertMissionDetail()
        {
            _records.RemoveAll(e => e.ExpandMission > 0);
            if (_expandMission == 0)
                return;
            var index = _records.FindIndex(e => e.Mission != null && e.Mission.Spec.Id == _expandMission);
            if (index < 0)
                return;

            var details = MissionFleetChecker.ToolTip(_records[index].Mission.Spec).Split('\n');
            _records.InsertRange(index + 1, details.Select(detail => new Record() { Title = "  " + detail, ExpandMission = _expandMission }));
        }

        public void Update(Sniffer sniffer, int currentFleet)
        {
            SetRecords(sniffer);
            _checkers = sniffer.Fleets.Select(fleet => new MissionFleetChecker(fleet)).ToArray();
            _currentFleet = currentFleet;
            UpdateList();
        }

        private void UpdateList()
        {
            InsertMissionDetail();
            SuspendLayout();
            CreateLabels();
            ResizeLabels();
            SetLabels();
            ResumeLayout();
        }

        private void CreateLabels()
        {
            for (var i = _labelList.Count; i < _records.Count; i++)
                CreateLabels(i);
        }

        private void CreateLabels(int i)
        {
            var y = 1 + LineHeight * i;
            var labels = new Labels
            {
                BackPanel = new Panel
                {
                    Location = new Point(0, y),
                    Size = new Size(ListForm.PanelWidth, LineHeight),
                }
            };
            labels.Name.DoubleClick += (sender, e) => NameDoubleClick(i);
            labels.Status.Click += StatusClickHandler;
            labels.Status.Cursor = Cursors.Hand;
            _labelList.Add(labels);
            labels.Arrange(this, CustomColors.ColumnColors.BrightFirst(i));
            labels.Scale();
            labels.Move(AutoScrollPosition);
        }

        private void NameDoubleClick(int index)
        {
            var record = _records[index];
            if (record.Mission == null)
                return;
            if (!MissionFleetChecker.Defined(record.Mission.Spec.Id))
                return;

            _expandMission = _expandMission == record.Mission.Spec.Id ? 0 : record.Mission.Spec.Id;
            UpdateList();
        }

        private void StatusClickHandler(object sender, EventArgs e)
        {
            ShowLimit = !ShowLimit;
            UpdateTimers();
        }

        public void ApplyResize()
        {
            SuspendLayout();
            ResizeLabels();
            ResumeLayout();
        }

        private void ResizeLabels()
        {
            var width = Width - SystemInformation.VerticalScrollBarWidth - 2;
            foreach (var labels in _labelList)
            {
                labels.BackPanel.Width = width;
                labels.Status.AdjustLocation();
            }
        }

        private void SetLabels()
        {
            foreach (var (record, i) in _records.Select((record, i) => (record, i)))
            {
                if (record.OnceGauge != null)
                    SetLabels(i, record.OnceGauge);
                else if (record.EOGauge != null)
                    SetLabels(i, record.EOGauge);
                else if (record.Mission != null)
                    SetLabels(i, record.Mission, _checkers[record.Mission.Fleet > 0 ? record.Mission.Fleet : _currentFleet]);
                else
                    SetLabels(i, record.Title);
            }
            for (var i = _records.Count; i < _labelList.Count; i++)
                _labelList[i].BackPanel.Visible = false;
        }

        private void SetLabels(int i, string title)
        {
            _labelList[i].Name.Text = title;
            _labelList[i].Name.ForeColor = Color.Black;
            ToolTip.SetToolTip(_labelList[i].Name, "");
            _labelList[i].Gauge.Visible = false;
            _labelList[i].Status.Text = "";
            _labelList[i].BackPanel.Visible = true;
        }

        private void SetLabels(int i, GaugeStatus gauge)
        {
            _labelList[i].Name.Text = gauge.MapName;
            _labelList[i].Name.ForeColor = Color.Black;
            ToolTip.SetToolTip(_labelList[i].Name, "");
            _labelList[i].Gauge.Text = gauge.GaugeText;
            _labelList[i].Gauge.Visible = true;
            _labelList[i].Status.Text = gauge.ShowStatus(GetStep().Now, ShowLimit);
            _labelList[i].BackPanel.Visible = true;
        }

        private void SetLabels(int i, MissionStatus mission, MissionFleetChecker checker)
        {
            _labelList[i].Name.Text = mission.Spec.Name;
            _labelList[i].Name.ForeColor = checker.Check(mission.Spec.Id) ? Color.Black : CUDColors.Gray;
            ToolTip.SetToolTip(_labelList[i].Name, MissionFleetChecker.ToolTip(mission.Spec));
            _labelList[i].Gauge.Visible = false;
            _labelList[i].Status.Text = mission.ShowStatus(GetStep().Now, ShowLimit);
            _labelList[i].BackPanel.Visible = true;
        }

        public void UpdateTimers()
        {
            SuspendLayout();
            foreach (var (record, i) in _records.Select((record, i) => (record, i)))
            {
                if (record.EOGauge != null)
                    _labelList[i].Status.Text = record.EOGauge.ShowStatus(GetStep().Now, ShowLimit);
                if (record.Mission != null)
                    _labelList[i].Status.Text = record.Mission.ShowStatus(GetStep().Now, ShowLimit);
            }
            ResumeLayout();
        }
    }
}
