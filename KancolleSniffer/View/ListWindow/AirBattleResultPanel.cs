// Copyright (C) 2016 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using KancolleSniffer.Model;
using KancolleSniffer.Util;

namespace KancolleSniffer.View.ListWindow
{
    [DesignerCategory("Code")]
    public class AirBattleResultPanel : PanelWithToolTip, IPanelResize
    {
        private readonly Label _phaseName = new()
        {
            Text = "航空戦",
            Location = new Point(4, 4),
            Size = new Size(49, 12),
            TextAlign = ContentAlignment.TopCenter
        };
        private readonly Label _stage1 = new()
        {
            Text = "stage1",
            Location = new Point(8, top),
            AutoSize = true
        };
        private readonly Label[] _stage1Labels = new Label[4];
        private readonly Label[] _stage2Labels = new Label[4];
        private AirBattleResult.AirBattleRecord[] _resultList;
        private int _resultIndex;
        private readonly ShipLabel.Name _ciShipName = new(new Point(ci, top), ShipNameWidth.CiShipName);
        private readonly Label _ciKind = new()
        {
            Location = new Point(ci, top + 14),
            Size = new Size(24, 12)
        };
        private const int InitWidth = 236;
        private const int top = 20;
        private const int ci = 168;
        private const int left = 53;
        private const int space = 55;

        public bool ShowResultAutomatic { get; set; }

        private bool ResultRemained
        {
            set => _phaseName.BorderStyle = value ? BorderStyle.FixedSingle : BorderStyle.None;
            get => _phaseName.BorderStyle == BorderStyle.FixedSingle;
        }

        public AirBattleResultPanel()
        {
            var labels = new List<Label>
            {
                _phaseName,
                _stage1,
                new() { Text = "stage2", Location = new Point(8, top + 14), AutoSize = true },
                new() { Text = "自軍", Location = new Point(67, 6), AutoSize = true },
                new() { Text = "敵軍", Location = new Point(122, 6), AutoSize = true },
                new() { Text = "CI", Location = new Point(ci, 4), AutoSize = true }
            };

            void initStageLabels(Label[] stageLabels, int top)
            {
                labels.Add(stageLabels[1] = new()
                {
                    Location = new Point(left + 34, top),
                    Size = new Size(24, 12),
                    TextAlign = ContentAlignment.TopLeft
                });
                labels.Add(new()
                {
                    Location = new Point(left + 21, top),
                    Text = "→",
                    AutoSize = true
                });
                labels.Add(stageLabels[0] = new()
                {
                    Location = new Point(left, top),
                    Size = new Size(24, 12),
                    TextAlign = ContentAlignment.TopRight
                });

                labels.Add(stageLabels[3] = new()
                {
                    Location = new Point(left + space + 34, top),
                    Size = new Size(24, 12),
                    TextAlign = ContentAlignment.TopLeft
                });
                labels.Add(new()
                {
                    Location = new Point(left + space + 21, top),
                    Text = "→",
                    AutoSize = true
                });
                labels.Add(stageLabels[2] = new()
                {
                    Location = new Point(left + space, top),
                    Size = new Size(24, 12),
                    TextAlign = ContentAlignment.TopRight
                });
            }
            initStageLabels(_stage1Labels, top);
            initStageLabels(_stage2Labels, top + 14);

            labels.AddRange([_ciShipName, _ciKind]);
            _phaseName.Click += PhaseNameOnClick;
            Controls.AddRange(labels.ToArray());
        }

        public void ApplyResize()
        {
            _ciShipName.AdjustWidth(Scaler.DownWidth(Width) - InitWidth, true);
        }

        public void SetResult(Sniffer sniffer)
        {
            var battleResult = sniffer.Battle.AirBattleResult;
            _resultList = battleResult.Result.ToArray();
            _resultIndex = battleResult.ResultPanelInitIndex();
            ResultRemained = _resultList.Length > 1 || _resultList.Length > 0 && !ShowResultAutomatic;
            if (_resultList.Length > 0 && ShowResultAutomatic)
            {
                ShowResult();
                _resultIndex = (_resultIndex + 1) % _resultList.Length;
            }
            else
            {
                ClearResult();
            }
        }

        private void PhaseNameOnClick(object sender, EventArgs eventArgs)
        {
            if (!ResultRemained)
                return;
            ShowResult();
            if (_resultList.Length == 1)
                ResultRemained = false;
            _resultIndex = (_resultIndex + 1) % _resultList.Length;
        }

        private void ShowResult()
        {
            var result = _resultList[_resultIndex];
            _phaseName.Text = result.PhaseName;
            _stage1.ForeColor = result.AirControlLevelColor;

            static void fill(AirBattleResult.StageResult stage, Label[] labels)
            {
                labels[0].Text = $"{stage.FriendCount}";
                labels[1].Text = $"{stage.FriendCount - stage.FriendLost}";
                labels[2].Text = $"{stage.EnemyCount}";
                labels[3].Text = $"{stage.EnemyCount - stage.EnemyLost}";
            }

            fill(result.Stage1, _stage1Labels);
            fill(result.Stage2, _stage2Labels);
            ShowAirFireResult();
        }

        private void ShowAirFireResult()
        {
            var result = _resultList[_resultIndex];
            if (result.AirFire == null)
            {
                _ciShipName.Reset();
                _ciKind.Text = "";
                ToolTip.SetToolTip(_ciKind, "");
            }
            else
            {
                _ciShipName.SetName(result.AirFire.ShipName);
                _ciKind.Text = result.AirFire.Kind.ToString();
                ToolTip.SetToolTip(_ciKind, result.AirFire.Items.Join("\n"));
            }
        }

        private void ClearResult()
        {
            _phaseName.Text = "航空戦";
            _stage1.ForeColor = DefaultForeColor;
            foreach (var label in _stage1Labels)
                label.Text = "";
            foreach (var label in _stage2Labels)
                label.Text = "";
            _ciShipName.Reset();
            _ciKind.Text = "";
            ToolTip.SetToolTip(_ciKind, "");
        }
    }
}
