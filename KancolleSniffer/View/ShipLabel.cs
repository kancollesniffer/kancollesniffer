// Copyright (C) 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using KancolleSniffer.Model;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.View
{
    [DesignerCategory("Code")]
    public abstract class ShipLabel : GrowLeftLabel
    {
        private Color _initialBackColor;

        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                if (_initialBackColor == Color.Empty)
                    _initialBackColor = value;
                base.BackColor = value;
            }
        }

        protected ShipLabel()
        {
            UseMnemonic = false;
        }

        public abstract void Set(ShipStatus status);

        public abstract void Reset();

        public new sealed class Name : ShipLabel
        {
            private ShipStatus.SlotStatus _slotStatus, _prevSlotStatus;
            private ShipStatus _status;

            public static Font LatinFont { get; set; }
            public static Font BaseFont { get; set; }
            private readonly ShipNameWidth _defaultWidth;
            private int _nameWidth;

            public void AdjustWidth(int adjust, bool update = false)
            {
                _nameWidth = (int)_defaultWidth + Math.Max(-24, adjust);
                if (update && _status != null)
                    Set(_status);
            }

            public Name(Point location, ShipNameWidth nameWidth)
            {
                _defaultWidth = nameWidth;
                _nameWidth = (int)nameWidth;
                Location = location;
                AutoSize = true;
            }

            public override void Set(ShipStatus status)
            {
                SetName(status, _nameWidth);
            }

            public override void Reset()
            {
                SetName("");
            }

            public void SetName(ShipStatus status, ShipNameWidth width)
            {
                SetName(status, (int)width);
            }

            private void SetName(ShipStatus status, int width)
            {
                _status = status;
                _slotStatus = status.GetSlotStatus();
                ChangeFont(status.Spec.Name);
                Text = status.ShipLabelPrefix + Scaler.Truncate(status.Spec.Name, "", width, Font);
                if (_prevSlotStatus != _slotStatus)
                    Invalidate(); // OnPaintを実行させるため
                _prevSlotStatus = _slotStatus;
            }

            public void SetName(string name)
            {
                Set(new ShipStatus { Spec = new ShipSpec { Name = name } });
            }

            private void ChangeFont(string name)
            {
                var lu = StartWithLetter(name);
                var shift = Scaler.ScaleHeight(1);
                if (lu && !Font.Equals(LatinFont))
                {
                    Location += new Size(0, -shift);
                    Font = LatinFont;
                }
                else if (!lu && Font.Equals(LatinFont))
                {
                    Location += new Size(0, shift);
                    Font = BaseFont;
                }
            }

            public static bool StartWithLetter(string name)
            {
                return Regex.IsMatch(name, @"^\p{Lu}");
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                if ((_slotStatus & ShipStatus.SlotStatus.NormalEmpty) != 0)
                {
                    e.Graphics.DrawRectangle(Pens.Black, new Rectangle(Scaler.Move(ClientSize.Width, 0, -3, 0), Scaler.Scale(2, 5)));
                }
                else if ((_slotStatus & ShipStatus.SlotStatus.SemiEquipped) != 0)
                {
                    e.Graphics.DrawLine(Pens.Black, Scaler.Move(ClientSize.Width, 0, -1, 0), Scaler.Move(ClientSize.Width, 0, -1, 5));
                }
                if ((_slotStatus & ShipStatus.SlotStatus.ExtraEmpty) != 0)
                {
                    e.Graphics.DrawRectangle(Pens.Black, new Rectangle(Scaler.Move(ClientSize.Width, 0, -3, 8), Scaler.Scale(2, 3)));
                }
            }
        }

        public sealed class Hp : ShipLabel
        {
            private bool _hpPercent;
            private ShipStatus _status;

            public Hp()
            {
            }

            public Hp(Point location, int height)
            {
                Location = location;
                MinimumSize = new Size(0, height);
                TextAlign = ContentAlignment.MiddleLeft;
                GrowLeft = true;
                Cursor = Cursors.Hand;
            }

            public override void Reset()
            {
                _status = null;
                Text = "";
                ForeColor = Color.Black;
                BackColor = _initialBackColor;
            }

            public void SetSurvivalScore(BattleInfo.BattleResult.Combined fleet)
            {
                _status = null;
                Text = ToS(fleet.SurvivalScore);
                ForeColor = fleet.SurvivalScoreColor;
                BackColor = _initialBackColor;
            }

            public override void Set(ShipStatus status)
            {
                _status = status;
                Text = _hpPercent ? status.DisplayHpPercent : status.DisplayHp;
                SetColor(status);
            }

            public void ToggleHpPercent()
            {
                _hpPercent = !_hpPercent;
                if (_status != null)
                    Set(_status);
            }

            public void SetHp(int now, int max)
            {
                Set(new ShipStatus { NowHp = now, MaxHp = max });
            }

            public void SetColor(ShipStatus status)
            {
                BackColor = status.DamageColor(_initialBackColor);
            }
        }

        public sealed class Cond : ShipLabel
        {
            public Cond(Point location, int height)
            {
                Location = location;
                Size = new Size(24, height);
                TextAlign = ContentAlignment.MiddleRight;
            }

            public override void Reset()
            {
                Text = "";
                BackColor = _initialBackColor;
            }

            public override void Set(ShipStatus status)
            {
                Text = status.Cond.ToString("D");
                BackColor = status.CondColor(_initialBackColor);
            }
        }

        public sealed class Level : ShipLabel
        {
            public Level(Point location, int height)
            {
                Location = location;
                Size = new Size(24, height);
                TextAlign = ContentAlignment.MiddleRight;
            }

            public override void Reset()
            {
                Text = "";
            }

            public override void Set(ShipStatus status)
            {
                Text = status.Level.ToString("D");
            }
        }

        public sealed class Exp : ShipLabel
        {
            public Exp(Point location, int height)
            {
                Location = location;
                Size = new Size(42, height);
                TextAlign = ContentAlignment.MiddleRight;
            }

            public override void Reset()
            {
                Text = "";
            }

            public override void Set(ShipStatus status)
            {
                Text = status.ExpToNext.ToString("D");
            }
        }

        public sealed class Fleet : ShipLabel
        {
            public Fleet(Point location)
            {
                Location = location;
                AutoSize = true;
            }

            public override void Reset()
            {
                Text = "";
            }

            public override void Set(ShipStatus status)
            {
                Text = status.Fleet == null ? "" : (status.Fleet.Number + 1).ToString();
            }
        }

        public sealed class RepairTime : ShipLabel
        {
            public RepairTime(Point location)
            {
                Location = location;
                AutoSize = true;
            }

            public override void Reset()
            {
                Text = "";
            }

            public override void Set(ShipStatus status)
            {
                Text = status.DisplayRepairTime();
            }
        }
    }
}
