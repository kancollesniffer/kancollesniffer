// Copyright (C) 2020 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Windows.Forms;
using KancolleSniffer.Util;

namespace KancolleSniffer.View.MainWindow
{
    public class MaterialHistoryPanel : Panel, IUpdateContext
    {
        private readonly Label _bauxite = new()
        {
            Location = new Point(117, 2),
            Size = new Size(42, 48),
            Text = "ボーキ",
            TextAlign = ContentAlignment.TopRight
        };

        private readonly Label _steel = new()
        {
            Location = new Point(78, 2),
            Size = new Size(42, 48),
            Text = "鋼材",
            TextAlign = ContentAlignment.TopRight
        };

        private readonly Label _bullet = new()
        {
            Location = new Point(39, 2),
            Size = new Size(42, 48),
            Text = "弾薬",
            TextAlign = ContentAlignment.TopRight
        };

        private readonly Label _fuel = new()
        {
            Location = new Point(0, 2),
            Size = new Size(42, 48),
            Text = "燃料",
            TextAlign = ContentAlignment.TopRight
        };

        private readonly Label _caption = new()
        {
            AutoSize = true,
            Location = new Point(158, 14),
            Size = new Size(29, 36),
            Text = "母港\n今日\n今週"
        };

        private readonly Label[] _labels;

        private Label _button;

        public UpdateContext Context { private get; set; }

        public void SetClickHandler(Label caption, Label button)
        {
            caption.Click += ClickHandler;
            button.Click  += ClickHandler;
            _button = button;
        }

        public MaterialHistoryPanel()
        {
            BorderStyle = BorderStyle.FixedSingle;
            Visible = false;
            Size = new Size(188, 52);
            _labels = [_fuel, _bullet, _steel, _bauxite];
            Controls.AddRange(_labels);
            Controls.Add(_caption);
            SetClickHandler();
        }

        private void SetClickHandler()
        {
            Click += ClickHandler;
            foreach (Control control in Controls)
                control.Click += ClickHandler;
        }

        private void ClickHandler(object sender, EventArgs e)
        {
            if (Visible)
            {
                Visible = false;
                _button.BackColor = DefaultBackColor;
            }
            else
            {
                Visible = true;
                BringToFront();
                _button.BackColor = CustomColors.ActiveButtonColor;
            }
        }

        public new void Update()
        {
            for (var i = 0; i < _labels.Length; i++)
            {
                var count = Context.Sniffer.Material.MaterialHistory[i];
                var port  = CutOverflow(count.Now - Context.Sniffer.Material.PrevPort[i], 99999);
                var day   = CutOverflow(count.Now - count.BegOfDay, 99999);
                var week  = CutOverflow(count.Now - count.BegOfWeek, 99999);
                _labels[i].Text = $"{Const.MaterialNamesHalf[i]}\n{port:+#;-#;±0}\n{day:+#;-#;±0}\n{week:+#;-#;±0}";
            }
        }

        private static int CutOverflow(int value, int limit)
        {
            if (value > limit)
                return limit;
            if (value < -limit)
                return -limit;
            return value;
        }
    }
}
