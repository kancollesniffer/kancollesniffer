// Copyright (C) 2020 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Windows.Forms;

namespace KancolleSniffer.View.MainWindow
{
    public class ContextMenuNotifyIcon : ContextMenuStrip
    {
        private readonly ToolStripMenuItem[] _menuItems =
        [
            new() {
                Font = new Font("メイリオ", 9F, FontStyle.Bold, GraphicsUnit.Point, 128),
                Size = new Size(121, 22),
                Text = "開く(&O)"
            },
            new() {
                Size = new Size(121, 22),
                Text = "終了(&X)"
            }
        ];

        public ContextMenuNotifyIcon()
        {
            Items.AddRange(_menuItems);
        }

        public void SetEventHandlers(Action open, Action exit)
        {
            _menuItems[0].Click += (sender, e) => open();
            _menuItems[1].Click += (sender, e) => exit();
        }
    }
}
