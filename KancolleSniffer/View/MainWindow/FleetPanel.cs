// Copyright (C) 2020 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using KancolleSniffer.Model;
using Clipboard = KancolleSniffer.Util.Clipboard;

namespace KancolleSniffer.View.MainWindow
{
    public class FleetPanel : PanelWithToolTip, IUpdateTimers
    {
        private readonly ShipInfoPanel _shipInfoPanel = new()
        {
            Location = new Point(0, 15),
            Size = new Size(220, 133)
        };

        private readonly ChargeStatus[] _chargeStatus =
        [
            new() {
                Location = new Point(34, 0),
                Size = new Size(17, 13)
            },
            new() {
                Location = new Point(89, 0),
                Size = new Size(17, 13)
            },
            new() {
                Location = new Point(144, 0),
                Size = new Size(17, 13)
            },
            new() {
                Location = new Point(199, 0),
                Size = new Size(17, 13)
            }
        ];

        private readonly TriangleMark[] _triangleMarks =
        [
            new() {
                Location = new Point(1, 0),
                Name = "triangleMark1",
                Size = new Size(5, 13)
            },
            new() {
                Location = new Point(56, 0),
                Name = "triangleMark2",
                Size = new Size(5, 13)
            },
            new() {
                Location = new Point(111, 0),
                Name = "triangleMark3",
                Size = new Size(5, 13)
            },
            new() {
                Location = new Point(166, 0),
                Name = "triangleMark4",
                Size = new Size(5, 13)
            }
        ];

        private readonly Control[] _fleets =
        [
            new Label
            {
                Location = new Point(6, 1),
                Size = new Size(45, 12),
                Text = "第一"
            },
            new Label
            {
                Location = new Point(61, 1),
                Size = new Size(45, 12),
                Text = "第二"
            },
            new Label
            {
                Location = new Point(116, 1),
                Size = new Size(45, 12),
                Text = "第三"
            },
            new Label
            {
                Location = new Point(171, 1),
                Size = new Size(45, 12),
                Text = "第四"
            }
        ];

        private UpdateContext _context;

        public UpdateContext Context
        {
            get => _context;
            set
            {
                _context = value;
                _shipInfoPanel.Context = value;
                foreach (var c in _chargeStatus)
                    c.Context = value;
            }
        }

        public int CurrentFleet => _shipInfoPanel.CurrentFleet;
        public bool CombinedFleet => _shipInfoPanel.CombinedFleet;

        public Action UpdateQuestList { get; set; }

        public Action UpdateMonthlyMission { get; set; }

        public FleetPanel()
        {
            SetupFleetClick();
            _fleets[0].MouseHover += LabelFleet1_MouseHover;
            _fleets[0].MouseLeave += LabelFleet1_MouseLeave;
            for (var i = 1; i < _triangleMarks.Length; i++)
                _triangleMarks[i].Visible = false;
            Controls.AddRange(_chargeStatus.Concat(_fleets).Concat(_triangleMarks).Concat([_shipInfoPanel]).ToArray());
        }

        private bool _started;

        public void Start()
        {
            _started = true;
            _shipInfoPanel.Guide.Visible = false;
        }

        public LinkLabel Guide => _shipInfoPanel.Guide;

        public Label AkashiRepairTimer
        {
            set => _shipInfoPanel.AkashiRepairTimer = value;
        }

        public Action<int> ShowShipOnList
        {
            set => _shipInfoPanel.ShowShipOnList = value;
        }

        public bool ShowHpInPercent => _shipInfoPanel.ShowHpInPercent;

        public void ToggleHpPercent() => _shipInfoPanel.ToggleHpPercent();

        public new void Update()
        {
            _shipInfoPanel.ChangeCurrentFleet();
            UpdatePanelShipInfo();
            UpdateQuestList();
            UpdateMonthlyMission();
        }

        public void UpdateBattleInfo()
        {
            _shipInfoPanel.UpdateBattleInfo();
        }

        private void UpdatePanelShipInfo()
        {
            _shipInfoPanel.Update();
            ShowCurrentFleetNumber();
            _fleets[0].Text = _shipInfoPanel.CombinedFleet ? CombinedName : "第一";
        }

        public void UpdateChargeInfo()
        {
            foreach (var status in _chargeStatus)
            {
                status.Update();
                ToolTip.SetToolTip(status, status.Text);
            }
        }

        private void ShowCurrentFleetNumber()
        {
            for (var i = 0; i < _triangleMarks.Length; i++)
                _triangleMarks[i].Visible = _shipInfoPanel.CurrentFleet == i;
        }

        private void SetupFleetClick()
        {
            SetupFleetClick(_fleets);
            SetupFleetClick(_chargeStatus);
        }

        private void SetupFleetClick(Control[] a)
        {
            a[0].Tag = 0;
            a[0].Click       += LabelFleet1_Click;
            a[0].DoubleClick += LabelFleet1_DoubleClick;
            for (var fleet = 1; fleet < a.Length; fleet++)
            {
                a[fleet].Tag = fleet;
                a[fleet].Click       += LabelFleet_Click;
                a[fleet].DoubleClick += LabelFleet_DoubleClick;
            }
        }

        private void LabelFleet_Click(object sender, EventArgs e)
        {
            if (!_started)
                return;
            var fleet = (int)((Control)sender).Tag;
            if (_shipInfoPanel.CurrentFleet == fleet)
                return;
            _shipInfoPanel.CombinedFleet = false;
            _shipInfoPanel.CurrentFleet = fleet;
            UpdatePanelShipInfo();
            UpdateQuestList();
            UpdateMonthlyMission();
        }

        private readonly SemaphoreSlim _clickSemaphore = new(1);
        private readonly SemaphoreSlim _doubleClickSemaphore = new(0);

        private async void LabelFleet1_Click(object sender, EventArgs e)
        {
            if (!_started)
                return;
            if (_shipInfoPanel.CurrentFleet != 0)
            {
                LabelFleet_Click(sender, e);
                return;
            }
            if (!_clickSemaphore.Wait(0))
                return;
            try
            {
                if (await _doubleClickSemaphore.WaitAsync(SystemInformation.DoubleClickTime))
                    return;
            }
            finally
            {
                _clickSemaphore.Release();
            }
            _shipInfoPanel.CombinedFleet = Context.Sniffer.IsCombinedFleet && !_shipInfoPanel.CombinedFleet;
            UpdatePanelShipInfo();
        }

        private void LabelFleet1_MouseHover(object sender, EventArgs e)
        {
            _fleets[0].Text = _shipInfoPanel.CurrentFleet == 0 && Context.Sniffer.IsCombinedFleet && !_shipInfoPanel.CombinedFleet ? "連合" : "第一";
        }

        private void LabelFleet1_MouseLeave(object sender, EventArgs e)
        {
            _fleets[0].Text = _shipInfoPanel.CombinedFleet ? CombinedName : "第一";
        }

        private string CombinedName => Context.Sniffer.Fleets[0].CombinedType switch
        {
            CombinedType.Carrier => "機動",
            CombinedType.Surface => "水上",
            CombinedType.Transport => "輸送",
            _ => "連合",
        };

        private void LabelFleet_DoubleClick(object sender, EventArgs e)
        {
            if (!_started)
                return;
            var fleet = (int)((Control)sender).Tag;
            var text = TextGenerator.GenerateFleetCsv(Context.Sniffer, fleet);
            CopyFleetText(text, (Control)sender);
        }

        private void LabelFleet1_DoubleClick(object sender, EventArgs e)
        {
            if (!_started)
                return;
            _doubleClickSemaphore.Release();
            var text = TextGenerator.GenerateFleetCsv(Context.Sniffer, 0);
            if (_shipInfoPanel.CombinedFleet)
                text += TextGenerator.GenerateFleetCsv(Context.Sniffer, 1);
            CopyFleetText(text, (Control)sender);
        }

        private void CopyFleetText(string text, Control fleetButton)
        {
            if (string.IsNullOrEmpty(text))
                return;
            Clipboard.SetText(text);
            ToolTip.Show("コピーしました。", fleetButton, 1000);
        }

        public void UpdateTimers()
        {
            _shipInfoPanel.UpdateTimers();
        }
    }
}
