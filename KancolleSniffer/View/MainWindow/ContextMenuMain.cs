// Copyright (C) 2020 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace KancolleSniffer.View.MainWindow
{
    public class ContextMenuMain : ContextMenuStrip
    {
        private readonly ToolStripMenuItem[] _menuItems =
        [
            new() {
                Name = "listToolStripMenuItem",
                Size = new Size(125, 22),
                Text = "一覧(&L)"
            },
            new() {
                Name = "LogToolStripMenuItem",
                Size = new Size(125, 22),
                Text = "報告書(&R)"
            },
            new() {
                Name = "CaptureToolStripMenuItem",
                Size = new Size(125, 22),
                Text = "撮影(&C)"
            },
            new() {
                Name = "ConfigToolStripMenuItem",
                Size = new Size(125, 22),
                Text = "設定(&O)"
            },
            new() {
                Name = "ExitToolStripMenuItem",
                Size = new Size(125, 22),
                Text = "終了(&X)"
            }
        ];

        public ContextMenuMain()
        {
            Items.AddRange(_menuItems);
        }

        public void SetClickHandlers(params Action[] actions)
        {
            foreach (var (item, handler) in _menuItems.Zip(actions, (item, handler) => (item, handler)))
                item.Click += (sender, e) => handler();
        }
    }
}
