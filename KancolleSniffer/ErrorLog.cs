// Copyright (C) 2017 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using DynaJson;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer
{
    public class BattleResultError : Exception
    {
    }

    public class ErrorLog
    {
        private readonly Sniffer _sniffer;
        private BattleState _prevBattleState = BattleState.None;
        private readonly List<Main.Session> _battleApiLog = [];

        public ErrorLog(Sniffer sniffer)
        {
            _sniffer = sniffer;
        }

        public void CheckBattleApi(Main.Session session)
        {
            if (_prevBattleState == BattleState.None)
                _battleApiLog.Clear();
            try
            {
                if (_sniffer.Battle.BattleState != BattleState.None)
                    _battleApiLog.Add(session);
                // battleresultのあとのship_deckかportでのみエラー判定する
                else if (_prevBattleState == BattleState.Result && _sniffer.IsBattleResultError)
                    throw new BattleResultError();
            }
            finally
            {
                _prevBattleState = _sniffer.Battle.BattleState;
            }
        }

        public string GenerateBattleErrorLog()
        {
            var api = CompressApi(Cat(BattleStartSlots(), "\r\n", BattleApiLogLines()));
            var result = $"```text\r\n{DateTime.Now:g} {AppInfo.Version}\r\n{RankErrorLog()}{HpDiffLog()}{api}\r\n```";
            File.WriteAllText("error.log", result);
            return result;
        }

        private string BattleStartSlots()
        {
            return new JsonObject((
                from ship in _sniffer.BattleStartStatus
                group ship by ship.Fleet
                into fleet
                select (
                    from s in fleet
                    select (
                        from item in s.Items
                        select item.Spec.Id
                        ).ToArray()
                    ).ToArray()
                ).ToArray()).ToString();
        }

        private string BattleApiLogLines() => _battleApiLog.SelectMany(s => Privacy.Remove(s).Lines).Join("\r\n");

        private string RankErrorLog()
        {
            var rank = _sniffer.Battle.DisplayedResultRank;
            return rank.IsError ? $"{rank.Assumed}->{rank.Actual}\r\n" : "";
        }

        private string HpDiffLog() => !_sniffer.BattleResultStatusDiff.Any() ? "" : string.Join(" ",
            from pair in _sniffer.BattleResultStatusDiff
            let assumed = pair.Assumed // こちらのFleetはnull
            let actual = pair.Actual
            select $"({actual.Fleet.Number}-{actual.DeckIndex}) {assumed.NowHp}->{actual.NowHp}") + "\r\n";

        public string GenerateErrorLog(Main.Session s, string exception)
        {
            var api = CompressApi(Privacy.Remove(s).Lines.Join("\r\n"));
            var result = $"```text\r\n{DateTime.Now:g} {AppInfo.Version}\r\n{exception}\r\n{api}\r\n```";
            File.WriteAllText("error.log", result);
            return result;
        }

        private string CompressApi(string api)
        {
            var output = new MemoryStream();
            var gzip = new GZipStream(output, CompressionLevel.Optimal);
            var bytes = Encoding.UTF8.GetBytes(api);
            gzip.Write(bytes, 0, bytes.Length);
            gzip.Close();
            var ascii85 = Ascii85.Encode(output.ToArray());
            var result = new List<string>();
            var rest = ascii85.Length;
            const int lineLength = 80;
            for (var i = 0; i < ascii85.Length; i += lineLength, rest -= lineLength)
                result.Add(ascii85.Substring(i, Math.Min(rest, lineLength)));
            return result.Join("\r\n");
        }
    }
}
