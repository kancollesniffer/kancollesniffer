// Copyright (C) 2013-2021 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using DynaJson;
using KancolleSniffer.Forms;
using KancolleSniffer.Log;
using KancolleSniffer.Net;
using KancolleSniffer.Util;
using Microsoft.CSharp.RuntimeBinder;

namespace KancolleSniffer
{
    public class Main
    {
        private ProxyManager _proxyManager;
        private Form _form;
        private MainWindow _mainBehavior;
        public string UpgradedText => _mainBehavior.UpgradedText;
        private readonly ErrorDialog _errorDialog = new();
        private ConfigDialog _configDialog;
        private ErrorLog _errorLog;
        private IEnumerator<string> _playLog;
        private string _debugLogFile;
        private bool _timerEnabled;
        private readonly Timer _mainTimer = new() { Interval = 1000, Enabled = true };
        private readonly Timer _upgradeTimer = new() { Interval = (int)new TimeSpan(12, 0, 0).TotalMilliseconds, Enabled = true };

        public TimeStep Step { get; } = new TimeStep();

        public Config Config { get; } = new Config();
        public Sniffer Sniffer { get; } = new Sniffer();

        public static void Run(string[] args)
        {
            ParseArguments(args);
            new Main().RunInternal();
        }

        private void RunInternal()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Config.Load();
            _configDialog = new ConfigDialog(this);
            _form = Config.Shape.StartsWith("横長") ? new HorizontalMainForm() : new VerticalMainForm();
            _mainBehavior = new MainWindow(this, _form);
            _proxyManager = new ProxyManager(_form, Config);
            _errorLog = new ErrorLog(Sniffer);
            Sniffer.RepeatingTimerController = _mainBehavior.Notifier;
            Task.Run(CheckUpgrade);
            BuildCache();
            LoadData();
            ApplyConfig();
            ApplySettings();
            HttpProxy.AfterSessionComplete += HttpProxy_AfterSessionComplete;
            _mainTimer.Tick += TimerTick;
            _upgradeTimer.Tick += UpgradeTick;
            Application.Run(_form);
            Terminate();
        }

        private void UpgradeTick(object sender, EventArgs ev) => CheckUpgrade();

        public UpgradeInfo CheckUpgrade()
        {
            var upgradeInfo = UpgradeManager.CheckUpgrade();
            UpgradeManager.DownloadUpgradeFiles(upgradeInfo);
            if (HttpProxy.IsInListening && upgradeInfo.ProxyPacStatus == UpgradeStatus.Upgraded)
                _proxyManager.ApplyConfig();
            _form.Invoke(() => _mainBehavior.ShowUpgradeInfo(upgradeInfo));
            return upgradeInfo;
        }

        private void BuildCache()
        {
            var builder = new CacheBuilder(Config.Log);
            if (builder.NeedBuild)
            {
                if (new CacheBuilderDialog(builder, Config.Location).ShowDialog() == DialogResult.Abort)
                    Exit();
            }
            Config.Log.RebuildCache = false;
        }

        private readonly FileSystemWatcher _watcher = new()
        {
            Path = AppInfo.BaseDir,
            NotifyFilter = NotifyFilters.LastWrite
        };

        private readonly Timer _watcherTimer = new() {Interval = 1000};

        private void LoadData()
        {
            var target = "";
            Sniffer.LoadState();
            _watcher.SynchronizingObject = _form;
            _watcherTimer.Tick += (sender, ev) =>
            {
                _watcherTimer.Stop();
                switch (target)
                {
                    case "status.xml":
                        Sniffer.LoadState();
                        break;
                    case "TP.csv":
                        Sniffer.AdditionalData.LoadTpSpec();
                        break;
                }
            };
            _watcher.Changed += (sender, ev) =>
            {
                target = ev.Name;
                _watcherTimer.Stop();
                _watcherTimer.Start();
            };
            _watcher.EnableRaisingEvents = true;
        }

        private void HttpProxy_AfterSessionComplete(HttpProxy.Session session)
        {
            _form.BeginInvoke(new Action<HttpProxy.Session>(ProcessRequest), session);
        }

        public class Session
        {
            public string Url { get; set; }
            public string Request { get; set; }
            public string Response { get; set; }

            public Session(string url, string request, string response)
            {
                Url = url;
                Request = request;
                Response = response;
            }

            public string[] Lines => [Url, Request, Response];
        }

        private void ProcessRequest(HttpProxy.Session session)
        {
            var url = session.Request.PathAndQuery;
            if (!url.Contains("kcsapi/"))
                return;
            var s = Privacy.Remove(url, session.Request.BodyAsString, session.Response.BodyAsString);
            if (s.Response == null || !s.Response.StartsWith("svdata="))
            {
                WriteDebugLog(s);
                return;
            }
            s.Response = UnEscapeString(s.Response.Remove(0, "svdata=".Length));
            WriteDebugLog(s);
            ProcessRequestMain(s);
        }

        private void ProcessRequestMain(Session s)
        {
            try
            {
                var update = (Sniffer.Update)Sniffer.Sniff(s.Url, s.Request, JsonObject.Parse(s.Response));
                _mainBehavior.UpdateInfo(update);
                if (!Sniffer.Started)
                    return;
                Step.SetNowIfNeeded();
                if ((update & Sniffer.Update.Timer) != 0)
                    _timerEnabled = true;
                _errorLog.CheckBattleApi(s);
            }
            catch (RuntimeBinderException e)
            {
                if (_errorDialog.ShowDialog(_form, "艦これに仕様変更があったか、受信内容が壊れています。",
                    _errorLog.GenerateErrorLog(s, e.ToString())) == DialogResult.Abort)
                {
                    Exit();
                }
            }
            catch (LogIOException e)
            {
                if (_errorDialog.ShowDialog(_form, e.Message, e.InnerException.ToString()) == DialogResult.Abort)
                    Exit();
            }
            catch (BattleResultError)
            {
                if (_errorDialog.ShowDialog(_form, "戦闘結果の計算に誤りがあります。", _errorLog.GenerateBattleErrorLog()) == DialogResult.Abort)
                    Exit();
            }
            catch (Exception e)
            {
                if (_errorDialog.ShowDialog(_form, "エラーが発生しました。", _errorLog.GenerateErrorLog(s, e.ToString())) == DialogResult.Abort)
                    Exit();
            }
        }

        private void Exit()
        {
            _proxyManager.Shutdown();
            Environment.Exit(1);
        }

        private void WriteDebugLog(Session s)
        {
            if (_debugLogFile != null)
            {
                File.AppendAllText(_debugLogFile, $"date: {DateTime.Now:g}\nurl: {s.Url}\nrequest: {s.Request}\nresponse: {s.Response ?? "(null)"}\n");
            }
        }

        private static readonly Regex UnEscapeRx = new(@"\\[uU]([0-9A-Fa-f]{4})");
        private string UnEscapeString(string s)
        {
            try
            {
                return UnEscapeRx.Replace(s, match => ((char)int.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString());
            }
            catch (ArgumentException)
            {
                return s;
            }
        }

        private void ApplySettings()
        {
            ApplyDebugLogSetting();
            ApplyLogSetting();
            ApplyProxySetting();
        }

        public void ApplyDebugLogSetting()
        {
            _debugLogFile = Config.DebugLogging ? Config.DebugLogFile : null;
        }

        public bool ApplyProxySetting()
        {
            return _proxyManager.ApplyConfig();
        }

        public void ApplyLogSetting()
        {
            LogServer.SetLogConfig(Config.Log);
            LogServer.LogProcessor = new LogProcessor(Sniffer.Material.MaterialHistory, Sniffer.MapDictionary);
            Sniffer.LogConfig = Config.Log;
        }

        public void SetPlayLog(string file)
        {
            try
            {
                _playLog = File.ReadLines(file).GetEnumerator();
            }
            catch (FileNotFoundException)
            {
            }
        }

        private void TimerTick(object sender, EventArgs ev)
        {
            if (_timerEnabled)
            {
                try
                {
                    Step.SetNow();
                    _mainBehavior.UpdateTimers();
                    _mainBehavior.Notifier.NotifyTimers();
                    Step.SetPrev();
                }
                catch (Exception ex)
                {
                    if (_errorDialog.ShowDialog(_form, "エラーが発生しました。", ex.ToString()) == DialogResult.Abort)
                        Exit();
                }
            }
            if (_playLog == null || _configDialog.Visible)
            {
                _mainBehavior.PlayLogSign.Visible = false;
                return;
            }
            PlayLog();
        }

        public void ResetAchievement()
        {
            Sniffer.Achievement.Reset();
            _mainBehavior.UpdateItemInfo();
        }

        public static string[] LogHeaders = ["url: ", "request: ", "response: "];

        private void PlayLog()
        {
            var lines = new List<string>();
            var sign = _mainBehavior.PlayLogSign;
            foreach (var s in LogHeaders)
            {
                do
                {
                    if (!_playLog.MoveNext() || _playLog.Current == null)
                    {
                        sign.Visible = false;
                        return;
                    }
                } while (!_playLog.Current.StartsWith(s));
                lines.Add(_playLog.Current.Substring(s.Length));
            }
            sign.Visible = !sign.Visible;
            ProcessRequestMain(new Session(lines[0], lines[1], lines[2]));
        }

        public void ShowConfigDialog()
        {
            if (_configDialog.ShowDialog(_form) == DialogResult.OK)
            {
                Config.Save();
                ApplyConfig();
                _mainBehavior.Notifier.StopRepeatingTimer(_configDialog.RepeatSettingsChanged);
            }
        }

        private void ApplyConfig()
        {
            Sniffer.ShipCounter.Margin = Config.MarginShips;
            Sniffer.ItemCounter.Margin = Config.MarginEquips;
            _mainBehavior.Notifier.NotifyShipItemCount();
            Sniffer.Achievement.ResetHours = Config.ResetHours;
            Sniffer.WarnBadDamageWithDameCon = Config.WarnBadDamageWithDameCon;
            _mainBehavior.ApplyConfig();
        }

        public IEnumerable<Control> Controls => [_errorDialog, _configDialog, _configDialog.NotificationConfigDialog];

        private static void ParseArguments(string[] args)
        {
            var argBind = false;
            foreach (var arg in args)
            {
                if (arg == "--bind")
                {
                    argBind = true;
                    continue;
                }
                if (argBind)
                {
                    argBind = false;
                    HttpProxy.SetBindAddress(arg);
                    continue;
                }
            }
        }

        private void Terminate()
        {
            _proxyManager.Shutdown();
            Config.Save();
            Sniffer.FlashLog();
            Sniffer.SaveState();
        }

        public void ShowReport()
        {
            Process.Start("http://localhost:" + Config.Proxy.Listen + "/");
        }

        public void StartCapture()
        {
            try
            {
                var proc = new ProcessStartInfo("BurageSnap.exe") {WorkingDirectory = "Capture"};
                Process.Start(proc);
            }
            catch (FileNotFoundException)
            {
            }
            catch (Win32Exception)
            {
            }
        }
    }
}
