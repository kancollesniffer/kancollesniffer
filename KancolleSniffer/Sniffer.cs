// Copyright (C) 2013, 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Log;
using KancolleSniffer.Model;
using KancolleSniffer.Util;

namespace KancolleSniffer
{
    public class Sniffer
    {
        public ItemMaster ItemMaster { get; } = new();
        public ItemInventory ItemInventory { get; } = new();
        private readonly ItemInfo _itemInfo;
        public ShipMaster ShipMaster { get; } = new();
        public ShipInventory ShipInventory { get; } = new();
        private readonly ShipInfo _shipInfo;
        public MaterialInfo Material { get; } = new();
        private readonly QuestInfo _questInfo;
        private readonly QuestCounter _questCounter;
        private readonly QuestCountList _questCountList = new();
        private readonly MissionInfo _missionInfo = new();
        private readonly ConditionTimer _conditionTimer;
        private readonly DockInfo _dockInfo;
        public AkashiTimer AkashiTimer { get; }
        public Achievement Achievement { get; } = new();
        public BattleInfo Battle { get; }
        private readonly Logger _logger;
        public ExMapInfo ExMap { get; } = new();
        private readonly MiscTextInfo _miscTextInfo;
        private readonly AirBase _airBase;
        private readonly PresetDeck _presetDeck;
        public CellInfo CellInfo { get; } = new();
        private readonly Status _status = new();
        private bool _saveState;
        private readonly IHaveState[] _haveState;
        private readonly IPort[] _receivers;
        private AdditionalData _additionalData;

        public interface IRepeatingTimerController
        {
            void Stop(string key);
            void Stop(string key, int fleet);
            void Suspend(string exception = null);
            void Resume();
        }

        public IRepeatingTimerController RepeatingTimerController { get; set; }

        public Dictionary<string, string> MapDictionary { get; } = new Dictionary<string, string>
        {
            {"南西作戦海域方面 バリ島沖", "42-1"},
            {"西方作戦海域方面 マラッカ海峡北方", "42-2"},
            {"西方作戦海域方面 セイロン島南西沖", "42-3"},
            {"欧州作戦海域方面 地中海マルタ島沖", "42-4"},
            {"欧州作戦海域方面 北海/北大西洋海域", "42-5"}
        };

        [Flags]
        public enum Update
        {
            None = 0,
            Error = 1,
            Start = 1 << 1,
            Item = 1 << 2,
            Ship = 1 << 3,
            Timer = 1 << 4,
            NDock = 1 << 5,
            Mission = 1 << 6,
            QuestList = 1 << 7,
            Battle = 1 << 8,
            Cell = 1 << 9,
            KDock = 1 << 10,
            All = (1 << 11) - 1,
            MapInfo = 1 << 24
        }

        public bool Started { get; private set; }

        public Sniffer(bool started = false)
        {
            Started = started;
            _itemInfo = new ItemInfo(ItemMaster, ItemInventory);
            _shipInfo = new ShipInfo(ShipMaster, ShipInventory, ItemInventory);
            _conditionTimer = new ConditionTimer(_shipInfo);
            _dockInfo = new DockInfo(_shipInfo, Material);
            _presetDeck = new PresetDeck(ShipInventory);
            AkashiTimer = new AkashiTimer(_shipInfo, _presetDeck);
            _airBase = new AirBase(_itemInfo);
            Battle = new BattleInfo(_shipInfo, _itemInfo, _airBase);
            _logger = new Logger(_shipInfo, _itemInfo, Battle);
            _questInfo = new QuestInfo(_questCountList);
            _questCounter = new QuestCounter(_questInfo, ItemInventory, ShipInventory, Battle);
            _miscTextInfo = new MiscTextInfo(_shipInfo, _itemInfo);
            _haveState = [Achievement, Material, _conditionTimer, ExMap, _questInfo];
            _receivers = [_conditionTimer, AkashiTimer, Battle, _miscTextInfo, CellInfo];
            AdditionalData = new AdditionalData();
        }

        public AdditionalData AdditionalData
        {
            get => _additionalData;
            private set
            {
                _additionalData = value;
                ItemMaster.AdditionalData = value;
                ShipMaster.AdditionalData = value;
            }
        }

        public void SaveState()
        {
            if (!_saveState)
                return;
            if (!_haveState.Any(x => x.NeedSave))
                return;
            foreach (var x in _haveState)
                x.SaveState(_status);
            _status.Save();
        }

        public void LoadState()
        {
            _status.Load();
            foreach (var x in _haveState)
                x.LoadState(_status);
            _saveState = true;
        }

        public Update Sniff(string url, string request, dynamic json)
        {
            _shipInfo.ChangingFleet = -1;
            Update update = DoSniff(url, request, json);
            if ((update & Update.Ship) != 0 || (update & Update.Item) != 0)
            {
                _shipInfo.CalcAntiAir();
                _airBase.CalcLandBasedReconModifier();
            }
            return update;
        }

        private Update DoSniff(string url, string request, dynamic json)
        {
            if (!json.api_result())
                return Update.Error;
            if ((int)json.api_result != 1)
                return Update.None;
            var data = json.api_data() ? json.api_data : new object();

            if (url.EndsWith("api_req_member/get_incentive"))
                ApiGetIncentive(data);
            // 本当は url.EndsWith("api_start2/getData") のほうがいいが、古いテストが通らないので保留
            if (url.Contains("api_start2") && !url.EndsWith("api_start2/get_option_setting"))
                return ApiStart(data);
            if (!Started)
                return Update.None;

            if (url.EndsWith("api_port/port"))
                return ApiPort(data);
            if (url.Contains("member"))
                return ApiMember(url, request, json);
            if (url.Contains("kousyou"))
                return ApiKousyou(url, request, data);
            if (url.Contains("practice"))
                return ApiPractice(url, request, data);
            if (IsBattleAPI(url))
                return ApiBattle(url, request, data);
            if (url.Contains("hensei"))
                return ApiHensei(url, request, data);
            if (url.Contains("kaisou"))
                return ApiKaisou(url, request, data);
            if (url.Contains("air_corps"))
                return ApiAirCorps(url, request, data);
            if (url.Contains("map"))
                return ApiMap(url, request, data);
            return ApiOthers(url, request, data);
        }

        private static bool IsBattleAPI(string url)
        {
            return url.Contains("api_req_sortie/") ||
                   url.Contains("api_req_battle_midnight/") ||
                   url.Contains("api_req_combined_battle/");
        }

        private void ApiGetIncentive(dynamic data)
        {
            _miscTextInfo.NeedSelectReward = data.api_select_reward_dict();
        }

        private Update ApiStart(dynamic data)
        {
            _shipInfo.InspectMaster(data);
            _shipInfo.ClearBattleResult();
            _missionInfo.InspectMaster(data);
            _itemInfo.InspectMaster(data);
            ExMap.ResetIfNeeded();
            _miscTextInfo.InspectMaster(data);
            _logger.InspectMapInfoMaster(data.api_mst_mapinfo);
            SetMapDictionary(data.api_mst_mapinfo);
            _questCountList.SetNames(data.api_mst_mission, data.api_mst_slotitem_equiptype, data.api_mst_slotitem);
            Started = true;
            return Update.Start;
        }

        private void SetMapDictionary(dynamic json)
        {
            foreach (var map in json)
                MapDictionary[map.api_name] = $"{map.api_maparea_id}-{map.api_no}";
        }

        public interface IPort
        {
            void Port();
        }

        private Update ApiPort(dynamic data)
        {
            _itemInfo.InspectBasic(data.api_basic);
            Material.InspectMaterialPort(data.api_material);
            _logger.InspectBasic(data.api_basic);
            _logger.InspectMaterial(data.api_material);
            _shipInfo.Port(data);
            _presetDeck.InspectBasic(data.api_basic);
            _missionInfo.InspectDeck(data.api_deck_port);
            _questCounter.InspectDeck(data.api_deck_port);
            _dockInfo.InspectNDock(data.api_ndock);
            Achievement.InspectBasic(data.api_basic);
            if (data.api_event_object())
                _airBase.InspectEventObject(data.api_event_object);
            if (data.api_plane_info())
                _airBase.InspectPlaneInfo(data.api_plane_info);
            foreach (var receiver in _receivers)
                receiver.Port();
            SaveState();
            RepeatingTimerController?.Resume();
            foreach (var s in Const.NotificationNamesRepeatingStop)
                RepeatingTimerController?.Stop(s);
            return Update.All;
        }

        private Update ApiMember(string url, string request, dynamic json)
        {
            var data = json.api_data() ? json.api_data : new object();

            if (url.EndsWith("api_get_member/require_info"))
            {
                _itemInfo.InspectSlotItem(data.api_slot_item, true);
                if (data.api_useitem())
                    _itemInfo.InspectUseItem(data.api_useitem);
                _dockInfo.InspectKDock(data.api_kdock);
                return Update.KDock;
            }
            if (url.EndsWith("api_get_member/basic"))
            {
                _itemInfo.InspectBasic(data);
                _logger.InspectBasic(data);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/slot_item"))
            {
                _itemInfo.InspectSlotItem(data, true);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/useitem"))
            {
                if (data == null)
                    return Update.None;
                _itemInfo.InspectUseItem(data);
                return Update.Item;
            }
            if (url.EndsWith("api_req_member/itemuse"))
            {
                if (data == null)
                    return Update.None;
                _itemInfo.InspectItemUse(data);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/payitem"))
            {
                if (data == null)
                    return Update.None;
                _itemInfo.InspectPayItem(data);
                return Update.Item;
            }
            if (url.EndsWith("api_req_member/payitemuse"))
            {
                if (data == null)
                    return Update.None;
                _itemInfo.InspectPayItemUse(request, data);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/kdock"))
            {
                _dockInfo.InspectKDock(data);
                _logger.InspectKDock(data);
                return Update.KDock | Update.Timer;
            }
            if (url.EndsWith("api_get_member/ndock"))
            {
                _dockInfo.InspectNDock(data);
                _conditionTimer.CheckCond();
                AkashiTimer.CheckFleet();
                RepeatingTimerController?.Stop("入渠終了");
                return Update.NDock | Update.Timer | Update.Ship;
            }
            if (url.EndsWith("api_get_member/questlist"))
            {
                _questInfo.InspectQuestList(request, data);
                return Update.QuestList;
            }
            if (url.EndsWith("api_get_member/deck"))
            {
                _shipInfo.InspectDeck(data);
                _missionInfo.InspectDeck(data);
                AkashiTimer.CheckFleet();
                _questCounter.InspectDeck(data);
                return Update.Mission | Update.Timer;
            }
            if (url.EndsWith("api_get_member/ship2"))
            {
                // ここだけjsonなので注意
                _shipInfo.InspectShip(url, json);
                AkashiTimer.CheckFleet();
                Battle.BattleState = BattleState.None;
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_get_member/ship_deck"))
            {
                _shipInfo.InspectShip(url, data);
                AkashiTimer.CheckFleet();
                Battle.BattleState = BattleState.None;
                return Update.None; // 画面更新と大破警告はこの直後の api_req_map/next で行う
            }
            if (url.EndsWith("api_get_member/ship3"))
            {
                _shipInfo.InspectShip(url, data);
                _dockInfo.InspectShip(data);
                AkashiTimer.CheckFleet();
                _conditionTimer.CheckCond();
                return Update.Ship;
            }
            if (url.EndsWith("api_get_member/material"))
            {
                Material.InspectMaterial(data);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/mapinfo"))
            {
                ExMap.InspectMapInfo(data);
                Battle.InspectMapInfo(data);
                if (data.api_air_base())
                    _airBase.Inspect(data.api_air_base);
                if (data.api_air_base_expanded_info())
                    _airBase.InspectExpandedInfo(data.api_air_base_expanded_info);
                return Update.Item | Update.MapInfo;
            }
            if (url.EndsWith("api_req_member/get_practice_enemyinfo"))
            {
                _miscTextInfo.InspectPracticeEnemyInfo(data);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/mission"))
            {
                _missionInfo.InspectMission(data);
                return Update.Mission;
            }
            if (url.EndsWith("api_get_member/preset_deck"))
            {
                _presetDeck.Inspect(data);
                return Update.Ship;
            }
            if (url.EndsWith("api_get_member/preset_slot"))
            {
                _itemInfo.InspectPresetSlot(data);
                return Update.Item;
            }
            if (url.EndsWith("api_get_member/base_air_corps"))
            {
                _airBase.Inspect(data);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_member/get_event_selected_reward"))
            {
                _miscTextInfo.InspectGetEventSelectedReward(data);
                return Update.None;
            }
            return Update.None;
        }

        private Update ApiKousyou(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_kousyou/createitem"))
            {
                _itemInfo.InspectCreateItem(data);
                Material.InspectCreateIem(data);
                _logger.InspectCreateItem(request, data);
                _questCounter.InspectCreateItem(request);
                return Update.Item | Update.QuestList;
            }
            if (url.EndsWith("api_req_kousyou/getship"))
            {
                _itemInfo.InspectGetShip(data);
                _shipInfo.InspectShip(url, data);
                _dockInfo.InspectKDock(data.api_kdock);
                _conditionTimer.CheckCond();
                RepeatingTimerController?.Stop("建造完了");
                return Update.Item | Update.KDock | Update.Timer;
            }
            if (url.EndsWith("api_req_kousyou/destroyship"))
            {
                _shipInfo.InspectDestroyShip(request);
                Material.InspectDestroyShip(data);
                _conditionTimer.CheckCond();
                AkashiTimer.CheckFleet();
                _questCounter.InspectDestroyShip(request);
                return Update.Item | Update.Ship | Update.QuestList;
            }
            if (url.EndsWith("api_req_kousyou/destroyitem2"))
            {
                _questCounter.InspectDestroyItem(request); // 本当に削除される前
                _itemInfo.InspectDestroyItem(request);
                Material.InspectDestroyItem(data);
                return Update.Item | Update.QuestList;
            }
            if (url.EndsWith("api_req_kousyou/remodel_slotlist_detail"))
            {
                _itemInfo.InspectRemodelSlotlistDetail(data);
                return Update.None;
            }
            if (url.EndsWith("api_req_kousyou/remodel_slot"))
            {
                _logger.SetCurrentMaterial(Material.Current);
                _logger.InspectRemodelSlot(request, data); // 資材の差が必要なので_materialInfoより前
                _itemInfo.InspectRemodelSlot(data);
                Material.InspectRemodelSlot(data);
                _questCounter.CountRemodelSlot();
                return Update.Item | Update.QuestList;
            }
            if (url.EndsWith("api_req_kousyou/createship"))
            {
                _logger.InspectCreateShip(request);
                Material.InspectCreateShip(request);
                _questCounter.CountCreateShip();
                return Update.Item | Update.QuestList;
            }
            if (url.EndsWith("api_req_kousyou/createship_speedchange"))
            {
                var largeFlag = _dockInfo.InspectCreateShipSpeedChange(request);
                Material.InspectCreateShipSpeedChange(largeFlag);
                return Update.Item | Update.Timer;
            }
            return Update.None;
        }

        private Update ApiPractice(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_practice/battle_result"))
            {
                Battle.InspectPracticeResult(data);
                _questCounter.InspectPracticeResult(data);
                return Update.Ship | Update.QuestList;
            }
            if (url.EndsWith("api_req_practice/battle"))
            {
                _shipInfo.StartPractice(request);
                _questCounter.StartPractice(request);
                CellInfo.StartPractice();
                _conditionTimer.InvalidateCond();
                RepeatingTimerController?.Suspend();
            }
            if (url.EndsWith("api_req_practice/battle") || url.EndsWith("api_req_practice/midnight_battle"))
            {
                Battle.InspectBattle(url, request, data);
                return Update.Ship | Update.Battle | Update.Timer;
            }
            return Update.None;
        }

        private Update ApiBattle(string url, string request, dynamic data)
        {
            if (url.EndsWith("/battleresult"))
            {
                var submarineSpecial = Battle.InspectBattleResult(data);
                _itemInfo.InspectBattleResult(submarineSpecial);
                ExMap.InspectBattleResult(data);
                _logger.InspectBattleResult(data);
                _questCounter.InspectBattleResult(data);
                _miscTextInfo.InspectBattleResult(data);
                return Update.Ship | Update.QuestList;
            }
            if (url.EndsWith("/goback_port"))
            {
                Battle.CauseEscape();
                return Update.Ship;
            }
            _shipInfo.ClearBadlyDamagedShips();
            RepeatingTimerController?.Stop("大破警告");
            Battle.InspectBattle(url, request, data);
            CellInfo.StartBattle();
            return Update.Ship | Update.Battle;
        }

        private Update ApiHensei(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_hensei/change"))
            {
                _shipInfo.InspectChange(request);
                AkashiTimer.InspectChange(request);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_hensei/preset_select"))
            {
                _shipInfo.InspectPresetSelect(data);
                AkashiTimer.CheckFleet();
                return Update.Ship;
            }
            if (url.EndsWith("api_req_hensei/preset_register"))
            {
                _presetDeck.InspectRegister(data);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_hensei/preset_delete"))
            {
                _presetDeck.InspectDelete(request);
                return Update.Ship | Update.Timer;
            }
            if (url.EndsWith("api_req_hensei/preset_expand"))
            {
                _presetDeck.InspectExpand();
                _itemInfo.InspectPresetExpand();
                return Update.Ship;
            }
            if (url.EndsWith("api_req_hensei/preset_order_change"))
            {
                _presetDeck.InspectOrderChange(request);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_hensei/combined"))
            {
                _shipInfo.InspectCombined(request);
                return Update.Ship;
            }
            return Update.None;
        }

        private Update ApiKaisou(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_kaisou/powerup"))
            {
                _questCounter.InspectPowerUp(request, data); // 艦種が必要なので艦が消える前
                _shipInfo.InspectPowerUp(request, data);
                _itemInfo.InspectPowerUp(request);
                _conditionTimer.CheckCond();
                AkashiTimer.CheckFleet();
                return Update.Item | Update.Ship | Update.QuestList;
            }
            if (url.EndsWith("api_req_kaisou/slot_exchange_index"))
            {
                _shipInfo.InspectSlotExchange(data);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_kaisou/slot_deprive"))
            {
                _shipInfo.InspectSlotDeprive(data);
                _dockInfo.InspectSlotDeprive(data);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_kaisou/preset_slot_register"))
            {
                var items = _shipInfo.InspectPresetSlotRegister(request);
                _itemInfo.InspectPresetSlotRegister(request, items);
                return Update.Item;
            }
            if (url.EndsWith("api_req_kaisou/preset_slot_delete"))
            {
                _itemInfo.InspectPresetSlotDelete(request);
                return Update.Item;
            }
            if (url.EndsWith("api_req_kaisou/preset_slot_expand"))
            {
                _itemInfo.InspectPresetSlotExpand(data);
            }
            if (url.EndsWith("api_req_kaisou/remodeling"))
            {
                var upgradeItems = _shipInfo.InspectRemodeling(request);
                _itemInfo.InspectRemodeling(upgradeItems);
                return Update.Item;
            }
            if (url.EndsWith("api_req_kaisou/open_exslot"))
            {
                _shipInfo.InspectOpenExslot(request);
                _itemInfo.InspectOpenExslot();
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_kaisou/marriage"))
            {
                _shipInfo.InspectMarriage(data);
                _itemInfo.InspectMarriage();
                return Update.Item | Update.Ship;
            }
            return Update.None;
        }

        private Update ApiAirCorps(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_air_corps/supply"))
            {
                Material.InspectAirCorpsSupply(data);
                _airBase.InspectSupply(request, data);
                return Update.Item;
            }
            if (url.EndsWith("api_req_air_corps/set_plane"))
            {
                Material.InspectAirCorpsSetPlane(data);
                _airBase.InspectSetPlane(request, data);
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_air_corps/change_deployment_base"))
            {
                _airBase.InspectChangeDeploymentBase(request, data);
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_air_corps/set_action"))
            {
                _airBase.InspectSetAction(request);
                return Update.Ship;
            }
            if (url.EndsWith("api_req_air_corps/expand_base"))
            {
                _airBase.InspectExpandBase(request, data);
                _itemInfo.InspectExpandBase();
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_air_corps/expand_maintenance_level"))
            {
                _airBase.InspectExpandMaintenanceLevel(request);
                _itemInfo.InspectExpandMaintenanceLevel();
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_air_corps/cond_recovery"))
            {
                _airBase.InspectCondRecovery(request, data);
                _itemInfo.InspectCondRecovery();
                return Update.Item | Update.Ship;
            }
            return Update.None;
        }

        private Update ApiMap(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_map/select_eventmap_rank"))
            {
                ExMap.InspectSelectEventmapRank(request, data);
                return Update.Mission; // 一覧の月次を更新したいので MapInfo ではなく Mission を返す
            }
            if (url.EndsWith("api_req_map/start"))
            {
                _shipInfo.InspectMapStart(request, data); // 出撃中判定が必要なので_conditionTimerより前
                _conditionTimer.InvalidateCond();
                ExMap.InspectMapStart(data);
                Battle.InspectMapStart(data);
                _logger.InspectMapStart(data);
                _miscTextInfo.InspectMapStart(data);
                _questCounter.InspectMapStart(data);
                CellInfo.InspectMapStart(data);
                RepeatingTimerController?.Suspend("大破警告");
                return Update.Timer | Update.Ship | Update.Cell;
            }
            if (url.EndsWith("api_req_map/next"))
            {
                _shipInfo.InspectMapNext(data);
                ExMap.InspectMapNext(data);
                Battle.InspectMapNext(data);
                _logger.InspectMapNext(data);
                _questCounter.InspectMapNext(data);
                _miscTextInfo.InspectMapNext(data);
                CellInfo.InspectMapNext(data);
                return Update.Battle | Update.Ship | Update.Item | Update.Cell;
            }
            if (url.EndsWith("api_req_map/anchorage_repair"))
            {
                _shipInfo.InspectAnchorageRepair(data);
                _itemInfo.InspectAnchorageRepair();
                return Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_map/air_raid"))
            {
                Battle.InspectAirRaid(request, data);
                _logger.InspectAirRaid();
                return Update.Battle | Update.Ship | Update.Cell;
            }
            return Update.None;
        }

        private Update ApiOthers(string url, string request, dynamic data)
        {
            if (url.EndsWith("api_req_hokyu/charge"))
            {
                _shipInfo.InspectCharge(data);
                Material.InspectCharge(data);
                _questCounter.CountCharge();
                return Update.Item | Update.Ship | Update.QuestList;
            }
            if (url.EndsWith("api_req_nyukyo/start"))
            {
                _dockInfo.InspectNyukyo(request);
                _conditionTimer.CheckCond();
                AkashiTimer.CheckFleet();
                _questCounter.CountNyukyo();
                var ndock = HttpUtility.ParseQueryString(request)["api_ndock_id"];
                if (ndock != null && int.TryParse(ndock, out int id))
                    RepeatingTimerController?.Stop("入渠終了", id - 1);
                return Update.Item | Update.Ship | Update.QuestList;
            }
            if (url.EndsWith("api_req_nyukyo/speedchange"))
            {
                _dockInfo.InspectSpeedChange(request);
                _conditionTimer.CheckCond();
                return Update.NDock | Update.Timer | Update.Item | Update.Ship;
            }
            if (url.EndsWith("api_req_mission/start"))
            {
                var deck = HttpUtility.ParseQueryString(request)["api_deck_id"];
                if (deck != null && int.TryParse(deck, out int id))
                    RepeatingTimerController?.Stop("遠征終了", id - 1);
                _missionInfo.InspectMissionStart(request);
                return Update.Timer;
            }
            if (url.EndsWith("api_req_mission/result"))
            {
                Material.InspectMissionResult(data);
                _logger.InspectMissionResult(data);
                _questCounter.InspectMissionResult(request, data);
                _missionInfo.InspectMissionResult(request, data);
                return Update.Item | Update.Timer;
            }
            if (url.EndsWith("api_req_mission/return_instruction"))
            {
                _missionInfo.InspectMissionReturnInstruction(request, data);
                return Update.Mission;
            }
            if (url.EndsWith("api_req_quest/stop"))
            {
                _questInfo.InspectStop(request);
                return Update.QuestList;
            }
            if (url.EndsWith("api_req_quest/clearitemget"))
            {
                var interval = _questInfo.InspectClearItemGet(request);
                _logger.InspectClearItemGet(data, interval);
                _itemInfo.InspectClearItemGet(data);
                return Update.QuestList;
            }
            if (url.EndsWith("api_req_furniture/buy"))
            {
                _itemInfo.InspectFurnitureBuy(request);
                return Update.Item;
            }
            return Update.None;
        }

        public NameAndTimer[] NDock => _dockInfo.NDock;

        public NameAndTimer[] KDock => _dockInfo.KDock;

        public AlarmCounter ItemCounter => _itemInfo.Counter;

        public QuestStatus[] Quests => _questInfo.Quests;

        public (string[], string[]) GetQuestNotifications() => _questInfo.GetNotifications();

        public NameAndTimer[] MissionTimers => _missionInfo.MissionTimers;

        public Dictionary<int, MissionStatus> MonthlyMissionStatus => _missionInfo.MonthlyMissionStatus;

        public DateTime GetConditionTimer(int fleet) => _conditionTimer.GetTimer(fleet);

        public int[] GetConditionNotice(TimeStep step) => _conditionTimer.GetNotice(step);

        public AlarmCounter ShipCounter => _shipInfo.Counter;

        public IReadOnlyList<Fleet> Fleets => _shipInfo.Fleets;

        public int InSortie => _shipInfo.InSortie;

        public double[] CalcSmokeScreenTriggerRates(int number) => _shipInfo.CalcSmokeScreenTriggerRates(number);

        public double CalcTransportPoint(int number) => _shipInfo.CalcTransportPoint(number);

        public int ChangingFleet => _shipInfo.ChangingFleet;

        public ShipInfo.ShipStatusPair[] BattleResultStatusDiff => _shipInfo.BattleResultDiff;

        public bool IsBattleResultError => _shipInfo.IsBattleResultError || Battle.DisplayedResultRank.IsError;

        public ShipStatus[] BattleStartStatus => _shipInfo.BattleStartStatus;

        public bool IsCombinedFleet => _shipInfo.Fleets[0].IsCombined;

        public ShipStatus[] RepairList => _shipInfo.GetRepairList();

        public IEnumerable<Fleet> PresetDecks => _presetDeck.Decks;

        public ShipStatus[] ShipList => _shipInfo.ShipList;

        public string[] BadlyDamagedShips => _shipInfo.BadlyDamagedShips;

        public bool WarnBadDamageWithDameCon
        {
            set => _shipInfo.WarnBadDamageWithDameCon = value;
        }

        public IEnumerable<ItemStatus> SlotItems => _itemInfo.SlotItems;

        public ItemStatus[] ItemList
        {
            get
            {
                _itemInfo.ClearHolder();
                _shipInfo.SetItemHolder();
                _airBase.SetItemHolder();
                return _itemInfo.ItemList;
            }
        }

        public Dictionary<int, int> RemodelSlotReqUseitem => _itemInfo.RemodelSlotReqUseitem;

        public List<ItemStatus[]> PresetItems => _itemInfo.PresetItems;

        public IEnumerable<MaterialItem> UseItems => Material.NowMaterials.Concat(_itemInfo.UseItems);

        public IEnumerable<MaterialItem> PayItems => _itemInfo.PayItems;

        public string MiscText => _miscTextInfo.Text;

        public AirBase.BaseInfo[] AirBase => _airBase.AllBase;

        public (string[], string[]) GetAirBaseCondNotifications() => _airBase.GetCondNotifications();

#if DEBUG
        public void SetLogWriter(Action<LogType, DateTime, string, string> writer, Func<DateTime> nowFunc)
        {
            _logger.SetWriter(writer, nowFunc);
        }

        public void EnableLog(LogType type)
        {
            _logger.EnableLog(type);
        }
#endif

        public LogConfig LogConfig
        {
            set => _logger.SetLogConfig(value);
        }

        public void FlashLog()
        {
            _logger.FlashLog();
        }
    }
}
