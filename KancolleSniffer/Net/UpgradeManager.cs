// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Net
{
    public enum UpgradeStatus
    {
        NotModified,
        Upgraded,
        Failed
    }

    public class UpgradeInfo
    {
        public string LatestVersion;
        public string ProxyPacHash;
        public UpgradeStatus ProxyPacStatus = UpgradeStatus.NotModified;
        public string EnemySlotHash;
        public UpgradeStatus EnemySlotStatus = UpgradeStatus.NotModified;
        public string MinAswHash;
        public UpgradeStatus MinAswStatus = UpgradeStatus.NotModified;

        public string GetFailedToolTip()
        {
            var tooltip = new StringBuilder();
            if (ProxyPacStatus == UpgradeStatus.Failed)
                tooltip.Cat("\n", "プロキシ自動構成ファイルを更新できませんでした");
            if (EnemySlotStatus == UpgradeStatus.Failed)
                tooltip.Cat("\n", "敵搭載数csvを更新できませんでした");
            if (MinAswStatus == UpgradeStatus.Failed)
                tooltip.Cat("\n", "素対潜csvを更新できませんでした");
            if (tooltip.Length > 0)
                tooltip.Append("\nインターネット接続を確認してください\nKancolleSnifferのフォルダとファイル\nTEMPフォルダの書き込み権限を\n確認してください");
            return tooltip.ToString();
        }

        public string GetRebootRequiredText()
        {
            if (ProxyPacStatus == UpgradeStatus.Upgraded)
                return "プロキシ設定を更新しました";
            return "";
        }

        public string GetUpgradedText()
        {
            var text = new StringBuilder();
            if (EnemySlotStatus == UpgradeStatus.Upgraded)
                text.Cat("\n", "敵搭載数csvを更新しました");
            if (MinAswStatus == UpgradeStatus.Upgraded)
                text.Cat("\n", "素対潜csvを更新しました");
            return text.ToString();
        }
    }

    public class UpgradeManager
    {
        private static readonly Regex splitter = new("\\s+\\**");

        public static UpgradeInfo CheckUpgrade()
        {
            var url = $"{AppInfo.Webpage}versions.txt";
            // var url = "https://httpstat.us/";
            // var url = "https://httpstat.us/304";
            // var url = "https://httpstat.us/200?sleep=4000";
            var request = WebRequest.Create(url);
            request.Timeout = 5000;
            try
            {
                using var response = request.GetResponse();
                if (!"text/plain".Equals(response.Headers["Content-Type"]))
                    return new UpgradeInfo();

                var upgradeInfo = new UpgradeInfo();
                string line;
                using var sr = new StreamReader(response.GetResponseStream());
                while ((line = sr.ReadLine()) != null)
                {
                    var values = splitter.Split(line);
                    if (values[1] == "KancolleSniffer")
                        upgradeInfo.LatestVersion = values[0];
                    if (values[1] == "proxy.pac")
                        upgradeInfo.ProxyPacHash = values[0];
                    if (values[1] == "EnemySlot.csv")
                        upgradeInfo.EnemySlotHash = values[0];
                    if (values[1] == "minasw.csv")
                        upgradeInfo.MinAswHash = values[0];
                }
#if DEBUG
                upgradeInfo.LatestVersion = AppInfo.Version;
#endif
                return upgradeInfo;
            }
            catch
            {
                return new UpgradeInfo();
            }
        }

        public static void DownloadUpgradeFiles(UpgradeInfo upgradeInfo)
        {
            Task[] tasks =
            [
                Task.Run(() => upgradeInfo.ProxyPacStatus  = DownloadVerifyFile("proxy.pac", upgradeInfo.ProxyPacHash)),
                Task.Run(() => upgradeInfo.EnemySlotStatus = DownloadVerifyFile("EnemySlot.csv", upgradeInfo.EnemySlotHash)),
                Task.Run(() => upgradeInfo.MinAswStatus    = DownloadVerifyFile("minasw.csv", upgradeInfo.MinAswHash))
            ];
            Task.WaitAll(tasks);
        }

        private static UpgradeStatus DownloadVerifyFile(string file, string hash)
        {
            if (string.IsNullOrEmpty(hash))
                return UpgradeStatus.Failed;
            if (CalcHash(AppInfo.AbsPath(file)) == hash)
                return UpgradeStatus.NotModified;

            var tmppath = Path.GetTempFileName();
            var url = $"{AppInfo.Webpage}{file}";
            var request = WebRequest.Create(url);
            request.Timeout = 5000;
            try
            {
                var mem = new MemoryStream();
                using (var stream = request.GetResponse().GetResponseStream())
                {
                    stream.CopyTo(mem);
                }
                mem.Position = 0;
                using (var tmpfile = new FileStream(tmppath, FileMode.Create, FileAccess.Write))
                {
                    mem.CopyTo(tmpfile);
                }

                if (CalcHash(tmppath) != hash)
                    return UpgradeStatus.Failed;

#if DEBUG
                return UpgradeStatus.NotModified;
#else
                File.Copy(tmppath, AppInfo.AbsPath(file), true);
                return UpgradeStatus.Upgraded;
#endif
            }
            catch
            {
                return UpgradeStatus.Failed;
            }
            finally
            {
                try
                {
                    File.Delete(tmppath);
                }
                catch
                {
                    // nop
                }
            }
        }

        private static string CalcHash(string file)
        {
            try
            {
                var sb = new StringBuilder();
                foreach (var b in SHA256.Create().ComputeHash(File.ReadAllBytes(file)))
                    sb.Append(b.ToString("x2"));
                return sb.ToString();
            }
            catch
            {
                return "";
            }
        }
    }
}
