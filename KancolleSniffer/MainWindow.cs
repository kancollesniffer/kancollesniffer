// Copyright (C) 2020 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using KancolleSniffer.Forms;
using KancolleSniffer.Net;
using KancolleSniffer.Notification;
using KancolleSniffer.Util;
using KancolleSniffer.View;
using KancolleSniffer.View.MainWindow;

namespace KancolleSniffer
{
    public class MainWindow
    {
        private readonly ResizableToolTip _toolTip = new();
        private readonly ResizableToolTip _tooltipCopy = new() {ShowAlways = false, AutomaticDelay = 0};
        private readonly ListFormGroup _listFormGroup;
        private readonly ContextMenuMain _contextMenuMain = new();
        private readonly ContextMenuNotifyIcon _contextMenuNotifyIcon = new();
        private readonly Components _c;

        private IEnumerable<IUpdateContext> Updateables => [_c.HqPanel, _c.MissionPanel, _c.KdockPanel, _c.NdockPanel, _c.MaterialHistoryPanel, _c.FleetPanel,
            Notifier, _listFormGroup];
        private IEnumerable<IUpdateTimers> _timers;
        private IEnumerable<Control> ZoomControls => [Form, _contextMenuMain];
        private IEnumerable<ResizableToolTip> ToolTips => [_toolTip, _tooltipCopy];
        private Main _main;
        public string UpgradedText { get; private set; } = "";

        public static bool SystemShutdown { get; set; }
        public Sniffer Sniffer { get; private set; }
        public Config Config { get; private set; }
        public Label PlayLogSign => _c.HqPanel.PlayLog;
        public int CurrentFleet() => _c.FleetPanel.CurrentFleet;
        public bool ShowCombinedFleet() => _c.FleetPanel.CombinedFleet;
        public Notifier Notifier { get; }
        public Form Form { get; }

        private class Components
        {
            public NotifyIcon NotifyIconMain { get; set; }
            public HqPanel HqPanel { get; set; }
            public FleetPanel FleetPanel { get; set; }
            public Label LabelNDockCaption { get; set; }
            public NDockPanel NdockPanel { get; set; }
            public Label LabelKDockCaption { get; set; }
            public KDockPanel KdockPanel { get; set; }
            public Label LabelMissionCaption { get; set; }
            public MissionPanel MissionPanel { get; set; }
            public Label LabelMaterialCaption { get; set; }
            public DropDownButton DropDownButtonMaterialHistory { get; set; }
            public MaterialHistoryPanel MaterialHistoryPanel { get; set; }
            public Label LabelQuestCaption { get; set; }
            public Label LabelQuestCount { get; set; }
            public QuestPanel QuestPanel { get; set; }
            public Label LabelAkashiRepair { get; set; }
            public Label LabelAkashiRepairTimer { get; set; }
            public Label LabelRepairListCaption { get; set; }
            public Label DropDownButtonRepairList { get; set; }
            public RepairListPanel PanelRepairList { get; set; }
            public IEnumerable<Control> Horizontal2RightsideControls() => [HqPanel, QuestPanel, LabelQuestCaption, LabelQuestCount, MaterialHistoryPanel,
                DropDownButtonMaterialHistory, LabelMaterialCaption];
            public IEnumerable<Control> Horizontal2LeftsideControls() => [MissionPanel, LabelMissionCaption, LabelRepairListCaption, PanelRepairList,
                DropDownButtonRepairList, LabelAkashiRepair, LabelAkashiRepairTimer, NdockPanel, LabelNDockCaption, KdockPanel, LabelKDockCaption];
            public IEnumerable<Control> ZoomControls() => [FleetPanel.Guide, HqPanel.Login, HqPanel.UpgradeInfo];
        }

        public MainWindow(Main main, Form form)
        {
            _c = GetComponents(form);
            Form = form;
            _c.NotifyIconMain.ContextMenuStrip = _contextMenuNotifyIcon;
            Form.ContextMenuStrip = _contextMenuMain;
            SetupMain(main);
            _listFormGroup = new ListFormGroup(this);
            Notifier = new Notifier(FlashWindow, ShowTaster, PlaySound);
            SetupView();
        }

        private Components GetComponents(Form form)
        {
            var r = new Components();
            foreach (var prop in typeof(Components).GetProperties())
                prop.SetValue(r, form.GetType().GetField(prop.Name, BindingFlags.NonPublic | BindingFlags.Instance).GetValue(form));
            return r;
        }

        private void SetupMain(Main main)
        {
            _main = main;
            Config = main.Config;
            Sniffer = main.Sniffer;
        }

        private void SetupView()
        {
            SetScaleFactorOfDpiScaling();
            if (Config.Shape == "横長2")
                ChangeHorizontalLayout();
            SetupQuestPanel();
            SetEventHandlers();
            _c.FleetPanel.AkashiRepairTimer = _c.LabelAkashiRepairTimer;
            _c.FleetPanel.ShowShipOnList = ShowShipOnShipList;
            _c.FleetPanel.UpdateQuestList = UpdateQuestList;
            _c.FleetPanel.UpdateMonthlyMission = _listFormGroup.UpdateMonthlyMission;
            _c.PanelRepairList.CreateLabels(PanelRepairList_Click);
            _c.NdockPanel.SetClickHandler(_c.LabelNDockCaption);
            _c.MissionPanel.SetClickHandler(_c.LabelMissionCaption);
            _c.MaterialHistoryPanel.SetClickHandler(_c.LabelMaterialCaption, _c.DropDownButtonMaterialHistory);
            SetupUpdateable();
            _listFormGroup.SetupView();
            PerformZoom();
        }

        private void ChangeHorizontalLayout()
        {
            var diff = _c.MissionPanel.Left - _c.HqPanel.Left;
            foreach (var control in _c.Horizontal2RightsideControls())
                control.Left += diff;
            foreach (var control in _c.Horizontal2LeftsideControls())
                control.Left -= diff;
        }

        private void SetEventHandlers()
        {
            SetMainFormEventHandler();
            SetContextMenuMainEventHandler();
            SetContextMenuNotifyIconEventHandler();
            SetNotifyIconEventHandler();
            SetRepairListEventHandler();
        }

        private void SetMainFormEventHandler()
        {
            Form.Load        += MainForm_Load;
            Form.FormClosing += MainForm_FormClosing;
            Form.Resize      += MainForm_Resize;
            Form.Activated   += MainForm_Activated;
        }

        private void SetContextMenuMainEventHandler()
        {
            _contextMenuMain.SetClickHandlers(_listFormGroup.ShowOrCreate, _main.ShowReport, _main.StartCapture, _main.ShowConfigDialog, Form.Close);
        }

        private void SetContextMenuNotifyIconEventHandler()
        {
            _contextMenuNotifyIcon.SetEventHandlers(RevertFromIcon, Form.Close);
        }

        private void SetNotifyIconEventHandler()
        {
            _c.NotifyIconMain.MouseDoubleClick += NotifyIconMain_MouseDoubleClick;
        }

        private void SetRepairListEventHandler()
        {
            _c.LabelRepairListCaption.Click   += LabelRepairListButton_Click;
            _c.DropDownButtonRepairList.Click += LabelRepairListButton_Click;
        }

        private void SetupUpdateable()
        {
            var context = new UpdateContext(Sniffer, Config, () => _main.Step);
            foreach (var updateable in Updateables)
                updateable.Context = context;
            _timers = [_c.MissionPanel, _c.KdockPanel, _c.NdockPanel, _c.FleetPanel, _listFormGroup];
        }

        private void SetScaleFactorOfDpiScaling()
        {
            var autoScaleDimensions = new SizeF(6f, 12f); // AutoScaleDimensionの初期値
            Scaler.Factor = new SizeF(Form.CurrentAutoScaleDimensions.Width / autoScaleDimensions.Width,
                Form.CurrentAutoScaleDimensions.Height / autoScaleDimensions.Height);
        }

        private void SetupQuestPanel()
        {
            var prevHeight = _c.QuestPanel.Height;
            _c.QuestPanel.CreateLabels(Config.QuestLines);
            Form.Height += _c.QuestPanel.Height - prevHeight;
        }

        public void UpdateInfo(Sniffer.Update update)
        {
            if (update == Sniffer.Update.Start)
            {
                _c.HqPanel.Login.Visible = false;
                if (!string.IsNullOrEmpty(UpgradedText))
                    ClearNeedLoginAgain();
                else if (!string.IsNullOrEmpty(_c.HqPanel.UpgradeInfo.Text))
                    _c.HqPanel.UpgradeInfo.Visible = true;
                _c.FleetPanel.Start();
                Notifier.StopAllRepeat();
                return;
            }
            if (!Sniffer.Started)
                return;
            if ((update & Sniffer.Update.Item) != 0)
                UpdateItemInfo();
            if ((update & Sniffer.Update.Timer) != 0)
                UpdateTimers();
            if ((update & Sniffer.Update.KDock) != 0)
                UpdateKDockLabels();
            if ((update & Sniffer.Update.NDock) != 0 || (update & Sniffer.Update.Ship) != 0)
                UpdateNDockLabels();
            if ((update & Sniffer.Update.Mission) != 0)
                UpdateMissionLabels();
            if ((update & Sniffer.Update.QuestList) != 0)
                UpdateQuestList();
            if ((update & Sniffer.Update.Ship) != 0)
                UpdateShipInfo();
            if ((update & Sniffer.Update.Battle) != 0)
                UpdateBattleInfo();
            if ((update & Sniffer.Update.Cell) != 0)
                UpdateCellInfo();
            if ((update & Sniffer.Update.MapInfo) != 0)
                UpdateMapInfo();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            SuppressActivate.Start();
            RestoreLocation();
            if (Config.HideOnMinimized && Form.WindowState == FormWindowState.Minimized)
                Form.ShowInTaskbar = false;
            if (Config.ShowHpInPercent)
                _c.FleetPanel.ToggleHpPercent();
            if (Config.ShipList.Visible)
                _listFormGroup.Show();
        }

        public void ShowUpgradeInfo(UpgradeInfo upgradeInfo)
        {
            if (AppInfo.Version != upgradeInfo.LatestVersion)
                ShowVersionUp(upgradeInfo);
            else
                ShowNeedLoginAgain(upgradeInfo);
        }

        private void ShowVersionUp(UpgradeInfo upgradeInfo)
        {
            if (string.IsNullOrEmpty(upgradeInfo.LatestVersion))
            {
                _c.FleetPanel.Guide.Location = new Point(12, 51);
                _c.FleetPanel.Guide.Text = "バージョンを確認できません\nインターネット接続を確認してください";
                _c.FleetPanel.Guide.LinkArea = new LinkArea(0, _c.FleetPanel.Guide.Text.Length);
                _c.FleetPanel.Guide.Click += (obj, ev) => { Process.Start(AppInfo.Webpage); };
                _c.HqPanel.UpgradeInfo.Text = "バージョンを確認できません";
                _c.HqPanel.ToolTip.SetToolTip(_c.HqPanel.UpgradeInfo, "インターネット接続を確認してください");
                _c.HqPanel.UpgradeInfo.Visible = Sniffer.Started;
            }
            else
            {
                _c.NotifyIconMain.Icon = Form.Icon = AppInfo.UpgradeIcon;
                _c.FleetPanel.Guide.Location = new Point(24, 51);
                _c.NotifyIconMain.Text = _c.FleetPanel.Guide.Text = _c.HqPanel.UpgradeInfo.Text = $"バージョン {upgradeInfo.LatestVersion} に更新できます";
                _c.FleetPanel.Guide.LinkArea = new LinkArea(0, _c.FleetPanel.Guide.Text.Length);
                _c.FleetPanel.Guide.Click += (obj, ev) => { Process.Start(AppInfo.Webpage); };
                _c.HqPanel.ToolTip.SetToolTip(_c.HqPanel.UpgradeInfo, "");
                _c.HqPanel.UpgradeInfo.Visible = Sniffer.Started;
            }
        }

        private void ShowNeedLoginAgain(UpgradeInfo upgradeInfo)
        {
            var tooltip = upgradeInfo.GetFailedToolTip();
            var upgraded = upgradeInfo.GetUpgradedText();
            if (!string.IsNullOrEmpty(upgraded))
                UpgradedText = upgraded;
            var rebootRequired = upgradeInfo.GetRebootRequiredText();
            if (!string.IsNullOrEmpty(rebootRequired))
                UpgradedText = rebootRequired;

            if (!string.IsNullOrEmpty(tooltip))
            {
                _c.FleetPanel.Guide.Location = new Point(0, 16);
                _c.FleetPanel.Guide.Text = tooltip;
                _c.NotifyIconMain.Text = _c.HqPanel.UpgradeInfo.Text = "データを更新できません";
                _c.HqPanel.ToolTip.SetToolTip(_c.HqPanel.UpgradeInfo, tooltip);
                _c.HqPanel.UpgradeInfo.Visible = Sniffer.Started;
            }
            else if (HttpProxy.IsInListening && !string.IsNullOrEmpty(UpgradedText + rebootRequired))
            {
                _c.NotifyIconMain.Icon = Form.Icon = AppInfo.UpgradeIcon;
                _c.NotifyIconMain.Text = "プロキシ設定を更新しました ブラウザを再起動してください";
                _c.HqPanel.UpgradeInfo.Text = "ブラウザを再起動してください";
                _c.HqPanel.ToolTip.SetToolTip(_c.HqPanel.UpgradeInfo, rebootRequired);
                _c.HqPanel.UpgradeInfo.Visible = true;
            }
            else if (Sniffer.Started && !string.IsNullOrEmpty(UpgradedText + upgraded))
            {
                _c.NotifyIconMain.Icon = Form.Icon = AppInfo.UpgradeIcon;
                _c.NotifyIconMain.Text = _c.HqPanel.UpgradeInfo.Text = "データ更新しました 再ログインしてください";
                _c.HqPanel.ToolTip.SetToolTip(_c.HqPanel.UpgradeInfo, upgraded);
                _c.HqPanel.UpgradeInfo.Visible = true;
            }
            else
            {
                _c.FleetPanel.Guide.Location = new Point(31, 51);
                _c.FleetPanel.Guide.Text = "右クリックでメニューが出ます。";
                ClearNeedLoginAgain();
            }
        }

        private void ClearNeedLoginAgain()
        {
            UpgradedText = "";
            _c.NotifyIconMain.Icon = Form.Icon = AppInfo.Favicon;
            _c.NotifyIconMain.Text = "KancolleSniffer";
            _c.HqPanel.UpgradeInfo.Visible = false;
            _c.HqPanel.UpgradeInfo.Text = "";
            _c.HqPanel.ToolTip.SetToolTip(_c.HqPanel.UpgradeInfo, "");
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Config.ExitSilently && !SystemShutdown)
            {
                using var dialog = new ConfirmDialog();
                if (dialog.ShowDialog(Form) != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }
            }
            if (SystemShutdown)
                _listFormGroup.WaitForCloseAll();
            else
                _listFormGroup.Close(); // 各自で終了処理するのでシャットダウン時は不要
            Config.Location = (Form.WindowState == FormWindowState.Normal ? Form.Bounds : Form.RestoreBounds).Location;
            Config.ShowHpInPercent = _c.FleetPanel.ShowHpInPercent;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (_listFormGroup == null) // DPIが100%でないときにInitializeComponentから呼ばれるので
                return;
            SuppressActivate.Start();
            if (Form.WindowState == FormWindowState.Minimized)
            {
                if (Config.HideOnMinimized)
                    Form.ShowInTaskbar = false;
            }
            _listFormGroup.Main.ChangeWindowState(Form.WindowState);
        }

        public readonly TimeOutChecker SuppressActivate = new();

        private void MainForm_Activated(object sender, EventArgs e)
        {
            if (SuppressActivate.Check())
                return;
            if (NeedRaise)
                RaiseBothWindows();
        }

        private bool NeedRaise => _listFormGroup.Main.Visible && Form.WindowState != FormWindowState.Minimized;

        private void RaiseBothWindows()
        {
            _listFormGroup.Main.Owner = null;
            Form.Owner = _listFormGroup.Main;
            Form.BringToFront();
            Form.Owner = null;
        }

        public class TimeOutChecker
        {
            private DateTime _lastCheck;
            private readonly TimeSpan _timeout = TimeSpan.FromMilliseconds(500);

            public void Start()
            {
                _lastCheck = DateTime.Now;
            }

            public bool Check()
            {
                var now = DateTime.Now;
                var last = _lastCheck;
                _lastCheck = now;
                return now - last < _timeout;
            }
        }

        private void RevertFromIcon()
        {
            Form.ShowInTaskbar = true;
            Form.WindowState = FormWindowState.Normal;
            Form.TopMost = _listFormGroup.TopMost = Config.TopMost; // 最前面に表示されなくなることがあるのを回避する
        }

        private void NotifyIconMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            RevertFromIcon();
        }

        private void PerformZoom()
        {
            if (Config.Zoom == 100)
            {
                ShipLabel.Name.BaseFont  = Form.Font;
                ShipLabel.Name.LatinFont = LatinFont();
                return;
            }
            var prev = Form.CurrentAutoScaleDimensions;
            foreach (var control in ZoomControls.Concat(_c.ZoomControls()).Concat(_main.Controls))
                control.Font = ZoomFont(control.Font);
            _listFormGroup.Font = ZoomFont(_listFormGroup.Font);
            foreach (var toolTip in ToolTips)
                toolTip.Font = ZoomFont(toolTip.Font);
            ShipLabel.Name.BaseFont = Form.Font;
            ShipLabel.Name.LatinFont = LatinFont();
            var cur = Form.CurrentAutoScaleDimensions;
            Scaler.Factor = Scaler.Scale(cur.Width / prev.Width, cur.Height / prev.Height);
        }

        private Font ZoomFont(Font font)
        {
            return new Font(font.FontFamily, font.Size * Config.Zoom / 100);
        }

        private Font LatinFont()
        {
            return new Font("Tahoma", 8f * Config.Zoom / 100);
        }

        private void RestoreLocation()
        {
            if (Config.Location.X == int.MinValue)
                return;
            if (IsTitleBarOnAnyScreen(Config.Location))
                Form.Location = Config.Location;
        }

        public void ApplyConfig()
        {
            if (Form.TopMost != Config.TopMost)
                Form.TopMost = _listFormGroup.TopMost = Config.TopMost;
            _c.HqPanel.Update();
            _c.LabelAkashiRepair.Visible = _c.LabelAkashiRepairTimer.Visible = Config.UsePresetAkashi;
        }

        public static bool IsTitleBarOnAnyScreen(Point location)
        {
            var rect = new Rectangle(
                new Point(location.X + SystemInformation.IconSize.Width + SystemInformation.HorizontalFocusThickness,
                    location.Y + SystemInformation.CaptionHeight), new Size(60, 1));
            return Screen.AllScreens.Any(screen => screen.WorkingArea.Contains(rect));
        }

        private void ShowShipOnShipList(int id)
        {
            _listFormGroup.ShowShip(id);
        }

        public void UpdateItemInfo()
        {
            _c.HqPanel.Update();
            _c.FleetPanel.Update();
            Notifier.NotifyShipItemCount();
            _c.MaterialHistoryPanel.Update();
            _listFormGroup.UpdateList();
        }

        private void UpdateShipInfo()
        {
            _c.FleetPanel.Update();
            Notifier.NotifyDamagedShip();
            UpdateChargeInfo();
            UpdateRepairList();
            UpdateMissionLabels();
            _listFormGroup.UpdateList();
        }

        public void RefreshShipList(string[] modes)
        {
            _listFormGroup.RefreshShipList(modes);
        }

        private void UpdateBattleInfo()
        {
            _listFormGroup.UpdateBattleResult();
            _listFormGroup.UpdateAirBattleResult();
            _c.FleetPanel.UpdateBattleInfo();
        }

        private void UpdateMapInfo()
        {
            Notifier.NotifyAirbaseCond();
        }

        private void UpdateCellInfo()
        {
            _listFormGroup.UpdateCellInfo();
        }

        private void UpdateChargeInfo()
        {
            _c.FleetPanel.UpdateChargeInfo();
        }

        private void UpdateKDockLabels()
        {
            _c.KdockPanel.Update();
        }

        private void UpdateNDockLabels()
        {
            _c.NdockPanel.Update();
        }

        private void UpdateMissionLabels()
        {
            _c.MissionPanel.Update();
            _listFormGroup.UpdateMonthlyMission();
        }

        public void UpdateTimers()
        {
            foreach (var timer in _timers)
                timer.UpdateTimers();
        }

        private void UpdateRepairList()
        {
            _c.PanelRepairList.SetRepairList(Sniffer.RepairList);
            _toolTip.SetToolTip(_c.LabelRepairListCaption, new RepairShipCount(Sniffer.RepairList).ToString());
        }

        private void UpdateQuestList()
        {
            _c.QuestPanel.Update(Sniffer.Quests, Sniffer.Fleets[_c.FleetPanel.CurrentFleet].Ships, _c.FleetPanel.CurrentFleet + 1,
                Sniffer.InSortie > -1 ? Sniffer.CellInfo.MapId : 0);
            _c.LabelQuestCount.Text = Sniffer.Quests.Length.ToString();
            Notifier.NotifyQuestComplete();
        }

        private void FlashWindow()
        {
            Win32API.FlashWindow(Form.Handle);
        }

        private void ShowTaster(string title, string message)
        {
            _c.NotifyIconMain.ShowBalloonTip(20000, title, message, ToolTipIcon.Info);
        }

        private void PlaySound(string file, int volume)
        {
            SoundPlayer.PlaySound(Form.Handle, file, volume);
        }

        private void LabelRepairListButton_Click(object sender, EventArgs e)
        {
            if (_c.PanelRepairList.Visible)
            {
                _c.PanelRepairList.Visible = false;
                _c.DropDownButtonRepairList.BackColor = Control.DefaultBackColor;
            }
            else
            {
                _c.PanelRepairList.Visible = true;
                _c.PanelRepairList.BringToFront();
                _c.DropDownButtonRepairList.BackColor = CustomColors.ActiveButtonColor;
            }
        }

        private void PanelRepairList_Click(object sender, EventArgs e)
        {
            _c.PanelRepairList.Visible = false;
            _c.DropDownButtonRepairList.BackColor = Control.DefaultBackColor;
        }
    }
}
