// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace KancolleSniffer
{
    public static class AppInfo
    {
        public static readonly string Version;
        public const string VersionSuffix = "";
        public static readonly string Copyright;
        public const string Webpage = "https://kancollesniffer.bitbucket.io/";
        public static readonly string BaseDir = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly Icon Favicon;
        public static readonly Icon UpgradeIcon;

        static AppInfo()
        {
            var info = FileVersionInfo.GetVersionInfo(AbsPath("KancolleSniffer.exe"));
            Version = $"{info.FileMajorPart}.{info.FileMinorPart}{VersionSuffix}";
            Copyright = info.LegalCopyright.Replace(", ", ",\n");
            Favicon = new Icon(AbsPath("favicon.ico"), 48, 48);
            UpgradeIcon = new Icon(AbsPath("upgrade.ico"), 48, 48);
        }

        public static string AbsPath(string file)
        {
            return Path.Combine(BaseDir, file);
        }
    }
}
