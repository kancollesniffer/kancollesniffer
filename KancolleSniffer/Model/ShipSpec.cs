// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using static KancolleSniffer.Model.ShipClassCode;
using static KancolleSniffer.Model.ShipTypeCode;

namespace KancolleSniffer.Model
{
    public class ShipTypeCode
    {
        public const int DE  = 1;  // 海防艦
        public const int DD  = 2;  // 駆逐艦
        public const int CL  = 3;  // 軽巡洋艦
        public const int CLT = 4;  // 重雷装巡洋艦
        public const int CT  = 21; // 練習巡洋艦
        public const int CA  = 5;  // 重巡洋艦
        public const int CAV = 6;  // 航空巡洋艦
        public const int CVL = 7;  // 軽空母
        public const int CV  = 11; // 正規空母
        public const int CVB = 18; // 装甲空母
        public const int BC  = 8;  // 巡洋戦艦
        public const int BB  = 9;  // 低速戦艦
        public const int BBV = 10; // 航空戦艦
        public const int SS  = 13; // 潜水艦
        public const int SSV = 14; // 潜水空母
        public const int AV  = 16; // 水上機母艦
        public const int LHA = 17; // 揚陸艦
        public const int AO  = 22; // 補給艦
        public const int AS  = 20; // 潜水母艦
        public const int AR  = 19; // 工作艦
        public const int AP  = 15; // 輸送ワ級
    }

    public class ShipClassCode
    {
        public const int Fusou   = 26;
        public const int Ise     = 2;
        public const int Nagato  = 19;
        public const int Yamato  = 37;
        public const int Taiyou  = 76;
        public const int Hayasui = 60;
        public const int Asahi   = 123;
        public const int Akashi  = 49;
        public const int Mogami  = 9;
        public const int Kuma    = 4;
        public const int Agano   = 41;
        public const int Ooyodo  = 52;
        public const int Akizuki = 54;
        public const int Ukuru   = 117;
        public const int TeiDE   = 104;
        public const int Yamashiomaru = 115;
        public const int No101Tp  = 120;
        public const int Zara     = 64;
        public const int Atlanta  = 99;
        public const int Fletcher = 91;
        public const int JohnCButler = 87;
        public const int ArkRoyal = 78;
        public const int JDD      = 82;
    }

    public class ShipNationCode
    {
        public const int US = 32;
        public const int UK = 33;
        public const int FR = 34;
    }

    public class ShipSpec
    {
        public int Id { get; set; }
        public int SortId { get; set; }
        public string Name { get; set; }
        public string Yomi { get; set; }
        public int MinFirepower { get; set; }
        public int MinTorpedo { get; set; }
        public int MinAntiAir { get; set; }
        public int MinArmor { get; set; }
        public int FuelMax { get; set; }
        public int BullMax { get; set; }
        public int SlotNum { get; set; }
        public Func<int> GetMinAsw { get; set; }
        public int MinAsw => GetMinAsw?.Invoke() ?? -1;
        public Func<int[]> GetMaxEq { get; set; }
        public int[] MaxEq => GetMaxEq?.Invoke();
        public Func<int> GetNumEquips { get; set; }
        public Action<int> SetNumEquips { get; set; }

        public int NumEquips
        {
            get => GetNumEquips();
            set => SetNumEquips(value);
        }

        public int ShipType { get; set; }
        public int ShipClass { get; set; }
        public string ShipTypeName { get; set; }
        public Dictionary<int, int> UpgradeItems { get; set; } = [];
        public RemodelInfo Remodel { get; } = new RemodelInfo();

        public class RemodelInfo
        {
            public int Level { get; set; }
            public int After { get; set; }
            public int Base { get; set; } // 艦隊晒しページ用
            public int Step { get; set; } // 同上
        }

        public ShipSpec()
        {
            Id = -1;
            Name = "";
        }

        public bool IsEnemy => IsEnemyId(Id);

        public static bool IsEnemyId(int id) => id > 1500;

        public string EnemyShortenName => Name.Replace("(elite)", "(e)").Replace("(flagship)", "(f)");

        public double RepairWeight
        {
            get
            {
                switch (ShipType)
                {
                    case DE:
                    case SS:
                        return 0.5;
                    case DD:
                    case CL:
                    case CLT:
                    case SSV:
                    case AV:
                    case LHA:
                    case CT:
                    case AO:
                        return 1.0;
                    case CA:
                    case CAV:
                    case CVL:
                    case BC:
                    case AS:
                        return 1.5;
                    case BB:
                    case BBV:
                    case CV:
                    case CVB:
                    case AR:
                        return 2.0;
                    default:
                        return 1.0;
                }
            }
        }

        public double TransportPoint
        {
            get
            {
                switch (ShipType)
                {
                    case DD:
                        return 5.0;
                    case CL:
                        return 2.0;
                    case CAV:
                        return 4.0;
                    case BBV:
                        return 7.0;
                    case SSV:
                        return 1.0;
                    case AV:
                        return 9.0;
                    case LHA:
                        return 12.0;
                    case AS:
                        return 7.0;
                    case CT:
                        return 6.0;
                    case AO:
                        return 15.0;
                    default:
                        return 0;
                }
            }
        }

        public bool IsSubmarine => ShipType is SS or SSV;

        public bool IsAircraftCarrier => ShipType is CVL or CV or CVB;

        public bool IsEscortCarrier => ShipType is CVL && MinAsw > 0;

        public bool IsLightCruiserClass => ShipType is CL or CLT or CT;

        public bool IsTrainingCruiser => ShipType == CT;

        public bool IsAsashiCT => ShipClass == Asahi && ShipType == CT;

        public bool IsRepairShip => ShipType == AR;

        public int RepairReach => ShipClass == Akashi ? 2 : 0;

        public int HullNumber => SortId / 10; // 改造段階を問わない個艦識別番号

        public int UpgradeLevel => SortId % 10; // 一の位は改造段階とみなせそう

        public int Nation => SortId < 30000 ? 1 : SortId / 1000;

        public bool EnableNightShelling => (ShipType != CVL || ShipClass == Taiyou) && MinFirepower > 0;

        public bool EnableAntiAirPropellantBarrage => IsAircraftCarrier || ShipType is BBV or CAV or AV;

        public bool IsNightAircraftCarrier =>
            Id is 545  // Saratoga Mk.II
               or 599  // 赤城改二戊
               or 610  // 加賀改二戊
               or 883; // 龍鳳改二戊

        public bool IsAswCruiserDestroyer =>
            Id is 141 // 五十鈴改二
               or 478 // 龍田改二
               or 624 // 夕張改二丁
            || ShipClass == Fletcher && Id != 941 // Fletcher級、Heywood L.E.未改以外
            || (ShipClass == JDD || ShipClass == JohnCButler) && UpgradeLevel > 1; // J級改 John C.Butler級改

        public bool IsAswAircraftCarrier =>
            Id == 646 // 加賀改二護
            || ShipClass == Taiyou && MinAsw > 59; // 大鷹型改 sort_idがズレてるので初期対潜で代用

        public bool IsOilerAircraftCarrier =>
            ShipClass == Yamashiomaru // 山汐丸
            || ShipClass == Hayasui && UpgradeLevel > 1; // 速吸改
    }
}
