// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using static KancolleSniffer.Model.ShipClassCode;
using static KancolleSniffer.Model.ShipTypeCode;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class EffectiveShelling
    {
        public double ShellingPower { get; set; }
        public double ShellingAccuracy { get; set; }
        public int SupportPower { get; set; }
        public double SupportAccuracy { get; set; }
        public double APShellPower { get; set; }
        public double APShellAccuracy { get; set; }

        public static EffectiveShelling Calc(ShipStatus ship)
        {
            if (ship.Spec.IsSubmarine)
                return new();

            var skilledDeckAviationMaintenance = new ItemStatus();
            var isCarrierBasedBomber       = false;
            var torpedo                    = 0;
            var bomber                     = 0;
            var hasCarrierBasedBomber      = false;
            var alvTotal                   = 0;
            var alvItems                   = 0;
            var levelBonus                 = 0.0;
            var tbBonus                    = 0;
            var clSingleGunCount           = 0;
            var clLTwinGunCount            = 0;
            var italyCAGunCount            = 0;
            var _140mmGunCount             = 0;
            var _152mmTwinGunCount         = 0;
            var _152mmTripleGunCount       = 0;
            var _127mmConcentratedGunCount = 0;
            var _155mmTripleGunCount       = 0;
            var _203mmTwinGunCount         = 0;
            var _203mmTripleGunCount       = 0;
            var accuracy                   = 0;
            var accuracyLevelBonus         = 0.0;
            var hasPrimaryGun              = false;
            var hasAPShell                 = false;
            var hasSecondaryGun            = false;
            var hasRadar                   = false;

            foreach (var item in ship.Items)
            {
                if (item.Spec.IsSkilledDeckAviationMaintenance && skilledDeckAviationMaintenance.Level <= item.Level)
                    skilledDeckAviationMaintenance = item;
                isCarrierBasedBomber  |= item.Spec.IsCarrierBasedBomber;
                if (item.Spec.IsAircraft || item.Spec.IsAviationPersonnel)
                    torpedo += item.Spec.Torpedo;
                if (item.Spec.IsAircraft || item.Spec.IsAviationPersonnel)
                    bomber  += item.Spec.Bomber;
                hasCarrierBasedBomber |= item.HasCarrierBasedBomber;
                if (item.HasAnyBomber)
                {
                    alvTotal += item.Alv;
                    alvItems++;
                }
                levelBonus            += item.FirepowerLevelBonus;
                tbBonus               += item.TbBonus;
                if (item.Spec.IsCLSingleGun)
                    clSingleGunCount++;
                if (item.Spec.IsCLTwinGun)
                    clLTwinGunCount++;
                if (item.Spec.IsItalyCAGun)
                    italyCAGunCount++;
                if (item.Spec.Is140mmGun)
                    _140mmGunCount++;
                if (item.Spec.Is152mmTwinGun)
                    _152mmTwinGunCount++;
                if (item.Spec.Is152mmTripleGun)
                    _152mmTripleGunCount++;
                if (item.Spec.Is127mmConcentratedGun)
                    _127mmConcentratedGunCount++;
                if (item.Spec.Is155mmTripleGun)
                    _155mmTripleGunCount++;
                if (item.Spec.Is203mmTwinGun)
                    _203mmTwinGunCount++;
                if (item.Spec.Is203mmTripleGun)
                    _203mmTripleGunCount++;
                accuracy              += item.Spec.Accuracy;
                accuracyLevelBonus    += item.AccuracyLevelBonus;
                hasPrimaryGun         |= item.Spec.IsPrimaryGun;
                hasAPShell            |= item.Spec.IsAPShell;
                hasSecondaryGun       |= item.Spec.IsSecondaryGun;
                hasRadar              |= item.Spec.IsRadar;
            }

            // 速吸改、山汐丸(艦攻艦爆装備、0機でも)
            var isRyuseiAttack = ship.Spec.IsOilerAircraftCarrier && isCarrierBasedBomber;
            torpedo += skilledDeckAviationMaintenance.AviationPersonnelTorpedoBonus;
            bomber  += skilledDeckAviationMaintenance.AviationPersonnelBomberBonus;
            var alv = alvItems > 0 ? alvTotal / alvItems : 0;

            var shellingPower = 0.0;
            if (ship.Spec.IsAircraftCarrier || isRyuseiAttack)
            {
                if (hasCarrierBasedBomber)
                {
                    shellingPower = (int)((ship.Firepower + torpedo + levelBonus + tbBonus
                        + (int)(bomber * 1.3) + ship.Fleet.CombinedFirepowerBonus) * 1.5) + 55;
                }
            }
            else
            {
                var shipTypeGunTypeFirepower = 0.0;
                if (ship.Spec.IsLightCruiserClass)
                    shipTypeGunTypeFirepower = Math.Sqrt(clSingleGunCount) + Math.Sqrt(clLTwinGunCount) * 2;

                if (ship.Spec.ShipClass == Zara)
                    shipTypeGunTypeFirepower = Math.Sqrt(italyCAGunCount);

                shellingPower = ship.Firepower + levelBonus + ship.Fleet.CombinedFirepowerBonus + 5 + shipTypeGunTypeFirepower;
            }

            var supportPower = 0;
            if (ship.Spec.IsAircraftCarrier)
            {
                if (hasCarrierBasedBomber)
                    supportPower = (int)((ship.Firepower + torpedo + (int)(bomber * 1.3) - 1) * 1.5) + 55;
            }
            else
            {
                supportPower = ship.Firepower + 4;
            }


            var shipTypeGunTypeAccuracy = 0.0;
            if (ship.Spec.IsLightCruiserClass)
            {
                var married = ship.Level > 99;
                shipTypeGunTypeAccuracy = married ? _140mmGunCount : 0.0;

                if (ship.Spec.ShipClass == Agano)
                    shipTypeGunTypeAccuracy += (married ? 6 : 5) * Math.Sqrt(_152mmTwinGunCount);
                else
                    shipTypeGunTypeAccuracy += married ? _152mmTwinGunCount : 0;

                shipTypeGunTypeAccuracy += (married ? -1.8 : -3) * _152mmTripleGunCount;

                if (ship.Spec.ShipClass == Atlanta)
                    shipTypeGunTypeAccuracy += married ? 1.5 * _127mmConcentratedGunCount : 0;
                else
                    shipTypeGunTypeAccuracy += (married ? -1.8 : -3) * _127mmConcentratedGunCount;

                if (ship.Spec.ShipClass == Ooyodo)
                    shipTypeGunTypeAccuracy += (married ? -0.6 : -1) * _155mmTripleGunCount;
                else
                    shipTypeGunTypeAccuracy += (married ? -0.5 : -2.5) * _155mmTripleGunCount;

                if (ship.Spec.ShipClass == Agano)
                    shipTypeGunTypeAccuracy += (married ? -3 : -4) * _203mmTwinGunCount;
                else if (ship.Spec.ShipClass == Ooyodo)
                    shipTypeGunTypeAccuracy += (married ? -3 : -5) * _203mmTwinGunCount;
                else
                    shipTypeGunTypeAccuracy += (married ? -1.8 : -3) * _203mmTwinGunCount;

                shipTypeGunTypeAccuracy += (married ? -8.8 : -11) * _203mmTripleGunCount;
            }
            if (ship.Spec.ShipClass == Zara)
                shipTypeGunTypeAccuracy = italyCAGunCount;
            if (ship.Spec.ShipType == DE)
                shipTypeGunTypeAccuracy = -13;

            var baseAccuracy     = Math.Sqrt(ship.Level) * 2 + Math.Sqrt(ship.Lucky) * 1.5 + accuracy;
            var alvAccuracy      = hasCarrierBasedBomber ? ItemStatus.AlvTypeAccuracyBonus[alv] : 0;
            var shellingAccuracy = ship.Fleet.BaseAccuracy + baseAccuracy + accuracyLevelBonus + shipTypeGunTypeAccuracy + alvAccuracy;
            var supportAccuracy  = 64 + baseAccuracy;


            var apShellModifierRank = 0;
            if (hasPrimaryGun && hasAPShell)
            {
                if (hasSecondaryGun && hasRadar)
                    apShellModifierRank = 4;
                else if (hasRadar)
                    apShellModifierRank = 3;
                else if (hasSecondaryGun)
                    apShellModifierRank = 2;
                else
                    apShellModifierRank = 1;
            }
            var apShellPower = shellingPower * apShellModifierRank switch
            {
                1 => 1.08,
                2 => 1.15,
                3 => 1.1,
                4 => 1.15,
                _ => 0,
            };
            var apShellAccuracy = shellingAccuracy * apShellModifierRank switch
            {
                1 => 1.1,
                2 => 1.2,
                3 => 1.25,
                4 => 1.3,
                _ => 0,
            };

            return new()
            {
                ShellingPower    = shellingPower,
                ShellingAccuracy = shellingAccuracy,
                SupportPower     = supportPower,
                SupportAccuracy  = supportAccuracy,
                APShellPower     = apShellPower,
                APShellAccuracy  = apShellAccuracy
            };
        }

        public string ShellingString() => ShellingPower > 0 ? $"砲{ToS(ShellingPower)} 命{ToS(ShellingAccuracy)}" : "";
        public string SupportString() => SupportPower > 0 ? $"砲撃支援:{SupportPower} 命中:{ToS(SupportAccuracy)}" : "";
        public string APShellString() => APShellPower > 0 ? $"徹甲弾:{ToS(APShellPower)} 命中:{ToS(APShellAccuracy)}" : "";
    }
}
