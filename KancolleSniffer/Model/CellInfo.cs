// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;

namespace KancolleSniffer.Model
{
    public class CellInfo : Sniffer.IPort
    {
        private int _battleCount;

        public string Next { get; private set; }

        public string Current { get; private set; }

        private string _info;
        private int _mapareaId;
        private int _mapinfoNo;

        public int MapId => _mapareaId * 10 + _mapinfoNo;

        public void Port()
        {
            Current = Next = CurrentText;
            _info = "";
            _mapareaId = _mapinfoNo = 0;
        }

        public void StartBattle()
        {
            Current = Next = CurrentText;
        }

        public void StartPractice()
        {
            Current = Next = CurrentText;
        }

        public void InspectMapStart(dynamic json)
        {
            _battleCount = 0;
            InspectMapNext(json);
        }

        public void InspectMapNext(dynamic json)
        {
            Current = CurrentText;
            SetInfo(json);
            Next = NextText;
        }

        // 最近のイベント海域の番号から逆算して、21以降をイベント海域と仮定し、数字の代わりに"E"と表示する
        private string MapNumber => _mapareaId > 20 ? $"E{_mapinfoNo}" : $"{_mapareaId}-{_mapinfoNo}";

        private string CurrentText => string.IsNullOrWhiteSpace(_info) ? "" : $"{MapNumber} {_info}";

        private string NextText => string.IsNullOrWhiteSpace(_info) ? "" : $"{MapNumber} 次{_info}";

        private void SetInfo(dynamic json)
        {
            _mapareaId = (int)json.api_maparea_id;
            _mapinfoNo = (int)json.api_mapinfo_no;

            switch ((int)json.api_color_no)
            {
                case 2:
                    _info = "資源";
                    break;
                case 3:
                    _info = "渦潮";
                    break;
                case 4:
                    switch ((int)json.api_event_id)
                    {
                        case 4:
                            _battleCount++;
                            _info = $"{BattleCount}戦目";
                            break;
                        case 6:
                            switch ((int)json.api_event_kind)
                            {
                                case 2:
                                    _info = "能動分岐";
                                    break;
                                default:
                                    _info = "気のせい";
                                    break;
                            }
                            break;
                    }
                    break;
                case 5:
                    _info = "ボス戦";
                    break;
                case 6:
                    _info = "揚陸地点";
                    break;
                case 7:
                    _battleCount++;
                    _info = $"{BattleCount}戦目(航空)";
                    break;
                case 8:
                    _info = "護衛成功";
                    break;
                case 9:
                    _info = "航空偵察";
                    break;
                case 10:
                    _battleCount++;
                    _info = $"{BattleCount}戦目(空襲)";
                    break;
                case 11:
                    _battleCount++;
                    _info = $"{BattleCount}戦目(夜戦)";
                    break;
                case 14:
                    _info = "泊地修理";
                    break;
                case 15:
                    _battleCount++;
                    _info = $"{BattleCount}戦目(潜+空)";
                    break;
            }
        }

        private static readonly char[] FullNumber = "０１２３４５６７８９".ToCharArray();

        private string BattleCount => new(_battleCount.ToString().Select(ch => FullNumber[ch - '0']).ToArray());
    }
}
