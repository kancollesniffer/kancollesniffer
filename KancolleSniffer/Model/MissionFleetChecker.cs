// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KancolleSniffer.Util;
using static KancolleSniffer.Model.ShipTypeCode;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class MissionFleetChecker
    {
        public static readonly Dictionary<int, MissionFleetCondition> _conditions = new()
        {
            // ミ船団護衛(一号船団)
            { 42,  new(4, 45, 200,   0,   0,   0,   0, 0, 0,
                (checker) => checker.Types.Contains(CL) && checker.Types.Count(type => type == DD) > 1 || checker.CommonAlternate,
                "軽巡1 駆逐2\nまたは、共通代替編成") },
            // ミ船団護衛(二号船団)
            { 43,  new(6, 55, 300, 500, 280, 280, 170, 0, 0,
                (checker) => checker.MiSendan2Cond1 || checker.MiSendan2Cond2 || checker.FlagshipType == CVL && checker.CommonAlternate,
                "旗艦護衛空母 駆逐2or海防2\nまたは、旗艦軽空母 軽巡1 駆逐4\nまたは、旗艦軽空母 共通代替編成") },
            // 航空装備輸送任務
            { 44,  new(6, 35, 210,   0, 200, 200, 150, 3, 6,
                (checker) => checker.Specs.Count(spec => spec.ShipType == AV || spec.IsAircraftCarrier) > 1
                            && checker.Types.Contains(AV)
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type is DD or DE) > 1,
                "水母or空母系1 水母1 軽巡1 駆逐+海防2") },
            // 南西海域戦闘哨戒
            { 46,  new(5, 60, 300, 350, 250, 220, 190, 0, 0,
                (checker) => checker.Types.Count(type => type == CA) > 1
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type == DD) > 1,
                "重巡2 軽巡1 駆逐2") },
            // 南西方面連絡線哨戒
            { 103, new(5, 40, 200, 300, 200, 200, 120, 0, 0,
                (checker) => checker.Types.Contains(CL) && checker.Types.Count(type => type == DD) > 1 || checker.CommonAlternate,
                "軽巡1 駆逐2\nまたは、共通代替編成") },
            // 小笠原沖哨戒線
            { 104, new(5, 45, 230, 280, 220, 240, 150, 0, 0,
                (checker) => checker.Types.Contains(CL) && checker.Types.Count(type => type == DD) > 2 || checker.CommonAlternate,
                "軽巡1 駆逐3\nまたは、共通代替編成") },
            // 小笠原沖戦闘哨戒
            { 105, new(6, 55, 290, 330, 300, 270, 180, 0, 0,
                (checker) => checker.Types.Contains(CL) && checker.Types.Count(type => type == DD) > 2 || checker.CommonAlternate,
                "軽巡1 駆逐3\nまたは、共通代替編成") },
            // 敵泊地強襲反撃作戦
            { 111, new(6, 45, 220, 360, 160, 160, 140, 0, 0,
                (checker) => checker.Types.Contains(CA)
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type == DD) > 2,
                "重巡1 軽巡1 駆逐3") },
            // 南西諸島離島哨戒作戦
            { 112, new(6, 50, 250, 400, 220, 220, 190, 0, 0,
                (checker) => checker.Types.Contains(AV)
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type == DD) > 3,
                "水母1 軽巡1 駆逐4") },
            // 南西諸島離島防衛作戦
            { 113, new(6, 55, 300, 500, 280, 280, 170, 0, 0,
                (checker) => checker.Types.Count(type => type == CA) > 1
                            && checker.Specs.Count(spec => spec.IsSubmarine) > 0
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type == DD) > 1,
                "重巡2 軽巡1 駆逐2 潜水1") },
            // 南西諸島捜索撃滅戦
            { 114, new(6, 60, 330, 510, 400, 285, 385, 0, 0,
                (checker) => checker.Types.Contains(AV)
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type == DD) > 1,
                "水母1 軽巡1 駆逐2") },
            // 精鋭水雷戦隊夜襲
            { 115, new(6, 75, 400, 410, 390, 410, 340, 0, 0,
                (checker) => checker.FlagshipType == CL && checker.Types.Count(type => type == DD) > 4,
                "旗艦軽巡 駆逐5") },
            // 西方潜水艦作戦
            { 132, new(5, 55, 270,  60,  80 , 50,   0, 0, 0,
                (checker) => checker.FlagshipType == AS && checker.Specs.Count(spec => spec.IsSubmarine) > 2,
                "旗艦潜母 潜水3") },
            // 欧州方面友軍との接触
            { 133, new(5, 65, 350, 115,  90,  70,  95, 0, 0,
                (checker) => checker.FlagshipType == AS && checker.Specs.Count(spec => spec.IsSubmarine) > 2,
                "旗艦潜母 潜水3") },
            // ラバウル方面艦隊進出
            { 141, new(6, 55, 290, 450, 350, 330, 250, 0, 0,
                (checker) => checker.FlagshipType == CA
                            && checker.Types.Contains(CL)
                            && checker.Types.Count(type => type == DD) > 2,
                "旗艦重巡 軽巡1 駆逐3") },
            // 強行鼠輸送作戦
            { 142, new(5, 70, 320, 280, 240, 200, 160, 3, 4,
                (checker) => checker.Types.Count(type => type == DD) > 4,
                "駆逐5") }
        };

        public ShipSpec[] Specs { get; }
        public int[] Types { get; }
        public ShipSpec Flagship { get; }
        public int FlagshipType { get; }
        public TotalParams TotalParams { get; }

        public MissionFleetChecker(Fleet fleet)
        {
            var ships    = fleet.Ships.Take(6).ToArray();
            Specs        = ships.Select(ship => ship.Spec).ToArray();
            Types        = Specs.Select(spec => spec.ShipType).ToArray();
            Flagship     = Specs.FirstOrDefault() ?? new ShipSpec();
            FlagshipType = Types.FirstOrDefault();
            TotalParams  = TotalParams.Calc(ships);
        }

        public bool Check(int id) => _conditions.TryGetValue(id, out var condition) && condition.Check(this);

        public bool MiSendan2Cond1 => Flagship.IsEscortCarrier
            && (Types.Count(type => type == DD) > 1
            || Types.Count(type => type == DE) > 1);

        public bool MiSendan2Cond2 => FlagshipType == CVL
            && Types.Contains(CL)
            && Types.Count(type => type == DD) > 3;

        public bool CommonAlternate
            => Types.Contains(DD) && Types.Count(type => type == DE) > 2
            || Types.Any(type => type is CL or CT) && Types.Count(type => type == DE) > 1
            || Specs.Any(spec => spec.IsEscortCarrier) && Types.Count(type => type == DE) > 1 || Types.Count(type => type == DD) > 1;

        public static bool Defined(int id) => _conditions.ContainsKey(id);
        public static string ToolTip(MissionSpec spec) => _conditions.TryGetValue(spec.Id, out var condition) ? condition.ToolTip(spec) : "";
    }

    public class MissionFleetCondition
    {
        public int Ships { get; }
        public int Drums { get; }
        public int DrumShips { get; }
        public int FlagshipLevel { get; }
        public int Level { get; }
        public int FirePower { get; }
        public int AntiAir { get; }
        public int Asw { get; }
        public int LoS { get; }
        public string Description { get; }
        public Func<MissionFleetChecker, bool> CheckFleet { get; }

        public MissionFleetCondition(int ships, int flagshipLevel, int level, int firepower, int antiair, int asw, int los, int drumships, int drums,
            Func<MissionFleetChecker, bool> checkfleet, string description)
        {
            Ships = ships;
            FlagshipLevel = flagshipLevel;
            Level = level;
            FirePower = firepower;
            AntiAir = antiair;
            Asw = asw;
            LoS = los;
            DrumShips = drumships;
            Drums = drums;
            CheckFleet = checkfleet;
            Description = description;
        }

        public bool Check(MissionFleetChecker checker)
        {
            return CheckFleet(checker)
                && Ships         <= checker.TotalParams.Ships
                && FlagshipLevel <= checker.TotalParams.FlagshipLevel
                && Level         <= checker.TotalParams.Level
                && FirePower     <= checker.TotalParams.FirePower / 10
                && AntiAir       <= checker.TotalParams.AntiAir / 10
                && Asw           <= checker.TotalParams.Asw / 10
                && LoS           <= checker.TotalParams.LoS / 10
                && DrumShips     <= checker.TotalParams.DrumShips
                && Drums         <= checker.TotalParams.Drum;
        }

        public string ToolTip(MissionSpec spec)
        {
            var baseparams = new List<string>();
            if (FirePower > 0)
                baseparams.Add($"火力{FirePower}");
            if (AntiAir > 0)
                baseparams.Add($"対空{AntiAir}");
            if (Asw > 0)
                baseparams.Add($"対潜{Asw}");
            if (LoS > 0)
                baseparams.Add($"索敵{LoS}");

            var shipsline = new List<string> { $"{Ships}隻", Description };
            if (Drums > 0)
                (baseparams.Count() < 4 ? baseparams : shipsline).Add($"ドラム缶{DrumShips}隻{Drums}個");

            var tooltip = new StringBuilder($"旗艦Lv{FlagshipLevel} 合計Lv{Level} {spec.TimeString} {spec.DispNo} {spec.Maparea}");
            tooltip.Cat("\n", baseparams.Join(" "));
            tooltip.Cat("\n", shipsline.Join(" "));
            return tooltip.ToString();
        }
    }
}
