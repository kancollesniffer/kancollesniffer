// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Linq;
using static KancolleSniffer.Model.ShipClassCode;
using static KancolleSniffer.Model.ShipNationCode;
using static KancolleSniffer.Model.ShipTypeCode;

namespace KancolleSniffer.Model
{
    public class QuestFleetChecker
    {
        public ShipSpec[] Specs { get; }
        public int[] Ids { get; }
        public int[] HullNumbers { get; }
        public int[] Types { get; }
        public ShipSpec Flagship { get; }
        public int FlagshipType { get; }
        public int Deck { get; set; }

        public QuestFleetChecker(BattleInfo battleInfo) : this(battleInfo.Result?.Friend.Main ?? [], battleInfo.Deck)
        {
        }

        public QuestFleetChecker(ShipStatus[] ships, int deck)
        {
            Specs = ships.Where(s => !s.Dropout).Select(ship => ship.Spec).ToArray();
            Ids = Specs.Select(spec => spec.Id).ToArray();
            HullNumbers = Specs.Select(spec => spec.HullNumber).ToArray();
            Types = Specs.Select(spec => spec.ShipType).ToArray();
            Flagship = Specs.FirstOrDefault() ?? new ShipSpec();
            FlagshipType = Types.FirstOrDefault();
            Deck = deck;
        }

        public bool Check(int id)
        {
            switch (id)
            {
                case 249: // 「第五戦隊」出撃せよ！
                    return HullNumbers.Contains(805)  // 妙高
                        && HullNumbers.Contains(806)  // 那智
                        && HullNumbers.Contains(808); // 羽黒
                case 257: // 「水雷戦隊」南西へ！
                    return FlagshipType == CL
                        && Types.Count(type => type == CL) < 4
                        && Types.Contains(DD)
                        && Types.All(type => type is DD or CL);
                case 259: // 「水上打撃部隊」南方へ！
                    return Types.Contains(CL)
                        && Specs.Count(spec => spec.ShipClass is Fusou or Ise or Nagato or Yamato) == 3; // 過不足なく3隻
                case 264: // 「空母機動部隊」西へ！
                    return Types.Count(type => type == DD) > 1
                        && Specs.Count(spec => spec.IsAircraftCarrier) > 1;
                case 266: // 「水上反撃部隊」突入せよ！
                    return FlagshipType == DD
                        && Types.Count(type => type == CL) == 1
                        && Types.Count(type => type == CA) == 1
                        && Types.All(type => type is DD or CL or CA);
                case 280: // 兵站線確保！海上警備を強化実施せよ！
                case 284: // 南西諸島方面「海上警備行動」発令！
                    return Types.Count(type => type is DE or DD) > 2
                        && Types.Any(type => type is CL or CLT or CT or CVL);
                case 840: // 【節分任務:豆】節分作戦二〇二五
                    return HullNumbers.Length > 1
                        && HullNumbers.Take(2).All(number => number
                        is 401  // 鳳翔
                        or 720  // 朝日
                        or 721  // 明石
                        or 701  // 迅鯨
                        or 702  // 長鯨
                        or 1417 // 朧
                        or 1418 // 曙
                        or 1419 // 漣
                        or 1420 // 潮
                        );
                case 841: // 【節分任務:鬼】南西方面節分作戦二〇二五
                    return HullNumbers.Length > 1
                        && HullNumbers.Take(2).All(number => number
                        is 3441 // Gloire
                        or 3213 // Ranger
                        or 3255 // Johnston
                        or 504  // 神鷹
                        or 1105 // 大淀
                        or 1017 // 夕張
                        or 1636 // 早霜
                        or 1635 // 朝霜
                        or 1622 // 風雲
                        );
                case 843: // 【節分任務:柊】節分拡張作戦二〇二五 五穀豊穣！
                    return Types.Count(type => type == CA) > 1
                        && Types.Contains(CVL);
                case 854: // 戦果拡張任務！「Z作戦」前段作戦
                    return Deck == 1;
                case 861: // 強行輸送艦隊、抜錨！
                    return Types.Count(type => type is BBV or AO) > 1;
                case 862: // 前線の航空偵察を実施せよ！
                    return Types.Count(type => type == CL) > 1
                        && Types.Contains(AV);
                case 872: // 戦果拡張任務！「Z作戦」後段作戦
                    return Deck == 1;
                case 873: // 北方海域警備を実施せよ！
                    return Types.Contains(CL);
                case 875: // 精鋭「三一駆」、鉄底海域に突入せよ！
                    return Ids.Contains(543) // 長波改二
                        && Specs.Any(spec => spec.UpgradeLevel > 1 && spec.HullNumber
                            is 1625   // 高波改
                            or 1633   // 沖波改
                            or 1635); // 朝霜改
                case 888: // 新編成「三川艦隊」、鉄底海峡に突入せよ！
                    return HullNumbers.Count(number => number
                        is 812  // 鳥海
                        or 803  // 青葉
                        or 804  // 衣笠
                        or 802  // 加古
                        or 801  // 古鷹
                        or 1001 // 天龍
                        or 1017 // 夕張
                        ) > 3;
                case 894: // 空母戦力の投入による兵站線戦闘哨戒
                    return Specs.Any(spec => spec.IsAircraftCarrier);
                case 903: // 拡張「六水戦」、最前線へ！
                    return Flagship.HullNumber == 1017 && Flagship.UpgradeLevel > 5 // 夕張改二
                        && (HullNumbers.Count(number => number
                            is 1325 // 睦月
                            or 1326 // 如月
                            or 1327 // 弥生
                            or 1328 // 卯月
                            or 1333 // 菊月
                            or 1335 // 望月
                            ) > 1
                        ||  Ids.Contains(488)); // 由良改二
                case 904: // 精鋭「十九駆」、躍り出る！
                    return Ids.Contains(195)  // 綾波改二
                        && Ids.Contains(627); // 敷波改二
                case 905: // 「海防艦」、海を護る！
                    return Types.Count(type => type == DE) > 2
                        && Types.Length < 6;
                case 912: // 工作艦「明石」護衛任務
                    return Flagship.HullNumber == 721 // 明石
                        && Types.Count(type => type == DD) > 2;
                case 914: // 重巡戦隊、西へ！
                    return Types.Count(type => type == CA) > 2
                        && Types.Contains(DD);
                case 928: // 歴戦「第十方面艦隊」、全力出撃！
                    return HullNumbers.Count(number => number
                        is 808  // 羽黒
                        or 807  // 足柄
                        or 805  // 妙高
                        or 809  // 高雄
                        or 1316 // 神風
                        ) > 1;
                case 944: // 鎮守府近海海域の哨戒を実施せよ！
                    return FlagshipType is DD or CA
                        && Types.Skip(1).Count(type => type is DE or DD) > 2;
                case 945: // 南西方面の兵站航路の安全を図れ！
                    return FlagshipType is DD or CL or CT
                        && Types.Skip(1).Count(type => type is DE or DD) > 2;
                case 946: // 空母機動部隊、出撃！敵艦隊を迎撃せよ！
                    return Flagship.IsAircraftCarrier
                        && Types.Count(type => type is CA or CAV) > 1;
                case 947: // AL作戦
                    return Types.Count(type => type == CVL) > 1;
                case 948: // 機動部隊決戦
                    return Deck == 1
                        && Flagship.IsAircraftCarrier;
                case 953: // 【梅雨限定任務】雨の南西諸島防衛戦！
                    return FlagshipType is CL or CLT or CT or CA or CAV
                        && Types.Any(type => type == AV)
                        && Types.Any(type => type == DD)
                        && Types.Any(type => type == DE);
                case 954: // 【梅雨拡張任務】梅雨の海上護衛強化！
                    return FlagshipType == DD
                        && Types.Count(type => type == DE) > 1;
                case 955: // 【梅雨限定月間任務】西方海域統合作戦
                    return Specs.Count(spec => spec.IsAircraftCarrier) > 1
                        && Specs.Any(spec => spec.ShipClass is Kuma or Ooyodo);
                case 973: // 日英米合同水上艦隊、抜錨せよ！
                    return Specs.Count(spec => spec.Nation is US or UK) > 2
                        && Specs.All(spec => !spec.IsAircraftCarrier);
                case 975: // 精鋭「第十九駆逐隊」、全力出撃！
                    return Ids.Contains(666)  // 磯波改二
                        && Ids.Contains(647)  // 浦波改二
                        && Ids.Contains(195)  // 綾波改二
                        && Ids.Contains(627); // 敷波改二
                case 987: // 【限定週間任務】秋の南瓜祭り、おかわりっ！
                    return HullNumbers.Count(number => number is 701 or 2229 or 2227) > 2   // 迅鯨 伊201 伊47
                        || Specs.Count(spec => spec.ShipClass == TeiDE) > 2                 // 丁型海防艦
                        || HullNumbers.Count(number => number is 1013 or 3240 or 3255) > 2; // 阿武隈 Brooklyn Johnston
                case 988: // 【Xmas限定任務】聖夜の哨戒線
                    return Flagship.IsLightCruiserClass;
                case 1005: // 精強「第七駆逐隊」緊急出動！
                    return Specs.Count(spec => spec.UpgradeLevel > 1 && spec.HullNumber
                        is 1417 // 朧改
                        or 1418 // 曙改
                        or 1419 // 漣改
                        or 1420 // 潮改
                        ) > 3;
                case 1010: // 【期間限定任務】対潜掃討作戦
                    return Types.Count(type => type is DE or DD) > 2;
                case 1011: // 【期間限定任務】精強海防艦、緊急近海防衛！
                    return FlagshipType == DE
                        && Types.Count(type => type == DE) > 2;
                case 1012: // 鵜来型海防艦、静かな海を防衛せよ！
                    return Flagship.ShipClass == Ukuru
                        && Types.Skip(1).Count(type => type == DE) is > 0 and < 4
                        && Types.All(type => type == DE);
                case 1018: // 「第三戦隊」第二小隊、鉄底海峡へ！
                    return HullNumbers.Contains(102) // 比叡
                        && HullNumbers.Contains(104) // 霧島
                        && Types.Count(type => type == DD) > 1;
                case 1022: // 【期間限定任務】「三十二駆」月次戦闘哨戒！
                    return HullNumbers.Count(number => number
                        is 1628 // 玉波
                        or 1629 // 涼波
                        or 1630 // 藤波
                        or 1631 // 早波
                        or 1632 // 浜波
                        ) > 2;

                case 318: // 給糧艦「伊良湖」の支援
                    return Types.Count(type => type == CL) > 1;
                case 329: // 【節分任務:枡】節分演習！二〇二五
                    return FlagshipType is AS or CVL
                        && (Specs.Count(spec => spec.IsSubmarine) > 1
                        || Types.Count(type => type == DE) > 1
                        || Types.Count(type => type == DD) > 3);
                case 330: // 空母機動部隊、演習始め！
                    return Flagship.IsAircraftCarrier
                        && Specs.Count(spec => spec.IsAircraftCarrier) > 1
                        && Types.Count(type => type == DD) > 1;
                case 337: // 「十八駆」演習！
                    return HullNumbers.Contains(1601)  // 陽炎
                        && HullNumbers.Contains(1602)  // 不知火
                        && HullNumbers.Contains(1525)  // 霰
                        && HullNumbers.Contains(1526); // 霞
                case 339: // 「十九駆」演習！
                    return HullNumbers.Contains(1409)  // 磯波
                        && HullNumbers.Contains(1410)  // 浦波
                        && HullNumbers.Contains(1411)  // 綾波
                        && HullNumbers.Contains(1412); // 敷波
                case 342: // 小艦艇群演習強化任務
                    return Types.Count(type => type is DE or DD) > 3
                        || Types.Count(type => type is DE or DD) > 2 && Specs.Any(spec => spec.IsLightCruiserClass);
                case 345: // 演習ティータイム！
                    return Specs.Count(spec => spec.ShipClass == JDD
                        || spec.HullNumber
                        is 101  // 金剛
                        or 3301 // Warspite
                        or 3305 // Nelson
                        or 3311 // Ark Royal
                        ) > 3;
                case 346: // 最精鋭！主力オブ主力、演習開始！
                    return Ids.Contains(542)  // 夕雲改二
                        && Ids.Contains(563)  // 巻雲改二
                        && Ids.Contains(564)  // 風雲改二
                        && Ids.Contains(648); // 秋雲改二
                case 348: // 「精鋭軽巡」演習！
                    return FlagshipType is CL or CT
                        && Specs.Skip(1).Count(spec => spec.IsLightCruiserClass) > 1
                        && Types.Count(type => type == DD) > 1;
                case 350: // 精鋭「第七駆逐隊」演習開始！
                    return HullNumbers.Contains(1417)  // 朧
                        && HullNumbers.Contains(1418)  // 曙
                        && HullNumbers.Contains(1419)  // 漣
                        && HullNumbers.Contains(1420); // 潮
                case 353: // 「巡洋艦戦隊」演習！
                    return FlagshipType is CA or CAV
                        && Types.Count(type => type is CA or CAV) > 3
                        && Types.Count(type => type == DD) > 1;
                case 354: // 「改装特務空母」任務部隊演習！
                    return Flagship.Id == 707 // Gambier Bay Mk.II
                        && Specs.Count(spec => spec.ShipClass is Fletcher or JohnCButler) > 1;
                case 355: // 精鋭「第十五駆逐隊」第一小隊演習！
                    return Ids.Take(2).Contains(568)  // 黒潮改二と
                        && Ids.Take(2).Contains(670); // 親潮改二が1番と2番にいる。順不同
                case 356: // 精鋭「第十九駆逐隊」演習！
                    return Ids.Contains(666)  // 磯波改二
                        && Ids.Contains(647)  // 浦波改二
                        && Ids.Contains(195)  // 綾波改二
                        && Ids.Contains(627); // 敷波改二
                case 357: // 「大和型戦艦」第一戦隊演習、始め！
                    return HullNumbers.Contains(111) // 大和
                        && HullNumbers.Contains(112) // 武蔵
                        && Types.Contains(CL)
                        && Types.Count(type => type == DD) > 1;
                case 362: // 特型初代「第十一駆逐隊」演習スペシャル！
                    return HullNumbers.Contains(1401)  // 吹雪
                        && HullNumbers.Contains(1402)  // 白雪
                        && HullNumbers.Contains(1403)  // 初雪
                        && HullNumbers.Contains(1404); // 深雪
                case 363: // 【艦隊11周年記念任務】記念艦隊演習！
                    return HullNumbers.Count(number => number
                        is 111  // 大和
                        or 205  // 翔鶴
                        or 1401 // 吹雪
                        or 1417 // 朧
                        or 3236 // Tuscaloosa
                        or 3231 // Houston
                        or 3230 // Northampton
                        or 2505 // 山汐丸
                        or 2506 // 熊野丸
                        ) > 2;
                case 367: // 【梅雨限定任務】海上護衛隊、雨中演習！
                    return Types.Count(type => type == DE) > 1
                        || Types.Count(type => type == DD) > 3;
                case 368: // 「十六駆」演習！
                    return HullNumbers.Count(number => number
                        is 1607 // 初風
                        or 1608 // 雪風
                        or 1609 // 天津風
                        or 1610 // 時津風
                        ) > 1;
                case 370: // 【Xmas限定】聖夜の海上護衛隊演習
                    return Specs.Count(spec => spec.HullNumber
                        // 鹿島    択捉    松輪    佐渡    福江    鵜来    岸波    早波    谷風    江風    山雲    朝雲  山汐丸
                        is 1202 or 2005 or 2006 or 2007 or 2014 or 2118 or 1634 or 1631 or 1614 or 1515 or 1522 or 1521 or 2505
                        || spec.ShipClass == TeiDE) > 3;
                case 371: // 春です！「春雨」、演習しますっ！
                    return Flagship.HullNumber == 1511
                        && HullNumbers.Count(number => number
                        is 1509 // 村雨
                        or 1510 // 夕立
                        or 1512 // 五月雨
                        or 1507 // 白露
                        or 1508 // 時雨
                        ) > 2;
                case 372: // 水上艦「艦隊防空演習」を実施せよ！
                    return Flagship.ShipClass == Akizuki
                        && Types.Skip(1).Count(type => type == DD) > 1
                        && Types.Count(type => type == BBV) > 1;
                case 373: // 「フランス艦隊」演習！
                    return Flagship.Nation == FR
                        && Specs.Count(spec => spec.Nation == FR) > 2;
                case 374: // 【期間限定任務】「三十二駆」特別演習！
                    return HullNumbers.Count(number => number
                        is 1628 // 玉波
                        or 1629 // 涼波
                        or 1630 // 藤波
                        or 1631 // 早波
                        or 1632 // 浜波
                        ) > 2;
                case 375: // 「第三戦隊」第二小隊、演習開始！
                    return HullNumbers.Contains(102) // 比叡
                        && HullNumbers.Contains(104) // 霧島
                        && Types.Contains(CL)
                        && Types.Count(type => type == DD) > 1;
                case 377: // 「第二駆逐隊(後期編成)」、練度向上！
                    return Flagship.HullNumber
                        is 1636 // 早霜
                        or 1637 // 秋霜
                        or 1638 // 清霜
                        && HullNumbers.Skip(1).Count(number => number
                            is 1636 // 早霜
                            or 1637 // 秋霜
                            or 1638 // 清霜
                            or 1635 // 朝霜
                            ) > 1;
                case 378: // 新春限定！第六艦隊特別演習
                    return FlagshipType == AS
                        && Specs.Any(spec => spec.IsSubmarine)
                        && Specs.Count(spec => spec.ShipType == AS || spec.IsSubmarine) > 2;
                case 379: // 【期間限定任務】「精鋭十一駆」特別演習！
                    return Specs.Count(spec => spec.Id
                        is 426 // 吹雪改二
                        or 986 // 白雪改二
                        or 959 // 深雪改二
                        || spec.HullNumber == 1403 && spec.UpgradeLevel > 1 // 初雪改
                        ) > 1;
                default:
                    return true;
            }
        }
    }
}
