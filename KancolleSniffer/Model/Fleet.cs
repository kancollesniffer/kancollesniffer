// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class ChargeStatus
    {
        public bool Empty => FuelRemains > 1.0;

        public double FuelRemains { get; set; }
        public double FuelPenalty { get; set; }
        public int Fuel { get; set; }

        public double BullRemains { get; set; }
        public double BullPenalty { get; set; }
        public int Bull { get; set; }

        public ChargeStatus()
        {
            FuelRemains = 2.0;
            BullRemains = 2.0;
        }

        public ChargeStatus(ShipStatus status)
        {
            if (status.Spec.FuelMax == 0)
            {
                FuelRemains = 2.0;
                BullRemains = 2.0;
                return;
            }
            FuelRemains = (double)status.Fuel / status.Spec.FuelMax;
            BullRemains = (double)status.Bull / status.Spec.BullMax;
        }

        public void SetState()
        {
            Fuel = CalcFuelChargeState(FuelRemains);
            Bull = CalcBullChargeState(BullRemains);
            FuelPenalty = CalcFuelPenalty(FuelRemains);
            BullPenalty = CalcBullPenalty(BullRemains);
        }

        public void SetOthersState()
        {
            SetState();
            Fuel += 5;
            Bull += 5;
        }

        private static int CalcFuelChargeState(double remains)
        {
            if (remains > 1.0)
                return 0;
            if (remains == 1.0)
                return 0;
            if (remains >= 0.75)
                return 1;
            if (remains >= 0.35)
                return 2;
            if (remains > 0)
                return 3;
            return 4;
        }

        private static int CalcBullChargeState(double remains)
        {
            if (remains > 1.0)
                return 0;
            if (remains == 1.0)
                return 0;
            if (remains >= 0.5)
                return 1;
            if (remains >= 0.25)
                return 2;
            if (remains > 0)
                return 3;
            return 4;
        }

        private static double CalcFuelPenalty(double remains)
        {
            return remains < 0.75 ? 75 - (remains * 100) : 0.0;
        }

        private static double CalcBullPenalty(double remains)
        {
            return remains < 0.5 ? 100 - (remains * 200) : 0.0;
        }

        public static ChargeStatus Min(ChargeStatus a, ChargeStatus b)
        {
            return new ChargeStatus
            {
                FuelRemains = Math.Min(a.FuelRemains, b.FuelRemains),
                BullRemains = Math.Min(a.BullRemains, b.BullRemains)
            };
        }
    }

    public class TotalParams
    {
        public int Ships;
        public int Drum;
        public int DrumShips;
        public int FlagshipLevel;
        public int Level;
        public int FirePower;
        public int AntiAir;
        public int Asw;
        public int LoS;
        public int Fuel;
        public int Bull;
        public bool HasNoneAircraft;

        public static TotalParams Calc(Fleet fleet) => Calc(fleet.Ships);

        public static TotalParams Calc(ShipStatus[] ships) => ships.Aggregate(new TotalParams(), (total, ship) => total.Add(ship));

        public TotalParams Add(ShipStatus ship)
        {
            Ships++;
            var drum = ship.Items.Count(item => item.Spec.IsDrum);
            if (drum > 0)
                DrumShips++;
            Drum            += drum;
            if (FlagshipLevel == 0)
                FlagshipLevel = ship.Level;
            Level           += ship.Level;
            FirePower       += ship.MissionFirepower;
            AntiAir         += ship.MissionAntiAir;
            Asw             += ship.MissionAsw;
            LoS             += ship.MissionLoS;
            Fuel            += ship.EffectiveFuelMax;
            Bull            += ship.EffectiveBullMax;
            HasNoneAircraft |= ship.HasNoneAircraft;
            return this;
        }

        // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理してるのを元に戻す
        public static int CutOverFlow(int value) => Math.Min(value / 10, 999);

        public int ViewFirePower => CutOverFlow(FirePower);

        public int ViewAntiAir => CutOverFlow(AntiAir);

        public int ViewAsw => CutOverFlow(Asw);

        public int ViewLoS => CutOverFlow(LoS);
    }

    public enum FleetState
    {
        Port,
        Mission,
        Sortie,
        Practice,
        Preset
    }

    public enum CombinedType
    {
        None,
        Carrier, // 機動
        Surface, // 水上
        Transport // 輸送
    }

    public class Fleet
    {
        protected readonly ShipInventory _shipInventory;
        private readonly Func<int> _getHqLevel;
        public int[] Deck { get; private set; } = [];
        public int Number { get; }
        public FleetState State { get; set; }
        public CombinedType CombinedType { get; set; }
        public bool IsCombined => CombinedType != CombinedType.None;
        public ShipStatus[] Ships { get; private set; } = [];
        public int Formation { get; set; }
        public double AntiAir { get; set; }
        public double CombinedAntiAir { get; set; }

        public Fleet(ShipInventory shipInventory, int number, Func<int> getHqLevel)
        {
            _shipInventory = shipInventory;
            Number = number;
            _getHqLevel = getHqLevel;
        }

        public IEnumerable<ShipStatus> ShipsWithoutDropout => Ships.Where(ship => !ship.Dropout);

        public Fleet SetShips(params int[] deck)
        {
            foreach (var ship in Ships)
            {
                if (State == FleetState.Preset || ship.Fleet != this) // 入れ替え操作で他の艦隊に移動しているときには触らない。
                    continue;
                ship.Fleet = null;
                ship.DeckIndex = -1;
            }

            Deck = deck;
            Ships = Deck.Where(shipId => shipId > 0 && _shipInventory.Contains(shipId)).Select((shipId, num) =>
            {
                var ship = _shipInventory[shipId];
                if (State == FleetState.Preset)
                    return ship;
                ship.DeckIndex = num;
                ship.Fleet = this;
                return ship;
            }).ToArray();

            return this;
        }

        public int SetShip(int index, int shipId)
        {
            var prev = Deck[index];
            Deck[index] = shipId;
            SetShips(Deck);
            return prev;
        }

        public void WithdrawAccompanyingShips()
        {
            for (var i = 1; i < Deck.Length; i++)
                Deck[i] = -1;
            SetShips(Deck);
        }

        public void WithdrawShip(int index)
        {
            var dst = index;
            for (var src = index + 1; src < Deck.Length; src++)
            {
                if (Deck[src] != -1)
                    Deck[dst++] = Deck[src];
            }
            for (; dst < Deck.Length; dst++)
                Deck[dst] = -1;
            SetShips(Deck);
        }

        public void CalcAntiAir()
        {
            AntiAir = ShipsWithoutDropout.Sum(ship => ship.EffectiveAntiAirForFleet);
        }

        public double AntiAirModifierFormation => Formation switch
        {
            2 => 1.2,// 複縦
            3 => 1.6,// 輪形
            6 => 1.1,// 警戒
            11 => 1.1,// 連合第一(対潜)
            13 => 1.5,// 連合第三(対空)
            _ => 1.0,
        };

        public double AntiAirModifierEnemy => Ships.FirstOrDefault()?.Spec.IsEnemy ?? false ? 1.0 : 1.3;

        public double AntiAirModifierCombined => IsCombined ? Number == 0 ? 0.8 : 0.48 : 1.0;

        public double CalcEffectiveAntiAir(double formationModifier) =>
            (int)((IsCombined ? CombinedAntiAir : AntiAir) * formationModifier) * 2.0 / AntiAirModifierEnemy;

        public double EffectiveAntiAir => CalcEffectiveAntiAir(AntiAirModifierFormation);

        public ChargeStatus ChargeStatus
        {
            get
            {
                var fs = new ChargeStatus(Ships.FirstOrDefault() ?? new ShipStatus());
                var others = Ships.Skip(1).Select(ship => new ChargeStatus(ship)).Aggregate(new ChargeStatus(), ChargeStatus.Min);
                fs.SetState();
                others.SetOthersState();
                if (others.Empty)
                    return fs;

                if (fs.Fuel == 0)
                {
                    fs.Fuel = others.Fuel;
                    fs.FuelRemains = others.FuelRemains;
                    fs.FuelPenalty = others.FuelPenalty;
                }
                if (fs.Bull == 0)
                {
                    fs.Bull = others.Bull;
                    fs.BullRemains = others.BullRemains;
                    fs.BullPenalty = others.BullPenalty;
                }
                return fs;
            }
        }

        public int Speed => Ships.Any() ? Ships.Min(s => s.Speed) : -1;

        public Range FighterPower => ShipsWithoutDropout.SelectMany(ship => ship.Items.Select(item => item.CalcFighterPower()))
            .Aggregate(new Range(0, 0), (prev, cur) => prev + cur);

        public int AirSupportFighter => ShipsWithoutDropout.SelectMany(ship => ship.Items).Sum(item => item.CalcAirSupportFighter());

        public int AswSupportFighter => ShipsWithoutDropout.SelectMany(ship => ship.Items).Sum(item => item.CalcAswSupportFighter());

        public double GetLineOfSights(int factor)
        {
            var actual = ShipsWithoutDropout.ToList();
            var result = actual.SelectMany(ship => ship.Items).Sum(item => item.EffectiveLoS * factor) + actual.Sum(ship => Math.Sqrt(ship.RawLoS));
            return result > 0 ? result - Math.Ceiling(_getHqLevel() * 0.4) + (6 - actual.Count()) * 2 : 0.0;
        }

        public int RadarShips => ShipsWithoutDropout.Count(ship => ship.Items.Any(item => item.Spec.IsRadar));

        public int SurfaceRadarShips => ShipsWithoutDropout.Count(ship => ship.Items.Any(item => item.Spec.IsSurfaceRadar));

        public int SubmarineRadarShips => ShipsWithoutDropout.Count(ship => ship.Items.Any(item => item.Spec.IsSubmarineRadar));

        public double AirReconScore => ShipsWithoutDropout.SelectMany(ship => ship.Items.Select(item => item.CalcAirReconScore())).Sum();

        private static readonly Dictionary<int, double> DaihatsuBonuses = new()
        {
            {68,  0.05}, // 大発動艇
            {193, 0.05}, // 特大発動艇
            {166, 0.02}, // 大発動艇(八九式中戦車&陸戦隊)
            {167, 0.01}, // 特二式内火艇
            {408, 0.02}, // 装甲艇(AB艇)
            {409, 0.03}, // 武装大発
            {436, 0.02}, // 大発動艇(II号戦車/北アフリカ仕様)
            {449, 0.02}, // 特大発動艇+一式砲戦車
            {525, 0.04}, // 特四式内火艇
            {526, 0.05}  // 特四式内火艇改
        };

        private static readonly double[][] TokudaiBonuses =
        [
        //   大発0  大発1  大発2  大発3  大発4
            [0.00,  0.00,  0.00,  0.00,  0.00],  // 特大発0
            [0.02,  0.02,  0.02,  0.02,  0.02],  // 特大発1
            [0.04,  0.04,  0.04,  0.04,  0.04],  // 特大発2
            [0.05,  0.05,  0.052, 0.054, 0.054], // 特大発3
            [0.054, 0.056, 0.058, 0.059, 0.06]   // 特大発4
        ];

        public double DaihatsuBonus
        {
            get
            {
                var daihatsu = 0;
                var tokudai = 0;
                var bonus = 0.0;
                var level = 0;
                var sum = 0;
                foreach (var ship in Ships)
                {
                    if (ship.Spec.Id == 487) // 鬼怒改二
                        bonus += 0.05;
                    foreach (var item in ship.Items)
                    {
                        if (DaihatsuBonuses.ContainsKey(item.Spec.Id))
                        {
                            level += item.Level;
                            sum++;
                            bonus += DaihatsuBonuses[item.Spec.Id];
                        }
                        switch (item.Spec.Id)
                        {
                            case 68: // 大発動艇
                                daihatsu++;
                                break;
                            case 193: // 特大発動艇
                                tokudai++;
                                break;
                        }
                    }
                }

                var levelAverage = sum == 0 ? 0.0 : (double)level / sum;
                bonus = Math.Min(bonus, 0.2);
                var result = bonus + 0.01 * bonus * levelAverage + TokudaiBonuses[Math.Min(tokudai, 4)][Math.Min(daihatsu, 4)];
                return Math.Floor(result * 1000) / 10;
            }
        }

        public int CombinedFirepowerBonus => CombinedType switch
        {
            CombinedType.None => 0,
            CombinedType.Carrier => Number == 0 ? 2 : 10,
            CombinedType.Surface => Number == 0 ? 10 : -5,
            CombinedType.Transport => Number == 0 ? -5 : 10,
            _ => 0,
        };

        public int BaseAccuracy => CombinedType switch
        {
            CombinedType.None => 90,
            CombinedType.Carrier => Number == 0 ? 78 : 45,
            CombinedType.Surface => Number == 0 ? 45 : 67,
            CombinedType.Transport => Number == 0 ? 54 : 45,
            _ => 0,
        };

        public int CombinedTorpedoPenalty => IsCombined && Number == 1 ? -5 : 0;

        public string MissionParameter
        {
            get
            {
                var result = new List<string>();
                var kira = Ships.Count(ship => ship.Cond > 49);
                if (kira > 0)
                {
                    var min = Ships.Where(ship => ship.Cond > 49).Min(ship => ship.Cond);
                    var mark = Ships[0].Cond > 49 ? "+" : "";
                    result.Add($"ｷﾗ{kira}{mark}⪰{min}");
                }
                var drums = Ships.SelectMany(ship => ship.Items).Count(item => item.Spec.IsDrum);
                var drumShips = Ships.Count(ship => ship.Items.Any(item => item.Spec.IsDrum));
                if (drums > 0)
                    result.Add($"ド{drums}({drumShips}隻)");
                var daihatsuBonus = DaihatsuBonus;
                if (daihatsuBonus > 0)
                    result.Add($"ﾀﾞ{ToS(daihatsuBonus)}%");
                return result.Join(" ");
            }
        }

        public bool FullKira => Ships.Length > 5 && Ships.All(ship => ship.Cond > 49);
    }
}
