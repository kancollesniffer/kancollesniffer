// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using static KancolleSniffer.Model.ShipClassCode;
using static KancolleSniffer.Model.ShipTypeCode;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class EffectiveAsw
    {
        public double AswPower { get; set; }
        public bool Enabled { get; set; }
        public bool Opening { get; set; }
        public bool Unresolved { get; set; }

        public static EffectiveAsw Calc(ShipStatus ship)
        {
            const int baseAswDC = 13;
            const int baseAswAir = 8;

            var itemAsw               = 0;
            var ineffectiveAsw        = 0;
            var levelBonus            = 0.0;
            var hasSmallSonar         = false;
            var hasLargeSonar         = false;
            var hasDCT                = false;
            var hasDC                 = false;
            var hasMiscDC             = false;
            var isCarrierBasedBomber  = false;
            var isAswTorpedoBomber    = false;
            var isAnyBomber           = false;
            var isAswPatrol           = false;
            var hasCarrierBasedBomber = false;
            var hasAswEnabledBomber   = false;
            var hasAnyBomber          = false;
            var hasAswPatrol          = false;
            var hasFlyingBoat         = false;
            var autoGiroCount         = 0;
            var helicopterCount       = 0;

            foreach (var item in ship.Items)
            {
                itemAsw               += item.Spec.Asw;
                ineffectiveAsw        += item.Spec.IneffectiveAsw;
                levelBonus            += item.AswLevelBonus;
                hasSmallSonar         |= item.Spec.IsSmallSonar;
                hasLargeSonar         |= item.Spec.IsLargeSonar;
                hasDCT                |= item.Spec.IsDCT;
                hasDC                 |= item.Spec.IsDC;
                hasMiscDC             |= item.Spec.IsMiscDC;
                isCarrierBasedBomber  |= item.Spec.IsCarrierBasedBomber;
                isAswTorpedoBomber    |= item.Spec.IsAswTorpedoBomber;
                isAnyBomber           |= item.Spec.IsAnyBomber;
                isAswPatrol           |= item.Spec.IsAswPatrol;
                hasCarrierBasedBomber |= item.HasCarrierBasedBomber;
                hasAswEnabledBomber   |= item.HasAswEnabledBomber;
                hasAnyBomber          |= item.HasAnyBomber;
                hasAswPatrol          |= item.HasAswPatrol;
                hasFlyingBoat         |= item.HasFlyingBoat;
                if (item.HasAutoGiro)
                    autoGiroCount++;
                if (item.HasHelicopter)
                    helicopterCount++;
            }

            var hasSonar = hasSmallSonar || hasLargeSonar;
            var hasDCs = hasDCT || hasDC || hasMiscDC;
            var hasAswEnabledAircraft = hasAswEnabledBomber || hasAswPatrol || hasFlyingBoat;
            var rawAsw = ship.RawAsw;


            var enabled = 0;
            // 軽空母、加賀改二護、速吸改山汐丸(艦攻艦爆装備、0機でも)
            if (ship.Spec.ShipType == CVL || ship.Spec.Id == 646 || ship.Spec.IsOilerAircraftCarrier && isCarrierBasedBomber)
            {
                if (hasCarrierBasedBomber && (hasAswEnabledBomber || hasAnyBomber && hasAswPatrol))
                    enabled = baseAswAir;
            }
            // 速吸改山汐丸(対潜可能機装備、0機でも)
            else if (ship.Spec.IsOilerAircraftCarrier && (isAnyBomber || isAswPatrol))
            {
                if (hasAswEnabledAircraft)
                    enabled = baseAswAir;
            }
            // 扶桑改二山城改二(爆雷装備)
            else if (ship.Spec.Id is 411 or 412 && hasDCs)
            {
                enabled = isAnyBomber || isAswPatrol ? baseAswAir : baseAswDC;
            }
            // 第百一号輸送艦(爆雷装備)
            else if (ship.Spec.ShipClass == No101Tp && hasDCs)
            {
                enabled = baseAswDC;
            }
            else
            {
                switch (ship.Spec.ShipType)
                {
                    case DE:
                    case DD:
                    case CL:
                    case CLT:
                    case CT:
                    case AO:
                        if (rawAsw > 0)
                            enabled = baseAswDC;
                        break;
                    case CAV:
                    case BBV:
                    case AV:
                    case LHA:
                        if (hasAswEnabledAircraft)
                            enabled = baseAswAir;
                        break;
                }
            }


            var opening = 0;
            if (ship.Spec.Id is 508 or 509) // 鈴谷航改二、熊野航改二
            {
                opening = 0;
            }
            else if (ship.Spec.IsAswCruiserDestroyer)
            {
                opening = baseAswDC;
            }
            else if (ship.Spec.IsAswAircraftCarrier)
            {
                if (hasAswEnabledAircraft)
                    opening = baseAswAir;
            }
            else if (ship.Spec.Id == 554) // 日向改二
            {
                if (autoGiroCount > 1 || helicopterCount > 0)
                    opening = baseAswAir;
            }
            else if (ship.Spec.ShipType == DE)
            {
                if (ship.ShownAsw > 74 && itemAsw > 3 ||
                    ship.ShownAsw > 59 && hasSonar)
                {
                    opening = baseAswDC;
                }
            }
            else if (ship.Spec.ShipType == CVL)
            {
                if (hasAswEnabledAircraft &&
                      (ship.ShownAsw > 99 && hasSonar
                    || ship.ShownAsw > 64 && (isAswTorpedoBomber || isAswPatrol)
                    || ship.ShownAsw > 49 && (isAswTorpedoBomber || isAswPatrol) && hasSonar))
                {
                    opening = baseAswAir;
                }
            }
            else if (ship.ShownAsw > 99 && hasSonar)
            {
                opening = enabled;
            }


            var aswPower = 0.0;
            var baseAsw = Math.Max(enabled, opening);
            if (baseAsw > 0)
            {
                // 表示対潜から素対潜と対潜攻撃力に計算されない装備を引くことで、ボーナス込みの装備対潜とする
                var actualItemAsw = ship.ShownAsw - rawAsw - ineffectiveAsw;
                var bonus = 1.0;
                if (hasSmallSonar && hasDCT && hasDC)
                    bonus = 1.15 * 1.25;
                else if (hasLargeSonar && hasDCT && hasDC)
                    bonus = 1.15 * 1.1;
                else if (hasSonar && hasDCs)
                    bonus = 1.15;
                else if (hasDCT && hasDC)
                    bonus = 1.1;
                aswPower = bonus * (Math.Sqrt(rawAsw) * 2 + actualItemAsw * 1.5 + levelBonus + baseAsw);
            }

            return new()
            {
                AswPower = aswPower,
                Enabled = enabled > 0,
                Opening = opening > 0,
                Unresolved = ship.UnresolvedAsw
            };
        }

        public override string ToString() => AswPower > 0 ? $"潜{(Unresolved ? "?" : "")}{ToS(AswPower)}{(Enabled && Opening ? "*" : Opening ? "~" : "")}" : "";
    }
}
