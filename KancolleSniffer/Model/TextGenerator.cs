// Copyright (C) 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public static class TextGenerator
    {
        public static string GenerateShipList(IEnumerable<ShipStatus> shipList) =>
            "ID,艦種,艦名,レベル,次レベルまで,cond,改修耐久,表示対潜,素対潜,改修対潜,Lv99対潜,運\n" +
            string.Join("\n",
                from ship in shipList
                orderby ship.Spec.ShipType, -ship.Level, ship.ExpToNext
                select $"{ship.Id},{ship.Spec.ShipTypeName},{ship.Spec.Name},{ship.Level},{ship.ExpToNext},{ship.Cond},{ship.ImprovedMaxHp}"
                    + $",{ship.ShownAsw},{ship.RawAsw},{ship.ImprovedAsw},{ship.MaxAsw},{ship.Lucky}");

        public static string GeneratePresetDecks(IEnumerable<Fleet> presetDecks)
        {
            var sb = new StringBuilder();
            foreach (var fleet in presetDecks)
            {
                sb.Append($"プリセット{fleet.Number},艦種,艦名,レベル,次レベルまで,cond,改修耐久,表示対潜,素対潜,改修対潜,Lv99対潜,運\n");
                foreach (var ship in fleet.Ships)
                {
                    sb.Append($"{ship.Id},{ship.Spec.ShipTypeName},{ship.Spec.Name},{ship.Level},{ship.ExpToNext},{ship.Cond},{ship.ImprovedMaxHp}"
                        + $",{ship.ShownAsw},{ship.RawAsw},{ship.ImprovedAsw},{ship.MaxAsw},{ship.Lucky}\n");
                }
            }
            return sb.ToString();
        }

        public static string GenerateKantaiSarashiData(IEnumerable<ShipStatus> shipList)
        {
            return ".2|" + string.Join("|",
                from ship in shipList
                where ship.Locked
                group ship by ship.Spec.Remodel.Base
                into grp
                orderby grp.Key
                select grp.Key + ":" + string.Join(",",
                    from ship in grp
                    orderby -ship.Level
                    select ship.Level + (ship.Level >= ship.Spec.Remodel.Level && ship.Spec.Remodel.Step != 0 ? "." + ship.Spec.Remodel.Step : "")));
        }

        public static string GenerateItemList(IEnumerable<ItemStatus> itemList, IEnumerable<MaterialItem> useItems, IEnumerable<MaterialItem> payItems) =>
            "区分,装備名,改修,熟練度,個数\n" + string.Join("\n",
                from item in itemList
                where !item.Spec.Empty
                orderby item.Spec.Type, item.Spec.Id, item.Level, item.Alv
                group item by $"{item.Spec.TypeName},{item.Spec.Name},{item.Level},{item.Alv}"
                into grp
                select grp.Key + $",{grp.Count()}") + "\n"
                    + useItems.Select(item => item.ToCsv()).Join("\n")
                    + (payItems == null ? "" : "\n" + payItems.Select(item => item.ToCsv()).Join("\n"));

        public static string GenerateKantaiBunsekiItemList(IEnumerable<ItemStatus> itemList) =>
            "[" + string.Join(",",
                from item in itemList
                where item.Locked
                select $"{{\"api_slotitem_id\":{item.Spec.Id},\"api_level\":{item.Level}}}") + "]";

        public static string GenerateFleetCsv(Sniffer sniffer)
        {
            var dict = new ItemName();
            var sb = new StringBuilder();
            for (var f = 0; f < ShipInfo.FleetCount; f++)
                sb.Append(GenerateFleetCsv(sniffer, f, dict));
            sb.Append(GenerateAirBaseCsv(sniffer, dict));
            return sb.ToString();
        }

        public static string GenerateFleetCsv(Sniffer sniffer, int fleet)
        {
            return GenerateFleetCsv(sniffer, fleet, new ItemName());
        }

        private static string GenerateFleetCsv(Sniffer sniffer, int fleet, ItemName dict)
        {
            var sb = new StringBuilder();
            var target = sniffer.Fleets[fleet];
            var fp = target.FighterPower;

            sb.Append(Const.FleetNamesLong[fleet]);
            sb.Append(",制空: ");
            sb.Append(fp.Diff ? fp.RangeString : fp.Min.ToString());
            sb.Append(",索敵: ");
            sb.Append(ToS(target.GetLineOfSights(1)));
            sb.Append("\n");

            foreach (var ship in target.Ships)
            {
                sb.Append(ship.Spec.Name);
                sb.Append(",Lv");
                sb.Append(ship.Level);
                foreach (var item in ship.Items)
                {
                    sb.Append(",");
                    sb.Append(dict[item.Spec.Name]);
                    sb.Append(ItemStatusString(item));
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        private static string GenerateAirBaseCsv(Sniffer sniffer, ItemName dict)
        {
            if (sniffer.AirBase == null)
                return "";

            var sb = new StringBuilder();
            foreach (var baseInfo in sniffer.AirBase.OrderBy(baseInfo => baseInfo.ListWindowSortKey))
            {
                sb.Append(baseInfo.AreaName);
                sb.Append(",整備Lv");
                sb.Append(baseInfo.MaintenanceLevel);
                sb.Append("\n");

                var i = 0;
                foreach (var airCorps in baseInfo.AirCorps)
                {
                    sb.Append(Const.AirCorpsNames[i++]);
                    foreach (var plane in airCorps.Planes)
                    {
                        sb.Append(",");
                        if (plane.Deploying)
                        {
                            sb.Append(dict[plane.Item.Spec.Name]);
                            sb.Append(ItemStatusString(plane.Item));
                        }
                        else
                        {
                            sb.Append(plane.StateName);
                        }
                    }
                    sb.Append("\n");
                }
            }
            return sb.ToString();
        }

        private static string ItemStatusString(ItemStatus item) => (item.Level == 0 ? "" : "★" + item.Level) + (item.Alv == 0 ? "" : "+" + item.Alv);

        private class ItemName
        {
            private readonly Dictionary<string, string> _dict = [];

            public ItemName()
            {
                try
                {
                    foreach (var line in File.ReadLines("ItemName.csv"))
                    {
                        var cols = line.Split(',');
                        _dict[cols[0]] = cols[1];
                    }
                }
                catch (IOException)
                {
                }
            }

            public string this[string name] => _dict.TryGetValue(name, out var shortName) ? shortName : name;
        }

        public static string GenerateDeckBuilderData(Sniffer sniffer)
        {
            var sb = new StringBuilder("{\"version\": 4,");
            foreach (var fleet in sniffer.Fleets)
            {
                if (fleet.Number != 0)
                    sb.Append(",");
                sb.Append($"\"f{fleet.Number + 1}\":{{");
                for (var s = 0; s < fleet.Ships.Length; s++)
                {
                    if (s != 0)
                        sb.Append(",");
                    var ship = fleet.Ships[s];
                    sb.Append($"\"s{s + 1}\":{{\"id\":\"{ship.Spec.Id}\",\"lv\":{ship.Level},\"luck\":{ship.Lucky},\"items\":{{");
                    for (var i = 0; i < ship.Items.Count; i++)
                    {
                        var item = ship.Items[i];
                        if (i != 0)
                            sb.Append(",");
                        sb.Append($"\"i{i + 1}\":{{\"id\":{item.Spec.Id},\"rf\":{item.Level},\"mas\":{item.Alv}}}");
                    }
                    sb.Append("}}");
                }
                sb.Append("}");
            }
            sb.Append("}");
            return sb.ToString();
        }

        private const string Noro6KcToolsShipHeader = "svdata={\"api_data\":{\"api_ship\":[";
        public static string GenerateNoro6KcToolsManagerData(IEnumerable<ShipStatus> shipList)
        {
            var sb = new StringBuilder(Noro6KcToolsShipHeader);
            foreach (var ship in shipList)
            {
                if (sb.Length > Noro6KcToolsShipHeader.Length)
                    sb.Append(",");

                sb.Append("{\"api_id\":");
                sb.Append(ship.Id);
                sb.Append(",\"api_ship_id\":");
                sb.Append(ship.Spec.Id);
                sb.Append(",\"api_lv\":");
                sb.Append(ship.Level);
                sb.Append(",\"api_exp\":[");
                sb.Append(ship.ExpTotal);
                sb.Append(",");
                sb.Append(ship.ExpToNext);
                sb.Append(",");
                sb.Append(ship.ExpToNextPercent);
                sb.Append("],\"api_kyouka\":[");
                sb.Append(ship.ImprovedFirepower);
                sb.Append(",");
                sb.Append(ship.ImprovedTorpedo);
                sb.Append(",");
                sb.Append(ship.ImprovedAntiAir);
                sb.Append(",");
                sb.Append(ship.ImprovedArmor);
                sb.Append(",");
                sb.Append(ship.ImprovedLucky);
                sb.Append(",");
                sb.Append(ship.ImprovedMaxHp);
                sb.Append(",");
                sb.Append(ship.ImprovedAsw);
                sb.Append("],\"api_slot_ex\":");
                sb.Append(ship.SlotEx);
                sb.Append(",\"api_locked\":");
                sb.Append(ship.Locked ? 1 : 0);
                if (ship.SallyArea > 0)
                {
                    sb.Append(",\"api_sally_area\":");
                    sb.Append(ship.SallyArea);
                }
                sb.Append("}");
            }
            sb.Append("]}}");
            return sb.ToString();
        }

        private const string Noro6KcToolsItemHeader = "svdata={\"api_data\":{\"api_slot_item\":[";
        public static string GenerateNoro6KcToolsManagerEquipData(IEnumerable<ItemStatus> itemList)
        {
            var sb = new StringBuilder(Noro6KcToolsItemHeader);
            foreach (var item in itemList)
            {
                if (sb.Length > Noro6KcToolsItemHeader.Length)
                    sb.Append(",");

                sb.Append("{\"api_slotitem_id\":");
                sb.Append(item.Spec.Id);
                sb.Append(",\"api_level\":");
                sb.Append(item.Level);
                sb.Append(",\"api_locked\":");
                sb.Append(item.Locked ? 1 : 0);
                sb.Append("}");
            }
            sb.Append("]}}");
            return sb.ToString();
        }
    }
}
