// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Globalization;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class GaugeStatus
    {
        public static readonly string[] RankChar = ["", "丁", "丙", "乙", "甲"];

        public int Id { get; set; }
        public bool Cleared { get; set; }
        public int DefeatCount { get; set; }
        public int RequiredDefeatCount { get; set; }
        public int GaugeType { get; set; }
        public int GaugeNum { get; set; }
        public int EventmapNowMaphp { get; set; }
        public int EventmapMaxMaphp { get; set; }
        public int EventmapSelectedRank { get; set; }
        public bool IsEO { get; set; }
        public DateTime LimitTime { get; set; }

        public bool IsEvent => GaugeType > 1;
        public bool IsOnce => GaugeType > 0 && !Cleared && !IsEO;
        public bool NotStarted => EventmapSelectedRank == 0;
        public int ListWindowSortKey => IsEvent ? Id : Id * 1000;
        public int NowMaphp => IsEvent ? EventmapNowMaphp : RequiredDefeatCount - DefeatCount;
        public int MaxMaphp => IsEvent ? EventmapMaxMaphp : RequiredDefeatCount;
        public bool AllowDefeat(int currentGauge) => GaugeType == 1 && !Cleared && DefeatCount < RequiredDefeatCount && GaugeNum == currentGauge;
        public bool AllowLimit(DateTime now) => LimitTime == DateTime.MinValue || LimitTime > now;

        public string MapName => IsEvent ? NotStarted ? $"E{Id % 10}" : $"E{Id % 10}-{GaugeNum}{RankChar[EventmapSelectedRank]}"
                        : $"{Id / 10}-{Id % 10}{(!Cleared && Const.MultiGaugeTranslator.ContainsKey(Id) ? "-" + GaugeNum : "")}";
        public string TypeLabel => GaugeType switch
        {
            2 => "HP",
            3 => "TP",
            _ => "残り"
        };
        public string GaugeText => Cleared ? "" : IsEvent && NotStarted ? "未出撃" : $"{TypeLabel} {NowMaphp}/{MaxMaphp}";

        public string ShowStatus(DateTime now, bool showLimit)
        {
            if (LimitTime == DateTime.MinValue)
                return "";

            if (Cleared)
                return "達成済";

            var span = LimitTime - now;
            if (span < TimeSpan.Zero)
                return "時間切れ";

            return showLimit ? LimitTime.ToString("MM/dd HH:mm", CultureInfo.InvariantCulture) : ToS(span);
        }

        public GaugeStatus Clone()
        {
            return (GaugeStatus)MemberwiseClone();
        }
    }
}
