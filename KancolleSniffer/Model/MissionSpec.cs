// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class MissionSpec
    {
        public int Id { get; set; }
        public string DispNo { get; set; }
        public string Maparea { get; set; }
        public string Name { get; set; }
        public int ResetType { get; set; }
        public int DamageType { get; set; }
        public int Time { get; set; }

        public bool IsMonthly => ResetType == 1;
        public string TimeString => ToS(TimeSpan.FromMinutes(Time), false);
    }
}
