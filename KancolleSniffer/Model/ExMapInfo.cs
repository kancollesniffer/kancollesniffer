// Copyright (C) 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;

namespace KancolleSniffer.Model
{
    public class ExMapInfo : IHaveState
    {
        public class ClearStatus
        {
            public int Map { get; set; }
            public bool Cleared { get; set; }
            public int Rate { get; set; }
        }

        private readonly Dictionary<int, ClearStatus> _clearStatus = new()
        {
            {15, new ClearStatus {Map = 15, Cleared = false, Rate = 75}},
            {16, new ClearStatus {Map = 16, Cleared = false, Rate = 75}},
            {25, new ClearStatus {Map = 25, Cleared = false, Rate = 100}},
            {35, new ClearStatus {Map = 35, Cleared = false, Rate = 150}},
            {45, new ClearStatus {Map = 45, Cleared = false, Rate = 180}},
            {55, new ClearStatus {Map = 55, Cleared = false, Rate = 200}},
            {65, new ClearStatus {Map = 65, Cleared = false, Rate = 250}},
            {75, new ClearStatus {Map = 75, Cleared = false, Rate = 170}}
        };

        public readonly Dictionary<int, GaugeStatus> GaugeStatus = [];

        public Func<DateTime> NowFunc { get; set; } = () => DateTime.Now;

        private DateTime _lastReset;

        private DateTime _limitTime;

        private int _currentMap;

        private int _currentGauge;

        private bool _bossCell;

        public bool NeedSave { get; private set; }

        public void InspectMapInfo(dynamic json)
        {
            var limitover = NowFunc() >= _limitTime;
            foreach (var entry in json.api_map_info() ? json.api_map_info : json)
            {
                var map = (int)entry.api_id;
                var cleared = (int)entry.api_cleared == 1;
                if (!GaugeStatus.TryGetValue(map, out var gauge))
                    GaugeStatus.Add(map, gauge = new() { Id = map });
                gauge.Cleared = cleared;
                gauge.DefeatCount          = entry.api_defeat_count() ? (int)entry.api_defeat_count : 0;
                gauge.RequiredDefeatCount  = entry.api_required_defeat_count() ? (int)entry.api_required_defeat_count : 0;
                gauge.GaugeType            = entry.api_gauge_type() ? (int)entry.api_gauge_type : 1;
                gauge.GaugeNum             = entry.api_gauge_num() ? (int)entry.api_gauge_num : 1;
                gauge.EventmapNowMaphp     = entry.api_eventmap() && entry.api_eventmap.api_now_maphp() ? (int)entry.api_eventmap.api_now_maphp : 0;
                gauge.EventmapMaxMaphp     = entry.api_eventmap() && entry.api_eventmap.api_max_maphp() ? (int)entry.api_eventmap.api_max_maphp : 0;
                gauge.EventmapSelectedRank = entry.api_eventmap() && entry.api_eventmap.api_selected_rank() ? (int)entry.api_eventmap.api_selected_rank : 0;
                gauge.IsEO = _clearStatus.ContainsKey(map);
                if (gauge.IsEO)
                    gauge.LimitTime = _limitTime;

                if (!_clearStatus.TryGetValue(map, out var stat))
                    continue;
                var prev = gauge.Cleared = stat.Cleared;
                if (limitover)
                    continue;
                stat.Cleared = gauge.Cleared = cleared;
                if (prev != stat.Cleared)
                    NeedSave = true;
            }
        }

        public void InspectSelectEventmapRank(string request, dynamic json)
        {
            if (json == null)
                return;

            var values = HttpUtility.ParseQueryString(request);
            var mapid = int.Parse(values["api_maparea_id"]) * 10 + int.Parse(values["api_map_no"]);
            if (!GaugeStatus.TryGetValue(mapid, out var gauge))
                GaugeStatus.Add(mapid, gauge = new() { Id = mapid });
            gauge.EventmapSelectedRank = int.Parse(values["api_rank"]);
            gauge.EventmapNowMaphp     = (int)json.api_maphp.api_now_maphp;
            gauge.EventmapMaxMaphp     = (int)json.api_maphp.api_max_maphp;
            gauge.GaugeType            = (int)json.api_maphp.api_gauge_type;
            gauge.GaugeNum             = (int)json.api_maphp.api_gauge_num;
        }

        public void InspectMapStart(dynamic json)
        {
            _currentMap = (int)json.api_maparea_id * 10 + (int)json.api_mapinfo_no;
            InspectMapNext(json);
        }

        public void InspectMapNext(dynamic json)
        {
            var now = NowFunc();
            var cell = (int)json.api_no;
            _currentGauge = Const.MultiGaugeTranslator.TryGetValue(_currentMap, out var cellIds) && cellIds.TryGetValue(cell, out var gaugeid)
                ? gaugeid % 100 / 10 : 1;
            var cellEvent = (int)json.api_event_id;
            _bossCell = cellEvent == 5;
            if (GaugeStatus.TryGetValue(_currentMap, out var gauge) && gauge.AllowDefeat(_currentGauge) && _currentMap == 16 && cellEvent == 8)
            {
                gauge.DefeatCount++;
                gauge.Cleared = gauge.AllowLimit(now) && gauge.DefeatCount == gauge.RequiredDefeatCount;
            }

            if (!json.api_get_eo_rate() || json.api_get_eo_rate == 0)
                return;
            if (!_clearStatus.TryGetValue(_currentMap, out var stat))
                _clearStatus.Add(_currentMap, stat = new ClearStatus { Map = _currentMap });
            if (now >= _limitTime)
                return;
            stat.Cleared = true;
            stat.Rate = (int)json.api_get_eo_rate;
            NeedSave = true;
        }

        public void InspectBattleResult(dynamic json)
        {
            var now = NowFunc();
            var destsf = (int)json.api_destsf == 1;
            var firstClear = (int)json.api_first_clear == 1;
            if (GaugeStatus.TryGetValue(_currentMap, out var gauge) && gauge.AllowDefeat(_currentGauge) && _bossCell && destsf)
            {
                gauge.DefeatCount++;
                gauge.Cleared = gauge.AllowLimit(now) && firstClear;
            }

            if (!json.api_get_exmap_rate())
                return;
            var rate = json.api_get_exmap_rate is string ? int.Parse(json.api_get_exmap_rate) : (int)json.api_get_exmap_rate;
            if (rate == 0)
                return;
            if (!_clearStatus.TryGetValue(_currentMap, out var stat))
                _clearStatus.Add(_currentMap, stat = new ClearStatus { Map = _currentMap });
            if (now >= _limitTime)
                return;
            stat.Cleared = true;
            stat.Rate = rate;
            NeedSave = true;
        }

        public int Achievement => _clearStatus.Values.Where(s => s.Cleared).Sum(s => s.Rate);

        public void ResetIfNeeded()
        {
            var now = NowFunc();
            _limitTime = new DateTime(now.Year, now.Month, 1, 22, 0, 0).AddMonths(1).AddDays(-1);
            if (_lastReset.Month == now.Month)
                return;
            _lastReset = now;
            foreach (var k in GaugeStatus.Where(entry => !entry.Value.AllowLimit(now)).Select(entry => entry.Key).ToArray())
                GaugeStatus.Remove(k);
            foreach (var e in _clearStatus.Values)
                e.Cleared = false;
        }

        public void SaveState(Status status)
        {
            NeedSave = false;
            status.ExMapState = new ExMapState
            {
                ClearStatusList = _clearStatus.Values.ToList(),
                LastReset = _lastReset
            };
        }

        public void LoadState(Status status)
        {
            if (status.ExMapState == null)
                return;
            _lastReset = status.ExMapState.LastReset;
            foreach (var s in status.ExMapState.ClearStatusList)
            {
                if (s.Map == 65)
                    s.Rate = 250;
                _clearStatus[s.Map] = s;
            }
        }

        public class ExMapState
        {
            public List<ClearStatus> ClearStatusList { get; set; }
            public DateTime LastReset { get; set; }
        }

#if DEBUG
        public bool Cleared(int map) => _clearStatus[map].Cleared;
        public void InjectCleared(int map) => _clearStatus[map].Cleared = true;
        public void InjectLastReset(DateTime time) => _lastReset = time;
#endif
    }
}
