// Copyright (C) 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;

namespace KancolleSniffer.Model
{
    public class MiscTextInfo : Sniffer.IPort
    {
        private readonly ShipInfo _shipInfo;
        private readonly ItemInfo _itemInfo;
        private bool _inSortie;
        private readonly Dictionary<int, string> _furniture = [];

        private const string GuideText = "[演習情報]\n 演習相手を選ぶと表示します。\n[獲得アイテム]\n 帰投したときに表示します。";

        public string Text { get; private set; } = GuideText;
        public bool NeedSelectReward { get; set; }

        public MiscTextInfo(ShipInfo shipInfo, ItemInfo itemInfo)
        {
            _shipInfo = shipInfo;
            _itemInfo = itemInfo;
        }

        public void Port()
        {
            if (_inSortie || NeedSelectReward)
            {
                _inSortie = NeedSelectReward;
                var text = GenerateItemGetText();
                Text = text == "" ? GuideText : "[獲得アイテム]\n" + text;
            }
            if (!NeedSelectReward)
                _items.Clear();
        }

        public void InspectMaster(dynamic json)
        {
            if (json.api_mst_furniture())
            {
                foreach (var entry in json.api_mst_furniture)
                    _furniture[(int)entry.api_id] = (string)entry.api_title;
            }
        }

        public void InspectPracticeEnemyInfo(dynamic json)
        {
            Text = $"[演習情報]\n敵艦隊名 : {json.api_deckname}\n";
            var ships = json.api_deck.api_ships;
            var s1 = (int)ships[0].api_id != -1 ? (int)ships[0].api_level : 1;
            var s2 = (int)ships[1].api_id != -1 ? (int)ships[1].api_level : 1;
            var exp = PracticeExp.GetExp(s1, s2);
            var (otherShipRate, submarineRate, actualShips) = PracticeExp.AsashiCTBonus(_shipInfo.Fleets[0].Ships);
            var rate = PracticeExp.TrainingCruiserBonus(actualShips);
            if (otherShipRate == 1.0 && submarineRate == 1.0)
            {
                Text += $"獲得経験値 : {(int)(exp * rate)}\nS勝利 : {(int)((int)(exp * 1.2) * rate)}";
            }
            else
            {
                Text += $"獲得経験値(水上艦) : {(int)(exp * rate * otherShipRate)}\nS勝利 : {(int)((int)(exp * 1.2) * rate * otherShipRate)}"
                    + $"\n獲得経験値(潜水艦) : {(int)(exp * rate * submarineRate)}\nS勝利 : {(int)((int)(exp * 1.2) * rate * submarineRate)}";
            }
        }

        public void InspectMapNext(dynamic json)
        {
            if (json.api_airsearch() && (int)json.api_airsearch.api_result != 0)
            {
                var item = json.api_itemget;
                AddItemCount((int)item.api_usemst + 100, (int)item.api_id, (int)item.api_getcount);
                return;
            }
            if (json.api_itemget())
            {
                foreach (var item in json.api_itemget)
                    AddItemCount((int)item.api_usemst, (int)item.api_id, (int)item.api_getcount);
            }
            if (json.api_itemget_eo_result())
            {
                var eo = json.api_itemget_eo_result;
                AddItemCount((int)eo.api_usemst, (int)eo.api_id, (int)eo.api_getcount);
            }
            if (json.api_itemget_eo_comment())
            {
                var eo = json.api_itemget_eo_comment;
                AddItemCount((int)eo.api_usemst, (int)eo.api_id, (int)eo.api_getcount);
            }
            if (json.api_eventmap() && json.api_eventmap.api_itemget())
            {
                foreach (var item in json.api_eventmap.api_itemget)
                    AddItemCount(item);
            }
        }

        public void InspectMapStart(dynamic json)
        {
            _inSortie = true;
            InspectMapNext(json);
        }

        public void InspectBattleResult(dynamic json)
        {
            if (json.api_get_eventitem())
            {
                foreach (var item in json.api_get_eventitem)
                    AddItemCount(item);
            }

            if (json.api_get_useitem())
                AddItemCount(5, (int)json.api_get_useitem.api_useitem_id, 1);

            if (json.api_mapcell_incentive() && (int)json.api_mapcell_incentive == 1)
            {
                foreach (var type in _items.Keys.Where(type => type > 100).ToArray())
                {
                    foreach (var id in _items[type])
                    {
                        foreach (var level in id.Value)
                            AddItemCount(type - 100, id.Key, level.Value, level.Key);
                    }
                }
            }
        }

        public void InspectGetEventSelectedReward(dynamic json)
        {
            foreach (var item in json.api_get_item_list)
                AddItemCount(item);
            NeedSelectReward = false;
        }

        private readonly Dictionary<int, SortedDictionary<int, SortedDictionary<int, int>>> _items = [];

        private void AddItemCount(dynamic item)
        {
            var type = (int)item.api_type;
            var id = (int)item.api_id;
            var count = (int)item.api_value;
            if (type == 1 && id / 10 == 90)
            {
                count = id % 10;
                type = id = 90;
            }
            else
            {
                type = type == 1 ? 5 : type == 5 ? 6 : type;
            }
            var level = item.api_slot_level() ? (int)item.api_slot_level : 0;
            AddItemCount(type, id, count, level);
        }

        private void AddItemCount(int type, int id, int count, int level = 0)
        {
            if (!_items.ContainsKey(type))
                _items[type] = [];
            var byId = _items[type];
            if (!byId.ContainsKey(id))
                byId[id] = [];
            var byLevel = byId[id];
            if (!byLevel.ContainsKey(level))
                byLevel[level] = 0;
            byLevel[level] += count;
        }

        private string GetName(int type, int id, int level)
        {
            return type switch
            {
                2 => _shipInfo.GetSpec(id).Name,
                3 => _itemInfo.GetSpecByItemId(id).Name,
                4 => Const.MaterialNames[id - 1],
                5 => _itemInfo.GetUseItemName(id),
                6 => _furniture[id],
                90 => "装備運用枠", // 他の種類の報酬が未定なので一旦決め打ち
                _ => "",
            } + (level > 0 ? "★" + level : "");
        }

        private static readonly int[] Types = [4, 5, 3, 6, 2, 90];
        private string GenerateItemGetText()
        {
            return Types.Where(_items.ContainsKey).SelectMany(type => _items[type].SelectMany(id => _items[type][id.Key]
                .Select(level => GetName(type, id.Key, level.Key) + ": " + level.Value))).Join("\n");
        }
    }
}
