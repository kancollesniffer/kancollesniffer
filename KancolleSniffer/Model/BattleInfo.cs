// Copyright (C) 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public enum BattleResultRank
    {
        P,
        S,
        A,
        B,
        C,
        D,
        E
    }

    public enum BattleState
    {
        None,
        Day,
        Night,
        SpNight,
        AirRaid,
        Result,
        Unknown
    }

    public class EnemyFighterPower
    {
        public EnemyAircrafts LoggerAirCombat { get; set; } = new();
        public EnemyAircrafts ActualAirCombat { get; set; } = new();
        public EnemyAircrafts Interception { get; set; } = new();
        public int AirCombatAircraftsOffset { get; set; }
        public int InterceptionAircraftsOffset { get; set; }
        public int CurrentAirBaseDecks { get; set; }
        public AirBattleResult AirBattleResult;

        // csvへの出力はsjisなので、≥は?に化けてしまう。代わりに≧を使う
        public string CsvLoggerAirCombatText => (LoggerAirCombat.Unresolved ? "≧" : "") + LoggerAirCombat.FighterPower;
        public string LoggerAirCombatText => (LoggerAirCombat.Unresolved ? "≥" : "") + LoggerAirCombat.FighterPower;
        public string ActualAirCombatText => (ActualAirCombat.Unresolved ? "≥" : "") + ActualAirCombat.FighterPower;
        public string InterceptionText => "対基地: " + (Interception.Unresolved ? "≥" : "") + Interception.FighterPower;
        public bool ShowInterception => CurrentAirBaseDecks > 0 && !LoggerAirCombat.Equals(Interception);

        public string LoggerAirCombatBorderText => BorderText(LoggerAirCombat.FighterPower, LoggerAirCombat.Unresolved ? "≥" : "");
        public string ActualAirCombatBorderText => BorderText(ActualAirCombat.FighterPower, ActualAirCombat.Unresolved ? "≥" : "");
        public string InterceptionBorderText => BorderText(Interception.FighterPower, Interception.Unresolved ? "≥" : "");

        public static int CalcSupremacy(int fighterpower) => fighterpower * 3;
        public static int CalcSuperiority(int fighterpower) => (int)Math.Ceiling(fighterpower * 1.5);
        public static int CalcParity(int fighterpower) => (int)(fighterpower * 2 / 3.0) + 1;
        public static int CalcDenial(int fighterpower) => (int)(fighterpower / 3.0) + 1;
        public static string BorderText(int fighterpower, string unresolvedMark) =>
            $"確保: {unresolvedMark + CalcSupremacy(fighterpower)}\n優勢: {unresolvedMark + CalcSuperiority(fighterpower)}"
            + $"\n拮抗: {unresolvedMark + CalcParity(fighterpower)}\n劣勢: {unresolvedMark + CalcDenial(fighterpower)}";

        public string AircraftsMismatch()
        {
            var firstResult = AirBattleResult.First;
            if (firstResult == null)
                return "";
            var airBase = firstResult.AirBase ? "(対基地)" : "";
            var aircrafts = firstResult.AirBase ? Interception.Aircrafts - InterceptionAircraftsOffset: LoggerAirCombat.Aircrafts - AirCombatAircraftsOffset;
            // 敵噴式機搭載艦が撃沈されず残っていたら、噴式機戦で撃墜した機数を補填する
            var resultAircrafts = firstResult.Stage1.EnemyCount + AirBattleResult.EnemyLostJets;
            return aircrafts == resultAircrafts ? "" : $"敵機数不一致{airBase}\n仮:{aircrafts} 実:{resultAircrafts}";
        }

        public string LabelTextForMain(bool showResultAutomatic) => showResultAutomatic ? ActualAirCombatText : LoggerAirCombatText;
        public string TooltipTextForMain(bool showResultAutomatic)
        {
            var tooltip = new StringBuilder();
            if (!showResultAutomatic)
            {
                if (LoggerAirCombat.FighterPower > 0)
                    tooltip.Cat("\n", LoggerAirCombatBorderText);
                return tooltip.ToString();
            }

            if (ActualAirCombat.FighterPower > 0)
                tooltip.Cat("\n", ActualAirCombatBorderText);
            foreach (var result in AirBattleResult.Result)
            {
                if (result.IsFleetAirControl) break;
                tooltip.Cat("\n", $"{result.PhaseName}: {result.AirControlLevelName}");
            }
            return tooltip.ToString();
        }

        public string LabelTextForList() => LoggerAirCombatText;
        public string TooltipTextForList()
        {
            var tooltip = new StringBuilder();
            if (LoggerAirCombat.FighterPower > 0)
                tooltip.Cat("\n", LoggerAirCombatBorderText);
            if (ShowInterception)
            {
                tooltip.Cat("\n", InterceptionText);
                if (Interception.FighterPower > 0)
                    tooltip.Cat("\n", InterceptionBorderText);
            }
            tooltip.Cat("\n", AircraftsMismatch());
            return tooltip.ToString();
        }
    }

    public class BattleInfo : Sniffer.IPort
    {
        private readonly ShipInfo _shipInfo;
        private readonly ItemInfo _itemInfo;
        private readonly AirBase _airBase;
        private Fleet _fleet;
        private TemporaryFleet _alliedFleet;
        private AirBaseFleet _airBaseFleet;
        private Record[] _friend;
        private Record[] _guard;
        private Record[] _enemy;
        private Record[] _enemyGuard;
        private Record[] _allied;
        private readonly List<int> _escapingShips = [];
        private bool _safeCell;
        private bool _submarineSpecial;
        private int _mapareaId;
        private int _currentMap;
        private readonly Dictionary<int, int> _airBaseDecks = [];

        public BattleState BattleState { get; set; }
        public int Deck { get; private set; }
        public int[] Formation { get; private set; }
        public TemporaryFleet EnemyFleet;
        public TemporaryFleet EnemyGuardFleet;
        public Range FighterPower { get; private set; }
        public AirContactRates AirContactRates { get; private set; }
        public EnemyFighterPower EnemyFighterPower { get; private set; }
        public int AirControlLevel { get; private set; }
        public BattleResultRank ResultRank { get; private set; }
        public RankPair DisplayedResultRank { get; } = new RankPair();
        public BattleResult Result { get; private set; }
        public bool EnemyIsCombined => _enemyGuard.Length > 0;
        public AirBattleResult AirBattleResult { get; }
        public int SupportType { get; private set; }
        public bool Debuffed { get; private set; }
        public bool Practice { get; private set; }

        public class RankPair
        {
            public char Assumed { get; set; } = 'X';
            public char Actual { get; set; }
            public bool IsError => Assumed != Actual;
        }

        public class BattleResult
        {
            public class Combined
            {
                public ShipStatus[] Main { get; set; }
                public ShipStatus[] Guard { get; set; }
                public double SurvivalScore { get; set; }
                public Color SurvivalScoreColor => SurvivalScore >= 3.0 ? CUDColors.Red : Color.Black;
            }

            public Combined Friend { get; set; }
            public Combined Enemy { get; set; }
        }

        public BattleInfo(ShipInfo shipInfo, ItemInfo itemInfo, AirBase airBase)
        {
            _shipInfo = shipInfo;
            _itemInfo = itemInfo;
            _airBase = airBase;
            AirBattleResult = new AirBattleResult(GetAirFireShipName, GetItemNames);
        }

        private string GetAirFireShipName(int idx, bool isAlliedFleet)
        {
            return isAlliedFleet ? _allied[idx].Name : idx < _friend.Length ? _friend[idx].Name : _guard[idx - 6].Name;
        }

        private string[] GetItemNames(int[] ids)
        {
            return ids.Select(id => _itemInfo.GetSpecByItemId(id).Name).ToArray();
        }

        public void Port()
        {
            CleanupResult();
            _safeCell = false;
            BattleState = BattleState.None;
        }

        public void InspectBattle(string url, string request, dynamic json)
        {
            SetDeck(json);
            SetFormation(json);
            SetSupportType(json);
            SetAllied(json);
            ClearDamagedShipWarning();
            ShowResult(); // 昼戦の結果を夜戦のときに表示する
            Practice = url.Contains("practice");
            if (Practice)
                _currentMap = 0;
            if (json.api_xal01())
                Debuffed = (int)json.api_xal01 == 1;
            SetupDamageRecord(request, json, Practice);

            BattleState = url.Contains("sp_midnight") ? BattleState.SpNight : url.Contains("midnight") ? BattleState.Night : BattleState.Day;
            var calcAirBattle = BattleState != BattleState.Night;
            if (calcAirBattle)
            {
                AirBattleResult.Clear();
                SetAirBaseOnSlot(json);
                SetAirControlLevel(json);
                SetFighterPower();
            }

            // 制空状態の確定まで進めてから敵制空を計算する
            // 夜戦では友軍まで進めてから生存スコアを計算する
            var enemySurvivalScore = double.NaN;
            CalcDamageAirBaseFriendly(json);
            if (calcAirBattle)
                SetEnemyFighterPower();
            else
                enemySurvivalScore = CalcEnemySurvivalScore();
            CalcDamage(json);
            if (calcAirBattle)
                enemySurvivalScore = CalcEnemySurvivalScore();
            ResultRank = url.Contains("/ld_") ? CalcLdResultRank() : CalcResultRank();
            SetResult(enemySurvivalScore);
        }

        private void SetDeck(dynamic json)
        {
            if (json.api_deck_id())
                Deck = (int)json.api_deck_id;
        }

        private void SetFormation(dynamic json)
        {
            if (json.api_formation())
                Formation = (int[])json.api_formation;
        }

        private void SetAllied(dynamic json)
        {
            if (json.api_friendly_info())
            {
                _alliedFleet = new TemporaryFleet(0);
                _allied = _alliedFleet.Setup((int[])json.api_friendly_info.api_nowhps, null,
                    (int[][])json.api_friendly_info.api_Param,
                    EnemyShipSpecs(json.api_friendly_info.api_ship_id),
                    EnemySlots(json.api_friendly_info.api_Slot),
                    json.api_friendly_info.api_slot_ex() ? EnemySlotExs(json.api_friendly_info.api_slot_ex) : null,
                    null, false);
            }
        }

        private void SetAirBaseOnSlot(dynamic json)
        {
            if (json.api_air_base_attack())
            {
                foreach (var airbaseAttack in (dynamic[])json.api_air_base_attack)
                    SetAirBaseOnSlot(_mapareaId, (int)airbaseAttack.api_base_id, airbaseAttack.api_squadron_plane);
            }
        }

        private void SetAirBaseOnSlot(int areaId, int airbaseId, dynamic squadron, bool airraid = false)
        {
            var airbase = _airBase.GetAirBase(areaId);
            if (airbase != null && squadron != null)
            {
                foreach (var (plane, index) in airbase.AirCorps[airbaseId - 1].Planes.Where(plane => plane.AllowSetOnSlotInSortie(airraid))
                    .Select((plane, index) => (plane.Item, index)))
                {
                    plane.OnSlot = (int)squadron[index].api_count;
                }
            }
        }

        private void SetAirControlLevel(dynamic json)
        {
            AirControlLevel = Const.AirControlLevelNull;
            if (!json.api_kouku())
                return;
            var stage1 = json.api_kouku.api_stage1;
            if (stage1 == null || stage1.api_f_count == 0 && stage1.api_e_count == 0)
                return;
            AirControlLevel = (int)stage1.api_disp_seiku;
        }

        private void SetSupportType(dynamic json)
        {
            SupportType = json.api_support_flag() ? (int)json.api_support_flag : json.api_n_support_flag() ? (int)json.api_n_support_flag : 0;
        }

        private void SetupDamageRecord(string request, dynamic json, bool practice)
        {
            if (_friend != null)
                return;
            _shipInfo.SaveBattleStartStatus();
            SetupFriendDamageRecord(request, json, practice);
            SetupEnemyDamageRecord(json, practice);
        }

        private void SetupFriendDamageRecord(string request, dynamic json, bool practice)
        {
            _fleet = _shipInfo.Fleets[Deck - 1];
            FlagshipRecovery(request, _fleet.Ships[0]);
            _friend = Record.Setup(_fleet.Ships, practice);
            _guard = json.api_f_nowhps_combined() ? Record.Setup(_shipInfo.Fleets[1].Ships, practice) : [];
            SetEscapedFlag(json);
        }

        /// <summary>
        /// EscapedはShipStatusにあるがBattleBriefTestの用のログにはShipStatusがないので、
        /// ここで戦闘用のAPIを元に設定する。
        /// </summary>
        private void SetEscapedFlag(dynamic json)
        {
            if (json.api_escape_idx())
            {
                foreach (int idx in json.api_escape_idx)
                    _friend[idx - 1].Escaped = true;
            }
            if (json.api_escape_idx_combined())
            {
                foreach (int idx in json.api_escape_idx_combined)
                    _guard[idx - 1].Escaped = true;
            }
        }

        private void SetupEnemyDamageRecord(dynamic json, bool practice)
        {
            EnemyFleet = new TemporaryFleet(0);
            EnemyGuardFleet = new TemporaryFleet(1);
            _enemy = EnemyFleet.Setup(EnemyHp(json.api_e_nowhps), null,
                json.api_eParam() ? (int[][])json.api_eParam : null,
                EnemyShipSpecs(json.api_ship_ke),
                EnemySlots(json.api_eSlot), null, null, practice);
            _enemyGuard = json.api_ship_ke_combined()
                ? EnemyGuardFleet.Setup(EnemyHp(json.api_e_nowhps_combined), null,
                    json.api_eParam_combined() ? (int[][])json.api_eParam_combined : null,
                    EnemyShipSpecs(json.api_ship_ke_combined),
                    EnemySlots(json.api_eSlot_combined), null, null, practice)
                : [];

            EnemyFleet.Formation       = EnemyGuardFleet.Formation       = Formation[1];
            EnemyFleet.CombinedType    = EnemyGuardFleet.CombinedType    = EnemyIsCombined ? CombinedType.Surface : CombinedType.None;
            EnemyFleet.CombinedAntiAir = EnemyGuardFleet.CombinedAntiAir = EnemyFleet.AntiAir + EnemyGuardFleet.AntiAir;
        }

        private ShipSpec[] EnemyShipSpecs(dynamic ships)
        {
            return ((int[])ships).Select(_shipInfo.GetSpec).ToArray();
        }

        private ItemSpec[][] EnemySlots(dynamic slots)
        {
            return ((int[][])slots).Select(slot => slot.Select(_itemInfo.GetSpecByItemId).ToArray()).ToArray();
        }

        private ItemSpec[] EnemySlotExs(dynamic slotExs)
        {
            return ((int[])slotExs).Select(_itemInfo.GetSpecByItemId).ToArray();
        }

        private int[] EnemyHp(dynamic hps)
        {
            var newhps = new List<int>();
            foreach (var hp in hps)
                newhps.Add(hp.ToString() == "N/A" ? -1 : (int)hp);
            return newhps.ToArray();
        }

        private void SetResult(double enemySurvivalScore = double.NaN)
        {
            Result = new BattleResult
            {
                Friend = new BattleResult.Combined
                {
                    Main = _friend.Select(r => r.SnapShot).ToArray(),
                    Guard = _guard.Select(r => r.SnapShot).ToArray(),
                    SurvivalScore = double.NaN
                },
                Enemy = new BattleResult.Combined
                {
                    Main = _enemy.Select(r => r.SnapShot).ToArray(),
                    Guard = _enemyGuard.Select(r => r.SnapShot).ToArray(),
                    SurvivalScore = enemySurvivalScore
                }
            };
        }

        private void FlagshipRecovery(string request, ShipStatus flagship)
        {
            var type = int.Parse(HttpUtility.ParseQueryString(request)["api_recovery_type"] ?? "0");
            switch (type)
            {
                case 0:
                    return;
                case 1:
                    flagship.NowHp = flagship.MaxHp / 2;
                    flagship.ConsumeDamecon(42); // ダメコン
                    break;
                case 2:
                    flagship.NowHp = flagship.MaxHp;
                    flagship.ConsumeDamecon(43); // 女神
                    break;
            }
            if (type != 0)
                _shipInfo.SetBadlyDamagedShips();
        }

        private void CleanupResult()
        {
            _friend = null;
            _submarineSpecial = false;
            Debuffed = false;
        }

        private void SetFighterPower()
        {
            var fleets = _shipInfo.Fleets;
            var combined = _guard.Length > 0 && _enemyGuard.Length > 0;
            FighterPower = combined ? fleets[0].FighterPower + fleets[1].FighterPower : _fleet.FighterPower;
            AirContactRates = _guard.Length > 0 ? AirContactRates.Calc(fleets[0], fleets[1], combined) : AirContactRates.Calc(_fleet);
        }

        private void SetEnemyFighterPower()
        {
            EnemyFighterPower = new()
            {
                CurrentAirBaseDecks = _airBaseDecks.ContainsKey(_currentMap) ? _airBaseDecks[_currentMap] : 0,
                AirBattleResult = AirBattleResult
            };
            foreach (var record in _enemy.Concat(_enemyGuard))
            {
                var loggerAirCombat = record.EnemyLoggerAirCombat();
                var interception    = record.EnemyInterception();
                EnemyFighterPower.LoggerAirCombat.Add(loggerAirCombat);
                EnemyFighterPower.Interception.Add(interception);

                if (record.SunkByJetBomber)
                {
                    EnemyFighterPower.AirCombatAircraftsOffset    += loggerAirCombat.Aircrafts;
                    EnemyFighterPower.InterceptionAircraftsOffset += interception.Aircrafts;
                }
                if (record.DamageLevel != ShipStatus.Damage.Sunk)
                    EnemyFighterPower.ActualAirCombat.Add(record.EnemyActualAirCombat());
            }
            if (BattleState == BattleState.AirRaid)
                EnemyFighterPower.LoggerAirCombat = EnemyFighterPower.ActualAirCombat = EnemyFighterPower.Interception;
        }

        private double CalcEnemySurvivalScore()
        {
            if (!EnemyIsCombined)
                return double.NaN;

            return _enemyGuard.Sum(record => record.SurvivalScore) + (_enemyGuard.First().DamageLevel == ShipStatus.Damage.Sunk ? 0.0 : 1.0);
        }

        public void InspectMapInfo(dynamic json)
        {
            foreach (var entry in json.api_map_info() ? json.api_map_info : json)
                _airBaseDecks[(int)entry.api_id] = entry.api_air_base_decks() ? (int)entry.api_air_base_decks : 0;
        }

        public void InspectMapStart(dynamic json)
        {
            _mapareaId = (int)json.api_maparea_id;
            _currentMap = _mapareaId * 10 + (int)json.api_mapinfo_no;
            InspectMapNext(json);
        }

        public void InspectMapNext(dynamic json)
        {
            SetSafeCell(json);
            BattleState = BattleState.None;
            if (!json.api_destruction_battle())
                return;
            InspectAirRaidBattle(_mapareaId, [json.api_destruction_battle]);
        }

        private void SetSafeCell(dynamic json)
        {
            var map = _mapareaId * 1000 + (int)json.api_mapinfo_no * 100 + (int)json.api_no;
            _safeCell =
                (int)json.api_next == 0 || // last cell
                map switch
                {
                    1613 => true, // 1-6-B
                    1611 => true, // 1-6-D
                    1616 => true, // 1-6-D
                    2202 => true, // 2-2-B
                    3102 => true, // 3-1-B
                    3201 => true, // 3-2-A
                    4206 => true, // 4-2-F
                    5302 => true, // 5-3-B
                    _ => false
                };
        }

        public void InspectAirRaid(string request, dynamic json)
        {
            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_scc"], out var scc))
                InspectAirRaidBattle(_mapareaId, (dynamic[])json.api_destruction_battle, scc);
        }

        private void InspectAirRaidBattle(int areaId, dynamic[] json, int scc = 3)
        {
            var last = json.LastOrDefault();
            if (last == null)
                return;

            SetFormation(last);
            AirControlLevel = (int)last.api_air_base_attack.api_stage1.api_disp_seiku;
            SetAirBaseOnSlot(areaId, last);
            _friend = CreateShipsForAirBase(areaId, last);
            _guard = [];
            FighterPower = _airBase.GetAirBase(areaId).CalcInterceptionFighterPower(scc);
            AirContactRates = AirContactRates.Calc(_airBaseFleet);
            SetupEnemyDamageRecord(last, false);
            BattleState = BattleState.AirRaid;
            SetEnemyFighterPower();
            AirBattleResult.Clear();
            if (json.Length > 1)
            {
                for (var i = 0; i < json.Length; i++)
                {
                    AirBattleResult.Add(json[i].api_air_base_attack, "空襲" + (i + 1), airBase: true);
                    CalcKoukuDamage(json[i].api_air_base_attack);
                }
            }
            else
            {
                AirBattleResult.Add(last.api_air_base_attack, "空襲", airBase: true);
                CalcKoukuDamage(last.api_air_base_attack);
            }

            SetAirRaidResultRank(last);
            SetResult();
            CleanupResult();
        }

        private void SetAirBaseOnSlot(int areaId, dynamic json)
        {
            if (json.api_air_base_attack.api_map_squadron_plane != null)
            {
                foreach (KeyValuePair<string, dynamic> squadron in json.api_air_base_attack.api_map_squadron_plane)
                    SetAirBaseOnSlot(areaId, int.Parse(squadron.Key), squadron.Value, true);
            }
        }

        private static readonly int[] AirBaseMaxEq = [18, 18, 18, 18];
        private Record[] CreateShipsForAirBase(int areaId, dynamic json)
        {
            var nowHps = (int[])json.api_f_nowhps;
            var maxHps = (int[])json.api_f_maxhps;
            var specs = nowHps.Select((_, n) => new ShipSpec {Id = 1, Name = "基地航空隊" + (n + 1), GetMaxEq = () => AirBaseMaxEq}).ToArray();

            var airbase = _airBase.GetAirBase(areaId);
            var items = nowHps.Select((_, n) => airbase.AirCorps[n].Planes.Where(plane => plane.Deploying).Select(plane => plane.Item.Id).ToArray()).ToArray();

            _airBaseFleet = new AirBaseFleet();
            return _airBaseFleet.Setup(nowHps, maxHps, specs, items, _itemInfo.GetStatus);
        }

        private void SetAirRaidResultRank(dynamic json)
        {
            switch ((int)json.api_lost_kind)
            {
                case 1:
                    ResultRank = BattleResultRank.A;
                    break;
                case 2:
                    ResultRank = BattleResultRank.B;
                    break;
                case 3:
                    ResultRank = BattleResultRank.C;
                    break;
                case 4:
                    ResultRank = BattleResultRank.S;
                    break;
            }
        }

        private void CalcDamageAirBaseFriendly(dynamic json)
        {
            foreach (KeyValuePair<string, dynamic> kv in json)
            {
                if (kv.Value == null)
                    continue;
                switch (kv.Key)
                {
                    case "api_air_base_injection":
                        var airBattleRecord = AirBattleResult.Add(kv.Value, "AB噴式", jetBomber: true, airBase: true);
                        CalcLostAircrafts(airBattleRecord);
                        CalcKoukuDamage(kv.Value, jetBomber: true);
                        break;
                    case "api_injection_kouku":
                        airBattleRecord = AirBattleResult.Add(kv.Value, "噴式", jetBomber: true);
                        CalcLostAircrafts(airBattleRecord);
                        CalcKoukuDamage(kv.Value, jetBomber: true);
                        break;
                    case "api_air_base_attack":
                        CalcAirBaseAttackDamage(kv.Value);
                        break;
                    case "api_friendly_kouku":
                        CalcFriendKoukuDamage(kv.Value);
                        break;
                    case "api_friendly_battle":
                        CalcFriendAttackDamage(kv.Value);
                        break;
                }
            }
        }

        private void CalcDamage(dynamic json)
        {
            foreach (KeyValuePair<string, dynamic> kv in json)
            {
                if (kv.Value == null)
                    continue;
                switch (kv.Key)
                {
                    case "api_n_support_info":
                        CalcSupportDamage(kv.Value);
                        break;
                    case "api_n_hougeki1":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_n_hougeki2":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_kouku":
                        AirBattleResult.Add(kv.Value, "航空戦", isFleetAirControl: true);
                        CalcKoukuDamage(kv.Value);
                        break;
                    case "api_kouku2":
                        AirBattleResult.Add(kv.Value, "航空戦2");
                        CalcKoukuDamage(kv.Value);
                        break;
                    case "api_support_info":
                        CalcSupportDamage(kv.Value);
                        break;
                    case "api_opening_taisen":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_opening_atack":
                        CalcDamageAtOnce(kv.Value);
                        break;
                    case "api_hougeki":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_hougeki1":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_hougeki2":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_hougeki3":
                        CalcDamageByTurn(kv.Value);
                        break;
                    case "api_raigeki":
                        CalcDamageAtOnce(kv.Value);
                        break;
                }
            }
        }

        private void CalcSupportDamage(dynamic json)
        {
            if (json.api_support_hourai != null)
            {
                CalcRawDamageAtOnce(json.api_support_hourai.api_damage, _enemy, _enemyGuard);
            }
            else if (json.api_support_airatack != null && json.api_support_airatack.api_stage3 != null)
            {
                CalcRawDamageAtOnce(json.api_support_airatack.api_stage3.api_edam, _enemy, _enemyGuard);
            }
        }

        private void CalcAirBaseAttackDamage(dynamic json)
        {
            var i = 1;
            foreach (var entry in json)
            {
                var airBattleRecord = AirBattleResult.Add(entry, "基地" + i++, airBase: true);
                CalcLostAircrafts(airBattleRecord);
                CalcKoukuDamage(entry);
            }
        }

        private void CalcFriendAttackDamage(dynamic json)
        {
            CalcDamageByTurn(json.api_hougeki, true);
        }

        private void CalcFriendKoukuDamage(dynamic json)
        {
            var airBattleRecord = AirBattleResult.Add(json, "友軍", isAlliedFleet: true);
            CalcLostAircrafts(airBattleRecord);
            if (json.api_stage3() && json.api_stage3 != null)
                CalcDamageAtOnce(json.api_stage3, _allied, _enemy);
            if (json.api_stage3_combined() && json.api_stage3_combined != null)
                CalcDamageAtOnce(json.api_stage3_combined, null, _enemyGuard);
        }

        private void CalcKoukuDamage(dynamic json, bool jetBomber = false)
        {
            if (json.api_stage3() && json.api_stage3 != null)
                CalcDamageAtOnce(json.api_stage3, _friend, _enemy, jetBomber);
            if (json.api_stage3_combined() && json.api_stage3_combined != null)
                CalcDamageAtOnce(json.api_stage3_combined, _guard, _enemyGuard, jetBomber);
        }

        private void CalcDamageAtOnce(dynamic json)
        {
            CalcDamageAtOnce(json, _friend, _guard, _enemy, _enemyGuard);
        }

        private void CalcDamageAtOnce(dynamic json, Record[] friend, Record[] enemy, bool jetBomber = false)
        {
            CalcDamageAtOnce(json, friend, null, enemy, null, jetBomber);
        }

        private void CalcDamageAtOnce(dynamic json, Record[] friend, Record[] guard, Record[] enemy, Record[] enemyGuard, bool jetBomber = false)
        {
            if (json.api_fdam() && json.api_fdam != null)
                CalcRawDamageAtOnce(json.api_fdam, friend, guard, jetBomber);
            if (json.api_edam() && json.api_edam != null)
                CalcRawDamageAtOnce(json.api_edam, enemy, enemyGuard, jetBomber);
        }

        private void CalcRawDamageAtOnce(dynamic rawDamage, Record[] friend, Record[] guard = null, bool jetBomber = false)
        {
            var damage = (int[])rawDamage;
            for (var i = 0; i < friend.Length; i++)
            {
                friend[i].ApplyDamage(damage[i]);
                friend[i].CheckDamageControl();
                friend[i].CheckSunkByJetBomber(jetBomber);
            }
            if (guard == null)
                return;
            for (var i = 0; i < guard.Length; i++)
            {
                guard[i].ApplyDamage(damage[i + 6]);
                guard[i].CheckDamageControl();
                guard[i].CheckSunkByJetBomber(jetBomber);
            }
        }

        private void CalcDamageByTurn(dynamic json, bool ignoreFriendDamage = false)
        {
            if (!(json.api_df_list() && json.api_df_list != null &&
                  json.api_damage() && json.api_damage != null &&
                  json.api_at_eflag() && json.api_at_eflag != null))
            {
                return;
            }

            var eFlags = (int[])json.api_at_eflag;
            var night = json.api_sp_list();
            var types = night ? (int[])json.api_sp_list : (int[])json.api_at_type;
            _submarineSpecial |= types.Any(type => type == 300);
            var targets = (int[][])json.api_df_list;
            var damages = (int[][])json.api_damage;
            var records = new BothRecord(_friend, _guard, _enemy, _enemyGuard);
            for (var turn = 0; turn < eFlags.Length; turn++)
            {
                if (ignoreFriendDamage && eFlags[turn] == 1)
                    continue;
                var type = types[turn];
                if (IsSpecialAttack(type))
                {
                    var pos = night && _guard.Length > 0 ? 6 : 0; // 連合第二の僚艦夜戦突撃は6
                    var side = (eFlags[turn] + 1) % 2;
                    var shipId = records.TriggerSpecialAttack(side, pos, type);
                    _shipInfo.GetShip(shipId).SpecialAttack.Trigger(type);
                }
                for (var shot = 0; shot < targets[turn].Length; shot++)
                {
                    var target = targets[turn][shot];
                    var damage = damages[turn][shot];
                    if (target == -1 || damage == -1)
                        continue;
                    records.ApplyDamage(eFlags[turn], target, damage);
                }
                records.CheckDamageControl();
            }
        }

        private bool IsSpecialAttack(int type)
        {
            // 100: Nelson Touch
            // 101: 長門一斉射
            // 102: 陸奥一斉射
            // 103: Colorado特殊攻撃
            // 104: 金剛型僚艦夜戦突撃
            // 200: 瑞雲立体攻撃
            // 201: 海空立体攻撃
            // 300: 潜水艦特殊攻撃
            // 400: 大和特殊砲撃(3隻)
            // 401: 大和特殊砲撃(2隻)
            return type is > 99 and < 200 or > 399 and < 500;
        }

        public void CalcLostAircrafts(AirBattleResult.AirBattleRecord airBattleRecord)
        {
            if (airBattleRecord == null || airBattleRecord.Stage1.EnemyCount < 1)
                return;

            Func<ItemStatus, bool> selector = item => item.Spec.IsAircraft;
            if (airBattleRecord.JetBomber)
                selector = item => item.Spec.IsJetBomber;
            else if (!airBattleRecord.AirBase)
                selector = item => item.Spec.CanAirCombat;
            var items = EnemyFleet.Ships.Concat(EnemyGuardFleet.Ships).Where(ship => ship.DamageLevel != ShipStatus.Damage.Sunk)
                .SelectMany(ship => ship.Items).Where(selector).OrderByDescending(item => item.OnSlot).ThenBy(item => item.Spec.AntiAir).ToArray();
            var lostCount = (int)Math.Ceiling((airBattleRecord.Stage1.EnemyCount - airBattleRecord.Stage1.EnemyLost - airBattleRecord.Stage2.EnemyLost)
                * items.Sum(item => item.OnSlot) / (double)airBattleRecord.Stage1.EnemyCount);

            var stage1SurviveRate = airBattleRecord.Stage1.EnemySurviveRate;
            foreach (var item in items)
                item.OnSlot = (int)Math.Ceiling(stage1SurviveRate * item.OnSlot);
            if (airBattleRecord.Stage2.EnemyCount > 0)
            {
                var stage2SurviveRate = airBattleRecord.Stage2.EnemySurviveRate;
                foreach (var item in items.Where(item => item.Spec.IsAnyBomber))
                    item.OnSlot = (int)Math.Ceiling(stage2SurviveRate * item.OnSlot);
            }

            var lostCountDiff = items.Sum(item => item.OnSlot) - lostCount;
            for (var d = 0; d < lostCountDiff; d++)
            {
                var item = items[0];
                for (var i = 1; i < items.Length; i++)
                {
                    if (item.OnSlot < items[i].OnSlot)
                    {
                        item = items[i];
                        break;
                    }
                }
                item.OnSlot--;
            }
        }

        private class BothRecord
        {
            private readonly Record[][] _records = [new Record[12], new Record[12]];

            public BothRecord(Record[] friend, Record[] guard, Record[] enemy, Record[] enemyGuard)
            {
                Array.Copy(friend, _records[1], friend.Length);
                Array.Copy(guard, 0, _records[1], 6, guard.Length);
                Array.Copy(enemy, _records[0], enemy.Length);
                Array.Copy(enemyGuard, 0, _records[0], 6, enemyGuard.Length);
            }

            public int TriggerSpecialAttack(int side, int index, int type)
            {
                return _records[side][index].TriggerSpecialAttack(type);
            }

            public void ApplyDamage(int side, int index, int damage)
            {
                _records[side][index].ApplyDamage(damage);
            }

            public void CheckDamageControl()
            {
                foreach (var ship in _records[1])
                    ship?.CheckDamageControl();
            }
        }

        public bool InspectBattleResult(dynamic json)
        {
            BattleState = BattleState.Result;
            if (_friend == null)
                return false;
            var submarineSpecial = _submarineSpecial;
            ShowResult();
            SetDamagedShipWarning();
            _shipInfo.SaveBattleResult();
            _shipInfo.DropShipId = json.api_get_ship() ? (int)json.api_get_ship.api_ship_id : -1;
            VerifyResultRank(json);
            CleanupResult();
            SetEscapeShips(json);
            return submarineSpecial;
        }

        public void InspectPracticeResult(dynamic json)
        {
            BattleState = BattleState.Result;
            if (_friend == null)
                return;
            ShowResult();
            VerifyResultRank(json);
            CleanupResult();
        }

        private void ShowResult()
        {
            if (_friend == null)
                return;
            var fleets = _shipInfo.Fleets;
            var ships = _guard.Length > 0 ? fleets[0].Ships.Concat(fleets[1].Ships) : _fleet.Ships;
            foreach (var (ship, now) in ships.Zip(_friend.Concat(_guard), (ship, now) => (ship, now)))
                now.UpdateShipStatus(ship);
        }

        private void SetDamagedShipWarning()
        {
            if (_safeCell)
                return;
            _shipInfo.SetBadlyDamagedShips();
        }

        private void ClearDamagedShipWarning()
        {
            _shipInfo.ClearBadlyDamagedShips();
        }

        private void VerifyResultRank(dynamic json)
        {
            if (!json.api_win_rank())
                return;
            var assumed = "PSABCDE"[(int)ResultRank];
            if (assumed == 'P')
                assumed = 'S';
            var actual = ((string)json.api_win_rank)[0];
            DisplayedResultRank.Assumed = assumed;
            DisplayedResultRank.Actual = actual;
        }

        private void SetEscapeShips(dynamic json)
        {
            _escapingShips.Clear();
            if (!json.api_escape_flag() || (int)json.api_escape_flag == 0)
                return;
            var damaged = (int)json.api_escape.api_escape_idx[0] - 1;
            if (json.api_escape.api_tow_idx())
            {
                _escapingShips.Add(_shipInfo.Fleets[damaged / 6].Deck[damaged % 6]);
                var escort = (int)json.api_escape.api_tow_idx[0] - 1;
                _escapingShips.Add(_shipInfo.Fleets[escort / 6].Deck[escort % 6]);
            }
            else
            {
                _escapingShips.Add(_fleet.Deck[damaged]);
            }
        }

        public void CauseEscape()
        {
            _shipInfo.SetEscapedShips(_escapingShips);
            _shipInfo.SetBadlyDamagedShips();
        }

        public class TemporaryFleet : Fleet
        {
            private readonly Dictionary<int, int> practiceItemLevel = new()
            {
                // 零式艦戦53型(岩本隊)
                {157, 10},
                // 零戦62型(爆戦/岩井隊)
                {154, 10},
            };

            protected readonly ItemInventory _itemInventory = new();

            public TemporaryFleet(int number) : base(new ShipInventory(), number, () => 0)
            {
            }

            public int AddItemStatus(ItemSpec spec, bool practice)
            {
                if (spec.Id < 1)
                    return spec.Id;

                var item = new ItemStatus(_itemInventory.MaxId + 1)
                {
                    Spec = spec,
                    Alv = practice && spec.CanAirCombat ? 7 : 0,
                    Level = practice && practiceItemLevel.ContainsKey(spec.Id) ? practiceItemLevel[spec.Id] : 0
                };
                _itemInventory.Add(item);
                return item.Id;
            }

            public Record[] Setup(int[] nowHps, int[] maxHps, int[][] param, ShipSpec[] specs, ItemSpec[][] slots, ItemSpec[] slotExs, int[][] onSlots,
                bool practice)
            {
                var ships = new List<int>();
                var records = EnumerableUtil.Repeat(nowHps.Length, i =>
                {
                    var slot = slots[i].Select(spec => AddItemStatus(spec, practice)).ToArray();
                    var slotEx = slotExs == null ? 0 : AddItemStatus(slotExs[i], practice);
                    var ship = new ShipStatus(_shipInventory.MaxId + 1)
                    {
                        NowHp = nowHps[i],
                        MaxHp = maxHps == null ? nowHps[i] : maxHps[i],
                        ImprovedFirepower = param == null ? 0 : param[i][0],
                        ImprovedTorpedo   = param == null ? 0 : param[i][1],
                        ImprovedAntiAir   = param == null ? 0 : param[i][2],
                        ImprovedArmor     = param == null ? 0 : param[i][3],
                        Spec = specs[i],
                        GetItem = itemId => _itemInventory[itemId],
                    }.SetItems(slot, slotEx).FillOnSlotMaxEq(onSlots == null ? specs[i].MaxEq : onSlots[i]).FillEnemyStatus();
                    _shipInventory.Add(ship);
                    ships.Add(ship.Id);
                    return Record.Setup(ship, practice, nowHps[i]);
                }).ToArray();

                SetShips(ships.ToArray());
                CalcAntiAir();
                return records;
            }
        }

        public class AirBaseFleet : Fleet
        {
            public AirBaseFleet() : base(new ShipInventory(), 0, () => 0)
            {
            }

            public Record[] Setup(int[] nowHps, int[] maxHps, ShipSpec[] specs, int[][] items, Func<int, ItemStatus> GetItem)
            {
                var ships = new List<int>();
                var records = EnumerableUtil.Repeat(nowHps.Length, i =>
                {
                    var ship = new ShipStatus(_shipInventory.MaxId + 1)
                    {
                        NowHp = nowHps[i],
                        MaxHp = maxHps[i],
                        Spec = specs[i],
                        GetItem = GetItem,
                    }.SetItems(items[i]);
                    _shipInventory.Add(ship);
                    ships.Add(ship.Id);
                    return Record.Setup(ship, false, nowHps[i]);
                }).ToArray();

                SetShips(ships.ToArray());
                return records;
            }
        }

        public class Record
        {
            private ShipStatus _status;
            private bool _practice;
            public ShipStatus SnapShot => (ShipStatus)_status.Clone();
            public int NowHp => _status.NowHp;
            public bool Escaped { get; set; }
            public bool SunkByJetBomber { get; set; }
            public bool SupportShip => _status.SupportShip;
            public ShipStatus.Damage DamageLevel => _status.DamageLevel;
            public double SurvivalScore => _status.SurvivalScore;
            public string Name => _status.Spec.Name;
            public EnemyAircrafts EnemyLoggerAirCombat() => _status.EnemyLoggerAirCombat();
            public EnemyAircrafts EnemyActualAirCombat() => _status.EnemyActualAirCombat();
            public EnemyAircrafts EnemyInterception() => _status.EnemyInterception();
            public int StartHp { get; private set; }

            public static Record[] Setup(IEnumerable<ShipStatus> ships, bool practice) =>
                ships.Select(ship => Setup((ShipStatus)ship.Clone(), practice, ship.NowHp)).ToArray();

            public static Record Setup(ShipStatus ship, bool practice, int nowHp)
            {
                return new Record { _status = ship, _practice = practice, StartHp = nowHp };
            }

            public int TriggerSpecialAttack(int type)
            {
                _status.SpecialAttack.Trigger(type);
                return _status.Id;
            }

            public void ApplyDamage(int damage)
            {
                if (!_status.SupportShip)
                    _status.NowHp = Math.Max(0, _status.NowHp - damage);
            }

            public void CheckDamageControl()
            {
                if (_status.NowHp > 0 || _practice)
                    return;

                var item = _status.FindDamageControl;
                if (item == 42)
                {
                    _status.NowHp = (int)(_status.MaxHp * 0.2);
                    _status.ConsumeDamecon(item);
                }
                if (item == 43)
                {
                    _status.NowHp = _status.MaxHp;
                    _status.ConsumeDamecon(item);
                }
            }

            public void CheckSunkByJetBomber(bool jetBomber)
            {
                SunkByJetBomber = jetBomber && _status.DamageLevel == ShipStatus.Damage.Sunk;
            }

            public void UpdateShipStatus(ShipStatus ship)
            {
                ship.NowHp = _status.NowHp;
                ship.SetItems(_status.Slot, _status.SlotEx);
                ship.SpecialAttack.Fire = _status.SpecialAttack.Fire = false;
            }
        }

        private BattleResultRank CalcLdResultRank()
        {
            var friend = new ResultRankParams(_friend.Concat(_guard).ToArray());

            if (friend.Gauge <= 0)
                return BattleResultRank.P;
            if (friend.GaugeRate < 10)
                return BattleResultRank.A;
            if (friend.GaugeRate < 20)
                return BattleResultRank.B;
            if (friend.GaugeRate < 50)
                return BattleResultRank.C;
            if (friend.GaugeRate < 80)
                return BattleResultRank.D;
            return BattleResultRank.E;
        }

        private BattleResultRank CalcResultRank()
        {
            var friend = new ResultRankParams(_friend.Concat(_guard).ToArray());
            var enemy = new ResultRankParams(_enemy.Concat(_enemyGuard).Where(e => !e.SupportShip).ToArray());
            if (friend.Sunk == 0 && enemy.Sunk == enemy.Count)
            {
                if (friend.Gauge <= 0)
                    return BattleResultRank.P;
                return BattleResultRank.S;
            }
            if (friend.Sunk == 0 && enemy.Sunk >= (int)(enemy.Count * 0.7) && enemy.Count > 1)
                return BattleResultRank.A;
            if (friend.Sunk < enemy.Sunk && _enemy[0].NowHp == 0)
                return BattleResultRank.B;
            if (friend.Count == 1 && _friend[0].DamageLevel == ShipStatus.Damage.Badly)
                return BattleResultRank.D;
            if (enemy.GaugeRate > friend.GaugeRate * 2.5)
                return BattleResultRank.B;
            if (enemy.GaugeRate > friend.GaugeRate * 0.9)
                return BattleResultRank.C;
            if (friend.Count > 1 && friend.Count - 1 == friend.Sunk)
                return BattleResultRank.E;
            return BattleResultRank.D;
        }

        private class ResultRankParams
        {
            public readonly int Count;
            public readonly int Sunk;
            public readonly int Gauge;
            public readonly int GaugeRate;

            public ResultRankParams(Record[] records)
            {
                Count = records.Length;
                Sunk  = records.Count(r => r.NowHp == 0);
                Gauge = records.Sum(r => r.StartHp - r.NowHp);
                GaugeRate = (int)((double)Gauge / records.Sum(r => r.Escaped ? 0 : r.StartHp) * 100);
            }
        }

#if DEBUG
        /// <summary>
        /// テスト専用
        /// </summary>
        public void InjectFleet(params ShipStatus[] main)
        {
            InjectResultStatus(main, [], [], []);
        }

        /// <summary>
        /// テスト専用
        /// </summary>
        public void InjectEnemy(params ShipStatus[] enemy)
        {
            InjectResultStatus([], [], enemy, []);
        }

        /// <summary>
        /// テスト専用
        /// </summary>
        public void InjectResultStatus(ShipStatus[] main, ShipStatus[] guard, ShipStatus[] enemy, ShipStatus[] enemyGuard)
        {
            Result = new BattleResult
            {
                Friend = new BattleResult.Combined { Main = main, Guard = guard },
                Enemy  = new BattleResult.Combined { Main = enemy, Guard = enemyGuard }
            };
        }

        /// <summary>
        /// テスト専用
        /// </summary>
        public void InjectDeck(int deck)
        {
            Deck = deck;
        }
#endif
    }
}
