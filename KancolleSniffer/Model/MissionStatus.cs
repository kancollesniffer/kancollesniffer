// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Globalization;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class MissionStatus
    {
        public MissionSpec Spec { get; set; } = new();
        public int State { get; set; }
        public DateTime LimitTime { get; set; }
        public int Fleet { get; set; }
        public bool Canceled { get; set; }

        public string ShowStatus(DateTime now, bool showLimit)
        {
            if (State == 2)
                return "達成済";

            if (Fleet > 0 && !Canceled)
                return "遠征中";

            var span = LimitTime - now;
            if (span < TimeSpan.Zero)
                return "時間切れ";

            return showLimit ? LimitTime.ToString("MM/dd HH:mm", CultureInfo.InvariantCulture) : ToS(span);
        }

        public MissionStatus Clone()
        {
            return (MissionStatus)MemberwiseClone();
        }
    }
}
