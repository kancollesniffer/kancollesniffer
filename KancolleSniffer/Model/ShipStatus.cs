// Copyright (C) 2017 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using KancolleSniffer.Util;
using static KancolleSniffer.Model.ShipClassCode;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Model
{
    public class ShipStatus : ICloneable
    {
        public int Id { get; set; }
        public bool Empty => Id == -1;
        public bool Unopened => Id == -2; // 購入済み未開封アイテムに設定した仮の装備艦
        public Fleet Fleet { get; set; }
        public int DeckIndex { get; set; }
        public ShipSpec Spec { get; set; } = new ShipSpec();
        public int Level { get; set; }
        public int ExpTotal { get; set; }
        public int ExpToNext { get; set; }
        public int ExpToNextPercent { get; set; }
        public int MaxHp { get; set; }
        public int NowHp { get; set; }
        public int Speed { get; set; }
        public int Cond { get; set; }
        public int Fuel { get; set; }
        public int Bull { get; set; }
        public int[] NdockItem { get; set; }
        public int LoS { get; set; }
        public int Firepower { get; set; }
        public int Torpedo { get; set; }
        public int ShownAsw { get; set; }
        public int MaxAsw { get; set; }
        public int AntiAir { get; set; }
        public int Armor { get; set; }
        public int Lucky { get; set; }
        public int ImprovedFirepower { get; set; }
        public int ImprovedTorpedo { get; set; }
        public int ImprovedAntiAir { get; set; }
        public int ImprovedArmor { get; set; }
        public int ImprovedLucky { get; set; }
        public int ImprovedMaxHp { get; set; }
        public int ImprovedAsw { get; set; }
        public bool Locked { get; set; }
        public int SallyArea { get; set; }

        public int RawFirepower => Spec.MinFirepower + ImprovedFirepower;
        public int RawTorpedo => Spec.MinTorpedo + ImprovedTorpedo;
        public int RawAntiAir => Spec.MinAntiAir + ImprovedAntiAir;
        public int RawArmor => Spec.MinArmor + ImprovedArmor;

        public bool InNDock { get; set; }
        public Damage DamageLevel => CalcDamage(NowHp, MaxHp);
        public bool Escaped { get; set; }
        public bool Dropout => Escaped || DamageLevel == Damage.Sunk;
        public bool CanTransport => !Escaped && DamageLevel < Damage.Badly;
        public bool SupportShip => MaxHp == -1;
        public SpecialAttackStatus SpecialAttack { get; set; } = new SpecialAttackStatus();

        public int[] Slot { get; private set; } = [];
        public int SlotEx { get; private set; } = 0;
        public int ItemNum => Slot.Count(slot => slot > 0);
        public bool SlotFull => Slot.Take(Spec.SlotNum).All(slot => slot > 0);
        public bool SlotSemi => Slot.Take(Spec.SlotNum).Any(slot => slot > 0);
        public bool SlotExEmpty => SlotEx == -1;
        public List<ItemStatus> Items { get; private set; } = [];
        public Func<int, ItemStatus> GetItem { get; set; } = itemId => new ItemStatus(itemId);

        public ShipStatus(int id = -1)
        {
            Id = id;
        }

        private IEnumerable<ItemStatus> ItemsEx(bool moveTop)
        {
            var items = Items.ToList();
            if (SlotEx > 0)
            {
                var ex = items.Find(item => item.Id == SlotEx);
                if (ex != null)
                {
                    items.Remove(ex);
                    if (moveTop)
                        items.Insert(0, ex);
                }
            }
            return items;
        }

        protected IEnumerable<ItemStatus> ItemsWithoutEx => ItemsEx(false);

        protected IEnumerable<ItemStatus> ExFirstItems => ItemsEx(true);

        public int FindDamageControl => ExFirstItems.FirstOrDefault(item => item.Spec.IsDamageControl)?.Spec.Id ?? -1;

        public int PreparedDamageControl => DamageLevel != Damage.Badly ? -1 : FindDamageControl;

        public void ConsumeDamecon(int specId)
        {
            var item = ExFirstItems.FirstOrDefault(item => item.Spec.Id == specId);
            if (item == null)
                return;

            Items.Remove(item);
            if (SlotEx == item.Id)
            {
                SlotEx = -1;
            }
            else
            {
                var index = Slot.FindIndex(slot => slot == item.Id);
                if (index > -1)
                    Slot[index] = -1;
            }
        }

        public void OpenExslot()
        {
            SlotEx = -1;
        }

        public ShipStatus SetItems(params int[] slot)
        {
            return SetItems(slot, 0);
        }

        public ShipStatus SetItems(int[] slot, int slotEx = 0)
        {
            Slot = slot;
            SlotEx = slotEx;
            Items.Clear();
            Items.AddRange(Slot.Where(slot => slot > 0).Select(slot => GetItem(slot)));
            if (SlotEx > 0)
            {
                Items.Add(GetItem(SlotEx));
            }
            return this;
        }

        public ShipStatus FillOnSlotMaxEq(int[] onSlot)
        {
            var index = 0;
            foreach (var item in ItemsWithoutEx)
            {
                item.OnSlot = onSlot?.ElementAtOrDefault(index) ?? 0;
                item.MaxEq = item.Spec.IsFlyingBoat ? 1 : Spec.MaxEq?.ElementAtOrDefault(index) ?? 0;
                index++;
            }
            return this;
        }

        public ShipStatus ResolveTbBonus(AdditionalData additionalData)
        {
            foreach (var item in Items)
                item.TbBonus = 0;

            var bonusItem = Items.Where(item => item.HasAnyBomber).OrderByDescending(item => item.Spec.BomberSpec).ThenByDescending(item => item.OnSlot)
                .FirstOrDefault();
            if (bonusItem != null)
                bonusItem.TbBonus = additionalData.TbBonus(Spec.Id, Items.Select(item => item.Spec.Id));

            return this;
        }

        public ShipStatus ResolveApBonus()
        {
            var bonusItem = GetSkilledDeckAviationMaintenance;
            foreach (var item in Items)
            {
                if (item.Spec.IsDiveBomber)
                    item.ApBonus = bonusItem.AviationPersonnelBomberBonus;
                else if (item.Spec.IsTorpedoBomber)
                    item.ApBonus = bonusItem.AviationPersonnelTorpedoBonus;
                else
                    item.ApBonus = 0;
            }
            return this;
        }

        public ShipStatus FillEnemyStatus()
        {
            Firepower = RawFirepower + Items.Sum(item => item.Spec.Firepower);
            Torpedo   = RawTorpedo   + Items.Sum(item => item.Spec.Torpedo);
            AntiAir   = RawAntiAir   + Items.Sum(item => item.Spec.AntiAir);
            Armor     = RawArmor     + Items.Sum(item => item.Spec.Armor);
            return this;
        }

        public string ShipLabelPrefix => (Escaped ? "[避]" : PreparedDamageControl switch
        {
            42 => "[ダ]",
            43 => "[メ]",
            _ => "",
        }) + SpecialAttack.ShipLabelPrefix();

        public string FleetSourcePrefix => Escaped ? "[避]" : "";

        public Color DamageColor(Color initialBackColor)
        {
            if (InNDock)
                return CUDColors.LightGreen;
            return DamageLevel switch
            {
                Damage.Sunk => Color.CornflowerBlue,
                Damage.Badly => Escaped ? CUDColors.LightGray : CUDColors.Red,
                Damage.Half => CUDColors.Orange,
                Damage.Small => Color.FromArgb(240, 240, 0),
                _ => initialBackColor
            };
        }

        public Color CondColor(Color initialBackColor)
        {
            return Cond >= 50
                ? CUDColors.Yellow
                : Cond >= 30
                    ? initialBackColor
                    : Cond >= 20
                        ? CUDColors.Orange
                        : CUDColors.Red;
        }

        [Flags]
        public enum SlotStatus
        {
            Equipped = 0,
            SemiEquipped = 1,
            NormalEmpty = 2,
            ExtraEmpty = 4
        }

        public SlotStatus GetSlotStatus()
        {
            if (Empty)
                return SlotStatus.Equipped;
            var normal = SlotFull ? SlotStatus.Equipped : SlotSemi ? SlotStatus.SemiEquipped : SlotStatus.NormalEmpty;
            var extra = SlotExEmpty ? SlotStatus.ExtraEmpty : SlotStatus.Equipped;
            return normal | extra;
        }

        public enum Damage
        {
            Minor,
            Small,
            Half,
            Badly,
            Sunk
        }

        public static readonly int DamageLength = typeof(Damage).GetEnumValues().Length;

        public void Repair()
        {
            NowHp = MaxHp;
            Cond = Math.Max(40, Cond);
            InNDock = false;
        }

        public string DisplayHp => SupportShip ? "-/-" : $"{NowHp:D}/{MaxHp:D}";

        public string DisplayHpPercent => SupportShip ? "100%" : $"{(int)Math.Floor(NowHp * 100.0 / MaxHp):D}%";

        public static Damage CalcDamage(int now, int max)
        {
            if (max == -1)
                return Damage.Minor;
            if (now == 0 && max > 0)
                return Damage.Sunk;
            var ratio = max == 0 ? 1 : (double)now / max;
            return ratio > 0.75 ? Damage.Minor :
                   ratio > 0.5 ? Damage.Small :
                   ratio > 0.25 ? Damage.Half : Damage.Badly;
        }

        public double SurvivalScore => DamageLevel switch { Damage.Sunk or Damage.Badly => 0.0, Damage.Half => 0.7, _ => 1.0 };

        public int RepairReach => Items.Count(item => item.Spec.IsRepairFacility) + Spec.RepairReach;

        public TimeSpan RepairTime => TimeSpan.FromSeconds((int)(RepairTimePerHp.TotalSeconds * (MaxHp - NowHp)) + 30);

        public TimeSpan RepairTimePerHp =>
            TimeSpan.FromSeconds(Spec.RepairWeight * (Level < 12 ? Level * 10 : Level * 5 + Math.Floor(Math.Sqrt(Level - 11)) * 10 + 50));

        public string DisplayRepairTime()
        {
            var span = RepairTime;
            return $@"{(int)span.TotalHours:d2}:{span:mm\:ss}";
        }

        private ItemStatus GetSkilledDeckAviationMaintenance => Items.Where(item => item.Spec.IsSkilledDeckAviationMaintenance)
            .OrderByDescending(item => item.Level).FirstOrDefault() ?? new ItemStatus();

        public double EffectiveTorpedo
        {
            get
            {
                if (RawTorpedo > 0)
                    return Torpedo + Items.Sum(item => item.TorpedoLevelBonus) + Fleet.CombinedTorpedoPenalty + 5;

                return 0;
            }
        }

        public int RawAsw
        {
            get
            {
                // 対潜最大値はapiから取得可能。この値が0なら、素対潜は改修分(瑞鳳改二など)のみと判定
                if (MaxAsw < 1)
                    return ImprovedAsw;

                // 対潜最大値が設定されていて対潜最小値が未定義(新艦など)なら、表示値から装備対潜を引いて仮の素対潜とする
                if (Spec.MinAsw < 0)
                    return ShownAsw - Items.Sum(item => item.Spec.Asw);

                // レベルと最大値最小値から現在の素対潜を計算する
                return (int)((MaxAsw - Spec.MinAsw) * Level / 99.0) + Spec.MinAsw + ImprovedAsw;
            }
        }

        public bool UnresolvedAsw => MaxAsw > 0 && Spec.MinAsw < 0;

        public double NightBattlePower
        {
            get
            {
                if (Items.Any(item => item.HasNightAircraft) && // 夜戦か夜攻
                   (Spec.IsNightAircraftCarrier ||              // 夜戦空母
                    Spec.IsAircraftCarrier && Items.Any(item => item.Spec.IsNightOperationAviationPersonnel))) // 空母かつ夜間作戦航空要員
                {
                    return RawFirepower + GetSkilledDeckAviationMaintenance.AviationPersonnelNightBattleBonus
                        + Items.Sum(item => item.CalcNightAircraftPower());
                }

                // アークロイヤル+Swordfish
                if (Spec.ShipClass == ArkRoyal && Items.Any(item => item.HasSwordfishTorpedoBomber))
                {
                    return RawFirepower + GetSkilledDeckAviationMaintenance.AviationPersonnelNightBattleBonus
                        + Items.Sum(item => item.CalcSwordfishTorpedoPower());
                }

                if (Spec.EnableNightShelling)
                    return Firepower + Torpedo + Items.Sum(item => item.NightBattleLevelBonus);

                return 0;
            }
        }

        // 遠征時の装備改修ボーナスは小数第一位まで有効なため、10倍の値の整数値で管理する
        public int MissionFirepower => Firepower * 10 + Items.Sum(item => item.MissionFirepowerLevelBonus);

        public int MissionAntiAir => AntiAir * 10 + Items.Sum(item => item.MissionAntiAirLevelBonus);

        public int MissionAsw => ShownAsw * 10 + Items.Sum(item => item.MissionAswLevelBonus);

        public int MissionLoS => LoS * 10 + Items.Sum(item => item.MissionLoSLevelBonus);

        public bool HasNoneAircraft => Items.Any(item => item.HasNoneAircraft);

        public double TransportPoint => Spec.TransportPoint + Items.Sum(item => item.Spec.TransportPoint);

        public double EquipBonusAntiAir => AntiAir - RawAntiAir - Items.Sum(item => item.Spec.AntiAir);

        public int EffectiveAntiAirForShip
        {
            get
            {
                if (!Items.Any())
                    return RawAntiAir;

                if (Spec.IsEnemy)
                    return 2 * (int)Math.Sqrt(RawAntiAir + Items.Sum(item => item.Spec.AntiAir)) + (int)Items.Sum(item => item.EffectiveAntiAirForShip);

                return (int)((RawAntiAir + Items.Sum(item => item.EffectiveAntiAirForShip) + EquipBonusAntiAir * 0.75) / 2) * 2;
            }
        }

        public int EffectiveAntiAirForFleet => (int)Items.Sum(item => item.EffectiveAntiAirForFleet);

        public double PropShootdown => EffectiveAntiAirForShip * Fleet.AntiAirModifierCombined / 4.0;

        public double FixedShootdownModifierEnemy => Spec.IsEnemy ? 0.09375 : 0.1;

        public double CalcFixedShootdown(double formationModifier) =>
            (EffectiveAntiAirForShip + Fleet.CalcEffectiveAntiAir(formationModifier) + EquipBonusAntiAir * 0.5)
            * Fleet.AntiAirModifierCombined * FixedShootdownModifierEnemy;

        public double FixedShootdown => CalcFixedShootdown(Fleet.AntiAirModifierFormation);

        public double AntiAirPropellantBarrageChance
        {
            get
            {
                if (!Spec.EnableAntiAirPropellantBarrage)
                    return 0;
                var launcherCount = Items.Count(item => item.Spec.Id == 274);
                if (launcherCount == 0)
                    return 0;
                var baseChance = (EffectiveAntiAirForShip + 0.9 * Lucky) / 281.0;
                return (baseChance + 0.15 * (launcherCount - 1) + (Spec.ShipClass == Ise ? 0.25 : 0)) * 100;
            }
        }

        public double EquipAccuracy => Items.Sum(item => item.Spec.Accuracy + item.AccuracyLevelBonus);

        public int SupportEquipAccuracy => Items.Sum(item => item.Spec.Accuracy);

        public int EffectiveFuelMax => Math.Max((int)(Spec.FuelMax * (Level >= 100 ? 0.85 : 1.0)), Empty ? 0 : 1);

        public int EffectiveBullMax => Math.Max((int)(Spec.BullMax * (Level >= 100 ? 0.85 : 1.0)), Empty ? 0 : 1);

        public int RawLoS => LoS - Items.Sum(item => item.Spec.LoS);

        public EnemyAircrafts EnemyLoggerAirCombat() =>
            new()
            {
                FighterPower = Spec.IsEnemy
                    ? Items.Where(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0 && item.MaxEq > 0)
                        .Sum(item => (int)Math.Floor(item.Spec.AntiAir * Math.Sqrt(item.MaxEq)))
                    : Items.Sum(item => item.CalcPracticeFighterPower(true)),
                Aircrafts = Items.Where(item => item.Spec.CanAirCombat && item.MaxEq > 0).Sum(item => item.MaxEq),
                Unresolved = Spec.MaxEq == null && Items.Any(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0)
            };

        public EnemyAircrafts EnemyActualAirCombat() =>
            new()
            {
                FighterPower = Spec.IsEnemy
                    ? Items.Where(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0 && item.OnSlot > 0)
                        .Sum(item => (int)Math.Floor(item.Spec.AntiAir * Math.Sqrt(item.OnSlot)))
                    : Items.Sum(item => item.CalcPracticeFighterPower(false)),
                Aircrafts = Items.Where(item => item.Spec.CanAirCombat && item.OnSlot > 0).Sum(item => item.OnSlot),
                Unresolved = Spec.MaxEq == null && Items.Any(item => item.Spec.CanAirCombat && item.Spec.AntiAir > 0)
            };

        public EnemyAircrafts EnemyInterception() =>
            Spec.IsEnemy ? new()
            {
                FighterPower = Items.Where(item => item.Spec.IsAircraft && item.Spec.AntiAir > 0 && item.MaxEq > 0)
                    .Sum(item => (int)Math.Floor(item.Spec.AntiAir * Math.Sqrt(item.MaxEq))),
                Aircrafts = Items.Where(item => item.Spec.IsAircraft && item.MaxEq > 0).Sum(item => item.MaxEq),
                Unresolved = Spec.MaxEq == null && Items.Any(item => item.Spec.IsAircraft && item.Spec.AntiAir > 0)
            } : new();

        public string GetToolTipString()
        {
            var result = new List<string>();
            if (Spec.IsEnemy)
            {
                result.Add("id:" + Spec.Id);
                result.Add($"装備命中:{EquipAccuracy}");
                result.Add($"装甲:{Armor} ({new RangeD(Armor * 0.7, Armor * 1.3).RangeString})");
                result.Add($"対空 割合:{ToS(PropShootdown)}% 固定:{ToS(FixedShootdown)}");

                if (Items.Any(item => item.Spec.IsAircraft))
                {
                    var airCombat    = EnemyLoggerAirCombat();
                    var interception = EnemyInterception();
                    var airCombatText    =    airCombat.Unresolved ? "?" : airCombat.FighterPower.ToString();
                    var interceptionText = interception.Unresolved ? "?" : interception.FighterPower.ToString();
                    result.Add($"制空:{airCombatText} 対基地:{interceptionText}");
                }
            }

            result.AddRange(Items.Select(item => item.Spec.Name + (item.Level > 0 ? $"★{item.Level}" : "")
                + (item.Spec.IsAircraft ? $" {item.OnSlot}/{item.MaxEq} +{item.Alv}" : "")));

            return result.Join("\n");
        }

        private static readonly string[] SpeedNames = ["地上", "低速", "高速", "高速+", "最速"];
        public static string SpeedName(int speed) =>
            speed < 0 ? "未定" :
            speed > 20 ? "不明" :
            SpeedNames[speed / 5];

        public object Clone()
        {
            var r = (ShipStatus)MemberwiseClone();
            r.Items = Items.ToList(); // 戦闘中のダメコンの消費が見えないように複製する
            r.Slot = Slot.ToArray();
            r.SpecialAttack = (SpecialAttackStatus)SpecialAttack.Clone();
            return r;
        }
    }

    public class SpecialAttackStatus : ICloneable
    {
        public bool Fire { get; set; }
        public int Type { get; set; }
        public int Count { get; set; }

        public void Trigger(int type)
        {
            Fire = true;
            Type = type;
            Count++;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public string ShipLabelPrefix()
        {
            if (Fire)
                return "+";
            // 104: 金剛僚艦夜戦突撃
            if (Count >= (Type == 104 ? 2 : 1))
                return "-";
            if (Count > 0)
                return ">";
            return "";
        }
    }

    public class EnemyAircrafts
    {
        public int FighterPower { get; set; }
        public int Aircrafts { get; set; }
        public bool Unresolved { get; set; }

        public EnemyAircrafts Add(EnemyAircrafts other)
        {
            FighterPower += other.FighterPower;
            Aircrafts    += other.Aircrafts;
            Unresolved   |= other.Unresolved;
            return this;
        }

        public bool Equals(EnemyAircrafts other)
        {
            return FighterPower == other.FighterPower && Aircrafts == other.Aircrafts && Unresolved == other.Unresolved;
        }
    }
}
