// Copyright (C) 2013, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;

namespace KancolleSniffer.Model
{
    public class MissionInfo
    {
        public NameAndTimer[] MissionTimers { get; } = EnumerableUtil.Repeat(3, i => new NameAndTimer()).ToArray();
        public readonly Dictionary<int, MissionStatus> MonthlyMissionStatus = [];

        private readonly Dictionary<int, MissionSpec> _missionSpecs = [];

        public void InspectMaster(dynamic json)
        {
            var maparea = new Dictionary<int, string>();
            foreach (var entry in json.api_mst_maparea)
                maparea[(int)entry.api_id] = (string)entry.api_name;
            foreach (var entry in json.api_mst_mission)
            {
                var id = (int)entry.api_id;
                var dispNo = entry.api_disp_no() ? (string)entry.api_disp_no : "";
                _missionSpecs[id] = new()
                {
                    Id = id,
                    DispNo = dispNo,
                    Maparea = maparea[(int)entry.api_maparea_id],
                    Name = (string)entry.api_name + (dispNo.StartsWith("S") ? "S" : ""),
                    ResetType = entry.api_reset_type() ? (int)entry.api_reset_type : 0,
                    DamageType = entry.api_damage_type() ? (int)entry.api_damage_type : 0,
                    Time = (int)entry.api_time
                };
            }
        }

        public void InspectMission(dynamic json)
        {
            if (!json.api_list_items())
                return;

            MonthlyMissionStatus.Clear();
            var limitTime = new DateTime(1970, 1, 1).ToLocalTime().AddSeconds((int)json.api_limit_time[0]);
            foreach (var entry in json.api_list_items)
            {
                var id = (int)entry.api_mission_id;
                var spec = _missionSpecs[id];
                if (!spec.IsMonthly)
                    continue;

                var fleet = MissionTimers.FindIndex(timer => timer.Name == spec.Name);
                var canceled = fleet > -1 && MissionTimers[fleet].Timer.Minus;
                MonthlyMissionStatus[id] = new()
                {
                    Spec = spec,
                    State = (int)entry.api_state,
                    LimitTime = limitTime.AddMinutes(spec.Time * -1),
                    Fleet = fleet > -1 ? fleet + 1 : 0,
                    Canceled = canceled
                };
            }
        }

        public void InspectMissionStart(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            var mission = int.Parse(values["api_mission_id"]);
            if (MonthlyMissionStatus.ContainsKey(mission))
                MonthlyMissionStatus[mission].Fleet = int.Parse(values["api_deck_id"]) - 1;
        }

        public void InspectMissionResult(string request, dynamic json)
        {
            var values = HttpUtility.ParseQueryString(request);
            var deck = int.Parse(values["api_deck_id"]) - 1;
            var mission = MonthlyMissionStatus.Values.FirstOrDefault(e => e.Fleet == deck);
            if (mission != null)
            {
                mission.Fleet = 0;
                mission.Canceled = false;
                if ((int)json.api_clear_result > 0)
                    mission.State = 2;
            }
        }

        public void InspectMissionReturnInstruction(string request, dynamic json)
        {
            var values = HttpUtility.ParseQueryString(request);
            var deck = int.Parse(values["api_deck_id"]) - 2;

            var missionStatus = json.api_mission;
            var missionId = (int)missionStatus[1];
            var canceled = missionStatus[0] == 3;
            MissionTimers[deck].Timer.SetEndTime(missionStatus[2]);
            MissionTimers[deck].Timer.Minus = canceled;

            if (MonthlyMissionStatus.ContainsKey(missionId))
                MonthlyMissionStatus[missionId].Canceled = canceled;
        }

        public void InspectDeck(dynamic json)
        {
            foreach (var entry in json)
            {
                var id = (int)entry.api_id;
                if (id == 1)
                    continue;
                id -= 2;

                var missionStatus = entry.api_mission;
                var missionId = (int)missionStatus[1];
                if (missionStatus[0] == 0)
                {
                    MissionTimers[id].Name = "";
                    MissionTimers[id].Timer.SetEndTime(0);
                    continue;
                }
                MissionTimers[id].Name = _missionSpecs.TryGetValue(missionId, out var mission) ? mission.Name : "不明";
                MissionTimers[id].Timer.SetEndTime(missionStatus[2]);
                MissionTimers[id].Timer.Minus = missionStatus[0] == 3;
            }
        }
    }
}
