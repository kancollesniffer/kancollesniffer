// Copyright (C) 2013, 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.EnumerableUtil;

namespace KancolleSniffer.Model
{
    public class ShipInfo
    {
        public const int FleetCount = 4;
        public const int MemberCount = 6;

        public IReadOnlyList<Fleet> Fleets { get; }
        public AlarmCounter Counter { get; }
        public ShipStatusPair[] BattleResultDiff { get; private set; } = [];
        public bool IsBattleResultError => BattleResultDiff.Length > 0;
        public ShipStatus[] BattleStartStatus { get; private set; } = [];
        public bool WarnBadDamageWithDameCon;
        public int DropShipId { private get; set; } = -1;
        public int ChangingFleet { get; set; } = -1;

        private readonly ShipMaster _shipMaster;
        private readonly ShipInventory _shipInventory;
        private readonly ItemInventory _itemInventory;
        private ShipStatus[] _battleResult = [];
        private readonly NumEquipsChecker _numEquipsChecker = new();
        private int _penddingHappeningIconId;
        private int _penddingHappeningCount;
        private int _hqLevel;

        private class NumEquipsChecker
        {
            public int MaxId { private get; set; } = int.MaxValue;

            public void Check(ShipStatus ship)
            {
                var spec = ship.Spec;
                var num = ship.ItemNum;
                if (spec.NumEquips == num || ship.Id <= MaxId)
                    return;
                spec.NumEquips = num;
            }
        }

        public class ShipStatusPair
        {
            public ShipStatus Assumed { get; }
            public ShipStatus Actual { get; }

            public ShipStatusPair(ShipStatus assumed, ShipStatus actual)
            {
                Assumed = assumed;
                Actual = actual;
            }
        }

        public ShipInfo(ShipMaster shipMaster, ShipInventory shipInventory, ItemInventory itemInventory)
        {
            _shipMaster = shipMaster;
            _shipInventory = shipInventory;
            Fleets = Repeat(FleetCount, i => new Fleet(_shipInventory, i, () => _hqLevel)).ToArray();
            _itemInventory = itemInventory;
            Counter = new AlarmCounter(() => _shipInventory.Count) { Margin = 5 };
        }

        public void InspectMaster(dynamic json)
        {
            _shipMaster.Inspect(json);
            _shipInventory.Clear();
        }

        public void Port(dynamic json)
        {
            HandlePort(json);
        }

        public void InspectShip(string url, dynamic json)
        {
            if (url.Contains("ship2"))
            {
                SetShipAndDeck(json.api_data, json.api_data_deck);
            }
            else if (url.Contains("ship3"))
            {
                SetShipAndDeck(json.api_ship_data, json.api_deck_data);
            }
            else if (url.Contains("ship_deck"))
            {
                HandleShipDeck(json);
            }
            else if (url.Contains("getship")) // getship
            {
                HandleGetShip(json);
            }
            DropShipId = -1;
        }

        private void HandlePort(dynamic json)
        {
            for (var i = 0; i < FleetCount; i++)
                Fleets[i].State = FleetState.Port;
            InspectBasic(json.api_basic);
            SetShipAndDeckForPort(json);
            if (json.api_combined_flag())
                Fleets[0].CombinedType = Fleets[1].CombinedType = (CombinedType)(int)json.api_combined_flag;
            VerifyBattleResult();
            ClearBadlyDamagedShips();
            ClearPenddingHappening();
        }

        private void SetShipAndDeckForPort(dynamic json)
        {
            _shipInventory.Clear();
            foreach (var entry in json.api_ship)
            {
                var ship = CreateShipStatus(entry);
                _shipInventory.Add(ship);
                _numEquipsChecker.Check(ship);
            }
            _numEquipsChecker.MaxId = _shipInventory.MaxId;
            InspectDeck(json.api_deck_port);
        }

        private void HandleShipDeck(dynamic json)
        {
            SetShipAndDeckForShipDeck(json);
            VerifyBattleResult();
            // ドロップ艦を反映する
            if (DropShipId != -1)
            {
                _shipInventory.InflateCount(1);
                var num = _shipMaster.GetSpec(DropShipId).NumEquips;
                if (num > 0)
                    _itemInventory.InflateCount(num);
            }
        }

        private void SetShipAndDeckForShipDeck(dynamic json)
        {
            SetShipsInSortie(json.api_ship_data);
            InspectDeck(json.api_deck_data);
        }

        private void SetShipsInSortie(dynamic json)
        {
            foreach (var entry in json)
            {
                var ship = CreateShipStatus(entry);
                var org = _shipInventory[ship.Id];
                ship.Escaped = org.Escaped; // 出撃中は継続する
                ship.SpecialAttack = org.SpecialAttack;
                _shipInventory.Add(ship);
            }
        }

        private void SetShipAndDeck(dynamic ship, dynamic deck)
        {
            SetShips(ship);
            InspectDeck(deck); // FleetのDeckを設定した時点でShipStatusを取得するので必ずdeckが後
        }

        private void HandleGetShip(dynamic json)
        {
            var ship = CreateShipStatus(json.api_ship);
            _shipInventory.Add(ship);
            _numEquipsChecker.Check(ship);
            _numEquipsChecker.MaxId = _shipInventory.MaxId;
        }

        private void SetShips(dynamic json)
        {
            foreach (var entry in json)
                _shipInventory.Add(CreateShipStatus(entry));
        }

        public void SaveBattleResult()
        {
            _battleResult = Fleets.Where(fleet => fleet.State >= FleetState.Sortie && !fleet.Ships[0].Spec.IsRepairShip)
                .SelectMany(fleet => fleet.Ships).ToArray();
        }

        public void ClearBattleResult()
        {
            _battleResult = [];
        }

        private void VerifyBattleResult()
        {
            BattleResultDiff = (from assumed in _battleResult
                                let actual = GetShip(assumed.Id)
                                where !assumed.Escaped && assumed.NowHp != actual.NowHp
                                select new ShipStatusPair(assumed, actual)).ToArray();
            ClearBattleResult();
        }

        public void SaveBattleStartStatus()
        {
            BattleStartStatus = Fleets.Where(fleet => fleet.State >= FleetState.Sortie)
                .SelectMany(fleet => fleet.Ships.Select(ship => (ShipStatus)ship.Clone())).ToArray();
        }

        public void InspectPresetSelect(dynamic json)
        {
            ChangingFleet = (int)json.api_id - 1;
            InspectDeck(JsonArray(json));
        }

        public void InspectDeck(dynamic json)
        {
            foreach (var entry in json)
            {
                var fleet = (int)entry.api_id - 1;
                Fleets[fleet].SetShips((int[])entry.api_ship);
                if ((int)entry.api_mission[0] != 0)
                    Fleets[fleet].State = FleetState.Mission;
            }
        }

        private ShipStatus CreateShipStatus(dynamic entry)
        {
            foreach (var itemId in ((int[])entry.api_slot).Where(itemId => itemId > 0 && !_itemInventory.Contains(itemId)))
                _itemInventory.Add(new ItemStatus(itemId));

            return new ShipStatus
            {
                Id = (int)entry.api_id,
                Spec = _shipMaster.GetSpec((int)entry.api_ship_id),
                Level = (int)entry.api_lv,
                ExpTotal = (int)entry.api_exp[0],
                ExpToNext = (int)entry.api_exp[1],
                ExpToNextPercent = (int)entry.api_exp[2],
                MaxHp = (int)entry.api_maxhp,
                NowHp = (int)entry.api_nowhp,
                Speed = entry.api_soku() ? (int)entry.api_soku : 0,
                Cond = (int)entry.api_cond,
                Fuel = (int)entry.api_fuel,
                Bull = (int)entry.api_bull,
                GetItem = itemId => _itemInventory[itemId],
                NdockItem = (int[])entry.api_ndock_item,
                LoS = (int)entry.api_sakuteki[0],
                Firepower = (int)entry.api_karyoku[0],
                Torpedo = (int)entry.api_raisou[0],
                ShownAsw = (int)entry.api_taisen[0],
                MaxAsw = (int)entry.api_taisen[1],
                AntiAir = (int)entry.api_taiku[0],
                Armor = (int)entry.api_soukou[0],
                Lucky = (int)entry.api_lucky[0],
                ImprovedFirepower = (int)entry.api_kyouka[0],
                ImprovedTorpedo = (int)entry.api_kyouka[1],
                ImprovedAntiAir = (int)entry.api_kyouka[2],
                ImprovedArmor = (int)entry.api_kyouka[3],
                ImprovedLucky = (int)entry.api_kyouka[4],
                ImprovedMaxHp = entry.api_kyouka.IsDefined(5) ? (int)entry.api_kyouka[5] : 0,
                ImprovedAsw = entry.api_kyouka.IsDefined(6) ? (int)entry.api_kyouka[6] : 0,
                Locked = entry.api_locked() && entry.api_locked == 1,
                SallyArea = entry.api_sally_area() ? (int)entry.api_sally_area : 0
            }.SetItems((int[])entry.api_slot, entry.api_slot_ex() ? (int)entry.api_slot_ex : 0).FillOnSlotMaxEq((int[])entry.api_onslot)
                .ResolveTbBonus(_shipMaster.AdditionalData).ResolveApBonus();
        }

        public void CalcAntiAir()
        {
            foreach (var fleet in Fleets)
                fleet.CalcAntiAir();
            Fleets[0].CombinedAntiAir = Fleets[1].CombinedAntiAir = Fleets[0].AntiAir + Fleets[1].AntiAir;
        }

        private void InspectBasic(dynamic json)
        {
            _shipInventory.ClearInflated();
            Counter.Max = (int)json.api_max_chara;
            _hqLevel = (int)json.api_level;
        }

        public void InspectCharge(dynamic json)
        {
            foreach (var entry in json.api_ship)
            {
                var status = _shipInventory[(int)entry.api_id];
                status.Bull = (int)entry.api_bull;
                status.Fuel = (int)entry.api_fuel;
                status.FillOnSlotMaxEq((int[])entry.api_onslot);
                status.ResolveTbBonus(_shipMaster.AdditionalData);
            }
        }

        public void InspectChange(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            ChangingFleet = int.Parse(values["api_id"]) - 1;
            var dstFleet = Fleets[ChangingFleet];
            var dstIdx = int.Parse(values["api_ship_idx"]);
            var shipId = int.Parse(values["api_ship_id"]);

            if (shipId == -2)
            {
                dstFleet.WithdrawAccompanyingShips();
                return;
            }
            if (shipId == -1)
            {
                dstFleet.WithdrawShip(dstIdx);
                return;
            }
            var (srcFleet, srcIdx) = FindFleet(shipId);
            var prevShipId = dstFleet.SetShip(dstIdx, shipId);
            if (srcFleet == null)
                return;
            // 入れ替えの場合
            srcFleet.SetShip(srcIdx, prevShipId);
            if (prevShipId == -1)
                srcFleet.WithdrawShip(srcIdx);
        }

        private (Fleet, int) FindFleet(int ship)
        {
            int idx;
            foreach (var fleet in Fleets)
            {
                idx = fleet.Deck.IndexOf(ship);
                if (idx < 0)
                    continue;
                return (fleet, idx);
            }
            return (null, -1);
        }

        public void InspectPowerUp(string request, dynamic json)
        {
            var values = HttpUtility.ParseQueryString(request);
            var ships = values["api_id_items"].Split(',').Select(int.Parse).ToArray();
            if (!_shipInventory.Contains(ships[0])) // 二重に実行された場合
                return;
            if ((values["api_slot_dest_flag"] ?? "1") == "1")
                _itemInventory.Remove(ships.SelectMany(id => _shipInventory[id].Slot));
            _shipInventory.Remove(ships);
            SetShipAndDeck(JsonArray(json.api_ship), json.api_deck);
        }

        public void InspectSlotExchange(dynamic json)
        {
            UpdateShips(JsonArray(json.api_ship_data));
        }

        public void InspectSlotDeprive(dynamic json)
        {
            UpdateShips(JsonArray(json.api_ship_data.api_set_ship, json.api_ship_data.api_unset_ship));
        }

        public ItemStatus[] InspectPresetSlotRegister(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            if (int.TryParse(values["api_ship_id"], out int id))
                return GetShip(id).Items.Select(item => new ItemStatus { Spec = item.Spec, Level = item.Level }).ToArray();
            else
                return [];
        }

        public Dictionary<int, int> InspectRemodeling(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            var ship = GetShip(int.Parse(values["api_id"]));
            return ship.Spec.UpgradeItems;
        }

        public void InspectOpenExslot(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            GetShip(int.Parse(values["api_id"])).OpenExslot();
        }

        public void InspectMarriage(dynamic json)
        {
            UpdateShips(JsonArray(json));
        }

        private void UpdateShips(dynamic json)
        {
            SetShips(json);
            UpdateFleets();
        }

        private void UpdateFleets()
        {
            foreach (var fleet in Fleets)
                fleet.SetShips(fleet.Deck); // ShipStatusの差し替え
        }

        public void InspectDestroyShip(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            var delItem = int.Parse(values["api_slot_dest_flag"] ?? "0") == 1;
            foreach (var shipId in values["api_ship_id"].Split(',').Select(int.Parse))
            {
                if (delItem)
                    _itemInventory.Remove(_shipInventory[shipId].Items);
                var (srcFleet, srcIdx) = FindFleet(shipId);
                srcFleet?.WithdrawShip(srcIdx);
                _shipInventory.Remove(shipId);
            }
        }

        public void InspectCombined(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            var combinedType = (CombinedType)int.Parse(values["api_combined_type"]);
            Fleets[0].CombinedType = Fleets[1].CombinedType = combinedType;
            if (combinedType != CombinedType.None)
                ChangingFleet = 0;
        }

        public void InspectMapStart(string request, dynamic json)
        {
            var values = HttpUtility.ParseQueryString(request);
            var fleet = int.Parse(values["api_deck_id"]) - 1;
            if (!Fleets[0].IsCombined || fleet > 1)
            {
                Fleets[fleet].State = FleetState.Sortie;
            }
            else
            {
                Fleets[0].State = Fleets[1].State = FleetState.Sortie;
            }
            SetBadlyDamagedShips();
            InspectMapNext(json);
        }

        public void InspectMapNext(dynamic json)
        {
            InspectHappening();
            SetPenddingHappening(json);
        }

        public void InspectHappening()
        {
            if (_penddingHappeningIconId == 1)
            {
                var ships = Fleets.Where(fleet => fleet.State == FleetState.Sortie).SelectMany(fleet => fleet.Ships.Select(ship => ship));
                var lost = _penddingHappeningCount / (double)ships.Select(ship => ship.Spec.FuelMax).Max();
                foreach (var ship in ships)
                    ship.Fuel = Math.Max(ship.Fuel - (int)Math.Ceiling(ship.Spec.FuelMax * lost), 0);
                ClearPenddingHappening();
            }
            if (_penddingHappeningIconId == 2)
            {
                var ships = Fleets.Where(fleet => fleet.State == FleetState.Sortie).SelectMany(fleet => fleet.Ships.Select(ship => ship));
                var lost = _penddingHappeningCount / (double)ships.Select(ship => ship.Spec.BullMax).Max();
                foreach (var ship in ships)
                    ship.Bull = Math.Max(ship.Bull - (int)Math.Ceiling(ship.Spec.BullMax * lost), 0);
                ClearPenddingHappening();
            }
        }

        public void SetPenddingHappening(dynamic json)
        {
            if (json.api_happening())
            {
                _penddingHappeningIconId = (int)json.api_happening.api_icon_id;
                _penddingHappeningCount = (int)json.api_happening.api_count;
            }
        }

        public void ClearPenddingHappening()
        {
            _penddingHappeningIconId = 0;
            _penddingHappeningCount = 0;
        }

        public void InspectAnchorageRepair(dynamic json)
        {
            SetShipsInSortie(json.api_ship_data);
            UpdateFleets();
        }

        public void StartPractice(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            var fleet = int.Parse(values["api_deck_id"]) - 1;
            Fleets[fleet].State = FleetState.Practice;
        }

        public int InSortie
        {
            get
            {
                var sorties = Fleets.Select(f => f.State >= FleetState.Sortie).ToArray();
                if (sorties[0] && sorties[1])
                    return 10;
                return sorties.FindIndex(s => s);
            }
        }

        public double[] CalcSmokeScreenTriggerRates(int number)
        {
            static double rateRange(double rate)
            {
                return Math.Min(Math.Max(rate, 0.0), 1.0);
            }

            var inSortie = InSortie;
            var ships = new List<ShipStatus>();
            if (Fleets[number].IsCombined)
            {
                if (number == 0)
                {
                    ships.AddRange(inSortie == 10 ? Fleets[0].Ships.Where(ship => !ship.Dropout) : Fleets[0].Ships);
                    ships.AddRange(inSortie == 10 ? Fleets[1].Ships.Where(ship => !ship.Dropout) : Fleets[1].Ships);
                }
            }
            else
            {
                ships.AddRange(inSortie == number ? Fleets[number].Ships.Where(ship => !ship.Dropout) : Fleets[number].Ships);
            }

            var lucky = Math.Sqrt((ships.FirstOrDefault() ?? new ShipStatus()).Lucky);
            var items = ships.SelectMany(ship => ship.Items).ToArray();
            var smokeGeneratorItems = items.Where(item => item.Spec.IsSmokeGenerator).ToArray();
            var smokeGeneratorKItems = items.Where(item => item.Spec.IsSmokeGeneratorK).ToArray();
            var generators = smokeGeneratorItems.Length + smokeGeneratorKItems.Length * 2;
            var itemLevels = smokeGeneratorItems.Sum(item => item.Level) * 0.3 + smokeGeneratorKItems.Sum(item => item.Level) * 0.5;
            var baseRate = 1 - rateRange(3.2 - Math.Ceiling(lucky + itemLevels) * 0.2 - generators);

            if (generators + itemLevels * 0.2 >= 3.0)
            {
                var triggerRate3 = rateRange((Math.Ceiling(lucky * 1.5 + generators * 5 + itemLevels - 15) * 3 + 1) / 100.0);
                var triggerRate2 = Math.Min(1 - triggerRate3, 0.3);
                var triggerRate1 = 1 - triggerRate2 - triggerRate3;
                triggerRate3 *= baseRate;
                triggerRate2 *= baseRate;
                triggerRate1 *= baseRate;
                return [1 - triggerRate1 - triggerRate2 - triggerRate3, triggerRate1, triggerRate2, triggerRate3];
            }
            if (generators == 2)
            {
                var triggerRate2 = rateRange((Math.Ceiling(lucky * 1.5 + generators * 5 + itemLevels - 5) * 3 + 1) / 100.0);
                var triggerRate1 = 1 - triggerRate2;
                triggerRate2 *= baseRate;
                triggerRate1 *= baseRate;
                return [1 - triggerRate1 - triggerRate2, triggerRate1, triggerRate2, 0.0];
            }
            if (generators == 1)
            {
                return baseRate == 0.0 ? [] : [1 - baseRate, baseRate, 0.0, 0.0];
            }
            return [];
        }

        public double CalcTransportPoint(int number)
        {
            var inSortie = InSortie;
            var ships = new List<ShipStatus>();
            if (Fleets[number].IsCombined)
            {
                if (number == 0)
                {
                    ships.AddRange(inSortie == 10 ? Fleets[0].Ships.Where(ship => ship.CanTransport) : Fleets[0].Ships);
                    ships.AddRange(inSortie == 10 ? Fleets[1].Ships.Where(ship => ship.CanTransport) : Fleets[1].Ships);
                }
            }
            else
            {
                ships.AddRange(inSortie == number ? Fleets[number].Ships.Where(ship => ship.CanTransport) : Fleets[number].Ships);
            }

            return ships.Sum(ship => ship.TransportPoint)
                + (ships.Any(ship => ship.Spec.Id == 487) ? 8 : 0); // 鬼怒改二は大発分を加算。連合でも1隻分のみ
        }

        public ShipStatus GetShip(int id) => _shipInventory[id];

        public void SetItemHolder()
        {
            foreach (var ship in _shipInventory.AllShips)
            {
                foreach (var item in ship.Items)
                    _itemInventory[item.Id].Holder = ship;
            }
        }

        public ShipSpec GetSpec(int id) => _shipMaster.GetSpec(id);

        public ShipStatus[] ShipList => _shipInventory.AllShips.ToArray();

        public ShipStatus[] GetRepairList() =>
            ShipList.Where(ship => ship.NowHp < ship.MaxHp && !ship.InNDock && (ship.Fleet == null || ship.Fleet.State != FleetState.Practice))
            .OrderByDescending(ship => ship.RepairTime).ThenBy(ship => ship.Spec.SortId).ToArray();

        public string[] BadlyDamagedShips { get; private set; } = [];

        public void SetBadlyDamagedShips()
        {
            BadlyDamagedShips =
               (from s in ShipsInSortie
                where !s.Escaped && (s.PreparedDamageControl == -1 || WarnBadDamageWithDameCon) && s.DamageLevel == ShipStatus.Damage.Badly
                select s.Spec.Name).ToArray();
        }

        private IEnumerable<ShipStatus> ShipsInSortie =>
            Fleets.Where(fleet => fleet.State == FleetState.Sortie)
                .SelectMany(fleet => fleet.IsCombined && fleet.Number == 1
                    ? fleet.Ships.Skip(1) // 連合艦隊の護衛の旗艦をのぞく
                    : fleet.Ships);

        public void ClearBadlyDamagedShips()
        {
            BadlyDamagedShips = [];
        }

        public void SetEscapedShips(List<int> ids)
        {
            foreach (var id in ids)
                _shipInventory[id].Escaped = true;
        }
    }
}
