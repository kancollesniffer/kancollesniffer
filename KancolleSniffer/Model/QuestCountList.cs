// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Util;
using static KancolleSniffer.Model.ShipTypeCode;

namespace KancolleSniffer.Model
{
    public class QuestCountList
    {
        private const QuestInterval Other = QuestInterval.Other;
        private const QuestInterval Daily = QuestInterval.Daily;
        private const QuestInterval Weekly = QuestInterval.Weekly;
        private const QuestInterval Monthly = QuestInterval.Monthly;
        private const QuestInterval Quarterly = QuestInterval.Quarterly;
        private const QuestInterval Yearly1 = QuestInterval.Yearly1;
        private const QuestInterval Yearly2 = QuestInterval.Yearly2;
        private const QuestInterval Yearly3 = QuestInterval.Yearly3;
        private const QuestInterval Yearly4 = QuestInterval.Yearly4;
        private const QuestInterval Yearly5 = QuestInterval.Yearly5;
        private const QuestInterval Yearly6 = QuestInterval.Yearly6;
        private const QuestInterval Yearly7 = QuestInterval.Yearly7;
        private const QuestInterval Yearly8 = QuestInterval.Yearly8;
        private const QuestInterval Yearly9 = QuestInterval.Yearly9;
        private const QuestInterval Yearly10 = QuestInterval.Yearly10;
        private const QuestInterval Yearly11 = QuestInterval.Yearly11;
        private const QuestInterval Yearly12 = QuestInterval.Yearly12;

        /// <summary>
        /// このテーブルは七四式電子観測儀を参考に作成した。
        /// https://github.com/andanteyk/ElectronicObserver/blob/develop/ElectronicObserver/Data/Quest/QuestProgressManager.cs
        /// Materialは 建造, 修復, 開発, 改修 の順で定義
        /// Shiftは、Max=3なら2、Max=4なら1
        /// </summary>
        private readonly Dictionary<int, QuestSpec> _questSpecs = new()
        {
            // 敵艦隊を撃滅せよ！
            {201, new QuestSortie {Interval = Daily, Max = 1, Rank = "B", Material = [0, 0, 1, 0]}},
            // 敵艦隊主力を撃滅せよ！
            {216, new QuestSortie {Interval = Daily, Max = 1, Rank = "B", Material = [0, 1, 1, 0]}},
            // 敵艦隊を10回邀撃せよ！
            {210, new QuestSortie {Interval = Daily, Max = 10, Material = [0, 0, 1, 0]}},
            // 敵空母を3隻撃沈せよ！
            {211, new QuestEnemyType {Interval = Daily, Max = 3, EnemyType = [CVL, CV, CVB], Material = [0, 2, 0, 0]}},
            // 敵輸送船団を叩け！
            {212, new QuestEnemyType {Interval = Daily, Max = 5, EnemyType = [AP], Material = [0, 0, 2, 0]}},
            // 敵補給艦を3隻撃沈せよ！
            {218, new QuestEnemyType {Interval = Daily, Max = 3, EnemyType = [AP], Material = [0, 1, 1, 0]}},
            // 南西諸島海域の制海権を握れ！
            {226, new QuestSortie {Interval = Daily, Max = 5, Rank = "B", Maps = [21, 22, 23, 24, 25], Material = [1, 1, 0, 0]}},
            // 敵潜水艦を制圧せよ！
            {230, new QuestEnemyType {Interval = Daily, Max = 6, EnemyType = [SS, SSV], Material = [0, 1, 0, 0]}},

            // 海上通商破壊作戦
            {213, new QuestEnemyType {Interval = Weekly, Max = 20, EnemyType = [AP], Material = [0, 0, 3, 0]}},
            // あ号作戦
            {214, new QuestSpec {Interval = Weekly, MaxArray = [36, 6, 24, 12], Material = [2, 0, 2, 0]}},
            // い号作戦
            {220, new QuestEnemyType {Interval = Weekly, Max = 20, EnemyType = [CVL, CV, CVB], Material = [0, 0, 2, 0]}},
            // ろ号作戦
            {221, new QuestEnemyType {Interval = Weekly, Max = 50, EnemyType = [AP], Material = [0, 3, 0, 0]}},
            // 海上護衛戦
            {228, new QuestEnemyType {Interval = Weekly, Max = 15, EnemyType = [SS, SSV], Material = [0, 2, 0, 1]}},
            // 敵東方艦隊を撃滅せよ！
            {229, new QuestSortie {Interval = Weekly, Max = 12, Rank = "B", Maps = [41, 42, 43, 44, 45], Material = [0, 0, 2, 0]}},
            // 敵北方艦隊主力を撃滅せよ！
            {241, new QuestSortie {Interval = Weekly, Max = 5, Rank = "B", Maps = [33, 34, 35], Material = [0, 0, 3, 3]}},
            // 敵東方中枢艦隊を撃破せよ！
            {242, new QuestSortie {Interval = Weekly, Max = 1, Rank = "B", Maps = [44], Material = [0, 1, 1, 0]}},
            // 南方海域珊瑚諸島沖の制空権を握れ！
            {243, new QuestSortie {Interval = Weekly, Max = 2, Rank = "S", Maps = [52], Material = [0, 0, 2, 2]}},
            // 「第五戦隊」出撃せよ！
            {249, new QuestSortie {Interval = Monthly, Max = 1, Rank = "S", Maps = [25], Material = [0, 0, 5, 0]}},
            // 「潜水艦隊」出撃せよ！
            {256, new QuestSortie {Interval = Monthly, Max = 3, Rank = "S", Maps = [61], Material = [0, 0, 0, 0]}},
            // 「水雷戦隊」南西へ！
            {257, new QuestSortie {Interval = Monthly, Max = 1, Rank = "S", Maps = [14], Material = [0, 0, 0, 3]}},
            // 「水上打撃部隊」南方へ！
            {259, new QuestSortie {Interval = Monthly, Max = 1, Rank = "S", Maps = [51], Material = [0, 3, 0, 4]}},
            // 海上輸送路の安全確保に努めよ！
            {261, new QuestSortie {Interval = Weekly, Max = 3, Rank = "A", Maps = [15], Material = [0, 0, 0, 3]}},
            // 「空母機動部隊」西へ！
            {264, new QuestSortie {Interval = Monthly, Max = 1, Rank = "S", Maps = [42], Material = [0, 0, 0, 2]}},
            // 海上護衛強化月間
            {265, new QuestSortie {Interval = Monthly, Max = 10, Rank = "A", Maps = [15], Material = [0, 0, 5, 3]}},
            // 「水上反撃部隊」突入せよ！
            {266, new QuestSortie {Interval = Monthly, Max = 1, Rank = "S", Maps = [25], Material = [0, 0, 4, 2]}},
            // 兵站線確保！海上警備を強化実施せよ！
            {280, new QuestSortie {Interval = Monthly, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [12, 13, 14, 21], Material = [0, 4, 4, 2]}},
            // 南西諸島方面「海上警備行動」発令！
            {284, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [14, 21, 22, 23], Material = [0, 0, 0, 4], Exp = 80}},

            // 沖ノ島海域迎撃戦
            {822, new QuestSortie {Interval = Quarterly, Max = 2, Rank = "S", Maps = [24], Material = [0, 0, 0, 5]}},
            // 【節分任務:豆】節分作戦二〇二五
            {840, new QuestSortie {Interval = Weekly, MaxArray = [2, 2, 2], Rank = "A", Maps = [11, 12, 13], Material = [0, 0, 6, 2], Shift = 2}},
            // 【節分任務:鬼】南西方面節分作戦二〇二五
            {841, new QuestSortie {Interval = Weekly, MaxArray = [2, 2, 2], Rank = "A", Maps = [14, 21, 22], Material = [0, 6, 0, 3], Shift = 2}},
            // 【節分任務:柊】節分拡張作戦二〇二五 五穀豊穣！
            {843, new QuestSortie {Interval = Weekly, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [23, 41, 51, 7530], Material = [0, 0, 0, 3], Exp = 50, Shift = 1}},
            // 発令！「西方海域作戦」
            {845, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1, 1, 1], Rank = "S", Maps = [41, 42, 43, 44, 45], Material = [0, 0, 1, 0], Exp = 330}},
            // 戦果拡張任務！「Z作戦」前段作戦
            {854, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1, 1], Ranks = ["A", "A", "A", "S"], Maps = [24, 61, 63, 64], Material = [0, 0, 0, 4], Exp = 350}},
            // 強行輸送艦隊、抜錨！
            {861, new QuestSortie {Interval = Quarterly, Max = 2, Rank = "B", Maps = [16], Material = [0, 4, 0, 0]}},
            // 前線の航空偵察を実施せよ！
            {862, new QuestSortie {Interval = Quarterly, Max = 2, Rank = "A", Maps = [63], Material = [0, 0, 8, 4]}},
            // 戦果拡張任務！「Z作戦」後段作戦
            {872, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [7220, 55, 62, 65], Material = [0, 0, 0, 4], Exp = 400}},
            // 北方海域警備を実施せよ！
            {873, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1], Rank = "A", Maps = [31, 32, 33], Material = [0, 0, 0, 0]}},
            // 精鋭「三一駆」、鉄底海域に突入せよ！
            {875, new QuestSortie {Interval = Quarterly, Max = 2, Rank = "S", Maps = [54], Material = [0, 0, 0, 0]}},
            // 新編成「三川艦隊」、鉄底海峡に突入せよ！
            {888, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1], Rank = "S", Maps = [51, 53, 54], Material = [0, 0, 0, 0], Exp = 200}},
            // 泊地周辺海域の安全確保を徹底せよ！
            {893, new QuestSortie {Interval = Quarterly, MaxArray = [3, 3, 3, 3], Rank = "S", Maps = [15, 71, 7210, 7220], Material = [0, 0, 0, 0], Exp = 300}},
            // 空母戦力の投入による兵站線戦闘哨戒
            {894, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1, 1, 1], Rank = "S", Maps = [13, 14, 21, 22, 23], Material = [0, 0, 0, 0]}},
            // 拡張「六水戦」、最前線へ！
            {903, new QuestSortie {Interval = Quarterly, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [51, 54, 64, 65], Material = [0, 10, 0, 0], Exp = 390}},
            // 精鋭「十九駆」、躍り出る！
            {904, new QuestSortie {Interval = Yearly2, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [25, 34, 45, 53], Material = [0, 8, 10, 4]}},
            // 「海防艦」、海を護る！
            {905, new QuestSortie {Interval = Yearly2, MaxArray = [1, 1, 1, 1, 1], Rank = "A", Maps = [11, 12, 13, 15, 16], Material = [0, 6, 8, 0]}},
            // 工作艦「明石」護衛任務
            {912, new QuestSortie {Interval = Yearly3, MaxArray = [1, 1, 1, 1, 1], Rank = "A", Maps = [13, 21, 22, 23, 16], Material = [0, 5, 6, 0]}},
            // 重巡戦隊、西へ！
            {914, new QuestSortie {Interval = Yearly3, MaxArray = [1, 1, 1, 1], Rank = "A", Maps = [41, 42, 43, 44], Material = [0, 5, 0, 4]}},
            // 歴戦「第十方面艦隊」、全力出撃！
            {928, new QuestSortie {Interval = Yearly9, MaxArray = [2, 2, 2], Rank = "S", Maps = [7320, 7220, 42], Material = [0, 0, 10, 8]}},
            // 鎮守府近海海域の哨戒を実施せよ！
            {944, new QuestSortie {Interval = Yearly6, MaxArray = [2, 2, 2], Rank = "A", Maps = [12, 13, 14], Material = [0, 0, 5, 0]}},
            // 南西方面の兵站航路の安全を図れ！
            {945, new QuestSortie {Interval = Yearly6, MaxArray = [2, 2, 2], Rank = "A", Maps = [15, 21, 16], Material = [0, 0, 0, 3]}},
            // 空母機動部隊、出撃！敵艦隊を迎撃せよ！
            {946, new QuestSortie {Interval = Yearly6, MaxArray = [1, 1, 1], Rank = "S", Maps = [22, 23, 24], Material = [0, 5, 0, 0]}},
            // AL作戦
            {947, new QuestSortie {Interval = Yearly6, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [31, 33, 34, 35], Material = [0, 0, 0, 0], Exp = 480}},
            // 機動部隊決戦
            {948, new QuestSortie {Interval = Yearly6, MaxArray = [2, 2, 2, 2], Ranks = ["S", "S", "A", "S"], Maps = [52, 55, 64, 65], Material = [0, 0, 0, 0], Exp = 600}},
            // 【梅雨限定任務】雨の南西諸島防衛戦！
            {953, new QuestSortie {Interval = Weekly, MaxArray = [1, 1, 1], Rank = "A", Maps = [21, 22, 23], Material = [0, 5, 5, 0], Shift = 2}},
            // 【梅雨拡張任務】梅雨の海上護衛強化！
            {954, new QuestSortie {Interval = Weekly, MaxArray = [1, 1, 1, 2], Rank = "S", Maps = [12, 13, 15, 16], Material = [0, 0, 8, 2]}},
            // 【梅雨限定月間任務】西方海域統合作戦
            {955, new QuestSortie {Interval = Monthly, MaxArray = [1, 1, 1, 1, 1], Rank = "S", Maps = [41, 42, 43, 44, 45], Material = [0, 0, 0, 0], Exp = 400}},
            // 日英米合同水上艦隊、抜錨せよ！
            {973, new QuestSortie {Interval = Yearly5, MaxArray = [1, 1, 1, 1], Rank = "A", Maps = [31, 33, 43, 7320], Material = [0, 0, 8, 0]}},
            // 精鋭「第十九駆逐隊」、全力出撃！
            {975, new QuestSortie {Interval = Yearly5, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [15, 23, 32, 53], Material = [0, 9, 0, 4]}},
            // 【限定週間任務】秋の南瓜祭り、おかわりっ！
            {987, new QuestSortie {Interval = Weekly, MaxArray = [1, 1, 1, 1], Rank = "A", Maps = [15, 21, 31, 61], Material = [0, 0, 0, 0], Shift = 1}},
            // 【Xmas限定任務】聖夜の哨戒線
            {988, new QuestSortie {Interval = Weekly, MaxArray = [1, 1, 1, 1], Rank = "S", Maps = [12, 13, 15, 21], Material = [0, 4, 5, 3], Shift = 1}},
            // 精強「第七駆逐隊」緊急出動！
            {1005, new QuestSortie {Interval = Yearly1, MaxArray = [1, 1, 1, 1], Rank = "A", Maps = [12, 13, 15, 32], Material = [0, 4, 7, 2], Shift = 1}},
            // 【期間限定任務】対潜掃討作戦
            {1010, new QuestSortie {Interval = Weekly, MaxArray = [3, 1], Rank = "S", Maps = [15, 16], Material = [0, 3, 6, 2]}},
            // 【期間限定任務】精強海防艦、緊急近海防衛！
            {1011, new QuestSortie {Interval = Weekly, MaxArray = [1, 1, 1, 1, 1], Rank = "A", Maps = [11, 12, 13, 15, 21], Material = [0, 8, 7, 0]}},
            // 鵜来型海防艦、静かな海を防衛せよ！
            {1012, new QuestSortie {Interval = Yearly5, MaxArray = [3, 2, 2], Ranks = ["S", "A", "A"], Maps = [11, 15, 12], Material = [0, 6, 16, 3]}},
            // 「第三戦隊」第二小隊、鉄底海峡へ！
            {1018, new QuestSortie {Interval = Yearly9, MaxArray = [1, 1, 1, 1], Rank = "A", Maps = [51, 53, 54, 55], Material = [0, 0, 0, 5]}},
            // 【期間限定任務】「三十二駆」月次戦闘哨戒！
            {1022, new QuestSortie {Interval = Monthly, MaxArray = [1, 1, 1, 1], Rank = "A", Maps = [23, 41, 51, 71], Material = [0, 0, 0, 4]}},

            // 「演習」で練度向上！
            {303, new QuestPractice {Interval = Daily, Max = 3, Rank = "E", Material = [1, 0, 0, 0]}},
            // 「演習」で他提督を圧倒せよ！
            {304, new QuestPractice {Interval = Daily, Max = 5, Rank = "B", Material = [0, 0, 1, 0]}},
            // 大規模演習
            {302, new QuestPractice {Interval = Weekly, Max = 20, Rank = "B", Material = [0, 0, 2, 1]}},
            // 精鋭艦隊演習
            {311, new QuestPractice {Interval = Daily, Max = 7, Rank = "B", Material = [0, 2, 0, 0]}},
            // 秋季大演習
            {313, new QuestPractice {Interval = Daily, Max = 8, Rank = "B", Material = [0, 0, 0, 0]}},
            // 冬季大演習
            {314, new QuestPractice {Interval = Daily, Max = 8, Rank = "B", Material = [0, 3, 0, 0]}},
            // 春季大演習
            {315, new QuestPractice {Interval = Daily, Max = 8, Rank = "B", Material = [0, 0, 0, 0]}},
            // 給糧艦「伊良湖」の支援
            {318, new QuestPractice {Interval = Daily, Max = 3, Rank = "B", Material = [0, 2, 2, 0], AdjustCount = false}},
            // 夏季大演習
            {326, new QuestPractice {Interval = Daily, Max = 8, Rank = "B", Material = [0, 0, 0, 0]}},
            // 【節分任務:枡】節分演習！二〇二五
            {329, new QuestPractice {Interval = Daily, Max = 3, Rank = "B", Material = [1, 0, 0, 0], Shift = 2, Exp = 11}},
            // 空母機動部隊、演習始め！
            {330, new QuestPractice {Interval = Daily, Max = 4, Rank = "B", Material = [0, 0, 3, 0], Shift = 1}},
            // 「十八駆」演習！
            {337, new QuestPractice {Interval = Daily, Max = 3, Rank = "S", Material = [0, 0, 0, 3], Shift = 2}},
            // 「十九駆」演習！
            {339, new QuestPractice {Interval = Daily, Max = 3, Rank = "S", Material = [0, 0, 8, 3], Shift = 2}},
            // 小艦艇群演習強化任務
            {342, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 4, 4, 0], Shift = 1}},
            // 演習ティータイム！
            {345, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 5, 4, 0], Shift = 1}},
            // 最精鋭！主力オブ主力、演習開始！
            {346, new QuestPractice {Interval = Daily, Max = 4, Rank = "S", Material = [0, 4, 6, 0], Shift = 1}},
            // 「精鋭軽巡」演習！
            {348, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 3, 0, 3], Shift = 1}},
            // 精鋭「第七駆逐隊」演習開始！
            {350, new QuestPractice {Interval = Daily, Max = 3, Rank = "A", Material = [0, 4, 0, 0], Shift = 2}},
            // 「巡洋艦戦隊」演習！
            {353, new QuestPractice {Interval = Daily, Max = 5, Rank = "B", Material = [0, 0, 0, 0]}},
            // 「改装特務空母」任務部隊演習！
            {354, new QuestPractice {Interval = Daily, Max = 4, Rank = "S", Material = [0, 0, 0, 0], Shift = 1}},
            // 精鋭「第十五駆逐隊」第一小隊演習！
            {355, new QuestPractice {Interval = Daily, Max = 4, Rank = "S", Material = [0, 0, 0, 3], Shift = 1}},
            // 精鋭「第十九駆逐隊」演習！
            {356, new QuestPractice {Interval = Daily, Max = 3, Rank = "S", Material = [0, 0, 4, 3], Shift = 2}},
            // 「大和型戦艦」第一戦隊演習、始め！
            {357, new QuestPractice {Interval = Daily, Max = 3, Rank = "S", Material = [0, 0, 6, 0], Shift = 2}},
            // 特型初代「第十一駆逐隊」演習スペシャル！
            {362, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 0, 6, 4], Shift = 1}},
            // 【艦隊11周年記念任務】記念艦隊演習！
            {363, new QuestPractice {Interval = Daily, Max = 5, Rank = "A", Material = [0, 3, 4, 3]}},
            // 【梅雨限定任務】海上護衛隊、雨中演習！
            {367, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 0, 1, 0], Shift = 1}},
            // 「十六駆」演習！
            {368, new QuestPractice {Interval = Daily, Max = 3, Rank = "S", Material = [0, 3, 3, 0], Shift = 2}},
            // 【Xmas限定】聖夜の海上護衛隊演習
            {370, new QuestPractice {Interval = Weekly, Max = 5, Rank = "A", Material = [0, 3, 3, 2]}},
            // 春です！「春雨」、演習しますっ！
            {371, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 2, 7, 2], Shift = 1}},
            // 水上艦「艦隊防空演習」を実施せよ！
            {372, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 0, 10, 0], Shift = 1}},
            // 「フランス艦隊」演習！
            {373, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 0, 8, 0], Shift = 1}},
            // 【期間限定任務】「三十二駆」特別演習！
            {374, new QuestPractice {Interval = Daily, Max = 3, Rank = "S", Material = [0, 4, 8, 3], Shift = 2}},
            // 「第三戦隊」第二小隊、演習開始！
            {375, new QuestPractice {Interval = Daily, Max = 4, Rank = "S", Material = [0, 0, 10, 5], Shift = 1}},
            // 「第二駆逐隊(後期編成)」、練度向上！
            {377, new QuestPractice {Interval = Daily, Max = 4, Rank = "S", Material = [0, 0, 6, 3], Shift = 1}},
            // 新春限定！第六艦隊特別演習
            {378, new QuestPractice {Interval = Daily, Max = 4, Rank = "A", Material = [0, 4, 8, 0], Shift = 1}},
            // 【期間限定任務】「精鋭十一駆」特別演習！
            {379, new QuestPractice {Interval = Daily, Max = 4, Rank = "S", Material = [0, 4, 4, 2], Shift = 1}},

            // 「遠征」を3回成功させよう！
            {402, new QuestMission {Interval = Daily, Max = 3, Material = [0, 0, 1, 0]}},
            // 「遠征」を10回成功させよう！
            {403, new QuestMission {Interval = Daily, Max = 10, Material = [0, 0, 0, 0]}},
            // 大規模遠征作戦、発令！
            {404, new QuestMission {Interval = Weekly, Max = 30, Material = [0, 0, 3, 0]}},
            // 南方への輸送作戦を成功させよ！
            {410, new QuestMission {Interval = Weekly, Max = 1, Ids = [37, 38], Material = [0, 0, 0, 0]}},
            // 南方への鼠輸送を継続実施せよ！
            {411, new QuestMission {Interval = Weekly, Max = 6, Shift = 1, Ids = [37, 38], Material = [0, 0, 2, 1]}},
            // 輸送船団護衛を強化せよ！
            {424, new QuestMission {Interval = Monthly, Max = 4, Shift = 1, Ids = [5], Material = [0, 0, 0, 0]}},
            // 海上通商航路の警戒を厳とせよ！
            {426, new QuestMission {Interval = Quarterly, MaxArray = [1, 1, 1, 1], Ids = [3, 4, 5, 10], Material = [0, 0, 4, 0]}},
            // 近海に侵入する敵潜を制圧せよ！
            {428, new QuestMission {Interval = Quarterly, MaxArray = [2, 2, 2], Ids = [4, 101, 102], Material = [0, 0, 0, 3]}},
            // 特設護衛船団司令部、活動開始！
            {434, new QuestMission {Interval = Yearly2, MaxArray = [1, 1, 1, 1, 1], Ids = [3, 5, 100, 101, 9], Material = [0, 5, 0, 3]}},
            // 練習航海及び警備任務を実施せよ！
            {436, new QuestMission {Interval = Yearly3, MaxArray = [1, 1, 1, 1, 1], Ids = [1, 2, 3, 4, 10], Material = [0, 4, 0, 0]}},
            // 小笠原沖哨戒線の強化を実施せよ！
            {437, new QuestMission {Interval = Yearly5, MaxArray = [1, 1, 1, 1], Ids = [4, 104, 105, 110], Material = [0, 0, 7, 3], Shift = 1}},
            // 南西諸島方面の海上護衛を強化せよ！
            {438, new QuestMission {Interval = Yearly8, MaxArray = [1, 1, 1, 1], Ids = [100, 4, 9, 114], Material = [0, 0, 5, 4], Shift = 1}},
            // 兵站強化遠征任務【基本作戦】
            {439, new QuestMission {Interval = Yearly9, MaxArray = [1, 1, 1, 1], Ids = [5, 100, 11, 110], Material = [0, 0, 5, 4], Shift = 1}},
            // 兵站強化遠征任務【拡張作戦】
            {440, new QuestMission {Interval = Yearly9, MaxArray = [1, 1, 1, 1, 1], Ids = [41, 5, 40, 142, 46], Material = [0, 0, 0, 4]}},
            // 西方連絡作戦準備を実施せよ！
            {442, new QuestMission {Interval = Yearly2, MaxArray = [1, 1, 1, 1], Ids = [131, 29, 30, 133], Material = [0, 0, 6, 3]}},
            // 新兵装開発資材輸送を船団護衛せよ！
            {444, new QuestMission {Interval = Yearly3, MaxArray = [1, 1, 1, 1, 1], Ids = [5, 12, 9, 110, 11], Material = [0, 5, 6, 4]}},
            // 【艦隊11周年記念任務】資源輸出
            {449, new QuestMission {Interval = Weekly, MaxArray = [2, 2, 2], Ids = [5, 9, 11], Material = [0, 11, 0, 3], Shift = 2}},

            // 艦隊大整備！
            {503, new QuestSpec {Interval = Daily, Max = 5, Material = [0, 2, 0, 0]}},
            // 艦隊酒保祭り！
            {504, new QuestSpec {Interval = Daily, Max = 15, Material = [1, 0, 1, 0]}},

            // 新装備「開発」指令
            {605, new QuestSpec {Interval = Daily, Max = 1, Material = [1, 0, 1, 0]}},
            // 新造艦「建造」指令
            {606, new QuestSpec {Interval = Daily, Max = 1, Material = [0, 1, 1, 0]}},
            // 装備「開発」集中強化！
            {607, new QuestSpec {Interval = Daily, Max = 3, Shift = 1, Material = [0, 0, 2, 0]}},
            // 艦娘「建造」艦隊強化！
            {608, new QuestSpec {Interval = Daily, Max = 3, Shift = 1, Material = [1, 0, 2, 0]}},
            // 軍縮条約対応！
            {609, new QuestSpec {Interval = Daily, Max = 2, Material = [0, 1, 0, 0]}},
            // 装備の改修強化
            {619, new QuestSpec {Interval = Daily, Max = 1, Material = [0, 0, 0, 1]}},

            // 資源の再利用
            {613, new QuestSpec {Interval = Weekly, Max = 24, Material = [0, 0, 0, 0]}},
            // 精鋭「艦戦」隊の新編成
            {626, new QuestDestroyItem {Interval = Monthly, MaxArray = [2, 1], Ids = [20, 19], Material = [0, 0, 0, 0], AdjustCount = false}},
            // 機種転換
            {628, new QuestDestroyItem {Interval = Monthly, Max = 2, Ids = [21], Material = [0, 0, 0, 0], AdjustCount = false}},
            // 対空機銃量産
            {638, new QuestDestroyItem {Interval = Weekly, Max = 6, Types = [[21]], Material = [0, 0, 2, 1]}},
            // 主力「陸攻」の調達
            {643, new QuestDestroyItem {Interval = Quarterly, Max = 2, Ids = [20], Material = [0, 0, 2, 0], AdjustCount = false}},
            // 「洋上補給」物資の調達
            {645, new QuestDestroyItem {Interval = Monthly, Max = 1, Ids = [35], Material = [0, 0, 0, 0], AdjustCount = false}},
            // 工廠稼働！次期作戦準備！
            {653, new QuestDestroyItem {Interval = Quarterly, Max = 6, Ids = [4], Material = [0, 0, 0, 0], AdjustCount = false}},
            // 精鋭複葉機飛行隊の編成
            {654, new QuestDestroyItem {Interval = Yearly10, MaxArray = [1, 2], Ids = [242, 249], Material = [0, 0, 0, 0], AdjustCount = false}},
            // 工廠フル稼働！新兵装を開発せよ！
            {655, new QuestDestroyItem {Interval = Yearly11, MaxArray = [5, 5, 5, 5, 5], Types = [[1], [2], [3], [10], [8]], Material = [0, 0, 10, 5], AdjustCount = false}},
            // 新型兵装開発整備の強化
            {657, new QuestDestroyItem {Interval = Yearly9, MaxArray = [6, 5, 4], Types = [[1], [2], [5, 32]], Material = [0, 0, 10, 5]}},
            // 新型艤装の継続研究
            {663, new QuestDestroyItem {Interval = Quarterly, Max = 10, Types = [[3]], Material = [0, 0, 3, 0]}},
            // 装備開発力の整備
            {673, new QuestDestroyItem {Interval = Daily, Max = 4, Types = [[1]], Shift = 1, Material = [0, 0, 1, 0]}},
            // 工廠環境の整備
            {674, new QuestDestroyItem {Interval = Daily, Max = 3, Types = [[21]], Shift = 2, Material = [0, 1, 1, 0]}},
            // 運用装備の統合整備
            {675, new QuestDestroyItem {Interval = Quarterly, MaxArray = [6, 4], Types = [[6], [21]], Material = [0, 0, 0, 0]}},
            // 装備開発力の集中整備
            {676, new QuestDestroyItem {Interval = Weekly, MaxArray = [3, 3, 1], Types = [[2], [4], [30]], Material = [0, 1, 7, 0]}},
            // 継戦支援能力の整備
            {677, new QuestDestroyItem {Interval = Weekly, MaxArray = [4, 2, 3], Types = [[3], [10], [5, 32]], Material = [0, 5, 0, 0]}},
            // 主力艦上戦闘機の更新
            {678, new QuestDestroyItem {Interval = Quarterly, MaxArray = [3, 5], Ids = [19, 20], Material = [0, 0, 8, 0]}},
            // 対空兵装の整備拡充
            {680, new QuestDestroyItem {Interval = Quarterly, MaxArray = [4, 4], Types = [[21], [12, 13]], Material = [0, 0, 6, 0]}},
            // 航空戦力の再編増強準備
            {681, new QuestDestroyItem {Interval = Yearly1, MaxArray = [4, 4], Types = [[7], [8]], Material = [0, 0, 0, 4]}},
            // 戦時改修A型高角砲の量産
            {686, new QuestDestroyItem {Interval = Quarterly, MaxArray = [4, 1], Ids = [3, 121], Material = [0, 0, 0, 0]}},
            // 航空戦力の強化
            {688, new QuestDestroyItem {Interval = Quarterly, MaxArray = [3, 3, 3, 3], Types = [[6], [7], [8], [10]], Material = [0, 0, 0, 0]}},
            // 潜水艦強化兵装の量産
            {1103, new QuestDestroyItem {Interval = Yearly6, Max = 3, Ids = [125], Material = [0, 0, 5, 2], AdjustCount = false}},
            // 潜水艦電子兵装の量産
            {1104, new QuestDestroyItem {Interval = Yearly6, Max = 3, Ids = [106], Material = [0, 0, 5, 2], AdjustCount = false}},
            // 夏の格納庫整備＆航空基地整備
            {1105, new QuestDestroyItem {Interval = Yearly7, Max = 3, Types = [[47]], Material = [0, 0, 10, 0], AdjustCount = false}},
            // 【鋼材輸出】基地航空兵力を増備せよ！
            {1107, new QuestDestroyItem {Interval = Yearly9, MaxArray = [2, 2], Types = [[6], [8]], Material = [0, 0, 0, 3], AdjustCount = false}},
            // 【期間限定】Halloweenの南瓜、食べりゅ?
            {1119, new QuestDestroyItem {Interval = Weekly, MaxArray = [6, 6, 6], Types = [[1], [2], [10]], Material = [0, 0, 10, 0], AdjustCount = false}},
            // 【機種整理統合】新型戦闘機の量産計画
            {1120, new QuestDestroyItem {Interval = Yearly12, MaxArray = [4, 4, 4], Types = [[6], [7], [8]], Material = [7, 0, 7, 0], AdjustCount = false}},
            // 改良三座水上偵察機の増備
            {1123, new QuestDestroyItem {Interval = Yearly1, Max = 2, Ids = [82], Material = [0, 0, 0, 0], AdjustCount = false}},
            // 【高射装置量産】94式高射装置の追加配備
            {1138, new QuestDestroyItem {Interval = Yearly6, Max = 4, Ids = [120], Material = [0, 3, 0, 2], AdjustCount = false}},

            // 艦の「近代化改修」を実施せよ！
            {702, new QuestPowerUp {Interval = Daily, Max = 2, Material = [0, 1, 0, 0]}},
            // 「近代化改修」を進め、戦備を整えよ！
            {703, new QuestPowerUp {Interval = Weekly, Max = 15, Material = [1, 0, 2, 0]}},
            // 「駆逐艦」の改修工事を実施せよ！
            {714, new QuestPowerUp {Interval = Yearly11, Max = 2, Material = [0, 2, 2, 0], Types = [DD], ItemsTypes = [DD], Requires = 3}},
            // 続：「駆逐艦」の改修工事を実施せよ！
            {715, new QuestPowerUp {Interval = Yearly11, Max = 2, Material = [0, 2, 3, 2], Types = [DD], ItemsTypes = [CL], Requires = 3}},
            // 「軽巡」級の改修工事を実施せよ！
            {716, new QuestPowerUp {Interval = Yearly2, Max = 2, Material = [0, 2, 3, 0], Types = [CL, CLT, CT], ItemsTypes = [CL, CLT, CT], Requires = 3}},
            // 続：「軽巡」級の改修工事を実施せよ！
            {717, new QuestPowerUp {Interval = Yearly2, Max = 2, Material = [0, 0, 4, 2], Types = [CL, CLT, CT], ItemsTypes = [CA, CAV], Requires = 3}},
        };

        private readonly Dictionary<int, QuestCount> _countDict = [];

        public void SetNames(dynamic jsonMission, dynamic jsonEquiptype, dynamic jsonSlotitem)
        {
            var missionNames = new Dictionary<int, string>();
            foreach (var entry in jsonMission)
                missionNames[(int)entry.api_id] = entry.api_name;

            var equiptypeNames = new Dictionary<int, string>();
            foreach (var entry in jsonEquiptype)
                equiptypeNames[(int)entry.api_id] = entry.api_name;

            var slotitemNames = new Dictionary<int, string>();
            foreach (var entry in jsonSlotitem)
                slotitemNames[(int)entry.api_id] = entry.api_name;

            foreach (var spec in _questSpecs)
            {
                if (spec.Value is QuestMission mission && mission.Ids != null)
                    mission.Names = mission.Ids.Select(id => missionNames.TryGetValue(id, out var name) ? name : "").ToArray();

                if (spec.Value is QuestDestroyItem equiptype && equiptype.Types != null)
                    equiptype.Names = equiptype.Types.Select(types => types.Select(id => equiptypeNames.TryGetValue(id, out var name) ? name : "").Join(" ")).ToArray();

                if (spec.Value is QuestDestroyItem slotitem && slotitem.Ids != null)
                    slotitem.Names = slotitem.Ids.Select(id => slotitemNames.TryGetValue(id, out var name) ? name : "").ToArray();
            }
        }

        public QuestCount GetCount(int id)
        {
            if (_countDict.TryGetValue(id, out var value))
                return value;
            if (_questSpecs.TryGetValue(id, out var spec))
            {
                var nowArray = spec.MaxArray?.Select(x => 0).ToArray();
                return _countDict[id] = new QuestCount
                {
                    Id = id,
                    Now = 0,
                    NowArray = nowArray,
                    Spec = spec
                };
            }
            return new QuestCount { Spec = new QuestSpec { Interval = Other, Material = [], AdjustCount = false } };
        }

        public void Remove(int id)
        {
            _countDict.Remove(id);
        }

        public void Remove(QuestInterval interval)
        {
            foreach (var id in _countDict.Where(pair => pair.Value.Spec.Interval == interval).Select(pair => pair.Key).ToArray())
            {
                _countDict.Remove(id);
            }
        }

        public IEnumerable<QuestCount> NonZeroCountList => _countDict.Values.Where(c => c.Now > 0 || (c.NowArray?.Any(n => n > 0) ?? false));

        public void SetCountList(IEnumerable<QuestCount> questCountList)
        {
            if (questCountList == null)
                return;
            foreach (var count in questCountList)
            {
                count.Spec = _questSpecs[count.Id];
                count.AdjustNowArray();
                _countDict[count.Id] = count;
            }
        }
    }
}
