// Copyright (C) 2017 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Drawing;

namespace KancolleSniffer.Util
{
    /// <summary>
    /// カラーユニバーサルデザイン推奨配色セットの色を定義する。
    /// https://ja.wikipedia.org/wiki/カラーユニバーサルデザイン
    /// http://jfly.iam.u-tokyo.ac.jp/colorset/CUD_color_set_ver3_panel_2013.pdf
    /// </summary>
    public static class CUDColors
    {
        public static Color Red = Color.FromArgb(255, 40, 0);
        public static Color Orange = Color.FromArgb(255, 153, 0);
        public static Color Yellow = Color.FromArgb(250, 245, 0);
        public static Color Green = Color.FromArgb(53, 161, 107);
        public static Color LightGreen = Color.FromArgb(135, 231, 176);
        public static Color Blue = Color.FromArgb(0, 65, 255);
        public static Color LightGray = Color.FromArgb(200, 200, 203);
        public static Color Gray = Color.FromArgb(127, 135, 143);
    }
}
