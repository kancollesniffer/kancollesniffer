// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections;
using System.Globalization;
using System.Text;

namespace KancolleSniffer.Util
{
    public static class StringUtil
    {
        public static string Cat(string parent, string separator, string child)
        {
            if (string.IsNullOrEmpty(parent) || string.IsNullOrEmpty(separator) || string.IsNullOrEmpty(child))
                return parent + child;
            else
                return parent + separator + child;
        }

        public static StringBuilder Cat(this StringBuilder builder, string separator, string value)
        {
            if (builder.Length == 0 || string.IsNullOrEmpty(separator) || string.IsNullOrEmpty(value))
                return builder.Append(value);
            else
                return builder.Append(separator).Append(value);
        }

        public static string ToS(double value, int digits = 1)
        {
            // MidpointRounding.ToNegativeInfinity は .NET Core 3.0 から
            var pow = Math.Pow(10, digits);
            return (Math.Floor(value * pow) / pow).ToString($"F{digits}");
        }

        public static string ToS(TimeSpan span, bool sohwSeconds = true)
        {
            return (span.Days > 0 ? span.Days + "日 " : "") + span.ToString(sohwSeconds ? "hh\\:mm\\:ss" : "hh\\:mm", CultureInfo.InvariantCulture);
        }

        public static string Unc(string dbfile)
        {
            if (dbfile.StartsWith("\\\\"))
                return "\\\\" + dbfile;
            return dbfile;
        }

        public static string GenerateErrorDetails(string detail, Exception e)
        {
            var sb = new StringBuilder(detail).Append("\r\n\r\n");
            if (e.Data.Count > 0)
            {
                foreach (DictionaryEntry entry in e.Data)
                    sb.Append(entry.Key).Append("\t : ").Append(entry.Value).Append("\r\n");
                sb.Append("\r\n");
            }
            sb.Append(e.ToString());
            return sb.ToString();
        }
    }
}
