// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using DynaJson;

namespace KancolleSniffer.Util
{
    public static class EnumerableUtil
    {
        public static JsonObject JsonArray(params object[] objs) => new(objs);

        public static bool ParamsEqual<TSource>(this IEnumerable<TSource> source, params TSource[] another)
        {
            return source.SequenceEqual(another);
        }

        public static IEnumerable<TResult> Repeat<TResult>(int count, Func<int, TResult> func)
        {
            return Enumerable.Range(0, count).Select(i => func(i));
        }

        public static string Join<T>(this IEnumerable<T> values, string separator)
        {
            return string.Join(separator, values);
        }

        public static T Find<T>(this T[] array, Predicate<T> match)
        {
            return Array.Find(array, match);
        }

        public static int IndexOf<T>(this T[] array, T value)
        {
            return Array.IndexOf(array, value);
        }

        public static int FindIndex<T>(this T[] array, Predicate<T> match)
        {
            return Array.FindIndex(array, match);
        }
    }
}
