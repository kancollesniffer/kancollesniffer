// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KancolleSniffer.Util
{
    public static class Const
    {
        public static readonly string[] FleetNamesLong = ["第一艦隊", "第二艦隊", "第三艦隊", "第四艦隊"];
        public static readonly string[] FleetNames = ["第一", "第二", "第三", "第四"];
        public static readonly string[] AirCorpsNames = ["第一", "第二", "第三"];
        public static readonly string[] ResultRankNames = ["完全S", "勝利S", "勝利A", "勝利B", "敗北C", "敗北D", "敗北E"];
        public static readonly string[] SupportNames = ["", "空支援", "砲支援", "雷支援", "潜支援"];
        public static readonly string[] FormationStateNames = ["同航戦", "反航戦", "T字有利", "T字不利"];
        public static readonly string[] AirControlLevelNames = ["拮抗", "確保", "優勢", "劣勢", "喪失", "不明"];
        public static readonly string[] AirControlLevelNamesForMain = ["拮抗", "確保", "優勢", "劣勢", "喪失", "制空"];
        public static readonly string[] AirControlLevelNamesForList = ["制空拮抗", "制空確保", "航空優勢", "航空劣勢", "制空喪失", ""];
        public static readonly Color[] AirControlLevelColors =
            [Control.DefaultForeColor, CUDColors.Blue, CUDColors.Green, CUDColors.Orange, CUDColors.Red, Control.DefaultForeColor];
        public static readonly int AirControlLevelNull = AirControlLevelNames.Length - 1;
        public static readonly string[] MaterialNamesHalf = ["燃料", "弾薬", "鋼材", "ボーキ"];
        public static readonly string[] MaterialNames = ["燃料", "弾薬", "鋼材", "ボーキサイト", "高速建造材", "高速修復材", "開発資材", "改修資材"];
        public static readonly string[] MaterialNamesShort = ["燃", "弾", "鋼", "ボ", "建造", "修復", "開発", "改修"];
        public static readonly string[] ItemLevelChars = ["", "➊", "➋", "➌", "➍", "➎", "➏", "➐", "➑", "➒", "➓"];
        public const int Gauge721 = 7210;
        public const int Gauge722 = 7220;
        public const int Gauge731 = 7310;
        public const int Gauge732 = 7320;
        public const int Gauge751 = 7510;
        public const int Gauge752 = 7520;
        public const int Gauge753 = 7530;
        public static readonly int[] MultiGaugeMaps = [Gauge721, Gauge722, Gauge731, Gauge732, Gauge751, Gauge752, Gauge753];
        public static readonly Dictionary<int, Dictionary<int, int>> MultiGaugeTranslator = new()
        {
            {
                72, new()
                {
                    { 7, Gauge721 },
                    { 15, Gauge722 }
                }
            },
            {
                73, new()
                {
                    { 5, Gauge731 },
                    { 8, Gauge731 },
                    { 18, Gauge732 },
                    { 23, Gauge732 },
                    { 24, Gauge732 },
                    { 25, Gauge732 }
                }
            },
            {
                75, new()
                {
                    { 11, Gauge751 },
                    { 19, Gauge752 },
                    { 24, Gauge753 },
                    { 25, Gauge753 }
                }
            }
        };
        public static readonly string[] NotificationNames =
        [
            "遠征終了", "入渠終了", "建造完了", "艦娘数超過", "装備数超過",
            "大破警告", "泊地修理20分経過", "泊地修理進行", "泊地修理完了", "疲労回復", "任務達成", "基地航空隊疲労"
        ];
        public static readonly string[] NotificationSoundFiles =
        [
            "ensei.mp3", "nyuukyo.mp3", "kenzou.mp3", "kanmusu.mp3", "soubi.mp3",
            "taiha.mp3", "20min.mp3", "syuuri.mp3", "syuuri2.mp3", "hirou.mp3", "ninmu.mp3", "kichi_hirou.mp3"
        ];
        public static readonly string[] NotificationNamesPreliminary = ["遠征終了", "入渠終了", "建造完了", "泊地修理20分経過", "疲労回復"];
        public static readonly string[] NotificationNamesContinual = ["遠征終了", "入渠終了"];
        public static readonly string[] NotificationNamesRepeatingStop = ["遠征終了", "入渠終了", "疲労回復", "泊地修理", "大破警告"];
        public static readonly string[] NotificationModeNames = ["", "[リピート] ", "[継続] ", "[予告] "];
        public static readonly string[] ShipListPanelNamesPrimary =
            ["全艦", "A", "B", "C", "D", "分類", "修復", "P", "装備", "艦隊", "基地", "対空", "戦況", "月次", "情報"];
        public static readonly string[] ShipListPanelNamesSecondary =
            ["全艦", "A", "B", "C", "D",         "修復", "P", "装備", "艦隊", "基地", "対空", "戦況", "月次", "情報"];
        public static readonly char[] ShipListPanelKeysPrimary =   ['Z', 'A', 'B', 'C', 'D', 'G', 'R', 'P', 'W', 'X', 'L', 'Y', 'S', 'M', 'I'];
        public static readonly char[] ShipListPanelKeysSecondary = ['Z', 'A', 'B', 'C', 'D',      'R', 'P', 'W', 'X', 'L', 'Y', 'S', 'M', 'I'];
        public static readonly string[] ShipListShipStatusModes = ["全艦", "A", "B", "C", "D", "P"];
        public static readonly string[] ShipListGroupNames =              ["A", "B", "C", "D"];
        public const string CacheFileName = @"KancolleSniffer.db";
        public static readonly Dictionary<string, string> TableCsvNames = new()
        {
            { @"Mission", @"遠征報告書.csv" },
            { @"Battle", @"海戦・ドロップ報告書.csv" },
            { @"Material", @"資材ログ.csv" },
            { @"CreateItem", @"開発報告書.csv" },
            { @"CreateShip", @"建造報告書.csv" },
            { @"RemodelSlot", @"改修報告書.csv" },
            { @"Achievement", @"戦果.csv" }
        };
        public static readonly Encoding CsvEncoding = Encoding.GetEncoding("Shift_JIS");
    }
}
