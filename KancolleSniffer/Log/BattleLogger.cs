// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using KancolleSniffer.Model;
using KancolleSniffer.Util;

namespace KancolleSniffer.Log
{
    public class BattleLogger
    {
        private readonly ItemInfo _itemInfo;
        private readonly BattleInfo _battleInfo;
        private readonly Action<LogType, string, string> _writer;
        private readonly Dictionary<int, string> _mapName = [];
        private readonly CellData _cell = new();

        public BattleLogger(ItemInfo itemInfo, BattleInfo battleInfo, Action<LogType, string, string> writer)
        {
            _itemInfo = itemInfo;
            _battleInfo = battleInfo;
            _writer = writer;
        }

        public void InspectMapInfoMaster(dynamic json)
        {
            foreach (var entry in json)
                _mapName[(int)entry.api_id] = entry.api_name;
        }

        private class CellData
        {
            public bool Start;
            public bool Boss;
            public int Id;
            public int Area;
            public int Map;
            public int Cell;

            public void Set(dynamic json)
            {
                Area = (int)json.api_maparea_id;
                Map  = (int)json.api_mapinfo_no;
                Cell = (int)json.api_no;
                Boss = (int)json.api_event_id == 5;
                Id = Area * 10 + Map;
            }
        }

        public void InspectMapStart(dynamic json)
        {
            _cell.Start = true;
            _cell.Set(json);
        }

        public void InspectMapNext(dynamic json)
        {
            _cell.Set(json);
            if (!json.api_destruction_battle())
                return;
            WriteLog(null);
            _cell.Start = false;
        }

        public void InspectBattleResult(dynamic result)
        {
            WriteLog(result);
            _cell.Start = false;
        }

        private void WriteLog(dynamic result)
        {
            var log = CreateLog(result);
            _writer(LogType.Battle, log,
                "日付,海域,経路,ボス,ランク,艦隊行動,味方陣形,敵陣形,敵艦隊,ドロップ艦種,ドロップ艦娘," +
                "味方艦1,味方艦1HP,味方艦2,味方艦2HP,味方艦3,味方艦3HP,味方艦4,味方艦4HP,味方艦5,味方艦5HP,味方艦6,味方艦6HP," +
                "敵艦1,敵艦1HP,敵艦2,敵艦2HP,敵艦3,敵艦3HP,敵艦4,敵艦4HP,敵艦5,敵艦5HP,敵艦6,敵艦6HP," +
                "味方制空値,敵制空値,制空状態,マップ"
            );
        }

        private string CreateLog(dynamic result)
        {
            var fShips = GenerateShipList(_battleInfo.Result.Friend, s => $"{s.Spec.Name}(Lv{s.Level})");
            var eShips = GenerateShipList(_battleInfo.Result.Enemy, s => $"{s.Spec.Name}");
            var boss = "";
            if (_battleInfo.BattleState == BattleState.AirRaid)
                boss = "基地空襲";
            else if (_cell.Start && _cell.Boss)
                boss = "出撃&ボス";
            else if (_cell.Start)
                boss = "出撃";
            else if (_cell.Boss)
                boss = "ボス";
            var dropType = CreateDropType(result);
            var dropName = CreateDropName(result);
            var enemyName = result?.api_enemy_info.api_deck_name ?? "";
            var rank = result?.api_win_rank ?? _battleInfo.ResultRank;
            var fp = _battleInfo.FighterPower;
            var fPower = fp.Diff ? fp.RangeString : fp.Min.ToString();
            return string.Join(",",
                _mapName[_cell.Id],
                _cell.Cell, boss,
                rank,
                BattleFormationName(_battleInfo.Formation[2]),
                FormationName(_battleInfo.Formation[0]),
                FormationName(_battleInfo.Formation[1]),
                enemyName,
                dropType, dropName,
                fShips.Join(","),
                eShips.Join(","),
                fPower, _battleInfo.EnemyFighterPower.CsvLoggerAirCombatText,
                AirControlLevelName(_battleInfo.AirControlLevel),
                $"{_cell.Area}-{_cell.Map}");
        }

        private static string CreateDropType(dynamic result)
        {
            if (result == null)
                return "";
            var type = result.api_get_ship() ? (string)result.api_get_ship.api_ship_type : "";
            if (!result.api_get_useitem())
                return type;
            return type == "" ? "アイテム" : type + "+アイテム";
        }

        private string CreateDropName(dynamic result)
        {
            if (result == null)
                return "";
            var name = result.api_get_ship() ? (string)result.api_get_ship.api_ship_name : "";
            if (!result.api_get_useitem())
                return name;
            var itemName = _itemInfo.GetUseItemName((int)result.api_get_useitem.api_useitem_id);
            return name == "" ? itemName : name + "+" + itemName;
        }

        private static IEnumerable<string> GenerateShipList(BattleInfo.BattleResult.Combined fleet, Func<ShipStatus, string> toName)
        {
            fleet = FillEmpty(fleet);
            if (fleet.Guard.Length > 0)
            {
                return fleet.Main.Zip(fleet.Guard, (main, guard) =>
                {
                    if (main.Empty && guard.Empty)
                        return ",";
                    var name = "";
                    var hp = "";
                    if (!main.Empty)
                    {
                        name = toName(main);
                        hp = main.DisplayHp;
                    }
                    name += "・";
                    hp += "・";
                    if (!guard.Empty)
                    {
                        name += toName(guard);
                        hp += guard.DisplayHp;
                    }
                    return name + "," + hp;
                }).ToList();
            }
            var ships = fleet.Main;
            if (fleet.Main.Length > 6)
            {
                var result = new List<string>();
                for (var i = 0; i < 12 - ships.Length; i++)
                {
                    var ship = fleet.Main[i];
                    result.Add($"{toName(ship)},{ship.DisplayHp}");
                }
                for (var i = 0; i < ships.Length - 6; i++)
                {
                    var s1 = ships[12 - ships.Length + i];
                    var s2 = ships[6 + i];
                    result.Add($"{toName(s1)}・{toName(s2)},{s1.DisplayHp}・{s2.DisplayHp}");
                }
                return result;
            }
            return ships.Select(ship => ship.Empty ? "," : $"{toName(ship)},{ship.DisplayHp}");
        }

        private static BattleInfo.BattleResult.Combined FillEmpty(BattleInfo.BattleResult.Combined fleet)
        {
            return new BattleInfo.BattleResult.Combined
            {
                Main = FillEmpty(fleet.Main),
                Guard = FillEmpty(fleet.Guard)
            };
        }

        private static readonly ShipStatus[] Padding = Enumerable.Repeat(new ShipStatus(), ShipInfo.MemberCount).ToArray();

        private static ShipStatus[] FillEmpty(ShipStatus[] ships)
        {
            return ships.Length is > ShipInfo.MemberCount or 0 ? ships : ships.Concat(Padding).Take(ShipInfo.MemberCount).ToArray();
        }

        private string FormationName(dynamic f)
        {
            if (f is string) // 連合艦隊のときは文字列
                f = int.Parse(f);
            return (int)f switch
            {
                1 => "単縦陣",
                2 => "複縦陣",
                3 => "輪形陣",
                4 => "梯形陣",
                5 => "単横陣",
                6 => "警戒陣",
                11 => "第一警戒航行序列",
                12 => "第二警戒航行序列",
                13 => "第三警戒航行序列",
                14 => "第四警戒航行序列",
                _ => "単縦陣",
            };
        }

        private static string BattleFormationName(int f)
        {
            return f switch
            {
                1 => "同航戦",
                2 => "反航戦",
                3 => "Ｔ字戦(有利)",
                4 => "Ｔ字戦(不利)",
                _ => "同航戦",
            };
        }

        private string AirControlLevelName(int level)
        {
            return level switch
            {
                0 => "航空均衡",
                1 => "制空権確保",
                2 => "航空優勢",
                3 => "航空劣勢",
                4 => "制空権喪失",
                _ => "",
            };
        }
    }
}
