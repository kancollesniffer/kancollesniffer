// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2023 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using KancolleSniffer.Model;
using KancolleSniffer.Util;

namespace KancolleSniffer.Log
{
    public class LogProcessor
    {
        private readonly MaterialCount[] _materialCount;
        private readonly BattleLogProcessor _battleLogProcessor;

        public LogProcessor(MaterialCount[] materialCount = null, Dictionary<string, string> mapDictionary = null)
        {
            _materialCount = materialCount ?? [];
            _battleLogProcessor = new BattleLogProcessor(mapDictionary);
        }

        public class Processor
        {
            protected virtual int Fields { get; }
            public bool Skip { get; protected set; }

            public Processor()
            {
            }

            public Processor(int fields)
            {
                Fields = fields;
            }

            public virtual string[] Process(string[] data)
            {
                Skip = data.Length != Fields;
                return Skip ? null : data;
            }
        }

        private class MissionProcessor : Processor
        {
            protected override int Fields { get; } = 11;

            public override string[] Process(string[] data)
            {
                return data.Concat(["0"]).Take(Fields).ToArray();
            }
        }

        private class MaterialProcessor : Processor
        {
            protected override int Fields { get; } = 9;

            public override string[] Process(string[] data)
            {
                if (data.Length >= Fields)
                    Array.Resize(ref data, Fields);
                return base.Process(data);
            }
        }

        private class AchievementProcessor : Processor
        {
            protected override int Fields { get; } = 4;

            public override string[] Process(string[] data)
            {
                return base.Process(data.Length == 3 ? data.Concat([""]).ToArray() : data);
            }
        }

        public IEnumerable<string> Process(IEnumerable<string> lines, string table, DateTime from, DateTime to, bool number, DateTime now = default)
        {
            var currentMaterial = table == "Material" && !number;
            var processor = DecideProcessor(table);
            var delimiter = "";
            foreach (var line in lines)
            {
                var data = line.Split(',');
                var date = ParseDateTime(data[0]);
                if (date == default)
                    continue;
                if (to < date)
                    yield break;
                if (date < from)
                    continue;
                data[0] = Logger.FormatDateTime(date);
                var entries = processor.Process(data);
                if (processor.Skip)
                    continue;
                var result = number
                    ? delimiter + "[" + JavaScriptTicks(date) + "," + entries.Skip(1).Join(",") + "]"
                    : delimiter + "[\"" + entries.Join("\",\"") + "\"]";
                delimiter = ",\n";
                yield return result;
            }
            if (now == default)
                now = DateTime.Now;
            if (currentMaterial) // 資材の現在値を出力する
                yield return delimiter + "[\"" + Logger.FormatDateTime(now) + "\",\"" + _materialCount.Select(c => c.Now).Join("\",\"") + "\"]";
        }

        private Processor DecideProcessor(string table)
        {
            return table switch
            {
                "Mission" => new MissionProcessor(),
                "RemodelSlot" => new Processor(15),
                "Battle" => _battleLogProcessor,
                "CreateItem" => new Processor(9),
                "CreateShip" => new Processor(12),
                "Material" => new MaterialProcessor(),
                "Achievement" => new AchievementProcessor(),
                _ => new Processor(),
            };
        }

        private DateTime ParseDateTime(string dateTime)
        {
            if (DateTime.TryParseExact(dateTime, Logger.DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out var date))
            {
                return date;
            }
            // システムが和暦に設定されていて和暦が出力されてしまったケースを救う
            if (dateTime[2] == '-')
            {
                if (!int.TryParse(dateTime.Substring(0, 2), out var year))
                    return default;
                dateTime = 1988 + year + dateTime.Substring(2);
                return DateTime.TryParseExact(dateTime, Logger.DateTimeFormat, CultureInfo.InvariantCulture,
                    DateTimeStyles.AssumeLocal, out date) ? date : default;
            }
            return DateTime.TryParse(dateTime, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal, out date) ? date : default;
        }

        private long JavaScriptTicks(DateTime date) =>
            (date.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks) / TimeSpan.TicksPerMillisecond;
    }
}
