// Copyright (C) 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Log
{
    [Flags]
    public enum LogType
    {
        None = 0,
        Mission = 1,
        Battle = 1 << 1,
        Material = 1 << 2,
        CreateItem = 1 << 3,
        CreateShip = 1 << 4,
        RemodelSlot = 1 << 5,
        Achievement = 1 << 6,
        All = (1 << 7) - 1
    }

    public class Logger
    {
        private LogType _logType;
        private readonly ShipInfo _shipInfo;
        private readonly ItemInfo _itemInfo;
        private Action<LogType, DateTime, string, string> _writer;
        private Func<DateTime> _nowFunc;
        public const string DateTimeFormat = @"yyyy\-MM\-dd HH\:mm\:ss";
        public const string AchievementHeader = "日付,経験値,EO,種類";
        private dynamic _basic;
        private int _kdockId;
        private DateTime _prevTime;
        private int[] _currentMaterial = new int[MaterialInfo.MaterialLength];
        private int _materialLogInterval = 10;
        private int _lastExp = -1;
        private DateTime _lastDate;
        private DateTime _endOfMonth;
        private DateTime _nextDate;
        private readonly BattleLogger _battleLogger;

        public void SetLogConfig(LogConfig config)
        {
            _logType = config.On ? LogType.All : LogType.None;
            _materialLogInterval = config.MaterialLogInterval;
            _writer = new LogWriter(config.OutputDir, useCache: config.UseCache).Write;
        }

        public static long ToSeconds(DateTime dateTime)
        {
            return dateTime.Ticks / (TimeSpan.TicksPerMillisecond * 1000);
        }

        public static string FormatDateTime(DateTime date)
        {
            return date.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
        }

        public Logger(ShipInfo shipInfo, ItemInfo itemInfo, BattleInfo battleInfo)
        {
            _shipInfo = shipInfo;
            _itemInfo = itemInfo;
            _battleLogger = new BattleLogger(itemInfo, battleInfo, WriteNow);
            _writer = new LogWriter().Write;
            _nowFunc = () => DateTime.Now;
        }

#if DEBUG
        public void EnableLog(LogType type)
        {
            _logType = type;
        }

        public void SetWriter(Action<LogType, DateTime, string, string> writer, Func<DateTime> nowFunc)
        {
            _writer = writer;
            _nowFunc = nowFunc;
        }
#endif

        private string AchievementLogString(int rate, string type = "") => $"{_lastExp},{rate},{type}";
        private string BasicAchievementLogString(int exp) => $"{exp},0,";

        private void WriteNow(LogType logType, string log, string header)
        {
            Write(logType, _nowFunc(), log, header);
        }

        private void Write(LogType logType, DateTime time, string log, string header)
        {
            _writer(logType, time, FormatDateTime(time) + "," + log, header);
        }

        public void FlashLog()
        {
            FlashAchievementLog();
        }

        public void InspectMapInfoMaster(dynamic json)
        {
            _battleLogger.InspectMapInfoMaster(json);
        }

        public void InspectMissionResult(dynamic json)
        {
            var r = (int)json.api_clear_result;
            var resStr = r == 2 ? "大成功" : r == 1 ? "成功" : "失敗";
            var material = new int[8];
            if (r != 0)
                ((int[])json.api_get_material).CopyTo(material, 0);
            for (var i = 0; i < 2; i++)
            {
                var attr = "api_get_item" + (i + 1);
                if (!json.IsDefined(attr))
                    continue;
                var count = (int)json[attr].api_useitem_count;
                var flag = ((int[])json.api_useitem_flag)[i];
                switch (flag)
                {
                    case 1:
                        material[(int)Material.Bucket] = count;
                        break;
                    case 2:
                        material[(int)Material.Burner + 2] = count; // 高速建造材と開発資材が反対なのでいつか直す
                        break;
                    case 3:
                        material[(int)Material.Nail - 2] = count;
                        break;
                    case 4 when (int)json[attr].api_useitem_id == 4:
                        material[(int)Material.Screw] = count;
                        break;
                }
            }
            if ((_logType & LogType.Mission) != 0)
            {
                WriteNow(LogType.Mission,
                    string.Join(",", resStr, json.api_quest_name, material.Join(",")),
                    "日付,結果,遠征,燃料,弾薬,鋼材,ボーキ,開発資材,高速修復材,高速建造材,改修資材");
            }
        }

        public void InspectMapStart(dynamic json)
        {
            if ((_logType & LogType.Battle) != 0)
                _battleLogger.InspectMapStart(json);
            if ((_logType & LogType.Material) != 0)
                WriteMaterialLog(_nowFunc());
        }

        public void InspectMapNext(dynamic json)
        {
            if ((_logType & LogType.Achievement) != 0 && json.api_get_eo_rate() && (int)json.api_get_eo_rate != 0)
                WriteNow(LogType.Achievement, AchievementLogString((int)json.api_get_eo_rate), AchievementHeader);
            if ((_logType & LogType.Battle) != 0 || json.api_destruction_flag() && (int)json.api_destruction_flag == 1)
                _battleLogger.InspectMapNext(json);
        }

        public void InspectAirRaid()
        {
            _battleLogger.InspectBattleResult(null);
        }

        public void InspectClearItemGet(dynamic json, QuestInterval interval)
        {
            if ((_logType & LogType.Achievement) == 0)
                return;
            if (!json.api_bounus())
                return;
            foreach (var entry in json.api_bounus)
            {
                if (entry == null)
                    continue;

                if (entry.api_type != 18)
                    continue;
                WriteNow(LogType.Achievement, AchievementLogString((int)entry.api_count, "Quest" + interval), AchievementHeader);
                break;
            }
        }

        public void InspectBattleResult(dynamic result)
        {
            if ((_logType & LogType.Achievement) != 0 && result.api_get_exmap_rate())
            {
                var rate = result.api_get_exmap_rate is string ? int.Parse(result.api_get_exmap_rate) : (int)result.api_get_exmap_rate;
                if (rate != 0)
                    WriteNow(LogType.Achievement, AchievementLogString(rate), AchievementHeader);
            }
            if ((_logType & LogType.Battle) != 0)
                _battleLogger.InspectBattleResult(result);
        }

        public void InspectBasic(dynamic json)
        {
            _basic = json;
            if ((_logType & LogType.Achievement) == 0)
                return;
            var now = _nowFunc();
            var exp = (int)json.api_experience;
            var isNewMonth = _endOfMonth == DateTime.MinValue || now.CompareTo(_endOfMonth) >= 0;
            var isNewDate  = _nextDate   == DateTime.MinValue || now.CompareTo(_nextDate)   >= 0;
            if (isNewDate || isNewMonth)
            {
                if (_lastDate != DateTime.MinValue)
                    Write(LogType.Achievement, _lastDate, AchievementLogString(0), AchievementHeader);
                Write(LogType.Achievement, now, BasicAchievementLogString(exp), AchievementHeader);
                if (isNewMonth)
                {
                    _endOfMonth = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month), 22, 0, 0);
                    if (_endOfMonth.CompareTo(now) <= 0)
                    {
                        var days = _endOfMonth.Month == 12
                            ? DateTime.DaysInMonth(_endOfMonth.Year + 1, 1)
                            : DateTime.DaysInMonth(_endOfMonth.Year, _endOfMonth.Month);
                        _endOfMonth = _endOfMonth.AddDays(days);
                    }
                }
                _nextDate = new DateTime(now.Year, now.Month, now.Day, 2, 0, 0);
                if (now.Hour >= 2)
                    _nextDate = _nextDate.AddDays(1);
                if (_nextDate.Day == 1)
                    _nextDate = _nextDate.AddDays(1);
            }
            _lastDate = now;
            _lastExp = exp;
        }

        private void FlashAchievementLog()
        {
            if ((_logType & LogType.Achievement) == 0)
                return;
            if (_lastDate != DateTime.MinValue)
                Write(LogType.Achievement, _lastDate, AchievementLogString(0), AchievementHeader);
        }

        public void InspectCreateItem(string request, dynamic json)
        {
            if ((_logType & LogType.CreateItem) == 0)
                return;
            var values = HttpUtility.ParseQueryString(request);
            foreach (var spec in CreateSpecList(json))
            {
                WriteNow(LogType.CreateItem,
                    string.Join(",", spec.Name, spec.TypeName, values["api_item1"], values["api_item2"], values["api_item3"], values["api_item4"],
                        Secretary(), _basic.api_level),
                    "日付,開発装備,種別,燃料,弾薬,鋼材,ボーキ,秘書艦,司令部Lv");
            }
        }

        private IEnumerable<ItemSpec> CreateSpecList(dynamic json)
        {
            var fail = new ItemSpec
            {
                Name = "失敗",
                TypeName = ""
            };
            if (json.api_get_items())
            {
                return ((dynamic[])json.api_get_items).Select(entry =>
                    (int)entry.api_slotitem_id != -1 ? _itemInfo.GetSpecByItemId((int)entry.api_slotitem_id) : fail);
            }
            return [json.api_slot_item() ? _itemInfo.GetSpecByItemId((int)json.api_slot_item.api_slotitem_id) : fail];
        }

        public void InspectCreateShip(string request)
        {
            var values = HttpUtility.ParseQueryString(request);
            _kdockId = int.Parse(values["api_kdock_id"]);
        }

        public void InspectKDock(dynamic json)
        {
            if ((_logType & LogType.CreateShip) == 0 || _basic == null || _kdockId == 0)
                return;
            var kdock = ((dynamic[])json).FirstOrDefault(e => e.api_id == _kdockId);
            if (kdock == null)
                return;
            var material = EnumerableUtil.Repeat(5, i => (int)kdock["api_item" + (i+1)]).ToArray();
            var ship = _shipInfo.GetSpec((int)kdock.api_created_ship_id);
            var avail = ((dynamic[])json).Count(e => (int)e.api_state == 0);
            WriteNow(LogType.CreateShip,
                string.Join(",", material.FirstOrDefault() >= 1500 ? "大型艦建造" : "通常艦建造", ship.Name, ship.ShipTypeName, material.Join(","),
                    avail, Secretary(), _basic.api_level),
                "日付,種類,名前,艦種,燃料,弾薬,鋼材,ボーキ,開発資材,空きドック,秘書艦,司令部Lv");
            _kdockId = 0;
        }

        private string Secretary()
        {
            var ship = _shipInfo.Fleets[0].Ships[0];
            return ship.Spec.Name + "(" + ship.Level + ")";
        }

        public void InspectMaterial(dynamic json)
        {
            if ((_logType & LogType.Material) == 0)
                return;
            foreach (var e in json)
                _currentMaterial[(int)e.api_id - 1] = (int)e.api_value;
            var now = _nowFunc();
            if (now - _prevTime < TimeSpan.FromMinutes(_materialLogInterval))
                return;
            WriteMaterialLog(now);
        }

        private void WriteMaterialLog(DateTime now)
        {
            _prevTime = now;
            Write(LogType.Material, now, _currentMaterial.Join(","), "日付,燃料,弾薬,鋼材,ボーキ,高速建造材,高速修復材,開発資材,改修資材");
        }

        public void SetCurrentMaterial(int[] material)
        {
            _currentMaterial = material;
        }

        public void InspectRemodelSlot(string request, dynamic json)
        {
            if ((_logType & LogType.RemodelSlot) == 0)
                return;
            var values = HttpUtility.ParseQueryString(request);
            var id = int.Parse(values["api_slot_id"]);
            var name = _itemInfo.GetName(id);
            var level = _itemInfo.GetStatus(id).Level;
            var success = (int)json.api_remodel_flag == 1 ? "○" : "×";
            var certain = int.Parse(values["api_certain_flag"]) == 1 ? "○" : "";
            var useName = "";
            var useNum = "";
            if (json.api_use_slot_id())
            {
                var use = (int[])json.api_use_slot_id;
                useName = _itemInfo.GetName(use[0]);
                useNum = use.Length.ToString();
            }
            var after = (int[])json.api_after_material;
            var diff = new int[after.Length];
            for (var i = 0; i < after.Length; i++)
                diff[i] = _currentMaterial[i] - after[i];
            var ship1 = Secretary();
            var ship2 = "";
            var ships = _shipInfo.Fleets[0].Ships;
            if (ships.Length > 1)
                ship2 = ships[1].Spec.Name + "(" + ships[1].Level + ")";
            WriteNow(LogType.RemodelSlot,
                string.Join(",", name, level, success, certain, useName, useNum,
                    diff[(int)Material.Fuel], diff[(int)Material.Bullet], diff[(int)Material.Steel], diff[(int)Material.Bauxite],
                    diff[(int)Material.Nail], diff[(int)Material.Screw], ship1, ship2),
                "日付,改修装備,レベル,成功,確実化,消費装備,消費数,燃料,弾薬,鋼材,ボーキ,開発資材,改修資材,秘書艦,二番艦");
        }
    }

    public class LogWriter
    {
        private readonly IFile _file;
        private readonly string _outputDir;
        private readonly bool _useCache;

        public interface IFile
        {
            string ReadAllText(string path);
            void AppendAllText(string path, string text);
            void Delete(string path);
            bool Exists(string path);
        }

        private class FileWrapper : IFile
        {
            // Shift_JISでないとExcelで文字化けする
            public string ReadAllText(string path) => File.ReadAllText(path, Const.CsvEncoding);

            public void AppendAllText(string path, string text)
            {
                File.AppendAllText(path, text, Const.CsvEncoding);
            }

            public void Delete(string path)
            {
                File.Delete(path);
            }

            public bool Exists(string path) => File.Exists(path);
        }

        public LogWriter(string outputDir = null, IFile file = null, bool useCache = false)
        {
            _outputDir = outputDir ?? AppInfo.BaseDir;
            _file = file ?? new FileWrapper();
            _useCache = useCache;
        }

        public void Write(LogType logType, DateTime time, string log, string header)
        {
            var table = logType.ToString();
            var csv = Path.Combine(_outputDir, Const.TableCsvNames[table]);
            var tmp = csv.Replace(".csv", ".tmp");
            if (_file.Exists(tmp))
            {
                try
                {
                    _file.AppendAllText(csv, _file.ReadAllText(tmp));
                    _file.Delete(tmp);
                }
                catch
                {
                }
            }

            var csvOutput = _file.Exists(csv) ? log : header + "\r\n" + log;
            try
            {
                _file.AppendAllText(csv, csvOutput + "\r\n");
            }
            catch
            {
                try
                {
                    _file.AppendAllText(tmp, csvOutput + "\r\n");
                }
                catch (Exception e)
                {
                    throw new LogIOException("報告書の出力中にエラーが発生しました。", e);
                }
            }

            WriteCache(table, time, log);
        }

        private void WriteCache(string table, DateTime time, string log)
        {
            var dbfile = Path.Combine(_outputDir, Const.CacheFileName);
            if (!_useCache || !File.Exists(dbfile))
                return;

            var retry = 0;
            do
            {
                retry++;
                try
                {
                    WriteCacheRetry(dbfile, table, time, log);
                    break;
                }
                catch (Exception e)
                {
                    if (retry > 2)
                        throw new LogIOException("報告書キャッシュの出力中にエラーが発生しました。", e);
                    else
                        Thread.Sleep(100);
                }
            }
            while (true);
        }

        private void WriteCacheRetry(string dbfile, string table, DateTime time, string log)
        {
            var settings = new SQLiteConnectionStringBuilder { DataSource = Unc(dbfile), JournalMode = SQLiteJournalModeEnum.Wal };
            using var connection = new SQLiteConnection(settings.ToString()).OpenAndReturn();
            var sql = $@"insert into {table}(timestamp, csv) values($timestamp, $csv)";
            using var cmd = new SQLiteCommand(sql, connection);
            cmd.Parameters.AddWithValue("$timestamp", Logger.ToSeconds(time));
            cmd.Parameters.AddWithValue("$csv", log);
            cmd.ExecuteNonQuery();
        }
    }

    public class LogIOException : Exception
    {
        public LogIOException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
