// Copyright (C) 2018 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2022 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using KancolleSniffer.Model;
using KancolleSniffer.Util;

namespace KancolleSniffer.Log
{
    public class BattleLogProcessor : LogProcessor.Processor
    {
        private readonly Dictionary<string, string> _mapDictionary;

        public BattleLogProcessor(Dictionary<string, string> mapDictionary = null)
        {
            _mapDictionary = mapDictionary ?? [];
        }

        public override string[] Process(string[] data)
        {
            string map;
            switch (data.Length)
            {
                case 35:
                    data = data.Concat(["", "", ""]).ToArray();
                    goto case 38;
                case 38:
                    map = _mapDictionary.TryGetValue(data[1], out var num) ? num : "";
                    break;
                case 39:
                    map = data[38];
                    break;
                case 40:
                    if (data[8] == "深海5" && data[9] == "500t級軽巡洋艦")
                    {
                        data[8] = "深海5,500t級軽巡洋艦";
                        Array.Copy(data, 10, data, 9, 30);
                        goto case 39;
                    }
                    // 七隻分のログが出力されている
                    if (data[23] != "")
                    {
                        data[21] += "・" + data[23];
                        data[22] += "・" + data[24];
                    }
                    Array.Copy(data, 25, data, 23, 15);
                    goto case 38;
                default:
                    Skip = true;
                    return null;
            }
            Skip = false;
            if (data[5] == "Ｔ字戦(有利)")
                data[5] = "Ｔ字有利";
            if (data[5] == "Ｔ字戦(不利)")
                data[5] = "Ｔ字不利";
            if (data[6].EndsWith("航行序列"))
                data[6] = data[6].Substring(0, 4);
            if (data[7].EndsWith("航行序列"))
                data[7] = data[7].Substring(0, 4);
            data[37] = ShortenAirBattleResult(data[37]);
            var result = new string[41];
            (result[38], result[39]) = GenerateDamagedShip(data);
            result[40] = map;
            Array.Copy(data, result, 38);
            return result;
        }

        private static string ShortenAirBattleResult(string result)
        {
            return result switch
            {
                "制空均衡" => "拮抗",
                "航空均衡" => "拮抗",
                "制空権確保" => "確保",
                "航空優勢" => "優勢",
                "航空劣勢" => "劣勢",
                "制空権喪失" => "喪失",
                _ => "",
            };
        }

        private static (string, string) GenerateDamagedShip(string[] data)
        {
            var badly = new List<string>();
            var half = new List<string>();
            for (var i = 11; i < 11 + 12; i += 2)
            {
                if (data[i] == "")
                    continue;
                var ship = data[i] = StripKana(data[i]);
                var hp = data[i + 1];
                try
                {
                    foreach (var (level, name) in from entry in ship.Split('・').Zip(hp.Split('・'), (name, h) => (name, h))
                                                  where entry.h.Contains("/")
                                                  let nm = entry.h.Split('/').Select(int.Parse).ToArray()
                                                  let level = ShipStatus.CalcDamage(nm[0], nm[1])
                                                  select (level, entry.name))
                    {
                        if (level == ShipStatus.Damage.Half)
                            half.Add(name);
                        else if (level == ShipStatus.Damage.Badly)
                            badly.Add(name);
                    }
                }
                catch (FormatException)
                {
                    return ("", "");
                }
            }
            return (badly.Join("・"), half.Join("・"));
        }

        private static readonly Regex Kana = new(@"\([^)]+\)\(", RegexOptions.Compiled);

        private static string StripKana(string name)
        {
            return Kana.Replace(name, "(");
        }
    }
}
