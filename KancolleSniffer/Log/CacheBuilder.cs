// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using KancolleSniffer.Util;
using static KancolleSniffer.Util.StringUtil;

namespace KancolleSniffer.Log
{
    public class CacheBuilder
    {
        private readonly string _outputDir;
        private readonly bool _useCache;
        private readonly bool _rebuildCache;

        public CacheBuilder(LogConfig config)
        {
            _outputDir = config.OutputDir;
            _useCache = config.UseCache;
            _rebuildCache = config.RebuildCache;
        }

        public bool NeedBuild => _useCache && (_rebuildCache || !File.Exists(Path.Combine(_outputDir, Const.CacheFileName)));

        public void CreateDatabase()
        {
            var dbfile = Path.Combine(_outputDir, Const.CacheFileName);
            try
            {
                if (File.Exists(dbfile))
                    File.Delete(dbfile);
                using var connection = new SQLiteConnection(new SQLiteConnectionStringBuilder { DataSource = Unc(dbfile) }.ToString()).OpenAndReturn();
                foreach (var pair in Const.TableCsvNames)
                    CreateTable(connection, pair.Key, Path.Combine(_outputDir, pair.Value));
            }
            catch (Exception e)
            {
                try
                {
                    File.Delete(dbfile);
                }
                finally
                {
                    e.Data["folder"] = _outputDir;
                    e.Data["dbfile"] = dbfile;
                    throw e;
                }
            }
        }

        private void CreateTable(SQLiteConnection connection, string table, string csv)
        {
            try
            {
                using var transaction = connection.BeginTransaction();
                using (var cmd = new SQLiteCommand(connection))
                {
                    cmd.CommandText = $@"create table {table}(timestamp integer, csv text)";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = $@"create index {table}Index on {table}(timestamp);";
                    cmd.ExecuteNonQuery();

                    if (File.Exists(csv))
                    {
                        cmd.CommandText = $@"insert into {table}(timestamp, csv) values($timestamp, $csv)";
                        foreach (var line in File.ReadLines(csv, Const.CsvEncoding).Skip(1))
                        {
                            try
                            {
                                var timestamp = ToSeconds(line.Split(',')[0]);
                                cmd.Parameters.AddWithValue("$timestamp", timestamp);
                                cmd.Parameters.AddWithValue("$csv", line);
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.RemoveAt("$timestamp");
                                cmd.Parameters.RemoveAt("$csv");
                            }
                            catch (Exception e)
                            {
                                e.Data["line"] = line;
                                throw e;
                            }
                        }
                    }
                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                e.Data["csv"] = csv;
                throw e;
            }
        }

        // copy and modify from KancolleSniffer.Log.Logger.ToSeconds()
        private static long ToSeconds(string dateTime)
        {
            return ParseDateTime(dateTime).Ticks / (TimeSpan.TicksPerMillisecond * 1000);
        }

        // copy and modify from KancolleSniffer.Log.LogProcessor.ParseDateTime()
        private static DateTime ParseDateTime(string dateTime)
        {
            if (DateTime.TryParseExact(dateTime, @"yyyy\-MM\-dd HH\:mm\:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out DateTime date))
                return date;

            // システムが和暦に設定されていて和暦が出力されてしまったケースを救う
            if (dateTime[2] == '-')
            {
                if (!int.TryParse(dateTime.Substring(0, 2), out int year))
                    return DateTime.Now;

                dateTime = 1988 + year + dateTime.Substring(2);
                return DateTime.TryParseExact(dateTime, @"yyyy\-MM\-dd HH\:mm\:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out date)
                    ? date : DateTime.Now;
            }
            return DateTime.TryParse(dateTime, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal, out date)
                ? date : DateTime.Now;
        }
    }
}
