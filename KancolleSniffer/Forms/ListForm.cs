// Copyright (C) 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using KancolleSniffer.Model;
using KancolleSniffer.Util;
using KancolleSniffer.View;
using KancolleSniffer.View.ListWindow;
using KancolleSniffer.View.ShipListPanel;
using Clipboard = KancolleSniffer.Util.Clipboard;

namespace KancolleSniffer.Forms
{
    public partial class ListForm : Form
    {
        private readonly Sniffer _sniffer;
        private readonly Config _config;
        private readonly Form _form;
        private readonly MainWindow.TimeOutChecker _suppressActivate;
        private readonly Func<int> CurrentFleet;
        private readonly Func<bool> ShowCombinedFleet;
        private readonly Action<string[]> RefreshAllShipList;
        private readonly CheckBox[] _shipTypeCheckBoxes;
        private readonly Panel[] _headers;
        private readonly IPanelResize[] _resizePanels;
        private bool _isMaster;
        private ShipListConfig _listConfig;
        private FormWindowState _windowState = FormWindowState.Normal;
        private bool _systemShutdown;
        private Size _defaultSize;
        public const int PanelWidth = 215;
        private Size _headerLabelSizeLock = new(29, 16);
        private Size _headerLabelSizeFree = new(29, 18);

        private string[] PanelNames => IsMaster ? Const.ShipListPanelNamesPrimary : Const.ShipListPanelNamesSecondary;

        private char[] PanelKeys => IsMaster ? Const.ShipListPanelKeysPrimary : Const.ShipListPanelKeysSecondary;

        private bool IsMaster
        {
            get => _isMaster;
            set
            {
                _isMaster = value;
                Text = _isMaster ? "一覧 プライマリ" : "一覧";
                comboBoxGroup.Items.Clear();
                comboBoxGroup.Items.AddRange(PanelNames);
            }
        }

        public enum SortOrder
        {
            None,
            Cond,
            CondAscend = Cond,
            CondDescend,
            ExpToNext,
            ExpToNextAscend = ExpToNext,
            ExpToNextDescend,
            Repair
        }

        public ListForm(MainWindow main, Func<TimeStep> getStep, bool isMaster = false)
        {
            InitializeComponent();
            Icon = AppInfo.Favicon;

            IsMaster = isMaster;
            _form = main.Form;
            _sniffer = main.Sniffer;
            _config = main.Config;
            _suppressActivate = main.SuppressActivate;
            CurrentFleet = main.CurrentFleet;
            ShowCombinedFleet = main.ShowCombinedFleet;
            RefreshAllShipList = main.RefreshShipList;
            _shipTypeCheckBoxes = [
                checkBoxSTypeBattleShip,
                checkBoxSTypeAircraftCarrier,
                checkBoxSTypeHeavyCruiser,
                checkBoxSTypeLightCruiser,
                checkBoxSTypeDestroyer,
                checkBoxSTypeEscort,
                checkBoxSTypeSubmarine,
                checkBoxSTypeAuxiliary
            ];
            _headers = [panelShipHeader, panelGroupHeader, panelRepairHeader];
            _resizePanels = [shipListPanel, antiAirPanel, airBattleResultPanel, battleResultPanel, fleetPanel, airBasePanel, monthlyMissionPanel];
            battleResultPanel.HpLabelClick += ToggleHpPercent;
            shipListPanel.HpLabelClick += ToggleHpPercent;
            shipListPanel.GroupConfigCheckboxChanged += GroupConfigCheckbox_CheckedChanged;
            fleetPanel.Source = new FleetSource();
            airBasePanel.Source = new AirBaseSource();
            monthlyMissionPanel.GetStep = getStep;
            monthlyMissionPanel.ShowLimit = (main.Config.ShowEndTime & TimerKind.Mission) != 0;
            var swipe = new SwipeScrollify();
            swipe.AddShipListPanel(shipListPanel);
            swipe.AddTreeView(itemTreeView);
            swipe.AddPanel(fleetPanel);
            swipe.AddPanel(airBasePanel);
            SetupSettings();
        }

        private void SetupSettings()
        {
            _listConfig = GetConfig();
            if (_listConfig.ShowHpInPercent)
            {
                shipListPanel.ToggleHpPercent();
                battleResultPanel.ToggleHpPercent();
            }
            if (!PanelNames.Contains(_listConfig.Mode))
            {
                _listConfig.Mode = "全艦";
            }
        }

        private ShipListConfig GetConfig()
        {
            if (_isMaster)
            {
                SetGroup();
                return _config.ShipList;
            }
            if (_config.ListFormGroup.Count == 0)
                return CreateSecondaryConfig();
            var config = _config.ListFormGroup[0];
            _config.ListFormGroup.RemoveAt(0);
            config.ShipGroup = _config.ShipList.ShipGroup;
            return config;
        }

        private ShipListConfig CreateSecondaryConfig()
        {
            var src = _config.ShipList;
            var config = new ShipListConfig
            {
                Mode = src.Mode,
                ShipCategories = src.ShipCategories,
                ShipType = src.ShipType,
                ShowHpInPercent = src.ShowHpInPercent,
                SortOrder = src.SortOrder,
                Location = src.Location,
                Size = src.Size,
                ShipGroup = src.ShipGroup
            };
            if (config.Mode == "分類" || string.IsNullOrEmpty(config.Mode))
                config.Mode = "全艦";
            return config;
        }

        private void SetGroup()
        {
            var groups = _config.ShipList.ShipGroup;
            for (var i = groups.Count; i < GroupConfigLabels.GroupCount; i++)
                groups.Add([]);
            shipListPanel.GroupSettings = groups;
        }

        public void UpdateList()
        {
            if (!Visible)
                return;
            SetHeaderVisibility();
            SetPanelVisibility();
            if (InItemList)
            {
                itemTreeView.SetNodes(_sniffer.ItemList, _sniffer.PresetItems, _sniffer.UseItems, _sniffer.PayItems);
            }
            else if (InFleetInfo)
            {
                ChangeCurrentFleet();
                fleetPanel.Update(_sniffer);
            }
            else if (InAirBaseInfo)
            {
                airBasePanel.Update(_sniffer);
            }
            else if (InAntiAir)
            {
                antiAirPanel.Update(_sniffer);
            }
            else if (InMonthlyMission)
            {
                monthlyMissionPanel.Update(_sniffer, CurrentFleet());
            }
            else if (InMiscText)
            {
                richTextBoxMiscText.Text = _sniffer.MiscText;
            }
            else if (InShipStatus || InGroupConfig || InRepairList)
            {
                SetHeaderSortOrder();
                shipListPanel.Update(_sniffer, comboBoxGroup.Text, _listConfig);
            }

            if (InAllShipStatus || InGroupConfig)
                contextMenuStripShipList.Items.Add(noro6KcToolsManagerToolStripMenuItem);
            else
                contextMenuStripShipList.Items.Remove(noro6KcToolsManagerToolStripMenuItem);
            if (InPresetList)
                contextMenuStripShipList.Items.Remove(kantaiSarashiToolStripMenuItem);
            else
                contextMenuStripShipList.Items.Add(kantaiSarashiToolStripMenuItem);
            if (InAnyGroup)
            {
                contextMenuStripShipList.Items.Add(groupAddFleetToolStripMenuItem);
                contextMenuStripShipList.Items.Add(groupDelFleetToolStripMenuItem);
                contextMenuStripShipList.Items.Add(groupClearToolStripMenuItem);
            }
            else
            {
                contextMenuStripShipList.Items.Remove(groupClearToolStripMenuItem);
                contextMenuStripShipList.Items.Remove(groupDelFleetToolStripMenuItem);
                contextMenuStripShipList.Items.Remove(groupAddFleetToolStripMenuItem);
            }
        }

        public void UpdateTimers()
        {
            if (InMonthlyMission)
                monthlyMissionPanel.UpdateTimers();
        }

        public void UpdateMonthlyMission()
        {
            if (InMonthlyMission)
                monthlyMissionPanel.Update(_sniffer, CurrentFleet());
        }

        public void RefreshShipList(string[] modes)
        {
            if (!Visible || !modes.Contains(comboBoxGroup.Text))
                return;
            shipListPanel.Update(_sniffer, comboBoxGroup.Text, _listConfig);
        }

        private bool _inSortie;

        public void ChangeCurrentFleet()
        {
            fleetPanel.TemporaryPosition = string.Empty;
            if (!_config.AutoChangeCurrentFleet)
                return;

            var inSortie = _sniffer.InSortie;
            if (_inSortie)
            {
                _inSortie = inSortie != -1;
                return;
            }

            if (inSortie == -1)
            {
                _inSortie = false;
                var changingFleet = _sniffer.ChangingFleet;
                if (changingFleet > -1)
                {
                    fleetPanel.TemporaryPosition = Const.FleetNames[_sniffer.IsCombinedFleet && changingFleet < 2 ? 0 : changingFleet];
                }
            }
            else
            {
                _inSortie = true;
                fleetPanel.TemporaryPosition = Const.FleetNames[inSortie == 10 ? 0 : inSortie];
            }
        }

        private void PurifyShipGroup()
        {
            var all = _sniffer.ShipList.Select(s => s.Id).ToArray();
            if (all.Length == 0)
                return;
            foreach (var g in _config.ShipList.ShipGroup)
            {
                var filtered = g.Intersect(all).ToArray();
                g.Clear();
                g.AddRange(filtered);
            }
        }

        private void SetHeaderVisibility()
        {
            static void Set(Control header, bool visible)
            {
                header.Visible = visible;
                if (visible)
                    header.BringToFront();
            }

            Set(panelShipHeader, InShipStatus);
            Set(panelGroupHeader, InGroupConfig);
            Set(panelRepairHeader, InRepairList);
            Set(panelFleetHeader, InFleetInfo);
            Set(panelAirBaseHeader, InAirBaseInfo);
            SetSTypeDropDownVisible(InShipStatus && !InPresetList || InGroupConfig || InRepairList);
        }

        private void SetPanelVisibility()
        {
            static void Set(Control panel, bool visible)
            {
                // SwipeScrollifyが誤作動するのでEnabledも切り替える
                panel.Visible = panel.Enabled = visible;
            }

            Set(shipListPanel, InShipStatus || InGroupConfig || InRepairList);
            Set(itemTreeView, InItemList);
            Set(fleetPanel, InFleetInfo);
            Set(airBasePanel, InAirBaseInfo);
            Set(antiAirPanel, InAntiAir);
            Set(airBattleResultPanel, InBattleResult);
            Set(battleResultPanel, InBattleResult);
            Set(monthlyMissionPanel, InMonthlyMission);
            Set(richTextBoxMiscText, InMiscText);
        }

        public void UpdateAirBattleResult()
        {
            airBattleResultPanel.ShowResultAutomatic = (_config.Spoilers & Spoiler.AirBattleResult) != 0;
            airBattleResultPanel.SetResult(_sniffer);
        }

        public void UpdateBattleResult()
        {
            MoveToBattleResult();
            battleResultPanel.Spoilers = _config.Spoilers;
            battleResultPanel.Update(_sniffer);
            BackFromBattleResult();
        }

        private string _prevSelectedText = string.Empty;
        private const string BattleResultText = "戦況";

        private void MoveToBattleResult()
        {
            if (!_isMaster || !_config.ShipList.AutoBattleResult || comboBoxGroup.Text == BattleResultText || _sniffer.InSortie == -1)
                return;
            _prevSelectedText = comboBoxGroup.Text;
            comboBoxGroup.Text = BattleResultText;
        }

        private void BackFromBattleResult()
        {
            if (_sniffer.InSortie != -1 || _prevSelectedText == string.Empty)
                return;
            comboBoxGroup.Text = _prevSelectedText;
            _prevSelectedText = string.Empty;
        }

        public void UpdateCellInfo()
        {
            MoveToBattleResult();
            battleResultPanel.Spoilers = _config.Spoilers;
            battleResultPanel.UpdateCellInfo(_sniffer.CellInfo);
        }

        private void SetHeaderSortOrder()
        {
            if (InPresetList)
            {
                labelHeaderCond.Text = "cond";
                labelHeaderExp.Text = "Exp";
                return;
            }

            switch (_listConfig.SortOrder)
            {
                case SortOrder.None:
                    labelHeaderCond.Text = "cond";
                    labelHeaderExp.Text = "Exp";
                    break;
                case SortOrder.CondAscend:
                    labelHeaderCond.Text = "cond▴";
                    labelHeaderExp.Text = "Exp";
                    break;
                case SortOrder.CondDescend:
                    labelHeaderCond.Text = "cond▾";
                    labelHeaderExp.Text = "Exp";
                    break;
                case SortOrder.ExpToNextAscend:
                    labelHeaderCond.Text = "cond";
                    labelHeaderExp.Text = "Exp▴";
                    break;
                case SortOrder.ExpToNextDescend:
                    labelHeaderCond.Text = "cond";
                    labelHeaderExp.Text = "Exp▾";
                    break;
            }
        }

        private bool InShipStatus => Const.ShipListShipStatusModes.Contains(_listConfig.Mode);

        private bool InAnyGroup => Const.ShipListGroupNames.Contains(_listConfig.Mode);

        private bool InAllShipStatus => _listConfig.Mode == "全艦";

        private bool InGroupConfig => _listConfig.Mode == "分類";

        private bool InRepairList => _listConfig.Mode == "修復";

        private bool InPresetList => _listConfig.Mode == "P";

        private bool InItemList => _listConfig.Mode == "装備";

        private bool InFleetInfo => _listConfig.Mode == "艦隊";

        private bool InAirBaseInfo => _listConfig.Mode == "基地";

        private bool InAntiAir => _listConfig.Mode == "対空";

        private bool InBattleResult => _listConfig.Mode == "戦況";

        private bool InMonthlyMission => _listConfig.Mode == "月次";

        private bool InMiscText => _listConfig.Mode == "情報";

        private void ListForm_Load(object sender, EventArgs e)
        {
            AdjustHeader();
            SetMinimumSize();
            comboBoxGroup.SelectedItem = _listConfig.Mode;
            SetCheckBoxSTypeState();
            if (_listConfig.Location.X == int.MinValue)
                return;
            var bounds = new Rectangle(_listConfig.Location, _listConfig.Size);
            if (MainWindow.IsTitleBarOnAnyScreen(bounds.Location))
                Location = bounds.Location;
            Size = bounds.Size;
        }

        private void AdjustHeader()
        {
            if (_config.Zoom == 100)
                return;

            var scrollbarWidth = SystemInformation.VerticalScrollBarWidth * (_config.Zoom - 100) / 100;
            foreach (var header in _headers)
                header.Left += scrollbarWidth;

            var offset = (_config.Zoom - 100) / 25;
            var scale = _config.Zoom / 100.0;
            _headerLabelSizeLock = new((int)((_headerLabelSizeLock.Width + offset) * scale) + 2, (int)(_headerLabelSizeLock.Height * scale));
            _headerLabelSizeFree = new((int)((_headerLabelSizeFree.Width + offset) * scale) + 2, (int)(_headerLabelSizeFree.Height * scale));
        }

        private void SetMinimumSize()
        {
            var zoomOffset = (int)((_config.Zoom - 100) / 6.25);
            var questLinesHeight = Scaler.ScaleHeight((_config.QuestLines - 8) * 14); // ListForm.Designer は任務 8 個前提
            _defaultSize = new Size(Width + zoomOffset - SystemInformation.VerticalScrollBarWidth    * (_config.Zoom - 100) / 100,
                questLinesHeight + Height + zoomOffset - SystemInformation.HorizontalScrollBarHeight * (_config.Zoom - 100) / 100);
            MinimumSize = new Size(_defaultSize.Width, 0);
        }

        private void SetCheckBoxSTypeState()
        {
            for (var type = 0; type < _shipTypeCheckBoxes.Length; type++)
                _shipTypeCheckBoxes[type].Checked = ((int)_listConfig.ShipCategories & (1 << type)) != 0;
            checkBoxSTypeAll.Checked = _listConfig.ShipCategories == ShipCategory.All;
            checkBoxSTypeDetails.Checked = _listConfig.ShipType;
        }

        private void ListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_systemShutdown)
            {
                SaveConfig();
                return;
            }
            e.Cancel = true;
            Hide();
        }

        public void SaveConfig()
        {
            if (_isMaster)
            {
                SaveMasterState();
            }
            else
            {
                SaveSlaveState();
            }
        }

        private void SaveMasterState()
        {
            PurifyShipGroup();
            _listConfig.Visible = Visible && WindowState == FormWindowState.Normal;
            SaveBounds(_listConfig); // 最小化時は以前のサイズを記録する
        }

        private void SaveSlaveState()
        {
            if (!Visible)
                return;
            if (WindowState != FormWindowState.Normal) // 最小化時は次回復旧しない
                return;
            _listConfig.Visible = true;
            _listConfig.ShipGroup = null;
            _config.ListFormGroup.Add(_listConfig);
            SaveBounds(_listConfig);
        }

        private void SaveBounds(ShipListConfig config)
        {
            var bounds = WindowState == FormWindowState.Normal ? Bounds : RestoreBounds;
            config.Location = bounds.Location;
            config.Size = bounds.Size;
        }

        public void ChangeWindowState(FormWindowState newState)
        {
            if (!Visible)
                return;
            if (newState == FormWindowState.Minimized)
            {
                if (WindowState == FormWindowState.Normal)
                    WindowState = FormWindowState.Minimized;
                if (_config.HideOnMinimized)
                    ShowInTaskbar = false;
            }
            else
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    Application.DoEvents();
                    if (_config.HideOnMinimized)
                        ShowInTaskbar = true;
                    WindowState = FormWindowState.Normal;
                }
            }
        }

        private void ListForm_Activated(object sender, EventArgs e)
        {
            if (!_isMaster)
                return;
            if (_suppressActivate.Check())
                return;
            if (WindowState == FormWindowState.Minimized)
                return;
            RaiseBothWindows();
        }

        private void RaiseBothWindows()
        {
            _form.Owner = null;
            Owner = _form;
            BringToFront();
            Owner = null;
        }

        public void ShowShip(int id)
        {
            if (!Visible)
                return;
            if (InShipStatus)
            {
                shipListPanel.ShowShip(id);
            }
            else if (InFleetInfo)
            {
                fleetPanel.MoveToSecondLevel(id);
            }
            else if (InAntiAir)
            {
                antiAirPanel.ShowShip(id);
            }
        }

        public void ScrollSemiLockFleetPanel(string text)
        {
            void Set(Label label, string text)
            {
                label.BorderStyle = label.Text == text ? BorderStyle.FixedSingle : BorderStyle.None;
                label.ClientSize = label.Text == text ? _headerLabelSizeLock : _headerLabelSizeFree;
            }

            if (fleetPanel.ScrollLockPosition == text)
                text = string.Empty;
            fleetPanel.ScrollLockPosition = text;
            Set(labelFleet1, text);
            Set(labelFleet2, text);
            Set(labelFleet3, text);
            Set(labelFleet4, text);
        }

        public void ScrollSemiLockAirBasePanel(string text)
        {
            void Set(Label label, string text)
            {
                label.BorderStyle = label.Text == text ? BorderStyle.FixedSingle : BorderStyle.None;
                label.ClientSize = label.Text == text ? _headerLabelSizeLock : _headerLabelSizeFree;
            }

            if (airBasePanel.ScrollLockPosition == text)
                text = string.Empty;
            airBasePanel.ScrollLockPosition = text;
            Set(labelAirBaseE, text);
            Set(labelAirBase5, text);
            Set(labelAirBase6, text);
            Set(labelAirBase7, text);
        }

        private void ComboBoxGroup_DropDownClosed(object sender, EventArgs e)
        {
            SetActiveControl();
        }

        private void ComboBoxGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            _listConfig.Mode = comboBoxGroup.Text;
            if (!Visible)
                return;
            UpdateList();
            SetActiveControl();
            if (!(InShipStatus || InGroupConfig || InRepairList))
                SetPanelSTypeState(false);
        }

        private void ListForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            var g = PanelKeys.IndexOf(char.ToUpper(e.KeyChar));
            if (g == -1)
                return;
            comboBoxGroup.SelectedIndex = g;
            e.Handled = true;
        }

        // マウスホイールでスクロールするためにコントロールにフォーカスを合わせる。
        private void SetActiveControl()
        {
            if (InItemList)
            {
                ActiveControl = itemTreeView;
            }
            else if (InFleetInfo)
            {
                ActiveControl = fleetPanel;
            }
            else if (InAirBaseInfo)
            {
                ActiveControl = airBasePanel;
            }
            else if (InAntiAir)
            {
                ActiveControl = antiAirPanel;
            }
            else if (InMonthlyMission)
            {
                ActiveControl = monthlyMissionPanel;
            }
            else
            {
                ActiveControl = shipListPanel;
            }
        }

        private void ItemCsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateItemList(_sniffer.ItemList, _sniffer.UseItems, _sniffer.PayItems));
        }

        private void KantaiBunsekiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateKantaiBunsekiItemList(_sniffer.ItemList));
        }

        private void Noro6KcToolsManagerEquipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateNoro6KcToolsManagerEquipData(_sniffer.SlotItems));
        }

        private void FleetCsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateFleetCsv(_sniffer));
        }

        private void DeckBuilderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateDeckBuilderData(_sniffer));
        }

        private void LabelHeaderCond_Click(object sender, EventArgs e)
        {
            if (InPresetList)
                return;

            _listConfig.SortOrder =_listConfig.SortOrder switch
            {
                SortOrder.CondAscend => SortOrder.CondDescend,
                SortOrder.CondDescend => SortOrder.None,
                _ => SortOrder.CondAscend,
            };
            UpdateList();
        }

        private void LabelHeaderExp_Click(object sender, EventArgs e)
        {
            if (InPresetList)
                return;

            _listConfig.SortOrder =_listConfig.SortOrder switch
            {
                SortOrder.ExpToNextAscend => SortOrder.ExpToNextDescend,
                SortOrder.ExpToNextDescend => SortOrder.None,
                _ => SortOrder.ExpToNextAscend,
            };
            UpdateList();
        }

        private void ShipCsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (InPresetList)
            {
                var presetDecks = _sniffer.PresetDecks;
                if (presetDecks.Any())
                    Clipboard.SetText(TextGenerator.GeneratePresetDecks(presetDecks));
            }
            else
            {
                Clipboard.SetText(TextGenerator.GenerateShipList(shipListPanel.CurrentShipList));
            }
        }

        private void KantaiSarashiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateKantaiSarashiData(shipListPanel.CurrentShipList));
        }

        private void Noro6KcToolsManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(TextGenerator.GenerateNoro6KcToolsManagerData(shipListPanel.CurrentShipList));
        }

        public void GroupConfigCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;
            var group = (int)cb.Tag % 10;
            var idx = (int)cb.Tag / 10;
            _listConfig.ShipGroup[group].RemoveAll(id => id == shipListPanel.GetShip(idx).Id);
            if (cb.Checked)
                _listConfig.ShipGroup[group].Add(shipListPanel.GetShip(idx).Id);

            PurifyShipGroup();
            _config.Save();
            RefreshAllShipList([Const.ShipListGroupNames[group]]);
        }

        private void GroupAddFleetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_sniffer.Started)
                return;
            var mode = comboBoxGroup.Text;
            var group = Const.ShipListGroupNames.IndexOf(mode);
            if (group == -1)
                return;

            if (ShowCombinedFleet())
            {
                _listConfig.ShipGroup[group].AddRange(_sniffer.Fleets[0].Deck.Where(deck => deck > 0));
                _listConfig.ShipGroup[group].AddRange(_sniffer.Fleets[1].Deck.Where(deck => deck > 0));
            }
            else
            {
                var deck = _sniffer.Fleets[CurrentFleet()].Deck.Where(deck => deck > 0).ToArray();
                if (!deck.Any())
                    return;
                _listConfig.ShipGroup[group].AddRange(deck);
            }

            PurifyShipGroup();
            _config.Save();
            RefreshAllShipList([mode, "分類"]);
        }

        private void GroupDelFleetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_sniffer.Started)
                return;
            var mode = comboBoxGroup.Text;
            var group = Const.ShipListGroupNames.IndexOf(mode);
            if (group == -1)
                return;

            if (ShowCombinedFleet())
            {
                foreach (var id in _sniffer.Fleets[0].Deck.Where(deck => deck > 0))
                    _listConfig.ShipGroup[group].Remove(id);
                foreach (var id in _sniffer.Fleets[1].Deck.Where(deck => deck > 0))
                    _listConfig.ShipGroup[group].Remove(id);
            }
            else
            {
                var deck = _sniffer.Fleets[CurrentFleet()].Deck.Where(deck => deck > 0).ToArray();
                if (!deck.Any())
                    return;
                foreach (var id in deck)
                    _listConfig.ShipGroup[group].Remove(id);
            }

            PurifyShipGroup();
            _config.Save();
            RefreshAllShipList([mode, "分類"]);
        }

        private void GroupClearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_sniffer.Started)
                return;
            var mode = comboBoxGroup.Text;
            var group = Const.ShipListGroupNames.IndexOf(mode);
            if (group == -1)
                return;

            if (!_listConfig.ShipGroup[group].Any())
                return;
            _listConfig.ShipGroup[group].Clear();

            PurifyShipGroup();
            _config.Save();
            RefreshAllShipList([mode, "分類"]);
        }

        private void LabelFleetBase_Click(object sender, EventArgs e)
        {
            comboBoxGroup.Text = ((Control)sender).Text;
        }

        private void LabelShowFleet_Click(object sender, EventArgs e)
        {
            comboBoxGroup.Text = ((Control)sender).Text;
        }

        private void LabelFleet_Click(object sender, EventArgs e)
        {
            fleetPanel.MoveToTopLevel(((Control)sender).Text);
        }

        private void LabelFleet_DoubleClick(object sender, EventArgs e)
        {
            ScrollSemiLockFleetPanel(((Control)sender).Text);
        }

        private void LabelAirBase_Click(object sender, EventArgs e)
        {
            airBasePanel.MoveToTopLevel(((Control)sender).Text);
        }

        private void LabelAirBase_DoubleClick(object sender, EventArgs e)
        {
            ScrollSemiLockAirBasePanel(((Control)sender).Text);
        }

        private void LabelHeaderHp_Click(object sender, EventArgs e)
        {
            ToggleHpPercent();
        }

        private void ToggleHpPercent()
        {
            _listConfig.ShowHpInPercent = !_listConfig.ShowHpInPercent;
            shipListPanel.ToggleHpPercent();
            battleResultPanel.ToggleHpPercent();
        }

        private void SetSTypeDropDownVisible(bool visible)
        {
            if (!visible)
                SetPanelSTypeState(false);
            dropDownButtonSType.Visible = visible;
            labelSType.Visible = visible;
        }

        private void LabelSTypeButton_Click(object sender, EventArgs e)
        {
            SetPanelSTypeState(!panelSType.Visible);
        }

        private void CheckBoxSType_Click(object sender, EventArgs e)
        {
            _listConfig.ShipCategories = SelectedShipTypes;
            UpdateList();
            SetActiveControl();
        }

        private ShipCategory SelectedShipTypes => (ShipCategory)_shipTypeCheckBoxes.Select((cb, type) => cb.Checked ? 1 << type : 0).Sum();

        private void CheckBoxSTypeAll_Click(object sender, EventArgs e)
        {
            foreach (var checkBox in _shipTypeCheckBoxes)
                checkBox.Checked = checkBoxSTypeAll.Checked;
            CheckBoxSType_Click(sender, e);
        }

        private void PanelSType_Click(object sender, EventArgs e)
        {
            SetPanelSTypeState(false);
        }

        private void SetPanelSTypeState(bool visible)
        {
            panelSType.Visible = visible;
            if (visible)
                panelSType.BringToFront();
            dropDownButtonSType.BackColor = visible ? CustomColors.ActiveButtonColor : DefaultBackColor;
        }

        private void CheckBoxSTypeDetails_Click(object sender, EventArgs e)
        {
            _listConfig.ShipType = checkBoxSTypeDetails.Checked;
            UpdateList();
            SetActiveControl();
        }

        private void ListForm_ResizeEnd(object sender, EventArgs e)
        {
            foreach (var panel in _resizePanels)
            {
                if (panel.Visible)
                    panel.ApplyResize();
            }
        }

        private void ListForm_Resize(object sender, EventArgs e)
        {
            if (_windowState != WindowState && WindowState == FormWindowState.Normal)
                UpdateList();
            _windowState = WindowState;
        }

        private void ListForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                UpdateList();
        }

        private void ListForm_DoubleClick(object sender, EventArgs e)
        {
            if (((MouseEventArgs)e).Button == MouseButtons.Right)
            {
                // メイン画面と同じ大きさ
                Size = _defaultSize;
                ListForm_ResizeEnd(sender, e);
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x11) // WM_QUERYENDSESSION
                _systemShutdown = true;
            base.WndProc(ref m);
        }
    }
}
