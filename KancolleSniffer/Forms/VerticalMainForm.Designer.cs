// Copyright (C) 2013, 2014, 2015 Kazuhiro Fujieda <fujieda@users.osdn.me>
// Copyright (C) 2021 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using KancolleSniffer.View;
using KancolleSniffer.View.MainWindow;

namespace KancolleSniffer.Forms
{
    partial class VerticalMainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerticalMainForm));
            this.LabelAkashiRepairTimer = new System.Windows.Forms.Label();
            this.LabelNDockCaption = new System.Windows.Forms.Label();
            this.LabelKDockCaption = new System.Windows.Forms.Label();
            this.LabelQuestCaption = new System.Windows.Forms.Label();
            this.LabelMissionCaption = new System.Windows.Forms.Label();
            this.NotifyIconMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.LabelMaterialCaption = new System.Windows.Forms.Label();
            this.LabelRepairListCaption = new System.Windows.Forms.Label();
            this.LabelAkashiRepair = new System.Windows.Forms.Label();
            this.DropDownButtonRepairList = new KancolleSniffer.View.DropDownButton();
            this.LabelQuestCount = new System.Windows.Forms.Label();
            this.KdockPanel = new KancolleSniffer.View.MainWindow.KDockPanel();
            this.PanelRepairList = new KancolleSniffer.View.MainWindow.RepairListPanel();
            this.NdockPanel = new KancolleSniffer.View.MainWindow.NDockPanel();
            this.MissionPanel = new KancolleSniffer.View.MainWindow.MissionPanel();
            this.QuestPanel = new KancolleSniffer.View.MainWindow.QuestPanel();
            this.HqPanel = new KancolleSniffer.View.MainWindow.HqPanel();
            this.MaterialHistoryPanel = new KancolleSniffer.View.MainWindow.MaterialHistoryPanel();
            this.DropDownButtonMaterialHistory = new KancolleSniffer.View.DropDownButton();
            this.FleetPanel = new KancolleSniffer.View.MainWindow.FleetPanel();
            this.SuspendLayout();
            //
            // labelAkashiRepairTimer
            //
            this.LabelAkashiRepairTimer.Location = new System.Drawing.Point(179, 275);
            this.LabelAkashiRepairTimer.Name = "LabelAkashiRepairTimer";
            this.LabelAkashiRepairTimer.Size = new System.Drawing.Size(32, 12);
            this.LabelAkashiRepairTimer.TabIndex = 43;
            //
            // labelNDockCaption
            //
            this.LabelNDockCaption.AutoSize = true;
            this.LabelNDockCaption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelNDockCaption.Location = new System.Drawing.Point(8, 194);
            this.LabelNDockCaption.Name = "LabelNDockCaption";
            this.LabelNDockCaption.Size = new System.Drawing.Size(29, 12);
            this.LabelNDockCaption.TabIndex = 3;
            this.LabelNDockCaption.Text = "入渠";
            //
            // labelKDockCaption
            //
            this.LabelKDockCaption.AutoSize = true;
            this.LabelKDockCaption.Location = new System.Drawing.Point(151, 194);
            this.LabelKDockCaption.Name = "LabelKDockCaption";
            this.LabelKDockCaption.Size = new System.Drawing.Size(29, 12);
            this.LabelKDockCaption.TabIndex = 6;
            this.LabelKDockCaption.Text = "建造";
            //
            // labelQuestCaption
            //
            this.LabelQuestCaption.AutoSize = true;
            this.LabelQuestCaption.Location = new System.Drawing.Point(8, 341);
            this.LabelQuestCaption.Name = "LabelQuestCaption";
            this.LabelQuestCaption.Size = new System.Drawing.Size(29, 12);
            this.LabelQuestCaption.TabIndex = 8;
            this.LabelQuestCaption.Text = "任務";
            //
            // labelMissionCaption
            //
            this.LabelMissionCaption.AutoSize = true;
            this.LabelMissionCaption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelMissionCaption.Location = new System.Drawing.Point(8, 275);
            this.LabelMissionCaption.Name = "LabelMissionCaption";
            this.LabelMissionCaption.Size = new System.Drawing.Size(29, 12);
            this.LabelMissionCaption.TabIndex = 10;
            this.LabelMissionCaption.Text = "遠征";
            //
            // notifyIconMain
            //
            this.NotifyIconMain.Text = "KancolleSniffer";
            this.NotifyIconMain.Visible = true;
            //
            // labelMaterialCaption
            //
            this.LabelMaterialCaption.AutoSize = true;
            this.LabelMaterialCaption.Location = new System.Drawing.Point(183, 341);
            this.LabelMaterialCaption.Name = "LabelMaterialCaption";
            this.LabelMaterialCaption.Size = new System.Drawing.Size(29, 12);
            this.LabelMaterialCaption.TabIndex = 43;
            this.LabelMaterialCaption.Text = "資材";
            //
            // labelRepairListCaption
            //
            this.LabelRepairListCaption.AutoSize = true;
            this.LabelRepairListCaption.Location = new System.Drawing.Point(81, 194);
            this.LabelRepairListCaption.Name = "LabelRepairListCaption";
            this.LabelRepairListCaption.Size = new System.Drawing.Size(41, 12);
            this.LabelRepairListCaption.TabIndex = 46;
            this.LabelRepairListCaption.Text = "要修復";
            //
            // labelAkashiRepair
            //
            this.LabelAkashiRepair.AutoSize = true;
            this.LabelAkashiRepair.Location = new System.Drawing.Point(151, 275);
            this.LabelAkashiRepair.Name = "LabelAkashiRepair";
            this.LabelAkashiRepair.Size = new System.Drawing.Size(29, 12);
            this.LabelAkashiRepair.TabIndex = 54;
            this.LabelAkashiRepair.Text = "修理";
            //
            // dropDownButtonRepairList
            //
            this.DropDownButtonRepairList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DropDownButtonRepairList.Location = new System.Drawing.Point(121, 192);
            this.DropDownButtonRepairList.Name = "DropDownButtonRepairList";
            this.DropDownButtonRepairList.Size = new System.Drawing.Size(14, 14);
            this.DropDownButtonRepairList.TabIndex = 45;
            //
            // labelQuestCount
            //
            this.LabelQuestCount.AutoSize = true;
            this.LabelQuestCount.Location = new System.Drawing.Point(35, 341);
            this.LabelQuestCount.Name = "LabelQuestCount";
            this.LabelQuestCount.Size = new System.Drawing.Size(11, 12);
            this.LabelQuestCount.TabIndex = 57;
            this.LabelQuestCount.Text = "0";
            //
            // kdockPanel
            //
            this.KdockPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KdockPanel.Location = new System.Drawing.Point(149, 208);
            this.KdockPanel.Name = "KdockPanel";
            this.KdockPanel.Size = new System.Drawing.Size(77, 64);
            this.KdockPanel.TabIndex = 60;
            //
            // panelRepairList
            //
            this.PanelRepairList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRepairList.Location = new System.Drawing.Point(6, 206);
            this.PanelRepairList.Name = "PanelRepairList";
            this.PanelRepairList.Size = new System.Drawing.Size(129, 21);
            this.PanelRepairList.TabIndex = 4;
            this.PanelRepairList.Visible = false;
            //
            // ndockPanel
            //
            this.NdockPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NdockPanel.Location = new System.Drawing.Point(6, 208);
            this.NdockPanel.Name = "NdockPanel";
            this.NdockPanel.Size = new System.Drawing.Size(140, 64);
            this.NdockPanel.TabIndex = 59;
            //
            // missionPanel
            //
            this.MissionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MissionPanel.Location = new System.Drawing.Point(6, 289);
            this.MissionPanel.Name = "MissionPanel";
            this.MissionPanel.Size = new System.Drawing.Size(220, 49);
            this.MissionPanel.TabIndex = 58;
            //
            // questPanel
            //
            this.QuestPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.QuestPanel.Location = new System.Drawing.Point(6, 355);
            this.QuestPanel.MinLines = 4;
            this.QuestPanel.Name = "QuestPanel";
            this.QuestPanel.Size = new System.Drawing.Size(220, 94);
            this.QuestPanel.TabIndex = 56;
            //
            // hqPanel
            //
            this.HqPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HqPanel.Location = new System.Drawing.Point(6, 6);
            this.HqPanel.Name = "HqPanel";
            this.HqPanel.Size = new System.Drawing.Size(220, 33);
            this.HqPanel.TabIndex = 61;
            //
            // materialHistoryPanel
            //
            this.MaterialHistoryPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MaterialHistoryPanel.Location = new System.Drawing.Point(38, 353);
            this.MaterialHistoryPanel.Name = "MaterialHistoryPanel";
            this.MaterialHistoryPanel.Size = new System.Drawing.Size(188, 52);
            this.MaterialHistoryPanel.TabIndex = 65;
            this.MaterialHistoryPanel.Visible = false;
            //
            // dropDownButtonMaterialHistory
            //
            this.DropDownButtonMaterialHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DropDownButtonMaterialHistory.Location = new System.Drawing.Point(211, 339);
            this.DropDownButtonMaterialHistory.Name = "DropDownButtonMaterialHistory";
            this.DropDownButtonMaterialHistory.Size = new System.Drawing.Size(14, 14);
            this.DropDownButtonMaterialHistory.TabIndex = 80;
            this.DropDownButtonMaterialHistory.Text = "dropDownButton1";
            //
            // fleetPanel
            //
            this.FleetPanel.Context = null;
            this.FleetPanel.Location = new System.Drawing.Point(6, 42);
            this.FleetPanel.Name = "FleetPanel";
            this.FleetPanel.Size = new System.Drawing.Size(220, 149);
            this.FleetPanel.TabIndex = 82;
            //
            // VerticalMainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 455);
            this.Controls.Add(this.FleetPanel);
            this.Controls.Add(this.DropDownButtonMaterialHistory);
            this.Controls.Add(this.MaterialHistoryPanel);
            this.Controls.Add(this.HqPanel);
            this.Controls.Add(this.KdockPanel);
            this.Controls.Add(this.PanelRepairList);
            this.Controls.Add(this.NdockPanel);
            this.Controls.Add(this.MissionPanel);
            this.Controls.Add(this.LabelQuestCount);
            this.Controls.Add(this.QuestPanel);
            this.Controls.Add(this.LabelAkashiRepair);
            this.Controls.Add(this.LabelAkashiRepairTimer);
            this.Controls.Add(this.DropDownButtonRepairList);
            this.Controls.Add(this.LabelRepairListCaption);
            this.Controls.Add(this.LabelMaterialCaption);
            this.Controls.Add(this.LabelMissionCaption);
            this.Controls.Add(this.LabelQuestCaption);
            this.Controls.Add(this.LabelKDockCaption);
            this.Controls.Add(this.LabelNDockCaption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "VerticalMainForm";
            this.Text = "KancolleSniffer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelNDockCaption;
        private System.Windows.Forms.Label LabelKDockCaption;
        private System.Windows.Forms.Label LabelQuestCaption;
        private System.Windows.Forms.Label LabelMissionCaption;
        private System.Windows.Forms.NotifyIcon NotifyIconMain;
        private System.Windows.Forms.Label LabelMaterialCaption;
        private DropDownButton DropDownButtonRepairList;
        private System.Windows.Forms.Label LabelRepairListCaption;
        private System.Windows.Forms.Label LabelAkashiRepairTimer;
        private System.Windows.Forms.Label LabelAkashiRepair;
        private RepairListPanel PanelRepairList;
        private QuestPanel QuestPanel;
        private System.Windows.Forms.Label LabelQuestCount;
        private MissionPanel MissionPanel;
        private NDockPanel NdockPanel;
        private KDockPanel KdockPanel;
        private HqPanel HqPanel;
        private MaterialHistoryPanel MaterialHistoryPanel;
        private DropDownButton DropDownButtonMaterialHistory;
        private FleetPanel FleetPanel;
    }
}
