// Copyright (C) 2024 hATrayflood <h.rayflood@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using KancolleSniffer.Log;
using KancolleSniffer.Util;

namespace KancolleSniffer.Forms
{
    public partial class CacheBuilderDialog : Form
    {
        private readonly CacheBuilder _builder;
        private DialogResult _result;

        public CacheBuilderDialog(CacheBuilder builder, Point location)
        {
            Location = location;
            _builder = builder;
            InitializeComponent();
        }

        public new DialogResult ShowDialog()
        {
            base.ShowDialog();
            return _result;
        }

        private void CacheBuilderDialog_Load(object sender, EventArgs e)
        {
            _result = DialogResult.None;
            Task.Run(() =>
            {
                try
                {
                    _builder.CreateDatabase();
                    _result = DialogResult.OK;
                }
                catch (Exception e)
                {
                    _result = new ErrorDialog(false).ShowDialog(this, "キャッシュを生成できませんでした", StringUtil.GenerateErrorDetails($"報告書の出力先フォルダと {Const.CacheFileName} が\r\n読み取り専用または他ユーザーの所有権になっていないか確認してください。", e));
                }
                finally
                {
                    Close();
                }
            });
        }

        private void CacheBuilderDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = _result == DialogResult.None;
        }
    }
}
